
    var app = angular.module('investmentApp', ['pbHeaderFooter','ngAnimate','ngRoute', 'ngCookies','ngSanitize','ngAria','ngMaterial','ngMessages','ngStorage','Analytic.services']);
    app.constant('isMobile',isMobileDevice());
    function isMobileDevice(){
        var isMobile;
        isMobile = {
            /**
             * @return {boolean}
             */
            Android: function () {
                return navigator.userAgent.match(/Android/i) !== null;
            },
            /**
             * @return {boolean}
             */
            BlackBerry: function () {
                return navigator.userAgent.match(/BlackBerry/i) !== null;
            },
            iOS: function () {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i) !== null;
            },
            /**
             * @return {boolean}
             */
            Opera: function () {
                return navigator.userAgent.match(/Opera Mini/i) !== null;
            },
            /**
             * @return {boolean}
             */
            Windows: function () {
                return navigator.userAgent.match(/IEMobile/i) !== null;
            },
            any: function () {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };

        return isMobile.any();
    }

    function includeJs(jsFilePath) {
        var js = document.createElement("script");

        js.type = "text/javascript";
        js.src = jsFilePath;

        document.body.appendChild(js);
    }

    String.prototype.toTitleCase = function(){
        return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    };

    Array.prototype.sortByProp = function(p){
        return this.sort(function(a,b){
            return (a[p] > b[p]) ? 1 : (a[p] < b[p]) ? -1 : 0;
        });
    };

    Array.prototype.unique = function(){
        var n = {},r=[];
        for(var i = 0; i < this.length; i++){
            if (!n[this[i]]){
                n[this[i]] = true;
                r.push(this[i]);
            }
        }
        return r;
    };

    

    Array.prototype.insert = function (index, item) {
        this.splice(index, 0, item);
    };

    app.run(['$route', '$rootScope', '$location','$cookies','GoogleTagManager','isMobile','NewInvestmentService', function ($route, $rootScope, $location,$cookies,GoogleTagManager,isMobile,NewInvestmentService) {
        var original = $location.path;
        $rootScope.isIFrame = false;
        $rootScope.isOnlyTraditional = false;
        /*NewInvestmentService.CheckOneTwoNineIP().success(function(data){
            if(data.toUpperCase()==="TRUE"){
                $rootScope.isOnlyTraditional = true;
            }
        }).error(function(data){

        });*/
        $location.path = function (path, reload) {
            if (reload === false) {
                var lastRoute = $route.current;
                var un = $rootScope.$on('$locationChangeSuccess', function () {
                    $route.current = lastRoute;
                    un();
                });
            }
            return original.apply($location, [path]);
        };

        $rootScope.$on('$routeChangeStart', function (ev,next,cur) {
            if(next.$$route!==undefined) {
                    $rootScope.isLanding = next.$$route.originalPath === '/landing';
					$rootScope.isMobile = isMobile;
					$rootScope.isLandingPC = $rootScope.isLanding && !isMobile;
                    $rootScope.isPaytm= next.$$route.originalPath === '/paytm';
            }
            $(document).ready(function () {

                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            });
        });
		$rootScope.$on('$routeChangeSuccess', function() {
            var path= $location.path(),
                absUrl = $location.absUrl(),
                virtualUrl = absUrl.substring(absUrl.indexOf(path));
            GoogleTagManager.push({ event: 'virtualPageView', virtualUrl: virtualUrl });
            if (location.hash.length > 3) {
                ga('send', 'pageview', location.hash);
            } else {
                ga('send', 'pageview', location.href.replace("#",""));
            }
        });
        if($location.$$search.iframe ){
            $rootScope.isIFrame = $location.$$search.iframe;
        }else{
            $rootScope.isIFrame = false;
        }

        if($location.$$search.isHeadIframe){
            $rootScope.isHeadIFrame = true;
        }else{
            $rootScope.isHeadIFrame = false;
        }

        $( window ).unload(function() {
            $cookies.remove('visitId');
        });



    }]);

    app.config(
        [
            '$routeProvider',
            '$locationProvider',
            '$controllerProvider',
            '$compileProvider',
            '$filterProvider',
            '$provide',
            '$animateProvider',
            '$sceDelegateProvider',
            'isMobile',

            function($routeProvider, $locationProvider, $controllerProvider, $compileProvider, $filterProvider, $provide,$animateProvider,$sceDelegateProvider,isMobile)
            {

                app.controller = $controllerProvider.register;
                app.directive  = $compileProvider.directive;
                app.filter     = $filterProvider.register;
                app.factory    = $provide.factory;
                app.service    = $provide.service;
                if(configObj.getEnv()==="live") {
                    $locationProvider.html5Mode(true);
                }else{
                    $locationProvider.html5Mode(false);
                }
                /******************/
                if(!isMobile) {
                    $routeProvider
                        .when('/', {templateUrl: 'templates/choose-plan.html', controller: 'ChooseController'})
                        .when('/landing',{templateUrl: 'templates/pre-quote-resp.html', controller: 'PreQuotesController'})
                        .when('/customer', {templateUrl: 'templates/enter-contact.html', controller: 'CustomerController'})
                        .when('/quote', {
                            templateUrl: 'templates/quotes.html',
                            controller: 'QuotesController',
                            reloadOnSearch: false
                        })
                        .when('/interim', {templateUrl: 'templates/intermediate.html', controller: 'InterController'})
						.when('/click2fill', { templateUrl: 'templates/c2fill_details.html', controller: 'ClickToFillController' })
                        .when('/paytm', { templateUrl: 'templates/paytmTemplate.html', controller: 'PaytmController' })
                        .otherwise({redirectTo: '/'});
                }else{
                    $routeProvider
                        .when('/', {templateUrl: 'templates/pre-quote-resp.html', controller: 'PreQuotesController'})
                        .when('/landing',{templateUrl: 'templates/pre-quote-resp.html', controller: 'PreQuotesController'})
                        .when('/quote', {
                            templateUrl: 'templates/quotes.html',
                            controller: 'QuotesController',
                            reloadOnSearch: false
                        })
                        .when('/interim', {templateUrl: 'templates/intermediate.html', controller: 'InterController'})
                        .otherwise({redirectTo: '/'});
                }
            /*************************/
                /*************************
                $routeProvider
                    .when('/', {templateUrl: 'templates/pre-quote-resp.html', controller: 'PreQuotesController'})
                    .when('/landing',{templateUrl: 'templates/pre-quote-resp.html', controller: 'PreQuotesController'})
                    .when('/customer', {templateUrl: 'templates/enter-contact.html', controller: 'CustomerController'})
                    .when('/quote', {
                        templateUrl: 'templates/quotes.html',
                        controller: 'QuotesController',
                        reloadOnSearch: false
                    })
                    .when('/interim', {templateUrl: 'templates/intermediate.html', controller: 'InterController'})
                    .when('/click2fill', { templateUrl: 'templates/c2fill_details.html', controller: 'ClickToFillController' })
                    .otherwise({redirectTo: '/'});
                *************************/
                $animateProvider.classNameFilter(/^(?:(?!ng-animate-disabled).)*$/);
                $sceDelegateProvider.resourceUrlWhitelist([
                    // Allow same origin resource loads.
                    'self',
                    // Allow loading from our assets domain.  Notice the difference between * and **.
                    'http://pbstatic.policybazaar.com'
                ]);

        }
    ]);

