app.filter('trimNameSpace', function () {
    return function (value) {
    		var arrString = value.split(" ");
        var nameString;
    		if (arrString[0].length == 1 || arrString[0].length == 2) {
    			if(arrString[1] !== undefined) {
    				nameString = arrString[0] + " " + arrString[1];
    			} else {
    				nameString = arrString[0];
    			}
    		} else {
    			nameString = arrString[0];
    		}
        return nameString;
    };
  });


app.filter('searchCity', function (CommonService) {
    return function (value,searchText) {
        var arr = [];
        if(searchText.search!==null && searchText.search.length>1){
            if(value!==undefined || value!==null ) {
                arr = searchText.search ? CommonService.createFilterForFirstValues(value,searchText.search) : [];
            }
        }
        return arr;
    };
});

  app.filter('spaceToUnderscore', function () {
    return function (value) {
        if(value!==undefined) {
            var convertString = value.split(' ').join('_');
            return convertString;
        }
        return;
    };
  });

    app.filter('yesNo', function () {
        return function (value) {
            if(value!==undefined) {
                if(value){
                return 'Yes';
                }else{
                    return 'No';
                }
            }
        };
    });

    app.filter('customDateFormat',function(){
        var months = ['Jan','Feb','Mar','Apr','May','June','July','Aug','Sept','Oct','Nov','Dec'];
        var dateForm = [];
       return function(value){
          if(value!==undefined){
              var x = value.indexOf('T');
              dateForm = value.substr(0,x).split('-');
              return dateForm[2]+" "+months[dateForm[1]-1]+" "+dateForm[0];
          }
       } ;
    });

    app.filter('amountConvertString', ["CommonService", function (CommonService) {
    return function (value) {
        if(value!==undefined) {
            return CommonService.AmountToString(value);
        }
        return "";
    };
  }]);

    app.filter('addCommas', ["CommonService", function (CommonService) {
        return function (value) {
            if(value!==undefined) {
                if (value.toString().indexOf(',') != -1) {
                    value = value.replace(/,/g, '');
                }
                return CommonService.AddCommas(value);
            }

        };
    }]);

    app.filter('removeTrailingZeros',function(){
       return function(value){
         if(value!==undefined ||Number(value)){
             value = parseFloat(value);
             return value.toString();
         }else{
             return 0;
         }
       };
    });

    app.filter('truncated', function () {
        return function (value) {
            if(value!==undefined) {
                value = value.toTitleCase();
                if(value.length>37){
                    value = value.substr(0,37)+'...';
                }
                return value;
            }else{
                return "";
            }
        };
    });

    app.filter('titleCase', function () {
        return function (value) {
            if(value!==undefined) {
                return value.toTitleCase();
            }else{
                return '';
            }
        };
    });

    app.filter('showDifferences', ["$rootScope", function($rootScope) {
        return function( items, difference) {
            var filtered = [];

            var filterFeatures = function(items){
                var filtered = [];
                if(!angular.isUndefined(items) && items.length){
                    for(var i=0; i<items.length; i++){
                        var item = items[i];
                        filtered[i] = item;
                        for(var j=0; j<item.Features.length; j++){
                            var innerItem = item.Features[j];
                            filtered[i].Features[j] = innerItem;
                            innerItem.isDisplay = true;
                            if($rootScope.showDifferences){
                                var response = changeArrayFormat(innerItem.Data);
                                var isIdentical = isValuesIdentical(response);
                                if(isIdentical){
                                    innerItem.isDisplay = false;
                                }
                            }
                        }
                    }
                }else{
                    filtered = items;
                }
                return filtered;
            };

            var isValuesIdentical = function(values){
                var len = values.length - 1;
                while(len){
                    if(values[len--].toUpperCase() !== values[len].toUpperCase())
                        return false;
                }
                return true;
            };

            var changeArrayFormat = function(input){
                var response = [];
                angular.forEach(input, function(item) {
                    response.push(item.Text);
                });
                return response;
            };

            filtered = filterFeatures(items);
            return filtered;

        };
    }]);

