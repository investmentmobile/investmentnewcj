/**
 * Created by Prashants on 8/10/2015.
 */

// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var ngAnnotate = require('gulp-ng-annotate');
var minifyCss = require('gulp-minify-css');

// Lint Task
gulp.task('lint', function() {
    return gulp.src(['controllers/*.js','directives/*.js','filters/*.js','services/*.js','config.js','app.js','OmnitureGeneric.js','ZopimService.js'])
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src(['controllers/*.js','directives/*.js','filters/*.js','services/*.js'])
        .pipe(concat('all.js'))
        .pipe(ngAnnotate())
        .pipe(gulp.dest('dist'))
        .pipe(rename('all.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist'));
});

// Concatenate & Minify JS
gulp.task('scriptsAng', function() {
    return gulp.src(['angular_new/angular-material.js'])
        .pipe(gulp.dest('angular_new'))
        .pipe(rename('angular-material.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('angular_new'));
});

// Concatenate & Minify JS
gulp.task('scripts-other', function() {
    return gulp.src(['app.js','OmnitureGeneric.js','ZopimService.js'])
        .pipe(concat('all_new.js'))
        .pipe(ngAnnotate())
        .pipe(gulp.dest('dist'))
        .pipe(rename('all_new.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist'));
});

// minify and concat css
gulp.task('minify-css', function() {
    return gulp.src(['css/style.css'])
        .pipe(minifyCss({compatibility: 'ie8'}))
        .pipe(concat('style.min.css'))
        .pipe(gulp.dest('dist'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch(['controllers/*.js','directives/*.js','filters/*.js','services/*.js','config.js','app.js','OmnitureGeneric.js','ZopimService.js','css/*.css'], ['lint', 'scripts','minify-css','scripts-other']);
});

// Default Task
gulp.task('default', ['lint', 'scripts','scripts-other', 'watch']);
