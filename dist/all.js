/**
 * Created by Prashants on 8/12/2015.
 */
app.controller('ChooseController',
    [
        '$route','$scope', '$rootScope','CommonService','NewInvestmentService','$location','$cookies','$timeout','$localStorage','$routeParams',
        function ($route,$scope, $rootScope, CommonService,NewInvestmentService,$location,$cookies,$timeout,$localStorage,$routeParams) {
            'use strict';
            var index;
            $scope.dontShowMessage = false;
            CommonService.visitCreation();

            CommonService.retrieveCustIdFromCookie(false);

        
            triggerPageParamEvents('pre-quotes');
            $scope.discountsAndPremium = [];
            $rootScope.slideFlag = angular.isDefined($rootScope.slideFlag) ? $rootScope.slideFlag : 'right';
            $scope.focusObj = {};
            $scope.hideObject = {};
            $scope.hideObject.dropDownNotSeen = true;
            $scope.dobObj = {};
            $scope.dob = {
                date: CommonService.getDateList(),
                month: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
                year: CommonService.getYearList()
            };


            $scope.focusObj.termFocus = false;
            $rootScope.loaderProceed = false;
            $scope.buttonName = "Continue";
            $scope.showDropDown = false;
            var chosenRetirementValues = {yourAge:0,retirementAge:0,amountNeeded:0};
            var chosenGrowthValues = {yourAge:0,payTerm:0,amountToInvest:0};
            var chosenChildValues = {yourAge:0,childAge:'',amountNeeded:0,childPayTerm:20};
            var previousValue = 0;
            $scope.monthlyInvestment = 0;
            $scope.sliderObj = {min:0,max:100,value:50,list:[]};
            $rootScope.pageClass = "choose";
            var plans = [{'name':'retirement','selected':false,id:2},{'name':'growth','selected':false,id:1},{'name':'child','selected':false,id:3}];

            $scope.plans = angular.copy(plans);

            if($rootScope.isLanding){
                $scope.discountsAndPremium = [];
                NewInvestmentService.getBanner().success(function(data){
                    _.each(data,function(value){
                        if(value.offerId == $location.$$search.offerid){
                            $scope.imageUrl = value.imageUrl;
                            $scope.discountsAndPremium = value.discountAndPremium;
                        }
                    });

                });
            }


            $scope.details = {};
            function countUpValues(value) {
                if (value> 0) {
                    $scope.monthlyInvestment = value;
                    var options = {
                        useEasing: true,
                        useGrouping: true,
                        separator: ',',
                        decimal: '.',
                        prefix: 'Rs.',
                        suffix: ''
                    };
                    $timeout(function () {
                        var demo = new CountUp("myTargetElement", previousValue, value, 0, 1.5, options);
                        demo.start(function () {
                            /*$scope.$apply(function () {

                            });*/
                        });
                        previousValue = value;
                    }, 10);

                }
            }

            $scope.monthlyPayoutChange = function(){
                if(!($scope.isError || $scope.isTermError)) {
                    $scope.dontShowMessage = true;
                    $timeout(function () {
                        var postObj = {};
                        if (index === 1) {
                            chosenGrowthValues.amountToInvest = $scope.sliderObj.list.indexOf($scope.details.amount);
                            postObj.payterm = $scope.details.age;
                            postObj.premium = $scope.details.amount;
                        } else if (index === 2) {
                            var lifeExpectancy = 80;
                            postObj.payterm = $scope.details.age - $scope.details.yourAge <= 30 ? $scope.details.age - $scope.details.yourAge : 30;
                            postObj.premium = $scope.details.amount * ($scope.details.age<80?(lifeExpectancy - $scope.details.age):1);

                            chosenRetirementValues.amountNeeded = $scope.sliderObj.list.indexOf($scope.details.amount);
                        } else {
                            chosenChildValues.amountNeeded = $scope.sliderObj.list.indexOf($scope.details.amount);
                            postObj.payterm = $scope.details.childTerm;
                            postObj.premium = $scope.details.amount;
                        }
                        var monthlyInvestment = 0;
                        if(index===1) {
                            CommonService.getPremiumForPreQuote(postObj, index).then(function (value) {
                                monthlyInvestment = value.maturityBenefit;
                                $scope.monthlyInvestment = CommonService.countUpValues(monthlyInvestment,$scope.monthlyInvestment,previousValue);
                            });
                        }else if(index===2){
                            monthlyInvestment = CommonService.GetRetirementCalc($scope.details.amount,$scope.details.age,postObj.payterm);
                            $scope.monthlyInvestment = CommonService.countUpValues(monthlyInvestment,$scope.monthlyInvestment,previousValue);
                        }else  if(index===3){
                           monthlyInvestment = CommonService.compoundInterest(postObj.premium,postObj.payterm);
                            $scope.monthlyInvestment = CommonService.countUpValues(monthlyInvestment,$scope.monthlyInvestment,previousValue);
                        }

                    }, 300);
                }
            };

            $scope.triggerSelectBox = function(){
              angular.element('.trigger-class-select').trigger('click');
            };

            function fillValues(){
                if(index===1){
                    $scope.index = 1;
                    $scope.sliderObj.list = CommonService.getAmountList(10000,100000,125000,500000,1000,25000);
                    $scope.sliderObj.max = $scope.sliderObj.list.length-1;
                    $scope.firstText = "Investment per year";
                    $scope.secondText = "You will invest";
                    $scope.thirdText = "You will get";
                    $scope.finalText = "per year";
                    $scope.perText = "per month";
                    $scope.inYearsText = " in";
                    if(chosenGrowthValues.amountToInvest>0){
                        $scope.sliderObj.value = chosenGrowthValues.amountToInvest;
                        $scope.details.amount = $scope.sliderObj.list[chosenGrowthValues.amountToInvest];
                        $scope.details.monthlyAmount = Math.round($scope.details.amount/12);

                    }else if(parseInt($scope.details.amount)<=0 || $scope.details.amount===undefined) {
                        $scope.details.amount = 50000;
                    }
                    if($scope.sliderObj.list.indexOf($scope.details.amount)<0){
                        $scope.details.amount = 50000;
                    }
                    $scope.details.monthlyAmount = Math.round($scope.details.amount/12);
                    $scope.sliderObj.value = $scope.sliderObj.list.indexOf($scope.details.amount);
                    chosenGrowthValues.amountToInvest = $scope.sliderObj.value;
                    if(chosenGrowthValues.payTerm>0){
                        $scope.details.age = chosenGrowthValues.payTerm;
                    }

                }else if(index === 2){
                    $scope.index = 2;
                    $scope.sliderObj.list = CommonService.getAmountList(10000,100000,125000,200000,5000,25000);
                    $scope.sliderObj.max = $scope.sliderObj.list.length-1;
                    $scope.firstText = "Pension Needed per Month";
                    $scope.secondText = "You will get";
                    $scope.thirdText = "You need to invest ";
                    $scope.finalText = "per month";
                    /*$scope.perText = "per month";*/
                    $scope.inYearsText = "per month";
                    if(chosenRetirementValues.amountNeeded>0){
                        $scope.sliderObj.value = chosenRetirementValues.amountNeeded;
                        $scope.details.amount = $scope.sliderObj.list[chosenRetirementValues.amountNeeded];
                    }else if((parseInt($scope.details.amount)<=0 || $scope.details.amount===undefined) || parseInt($scope.details.amount)>500000 ) {
                        $scope.details.amount = 50000;
                    }
                    if($scope.sliderObj.list.indexOf($scope.details.amount)<0){
                        $scope.details.amount = 50000;
                    }
                    $scope.details.monthlyAmount = Math.round($scope.details.amount/12);
                    $scope.sliderObj.value = $scope.sliderObj.list.indexOf($scope.details.amount);
                    chosenRetirementValues.amountNeeded = $scope.sliderObj.value;
                    if(chosenRetirementValues.retirementAge>0){
                        $scope.details.age = chosenRetirementValues.retirementAge;
                    }
                }else{
                    $scope.index = 3;
                    $scope.sliderObj.list = CommonService.getChildAmountList(500000,5000000,10000000,50000000,100000,500000,2500000);
                    $scope.sliderObj.max = $scope.sliderObj.list.length-1;
                    $scope.firstText = "Amount you Need";
                    $scope.secondText = "Your child gets";
                    $scope.thirdText = "You need to invest";
                    $scope.inYearsText = "per month";
                    if(chosenChildValues.amountNeeded>0){
                        $scope.sliderObj.value = chosenChildValues.amountNeeded;
                        $scope.details.amount = $scope.sliderObj.list[chosenChildValues.amountNeeded];
                    }else if(parseInt($scope.details.amount)<=0 || $scope.details.amount===undefined) {
                        $scope.details.amount = 3000000;
                    }
                    if($scope.sliderObj.list.indexOf($scope.details.amount)<0){
                        $scope.details.amount = 3000000;
                    }
                    $scope.details.monthlyAmount = Math.round($scope.details.amount/12);
                    $scope.sliderObj.value = $scope.sliderObj.list.indexOf($scope.details.amount);
                    chosenChildValues.amountNeeded = $scope.sliderObj.value;
                    if(parseInt(chosenChildValues.childAge)>=0){
                        $scope.details.age = chosenChildValues.childAge;
                    }else{
                        $scope.details.age = '';
                    }
                    $scope.details.childTerm = chosenChildValues.childPayTerm;
                }
                $scope.details.index = index;
            }



            $scope.checkForError = function(value){
                if(value!==undefined && value.toString().length==2){
                    $scope.focus = true;
                }
                $scope.isError = (value === undefined || value.length===0) || (value>60 || (index!==2 && value<18) ||(index===2 && value<20) || (index===2 && (value>$scope.details.age || $scope.details.age - value<10)) || (index===3 && (value-$scope.details.age<18)));
                    if (value!==undefined && value.toString().length==2) {
                        $scope.checkForTermError($scope.details.age);
                        $scope.monthlyPayoutChange();
                    }
            };

            $scope.changeInYourAge = function(){
                if(index===3) {
                    if($scope.details.yourAge-$scope.details.age>=18){
                        chosenChildValues.yourAge = $scope.details.yourAge;
                    }
                }else {
                    if($scope.details.yourAge>=18){
                        if(index===1){
                            chosenGrowthValues.yourAge = $scope.details.yourAge;
                        }else{
                            chosenRetirementValues.yourAge = $scope.details.yourAge;
                        }
                    }
                }
            };

            $scope.checkForTermError = function(value){
                childTermChange();
                if(index===1){
                    $scope.isTermError = false;
                }else {
                    if (value !== undefined && value.toString().length > 0) {
                        $scope.focusObj.termFocus = true;
                    }else if(value !== undefined && value.toString().length===0){
                        $scope.focusObj.termFocus = false;
                    }
                    $scope.isTermError = (value === undefined || value.length === 0) || ((index === 3 && value > 17) || (index === 2 && (value < 50 || value > 80)) || (index === 2 && value < $scope.details.yourAge) || (index === 3 && $scope.details.yourAge - value < 18) ||(index === 2 && value - $scope.details.yourAge < 10));
                    $timeout(function(){
                    if(!$scope.isError && !$scope.isTermError){
                        $scope.monthlyPayoutChange();
                    }
                    },10);
                }
            };

            function childTermChange(){
                if(index===3) {
                    $scope.details.age = parseInt($scope.details.age);
                    changeTermChild();
                }
            }

            $scope.changeInTerm = function(isValid){
                $scope.checkForTermError($scope.details.age);
                $scope.checkForError($scope.details.yourAge);
                if(index===2){
                    chosenRetirementValues.retirementAge = $scope.details.age;
                }else if(index===3){
                    chosenChildValues.childAge = $scope.details.age;
                }
            };

            function changeTermChild(){

                $scope.details.childTerm = $scope.details.childTerm<10?10:$scope.details.childTerm<15 && $scope.details.childTerm>10?15:$scope.details.childTerm>15?20:20;
                if($scope.details.age>=14){
                    $scope.details.childTerm = 10;
                }
                if(parseInt($scope.details.childTerm)>15 && (parseInt($scope.details.age)>=9 &&parseInt($scope.details.age)<14)){
                    $scope.details.childTerm = 15;
                }
                return parseInt($scope.details.childTerm);
            }

            $scope.changeInGrowthTerm = function(isValid){
                $scope.checkForError($scope.details.yourAge,isValid);
                if(index===1){
                    chosenGrowthValues.payTerm = $scope.details.age;
                }
                if(isValid===undefined && !$scope.isError){
                    $scope.monthlyPayoutChange();
                }
            };

            $scope.changeInChildTerm = function(){
                $scope.checkForTermError($scope.details.age);
                if(index===3){
                    chosenChildValues.childPayTerm = $scope.details.childTerm;
                }
                if(!$scope.isError && !$scope.isTermError){
                    $scope.monthlyPayoutChange();
                }
            };

            $scope.changeInvestmentAmount = function(){
                if(!$scope.isError && !$scope.isTermError){
                    $scope.monthlyPayoutChange();
                }
            };

            $scope.createEnquiry = function(isValid){

                if(isValid ||!$scope.isValidDateOfBirth()) {
                    $rootScope.slideFlag = 'right';
                    isValid = isValid && !$scope.isError && !$scope.isTermError;
                    if (isValid) {
                        $rootScope.loaderProceed = true;
                        $scope.buttonName = "Please Wait";
                        CommonService.saveEnquiry(index,$scope.details,$scope.monthlyInvestment,$scope.data).then(function(data){

                            if(data){
                                $scope.buttonName = "Continue";

                            }else {
                                $scope.buttonName = "Retry";
                            }
                        });
                    }
                }
            };

            $scope.showDropDownfunc = function(){
                $scope.showDropDown = !$scope.showDropDown;
            };
            $scope.isAnyPlanSelected = function(planId){
                previousValue = 0;
                angular.element(function(){
                    if($rootScope.isLanding){
                        angular.element('html,body').animate({scrollTop: 0}, 600);
                    }else {
                        ///$('html,body').animate({scrollTop: 255}, 600);
                    }
                });
                if(planId !== index) {
                    $scope.plans = angular.copy(plans);
                    if (planId === 1) {
                        $scope.plans[1].selected = true;
                    } else if (planId === 2) {
                        $scope.plans[0].selected = true;
                    } else {
                        $scope.plans[2].selected = true;

                    }
                    index = CommonService.whichPlanSelected($scope.plans);
                    $scope.ages = CommonService.getAges(index);
                    $scope.details.age = CommonService.getAge(index);
                    $scope.nameOfAge = CommonService.getTypeName(index);
                    fillValues();
                    $scope.checkForError($scope.details.yourAge);
                    $scope.checkForTermError($scope.details.age);
                    if ($scope.details.yourAge !== undefined && $scope.details.yourAge.toString().length == 2) {
                        $timeout(function(){
                            $scope.monthlyPayoutChange();
                        },200);
                    }

                    if ($scope.planSelected) {
                        $scope.planSelected = false;
                        $timeout(function () {
                            $scope.planSelected = true;
                        }, 500);
                    } else {
                        $scope.planSelected = true;
                    }
                }
            };

            $scope.data = {
                dobDate: null,
                dobMonth: null,
                dobYear: null
            };

            $scope.changeDate = function(model) {
                $scope.data.dobDate = model;
                $scope.changeDateOfBirth();
            };

            $scope.changeMonth = function(model) {
                $scope.data.dobMonth = model;
                $scope.changeDateOfBirth();
            };

            $scope.changeYear = function(model) {
                $scope.data.dobYear = model;
                $scope.changeDateOfBirth();
            };

            $scope.changeDateOfBirth = function(){
                $scope.dobObj.dob = ($scope.data.dobDate!==undefined && $scope.data.dobDate!==null && $scope.data.dobDate.toString().length===1?'0':'') + $scope.data.dobDate + "-" + ($scope.data.dobMonth!==undefined && $scope.data.dobMonth!==null && $scope.data.dobMonth.toString().length===1?'0':'')+$scope.data.dobMonth + "-" + $scope.data.dobYear;
                if($scope.isValidDateOfBirth()) {
                    $scope.details.yourAge = CommonService.getYourAge($scope.dobObj.dob);
                    $scope.checkForError($scope.details.yourAge);
                }
            };

            $scope.isAgeWithinRange = function() {
                return !$scope.isDOBMissing() && $scope.isValidDateOfBirth();
            };

            $scope.isAgeBelowMinimum = function() {
                return !$scope.isDOBMissing() && $scope.isValidDateOfBirth() && CommonService.getYourAge($scope.dobObj.dob) < 18;
            };

            $scope.isAgeAboveMaximum = function() {
                return !$scope.isDOBMissing() && $scope.isValidDateOfBirth() && CommonService.getYourAge($scope.dobObj.dob) > 65;
            };

            $scope.isValidAge = function() {
                return !$scope.isDOBMissing() && $scope.isValidDateOfBirth() && CommonService.getAge($scope.dobObj.dob) >= 18 && CommonService.getAge($scope.dobObj.dob) <= 65;
            };

            $scope.isYearSelected = function(index, dobYear){
                return (index*1 === $scope.dob.year.indexOf(parseInt(dobYear)));
            };

            $scope.isDOBMissing = function() {
                return (!$scope.data.dobDate || !$scope.data.dobMonth || !$scope.data.dobYear);
            };

            $scope.isValidDateOfBirth = function() {
                return $scope.dobObj.dob && CommonService.isValidDateOfBirth($scope.dobObj.dob);
            };

            function onLoad(){
                var obj = $localStorage.searchDetails;
                if(!CommonService.isEmptyObject(obj)){
                    index = obj.investmentFor>0?obj.investmentFor:1;
                    $scope.index = index;
                    $scope.details.yourAge = obj.yourAge;
                    $scope.details.age = index===1?obj.payTerm:index===2?obj.retirementAge:obj.childAge;
                    chosenChildValues.childAge = index===3?obj.childAge:'';
                    chosenChildValues.childPayTerm = index===3?obj.payTerm:20;
                    $scope.plans = angular.copy(plans);
                    if(index===1){
                        $scope.plans[1].selected = true;
                    }else if(index===2){
                        $scope.plans[0].selected = true;
                    }else{
                        $scope.plans[2].selected = true;
                    }
                    $scope.data = {};
                    $scope.data.dobDate = obj.dobDate;
                    $scope.data.dobMonth = obj.dobMonth;
                    $scope.data.dobYear = obj.dobYear;
                    if($scope.data!==null) {
                        $scope.dobObj.dob = (($scope.data.dobDate !== null && $scope.data.dobDate !== undefined) && $scope.data.dobDate.length === 1 ? '0' : '') + $scope.data.dobDate + "-" + (($scope.data.dobMonth !== null && $scope.data.dobMonth !== undefined) && $scope.data.dobMonth.length === 1 ? '0' : '') + $scope.data.dobMonth + "-" + $scope.data.dobYear;
                    }
                    $scope.details.yourAge = CommonService.getYourAge($scope.dobObj.dob);
                    $scope.planSelected = true;
                    $scope.ages = CommonService.getAges(index);
                    $scope.nameOfAge = CommonService.getTypeName(index);
                    if(index===1){
                        $scope.details.amount = obj.investment;
                    }else{
                        $scope.details.amount = obj.requiredAmt;
                    }
                    fillValues();
                    if($scope.details.yourAge!==undefined && $scope.details.yourAge.toString().length==2){
                        $scope.checkForError($scope.details.yourAge);
                        /*$scope.monthlyPayoutChange();*/
                    }
                }
                if($location.$$search.planType){
                    $scope.isAnyPlanSelected(parseInt($localStorage.searchDetails?parseInt($localStorage.searchDetails.investmentFor):parseInt($location.$$search.planType)));
                    /*$location.search("planType",null);*/
                }
            }

            onLoad();
        }
    ]);


/**
 * Created by Prashants on 8/12/2015.
 */
app.controller('CustomerController',
    [
        '$scope','$rootScope', 'CommonService','NewInvestmentService','$location','$cookies','$localStorage',
        function ($scope, $rootScope,CommonService,NewInvestmentService,$location,$cookies,$localStorage) {
            'use strict';
            $rootScope.pageClass = 'customer';
            $scope.changeSlideFlag = function(flag) {
                $rootScope.slideFlag = flag;
            };

            $scope.privacy = true;
            $rootScope.slideFlag = angular.isDefined($rootScope.slideFlag) ? $rootScope.slideFlag : 'right';

            $scope.blurObject = {customerNameBlurOut:false,emailBlurOut:false,mobileBlurOut:false,countryBlurOut:false,cityBlurOut:false};

            $scope.cityChangesObj = {};
            $scope.cityChangesObj = CommonService.prefillCityChangeObject($scope.cityChangesObj);

            $scope.trueThat = function(value){
                $scope.blurObject = CommonService.getBlurObjectChange(value,$scope.blurObject);
            };

            $rootScope.leave = false;

            $scope.errorPresent = false;
            var obj = {};
            if($localStorage.customerDetails!==undefined) {
                obj = $localStorage.customerDetails;
            }


            NewInvestmentService.getCity().success(function(data){
                $scope.selectedCityItem = {};
                var newData = _.uniq(data.Data.CityList,function(a){
                    return a.CityId;
                });

                var newDelhiObject = {"CityId":"551","CityName":"New Delhi","StateId":"35","state":"Delhi"};
                newData.unshift(newDelhiObject);
                $scope.city = CommonService.getCity(newData);
                if(obj.CountryID*1===392 && obj.CityID!==9999) {
                    $scope.selectedCityItem = CommonService.findCityFromId($scope.city, obj.CityID) || {};
                }
                if(obj.CountryID*1!==392){
                    $scope.cityChangesObj.citySearchText = "Others";
                }else if(CommonService.isEmptyObject($scope.selectedCityItem)){
                    $scope.cityChangesObj.citySearchText = "";
                }
                $scope.changeMaxLength($scope.customerEntity.CountryID);
            }).error(function(data){

            });

            $scope.customerEntity = {};
            $scope.customerEntity.CountryID = obj.CountryID!==undefined?obj.CountryID:"392";
            CommonService.prefillCustomerEntityObject($scope.customerEntity,obj);
            $scope.countryCodes = {};

            $scope.customerProceed = false;
            $scope.custFormSubmitName = "View Quotes";
            $scope.maxNum = 10;
            $scope.minNum = 10;
            $scope.buttonName = "View Quotes";
            $scope.cityChangesObj.countryCode = obj.countryCode*1;
            var enquiryId =  CommonService.decode($location.$$search.enquiryId)||$localStorage.equiryid;

            $scope.searchForCountryCode = function(countryId){

                $scope.cityChangesObj = CommonService.changeCityChangeObj($scope.countryCodes,$scope.cityChangesObj, countryId, obj);
                $scope.cityChangesObj.countryCode = CommonService.CheckForExistingCountryCodes($scope.cityChangesObj,$scope.countryCodes)?"":$scope.cityChangesObj.countryCode;
            };

            $scope.checkIfAvailableList = function(countryId){
                _.each($scope.countryCodes,function(val){
                    if(val.CountryCode===(countryId)*1){
                        $scope.customerEntity.CountryID = val.CountryCodeId;
                        $scope.cityChangesObj.isEditable = false;
                        $scope.changeMaxLength($scope.customerEntity.CountryID);
                        $scope.searchForCountryCode($scope.customerEntity.CountryID);
                    }
                });
            };

            NewInvestmentService.getCountryCodes().success(function(data){
                $scope.countryCodes = data;
                $scope.customerEntity.CountryID = obj.CountryID!==undefined?obj.CountryID.toString():"392";
                $scope.searchForCountryCode($scope.customerEntity.CountryID);

            }).error(function(data){

            });
            $scope.changeMaxLength = function(countryId){
                $scope.cityChangesObj.isNRI = false;
                $scope.cityChangesObj.citySearchText = 'Others';
                if(countryId*1 === 392){
                    $scope.cityChangesObj.isNRI = false;
                    $scope.cityChangesObj.minValue = 2;
                    if(CommonService.isEmptyObject($scope.selectedCityItem)){
                        $scope.cityChangesObj.citySearchText = '';
                    }

                }else{
                    $scope.cityChangesObj.minValue = 7;

                    $scope.cityChangesObj.citySearchText = 'Others';
                    $scope.cityChangesObj.isNRI = true;
                    $scope.errorMessageHide=false;
                    $scope.maxNum = 11;
                    $scope.minNum = 8;
                }
                _.each($scope.countryCodes,function(val){
                    if(countryId===val.CountryCodeId){
                        $scope.maxNum = val.MAX;
                        $scope.minNum = val.MIN;
                    }
                });
            };

            // validation for city
            var checkForCity = function(isvalid,searchForm){
                if($scope.selectedCityItem===undefined || $scope.selectedCityItem === null || Object.keys($scope.selectedCityItem).length<1){
                    $scope.selectedCityItem = CommonService.findCityFromText($scope.city,$scope.cityChangesObj.citySearchText);
                    if($scope.selectedCityItem === null || Object.keys($scope.selectedCityItem).length<1) {
                        searchForm.cityDetails.$invalid = true;
                        $scope.errorPresent = true;
                        $scope.cityErrorMessage = "Enter city name";
                    }
                }
                return !searchForm.cityDetails.$invalid;
            };

            $scope.searchCityTextChange = function(citySearchText){
                if(citySearchText!==$scope.cityChangesObj.citySearchText) {
                    if($scope.selectedCityItem===undefined) {
                        $scope.selectedCityItem = {};
                    }
                    $scope.citySearchText = citySearchText;
                }
            };

            /*$scope.$watch('cityChangesObj.citySearchText',function(val){
                if(CommonService.isEmptyObject($scope.selectedCityItem) && $scope.customerEntity.CountryID===999){
                    //$scope.cityChangesObj.citySearchText = $scope.cityChangesObj.citySearchText===undefined?"Others":$scope.cityChangesObj.citySearchText;
                    $scope.selectedCityItem = CommonService.findCityFromId($scope.city, obj.CityID) || {};
                }
            });*/

            $scope.selectedCityItemChange = function(cityItem){
                $scope.errorPresent = false;
                if(cityItem!==undefined &&cityItem!==null && $scope.selectedCityItem!==undefined && $scope.selectedCityItem!==null) {
                    if ($scope.selectedCityItem.cityId !== cityItem.cityId) {
                        if (cityItem !== undefined && cityItem !== null) {
                            $scope.selectedCityItem = cityItem;
                        }
                    }
                }
            };

            $scope.populateCustomerDetails = function(isValid,form){
                    if ($scope.customerEntity.CountryID*1 === 392) {
                        isValid = isValid && ($scope.customerEntity.MobileNo <= 9999999999 && $scope.customerEntity.MobileNo >= 6666666666);
                        $scope.errorMessageHide = $scope.customerEntity.MobileNo.length != 10 || !isValid;
                    }
                        $rootScope.slideFlag = 'right';
                    if ($scope.customerEntity.CountryID*1 === 392) {
                        isValid = checkForCity(isValid, form) && isValid;
                    }
    				if (!$scope.privacy) {
                        isValid = false;
                    }
                    if (isValid) {
                        $scope.customerProceed = true;
                        $scope.buttonName = "Please Wait";

                       obj = CommonService.setCustomerValuesToStorage($scope.customerEntity,$scope.selectedCityItem,$scope.cityChangesObj.countryCode);


                        $localStorage.customerDetails = obj;
                        CommonService.saveCustomerDetails(obj);

                        var searchObj = $localStorage.searchDetails||{};
                        searchObj.gender = $scope.customerEntity.gender;
                        CommonService.setSearchDetails(searchObj);
                        $localStorage.searchDetails = searchObj;
                        
                        CommonService.GetCustIDFromCustProfileCookie().then(function(data){
                        var SocialProfileID =0;
                            var custIDJson = {};
                            if(data!=="" && data) {
                                custIDJson = JSON.parse(data);
                            }
                        SocialProfileID = custIDJson.SocialProfileID || 0;
                        NewInvestmentService.RegisterCustomer(populateCustomerData(SocialProfileID)).success(function (data) {

                            if (!data.ResponseDetails.HasError) {
                               // CommonService.setCookies('CustId',data.CustId);
                                var custObj  ={};
                                var objCust ={};
                                custObj.key ="CustProfileCookie";
                                NewInvestmentService.GetHttpValues(custObj).success(function(response){
                                    var customerProfileData = {};
                                    if(response.d && response.d!=="") {
                                        customerProfileData = JSON.parse(response.d);
                                    }
                                objCust.CustID=data.CustId;
                                objCust.VisitorToken=customerProfileData.VisitorToken;
                                objCust.SocialProfileID=customerProfileData.SocialProfileID || "";           
                                var value ="'"+ JSON.stringify(objCust)+"'";
                                var dataToSend = '{"key":"CustProfileCookie","value":'+value+',"expires":-1}';
                                NewInvestmentService.SetHttpValues(dataToSend);
                                    CommonService.setShouldPageSkipCookie();
                                    CommonService.goToQuotesPage(enquiryId);
                                    $scope.buttonName = "View Quotes";
                                    /*$scope.customerProceed = false;*/
                                });

                            } else {
                                $scope.customerProceed = false;
                                CommonService.showQuoteToast();
                                $scope.buttonName = "Retry";
                            }


                        }).error(function () {
                            CommonService.showQuoteToast();
                        });
                    });
                    }
            };

            function populateCustomerData(SocialProfileID){
                var obj = {};
                obj.IsAssistanceReq = !$scope.customerEntity.IsAssistanceReq;
                obj.CountryID = $scope.customerEntity.CountryID;
                obj.CountryCode = $scope.cityChangesObj.countryCode;
                obj.CustomerName = $scope.customerEntity.CustomerName;
                obj.Email = $scope.customerEntity.Email;
                obj.MobileNo = $scope.customerEntity.MobileNo;
                obj.GenderID = $scope.customerEntity.gender;
                obj.IsNRI = $scope.customerEntity.CountryID!="392";
                obj.Title = $scope.customerEntity.gender===1?'Mr':'Ms/Mrs';
                obj.CityId = $scope.selectedCityItem?$scope.selectedCityItem.cityId:9999;
                obj.StateId = $scope.selectedCityItem?$scope.selectedCityItem.stateId:99;
                obj.EnquiryId = enquiryId;
                obj.CustId = SocialProfileID;
                return obj;
            }

            $scope.goBackToHome = function(){
                $rootScope.slideFlag = 'left';
                $location.path('/choose');
            };
        }
    ]);

/**
 * Created by Prashants on 10/6/2015.
 */


app.controller('InterController',
    [
        'CommonService','NewInvestmentService','$location','$cookies',
        function (CommonService,NewInvestmentService,$location,$cookies) {

                var leadId = $location.$$search.leadId;

            var obj = {};
            obj.MatrixLeadID = leadId;
            obj.UtmSource = $location.$$search.utm_source||"";
            obj.UtmTerm = $location.$$search.utm_term||"";
            obj.UtmMedium = $location.$$search.utm_medium||"";
            obj.UtmCampaign = $location.$$search.utm_campaign||"";

            NewInvestmentService.getInvestDataForLead(obj).success(function(data) {
                if (data !== null && data.Data !== null && data.Data.VisitId > 0 && data.Data.CustomerId > 0 && data.Data.EnquiryId > 0){
                   // $cookies.put('visitId', data.Data.VisitId);
                    //$cookies.put('CustId', data.Data.CustomerId);
                    
                    $location.$$search.enquiryId = CommonService.encode(data.Data.EnquiryId , false);
                    $location.$$search.leadId = CommonService.encode(leadId, false);
                    $location.path('/quote');
                }else{
                    delete $location.$$search.leadId;
                    $location.path('/');
                }
            });
        }]);


/**
* Created by Prashants on 12/3/2015.
*/
app.controller('PaytmController',
    [
        '$route', '$scope', '$rootScope', 'CommonService', 'NewInvestmentService', '$location', '$cookies', '$timeout', '$localStorage', '$routeParams',
        function ($route, $scope, $rootScope, CommonService, NewInvestmentService, $location, $cookies, $timeout, $localStorage, $routeParams) {
            'use strict';
            var index;
            $scope.citywaterMark = 'Enter the City that you reside in';
            var defaultIncome = 100000;
            $scope.showStep = 'enquiry';
            $scope.stepNo = 1;
            $scope.Mobile_New_Disable = true;
            var newObj = {};
            newObj.key = "CustProfileCookie";

            NewInvestmentService.GetHttpValues(newObj).success(function (data) {
                CommonService.createVisit(data);
            });

            CommonService.GetCustIDFromCustProfileCookie().then(function (data) {
                var custIDJson = {};
                if (data && data !== "") {
                    custIDJson = JSON.parse(data);
                }
                var custIDd = custIDJson.CustID || 0;
                var planType;
                if ($location.$$search.planType) {
                    planType = $localStorage.searchDetails ? parseInt($localStorage.searchDetails.investmentFor) : parseInt($location.$$search.planType);
                }
                triggerEnquiryFormStartEvent(custIDd, planType);
            });

            triggerPageParamEvents('pre-quotes');

            $scope.showCustSection = false;
            $scope.discountsAndPremium = [];
            $rootScope.slideFlag = angular.isDefined($rootScope.slideFlag) ? $rootScope.slideFlag : 'right';
            $scope.focusObj = {};
            $scope.hideObject = {};
            $scope.hideObject.dropDownNotSeen = true;
            $scope.hideObject.investmentErrorMessageHide = true;
            $scope.hideObject.investmentErrorMessageHideForStep = true;
            $scope.hideObject.annualErrorMessageHide = true;
            $scope.hideObject.annualErrorMessageHideForStep = true;
            $scope.dobObj = {};
            $scope.dob = {
                date: CommonService.getDateList(),
                month: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                year: CommonService.getYearList()
            };


            $scope.annualMin = 100000;
            $scope.annualMax = 50000000;

            $scope.checkIfAnnualIncomeIsCorrect = function (objError) {                
                if (!CommonService.isEmptyObject(objError)) {                    
                    return;
                }
                $scope.hideObject.annualErrorMessageHide = $scope.details.annual >= $scope.annualMin && $scope.details.annual <= $scope.annualMax;
                if(!$scope.hideObject.annualErrorMessageHide){return;}
                $scope.hideObject.annualErrorMessageHideForStep = $scope.details.annual !== undefined && $scope.details.annual % 100000 === 0;
            };
            $scope.checkIfInvestmentAmountIsCorrect =  function (objError) {                
                if (!CommonService.isEmptyObject(objError)) {
                    return;
                }
                $scope.hideObject.investmentErrorMessageHide = $scope.details.investment >= $scope.investMin && $scope.details.investment <= $scope.investMax;
                if(!$scope.hideObject.investmentErrorMessageHide){return;}
                $scope.hideObject.investmentErrorMessageHideForStep = $scope.details.investment !== undefined && $scope.details.investment % 1000 === 0;
            };

            $scope.focusObj.termFocus = false;
            $rootScope.loaderProceed = false;
            $scope.buttonName = "Continue";
            $scope.showDropDown = false;
            var chosenRetirementValues = { yourAge: 0, retirementAge: 0, amountNeeded: 0 };
            var chosenGrowthValues = { yourAge: 0, payTerm: 0, amountToInvest: 0 };
            var chosenChildValues = { yourAge: 0, childAge: '', amountNeeded: 0, childPayTerm: 20 };
            var previousValue = 0;
            $scope.monthlyInvestment = 0;
            $scope.sliderObj = { min: 0, max: 100, value: 50, list: [] };
            $rootScope.pageClass = "choose";
            var plans = [{ 'name': 'retirement', 'selected': false, id: 2 }, { 'name': 'growth', 'selected': false, id: 1 }, { 'name': 'child', 'selected': false, id: 3}];

            $scope.plans = angular.copy(plans);

            if ($rootScope.isLanding) {
                $scope.discountsAndPremium = [];
                NewInvestmentService.getBanner().success(function (data) {
                    _.each(data, function (value) {
                        if (value.offerId === $location.$$search.offerid) {
                            $scope.imageUrl = value.imageUrl;
                            $scope.discountsAndPremium = value.discountAndPremium;
                        }
                    });

                });
            }


            $scope.details = {};
            function countUpValues(value) {
                if (value > 0) {
                    $scope.monthlyInvestment = value;
                    var options = {
                        useEasing: true,
                        useGrouping: true,
                        separator: ',',
                        decimal: '.',
                        prefix: 'Rs.',
                        suffix: ''
                    };
                    $timeout(function () {
                        var demo = new CountUp("myTargetElement", previousValue, value, 0, 1.5, options);
                        demo.start(function () {
                            /*$scope.$apply(function () {

                            });*/
                        });
                        previousValue = value;
                    }, 10);

                }
            }

            $scope.monthlyPayoutChange = function () {
                if (!($scope.isError || $scope.isTermError)) {
                    $scope.dontShowMessage = true;
                    $timeout(function () {
                        var postObj = {};
                        if (index === 1) {
                            chosenGrowthValues.amountToInvest = $scope.details.investment;
                            postObj.payterm = $scope.details.age;
                            postObj.premium = $scope.details.investment;
                        } else if (index === 2) {
                            var lifeExpectancy = 80;
                            postObj.payterm = $scope.details.age - $scope.details.yourAge <= 30 ? $scope.details.age - $scope.details.yourAge : 30;
                            postObj.premium = $scope.details.investment * ($scope.details.age < 80 ? (lifeExpectancy - $scope.details.age) : 1);

                            chosenRetirementValues.amountNeeded = $scope.details.investment;
                        } else {
                            chosenChildValues.amountNeeded = $scope.details.investment;
                            postObj.payterm = $scope.details.childTerm;
                            postObj.premium = $scope.details.investment;
                        }
                        var monthlyInvestment = 0;
                        if (index === 1) {
                            CommonService.getPremiumForPreQuote(postObj, index).then(function (value) {
                                monthlyInvestment = value.maturityBenefit;
                                countUpValues(monthlyInvestment);
                            });
                        } else if (index === 2) {
                            monthlyInvestment = CommonService.GetRetirementCalc($scope.details.investment, $scope.details.age, postObj.payterm);
                            countUpValues(monthlyInvestment);
                        } else if (index === 3) {
                            monthlyInvestment = CommonService.compoundInterest(postObj.premium, postObj.payterm);
                            countUpValues(monthlyInvestment);
                        }

                    }, 300);
                }
            };

            $scope.triggerSelectBox = function () {
                angular.element('.trigger-class-select').trigger('click');
            };

            function fillValues() {
                if (index === 1) {
                    $scope.index = 1;
                    $scope.investMin = 10000;
                    $scope.investMax = 200000;
                    $scope.investmentStep=1000;
                    $scope.firstTextHolder ="Please enter the amount that you wish to invest per year";
                    $scope.nameOfAgeHolder ="Select the number of years that you wish to pay for";
                    $scope.sliderObj.list = CommonService.getAmountList(10000, 100000, 125000, 500000, 1000, 25000);
                    $scope.sliderObj.max = $scope.sliderObj.list.length - 1;
                    $scope.firstText = "Invest per year";
                    $scope.secondText = "You will invest";
                    $scope.thirdText = "You will get";
                    $scope.finalText = "per year";
                    $scope.perText = "per month";
                    $scope.inYearsText = " in";
                    if (chosenGrowthValues.amountToInvest > 0) {
                        $scope.sliderObj.value = chosenGrowthValues.amountToInvest;
                        $scope.details.investment = $scope.sliderObj.list[chosenGrowthValues.amountToInvest];
                        $scope.details.investment = chosenGrowthValues.amountToInvest;
                        $scope.details.monthlyAmount = Math.round($scope.details.investment / 12);

                    } 
                    /**********************
                    else if (parseInt($scope.details.investment) <= 0 || $scope.details.investment === undefined) {
                        $scope.details.investment = 50000;
                    }
                    if ($scope.sliderObj.list.indexOf($scope.details.investment) < 0) {
                        $scope.details.investment = 50000;
                    }
                    **********************/
                    $scope.details.monthlyAmount = Math.round($scope.details.investment / 12);
                    $scope.sliderObj.value = $scope.sliderObj.list.indexOf($scope.details.investment);
                    chosenGrowthValues.amountToInvest = $scope.sliderObj.value;
                    if (chosenGrowthValues.payTerm > 0) {
                        $scope.details.age = chosenGrowthValues.payTerm;
                    }

                } else if (index === 2) {                    
                    $scope.index = 2;
                    $scope.investMin = 10000;
                    $scope.investMax = 200000;
                    $scope.investmentStep=1000;
                    $scope.firstTextHolder ="Please enter the monthly pension that you wish to receive post retirement";
                    $scope.nameOfAgeHolder ="Please enter the age at which you wish to retire";
                    $scope.sliderObj.list = CommonService.getAmountList(10000, 100000, 125000, 200000, 5000, 25000);
                    $scope.sliderObj.max = $scope.sliderObj.list.length - 1;
                    $scope.firstText = "Monthly pension";
                    $scope.secondText = "You will get";
                    $scope.thirdText = "You need to invest ";
                    $scope.finalText = "per month";
                    /*$scope.perText = "per month";*/
                    $scope.inYearsText = "per month";
                    if (chosenRetirementValues.amountNeeded > 0) {
                        $scope.sliderObj.value = chosenRetirementValues.amountNeeded;
                        $scope.details.investment = chosenRetirementValues.amountNeeded;
                    }                    
                    /************************
                    else if ((parseInt($scope.details.investment) <= 0 || $scope.details.investment === undefined) || parseInt($scope.details.investment) > 500000) {
                        $scope.details.investment = 50000;
                    }
                    if ($scope.sliderObj.list.indexOf($scope.details.investment) < 0) {
                        $scope.details.investment = 50000;
                    }
                    **********************/
                    $scope.details.monthlyAmount = Math.round($scope.details.investment / 12);
                    $scope.sliderObj.value = $scope.sliderObj.list.indexOf($scope.details.investment);
                    chosenRetirementValues.amountNeeded = $scope.sliderObj.value;
                    if (chosenRetirementValues.retirementAge > 0) {
                        $scope.details.age = chosenRetirementValues.retirementAge;
                    }
                } else {                    
                    $scope.investMin = 500000;
                    $scope.investMax = 50000000;
                    $scope.investmentStep=100000;
                    $scope.index = 3;                 
                    $scope.firstTextHolder ="Please enter the amount that you need for your Child’s future";
                    $scope.nameOfAgeHolder ="Please enter your child’s current age";
                    $scope.sliderObj.list = CommonService.getChildAmountList(500000, 5000000, 10000000, 50000000, 100000, 500000, 2500000);
                    $scope.sliderObj.max = $scope.sliderObj.list.length - 1;
                    $scope.firstText = "Amount you need";
                    $scope.secondText = "Your child gets";
                    $scope.thirdText = "You need to invest";
                    $scope.inYearsText = "per month";
                    if (chosenChildValues.amountNeeded > 0) {
                        $scope.sliderObj.value = chosenChildValues.amountNeeded;
                        $scope.details.investment = chosenChildValues.amountNeeded;
                    } 
                    /**********************
                    else if (parseInt($scope.details.investment) <= 0 || $scope.details.investment === undefined) {
                        $scope.details.investment = 2000000;
                    }
                    if ($scope.sliderObj.list.indexOf($scope.details.investment) < 0) {
                        $scope.details.investment = 2000000;
                    }
                    **********************/
                    $scope.details.monthlyAmount = Math.round($scope.details.investment / 12);
                    $scope.sliderObj.value = $scope.sliderObj.list.indexOf($scope.details.investment);
                    chosenChildValues.amountNeeded = $scope.sliderObj.value;
                    if (parseInt(chosenChildValues.childAge) >= 0) {
                        $scope.details.age = chosenChildValues.childAge;
                    } else {
                        $scope.details.age = '';
                    }
                    $scope.details.childTerm = chosenChildValues.childPayTerm;
                }
                $scope.details.index = index;
            }



            $scope.checkForError = function (value) {
                if (value !== undefined && value.toString().length == 2) {
                    $scope.focus = true;
                }
                $scope.isError = (value === undefined || value.length === 0) || (value > 60 || (index !== 2 && value < 18) || (index === 2 && value < 20) || (index === 2 && (value > $scope.details.age || $scope.details.age - value < 10)) || (index === 3 && (value - $scope.details.age < 18)));
                if (value !== undefined && value.toString().length == 2) {
                    var currentyear = new Date().getFullYear();
                    $scope.data.dobYear = currentyear - value;
                    $scope.data.dobDate = 1;
                    $scope.data.dobMonth = 1;
                    $scope.checkForTermError($scope.details.age);
                    $scope.monthlyPayoutChange();
                }
            };

            $scope.changeInYourAge = function () {
                if (index === 3) {
                    if ($scope.details.yourAge - $scope.details.age >= 18) {
                        chosenChildValues.yourAge = $scope.details.yourAge;
                    }
                } else {
                    if ($scope.details.yourAge >= 18) {
                        if (index === 1) {
                            chosenGrowthValues.yourAge = $scope.details.yourAge;
                        } else {
                            chosenRetirementValues.yourAge = $scope.details.yourAge;
                        }
                    }
                }
            };

            $scope.checkForTermError = function (value) {
                childTermChange();
                if (index === 1) {
                    $scope.isTermError = false;
                } else {
                    if (value !== undefined && value.toString().length > 0) {
                        $scope.focusObj.termFocus = true;
                    } else if (value !== undefined && value.toString().length === 0) {
                        $scope.focusObj.termFocus = false;
                    }
                    $scope.isTermError = (value === undefined || value.length === 0) || ((index === 3 && value > 17) || (index === 2 && (value < 50 || value > 80)) || (index === 2 && value < $scope.details.yourAge) || (index === 3 && $scope.details.yourAge - value < 18) || (index === 2 && value - $scope.details.yourAge < 10));
                    $timeout(function () {
                        if (!$scope.isError && !$scope.isTermError) {
                            $scope.monthlyPayoutChange();
                        }
                    }, 10);
                }
            };

            function childTermChange() {
                if (index === 3) {
                    $scope.details.age = parseInt($scope.details.age);
                    changeTermChild();
                }
            }

            $scope.changeInTerm = function (isValid) {
                $scope.checkForTermError($scope.details.age);
                $scope.checkForError($scope.details.yourAge);
                if (index === 2) {
                    chosenRetirementValues.retirementAge = $scope.details.age;
                } else if (index === 3) {
                    chosenChildValues.childAge = $scope.details.age;
                }
            };

            function changeTermChild() {

                $scope.details.childTerm = $scope.details.childTerm < 10 ? 10 : $scope.details.childTerm < 15 && $scope.details.childTerm > 10 ? 15 : $scope.details.childTerm > 15 ? 20 : 20;
                if ($scope.details.age >= 14) {
                    $scope.details.childTerm = 10;
                }
                if (parseInt($scope.details.childTerm) > 15 && (parseInt($scope.details.age) >= 9 && parseInt($scope.details.age) < 14)) {
                    $scope.details.childTerm = 15;
                }
                return parseInt($scope.details.childTerm);
            }

            $scope.changeInGrowthTerm = function (isValid) {
                $scope.checkForError($scope.details.yourAge, isValid);
                if (index === 1) {
                    chosenGrowthValues.payTerm = $scope.details.age;
                }
                if (isValid === undefined && !$scope.isError) {
                    $scope.monthlyPayoutChange();
                }
            };

            $scope.changeInChildTerm = function () {
                $scope.checkForTermError($scope.details.age);
                if (index === 3) {
                    chosenChildValues.childPayTerm = $scope.details.childTerm;
                }
                if (!$scope.isError && !$scope.isTermError) {
                    $scope.monthlyPayoutChange();
                }
            };

            $scope.changeInvestmentAmount = function () {
                if (!$scope.isError && !$scope.isTermError) {
                    $scope.monthlyPayoutChange();
                }
            };

            function showCustomerSection(form) {
                $scope.showStep = 'customer';
                $scope.stepNo = 2;
            }

            function sendMobileOTP(mobileNo) {
                var objLeadData = {};
                objLeadData.MobileNo = mobileNo;
                registerUpdateCustomer().success(function (data) {
                    NewInvestmentService.SendMobileOTP(objLeadData).success(function (data) {
                        var resp = data;
                    }).error(function (data) { });
                }).error(function (data) { });
            }

            function registerUpdateCustomer(enquiryId, SocialProfileID) {
                var obj = {};
                obj.IsAssistanceReq = true;
                obj.CountryID = $scope.details.CountryID;
                obj.CountryCode = $scope.details.CountryID;
                obj.CustomerName = $scope.details.CustomerName;
                obj.Email = $scope.details.Email;
                obj.MobileNo = $scope.details.MobileNo;
                obj.GenderID = $scope.details.gender;
                obj.IsNRI = $scope.details.CountryID != 392;
                obj.Title = $scope.details.gender === 1 ? 'Mr' : 'Ms/Mrs';
                obj.CityId = $scope.selectedCityItem ? $scope.selectedCityItem.cityId : 9999;
                obj.StateId = $scope.selectedCityItem ? $scope.selectedCityItem.stateId : 99;
                if (enquiryId !== undefined && enquiryId === 0)
                { obj.EnquiryId = enquiryId; }
                if (SocialProfileID !== undefined && SocialProfileID === 0)
                { obj.CustId = SocialProfileID; }
                return NewInvestmentService.RegisterCustomer(obj).success(function (data) { }).error(function (data) { });
            }

            $scope.ShowMobileVerification = function (isValid, form) {
                form.$submitted = true;
                if (!isValid) { return false; }
                ///isMobileNumberChanged();
                if(!$scope.IsOTPRequired){
                    $scope.createEnquiry(isValid, form);
                    $scope.isSMSVerified = true;
                    return false;
                }
                $scope.details.MobileNo_New = $scope.details.MobileNo;
                sendMobileOTP($scope.details.MobileNo_New);
                $scope.stepNo = 3;
                $scope.showStep = 'smsOTP';
                return false;
            };
            $scope.IsResendOTPValid = true;
            $scope.ResendOTP = function (src) {
                var resend = src === "resend";
                $scope.IsResendOTPValid = $scope.details.MobileNo_New !== '';
                if (!$scope.IsResendOTPValid) { $scope.ResendOTPError = "Enter mobile number"; return false; }
                $scope.IsResendOTPValid = $scope.details.MobileNo_New !== undefined && $scope.IsResendOTPValid && ($scope.details.MobileNo_New <= 9999999999 && $scope.details.MobileNo_New >= 6666666666) && ($scope.details.MobileNo_New.length === 10);
                if (!$scope.IsResendOTPValid) {
                    $scope.ResendOTPError = "Enter valid mobile number";
                    if (!resend) { $scope.Mobile_New_Disable = true; }
                    return false;
                }
                $scope.details.MobileNo = $scope.details.MobileNo_New;
                sendMobileOTP($scope.details.MobileNo_New);
                $scope.Mobile_New_Disable = true;
                return true;
            };
            $scope.isSMSVerified = true;
            $scope.VerifyOtpAndGo = function (form) {
                $scope.details.MobileNo_New = $scope.details.MobileNo;
                //var form = mobileForm;
                form.$submitted = true;
                var isValid = form.$valid;
                if (!isValid) { return; }
                var objLeadData = {};
                objLeadData.MobileNo = $scope.details.MobileNo_New;
                objLeadData.OTPsms = $scope.details.OTP;
                NewInvestmentService.VerifyMobileOTP(objLeadData).success(function (data) {
                    var resp = data;
                    var smsRight = resp.IsVerified;
                    if (resp.OKText === "1" && smsRight) {
                        $scope.createEnquiry(isValid, form);
                        $scope.isSMSVerified = true;
                    }
                    else {
                        $scope.isSMSVerified = false;
                    }
                }).error(function (data) {
                    $scope.isSMSVerified = false;
                });
            };
            $scope.createEnquiry = function (isValid, form) {
                $rootScope.isHeadIFrame = true;
                isValid = isValid && $scope.hideObject.investmentErrorMessageHide && $scope.hideObject.investmentErrorMessageHideForStep;
                isValid = (($scope.showStep !== 'enquiry') || checkForCity(isValid, form)) && isValid;
                if ($scope.details.yourAge < 18) { isValid = false; }
                if (isValid) {
                    $rootScope.slideFlag = 'right';                    
                    isValid = isValid && !$scope.isError && !$scope.isTermError;
                    if (form.$name === 'searchForm' && $scope.IsOTPRequired) {
                        showCustomerSection(form);
                        return;
                    }
                    if ($scope.details.CountryID === 392) {
                        isValid = $scope.details.MobileNo <= 9999999999 && $scope.details.MobileNo >= 6666666666;
                        $scope.errorMessageHide = $scope.details.MobileNo.length != 10 || !isValid;
                    }
                    $rootScope.slideFlag = 'right';
                    var cityObj = {};
                    cityObj.CityID = $scope.selectedCityItem.cityId;
                    cityObj.City = $scope.selectedCityItem.value;
                    cityObj.stateId = $scope.selectedCityItem.stateId;
                    if (isValid) {
                        $rootScope.loaderProceed = true;
                        $scope.buttonName = "Please Wait";
                        CommonService.saveEnquiryAndGenerateLead(index, $scope.details, $scope.monthlyInvestment, $scope.data, cityObj).then(function (data) {
                            $rootScope.loaderProceed = false;
                            if (data) {
                                /// $scope.buttonName = "Continue";
                            } else {
                                $scope.buttonName = "Retry";
                            }
                        });
                    }
                }
            };

            $scope.showDropDownfunc = function () {
                $scope.showDropDown = !$scope.showDropDown;
            };
            $scope.isAnyPlanSelected = function (planId) {
                previousValue = 0;
                angular.element(function () {
                    if ($rootScope.isLanding) {
                        $('html,body').animate({ scrollTop: 0 }, 600);
                    } else {
                        $('html,body').animate({ scrollTop: 255 }, 600);
                    }
                });
                if (planId !== index) {


                    $scope.plans = angular.copy(plans);
                    if (planId === 1) {
                        $scope.plans[1].selected = true;
                    } else if (planId === 2) {
                        $scope.plans[0].selected = true;
                    } else {
                        $scope.plans[2].selected = true;

                    }
                    index = CommonService.whichPlanSelected($scope.plans);
                    $scope.ages = CommonService.getAges(index);
                    /*****************************
                     $scope.details.age = CommonService.getAge(index);
                    *****************************/
                    $scope.nameOfAge = CommonService.getTypeName(index);
                    fillValues();
                    $scope.checkForError($scope.details.yourAge);
                    $scope.checkForTermError($scope.details.age);
                    if ($scope.details.yourAge !== undefined && $scope.details.yourAge.toString().length == 2) {
                        $timeout(function () {
                            $scope.monthlyPayoutChange();
                        }, 200);
                    }

                    if ($scope.planSelected) {
                        $scope.planSelected = false;
                        $timeout(function () {
                            $scope.planSelected = true;
                        }, 500);
                    } else {
                        $scope.planSelected = true;
                    }
                }
            };

            $scope.data = {
                dobDate: null,
                dobMonth: null,
                dobYear: null
            };

            $scope.changeDate = function (model) {
                $scope.data.dobDate = model;
                $scope.changeDateOfBirth();
            };

            $scope.changeMonth = function (model) {
                $scope.data.dobMonth = model;
                $scope.changeDateOfBirth();
            };

            $scope.changeYear = function (model) {
                $scope.data.dobYear = model;
                $scope.changeDateOfBirth();
            };

            $scope.changeDateOfBirth = function () {
                $scope.dobObj.dob = ($scope.data.dobDate !== undefined && $scope.data.dobDate !== null && $scope.data.dobDate.length === 1 ? '0' : '') + $scope.data.dobDate + "-" + ($scope.data.dobMonth !== undefined && $scope.data.dobMonth !== null && $scope.data.dobMonth.length === 1 ? '0' : '') + $scope.data.dobMonth + "-" + $scope.data.dobYear;
                if ($scope.isValidDateOfBirth()) {
                    $scope.details.yourAge = CommonService.getYourAge($scope.dobObj.dob);
                    $scope.checkForError($scope.details.yourAge);
                }
            };

            $scope.isAgeWithinRange = function () {
                return !$scope.isDOBMissing() && $scope.isValidDateOfBirth();
            };

            $scope.isAgeBelowMinimum = function () {
                return !$scope.isDOBMissing() && $scope.isValidDateOfBirth() && CommonService.getYourAge($scope.dobObj.dob) < 18;
            };

            $scope.isAgeAboveMaximum = function () {
                return !$scope.isDOBMissing() && $scope.isValidDateOfBirth() && CommonService.getYourAge($scope.dobObj.dob) > 65;
            };

            $scope.isValidAge = function () {
                return !$scope.isDOBMissing() && $scope.isValidDateOfBirth() && CommonService.getAge($scope.dobObj.dob) >= 18 && CommonService.getAge($scope.dobObj.dob) <= 65;
            };

            $scope.isYearSelected = function (index, dobYear) {
                return (index * 1 === $scope.dob.year.indexOf(parseInt(dobYear)));
            };

            $scope.isDOBMissing = function () {
                return (!$scope.data.dobDate || !$scope.data.dobMonth || !$scope.data.dobYear);
            };

            $scope.isValidDateOfBirth = function () {
                return $scope.dobObj.dob && CommonService.isValidDateOfBirth($scope.dobObj.dob);
            };

            function FillObjectFromLead() {
                var leadID = $location.$$search.leadid !== undefined ? $location.$$search.leadid : 0;
                if (leadID === 0) { return; }
                var objLeadData = {};
                objLeadData.MatrixLeadID = leadID;
                NewInvestmentService.getCity().success(function (data) {
                    $scope.city = CommonService.getCity(data.Data.CityList);
                    NewInvestmentService.GetLeadInfoByLeadId(objLeadData).success(function (data) {
                        $scope.selectedCityItem = CommonService.findCityFromId($scope.city, parseInt(data.CityID)) || {};
                        ///$scope.isAnyPlanSelected(parseInt(data.InvestmentTypeID));
                        index = parseInt(data.InvestmentTypeID);
                        var uGender = data.Gender;
                        if (isNaN(uGender)) { $scope.details.gender = uGender === 'Male' ? 1 : 2; }
                        else {
                            $scope.details.gender = data.Gender;
                        }
                        $scope.details.CustomerName = data.Name;
                        $scope.details.Email = data.EmailID;
                        $scope.details.MobileNo = data.MobileNo;
                        var dobDt = new Date(data.DOB);
                        $scope.data.dobDate = dobDt.getDate();
                        $scope.data.dobMonth = dobDt.getMonth() + 1;
                        $scope.data.dobYear = dobDt.getFullYear();
                        if (index === 1) {
                            $scope.details.age = parseInt(data.PayTerm);
                        }
                        $scope.index = index;
                        $scope.plans = angular.copy(plans);
                        if (index === 1) {
                            $scope.details.investment = obj.investment;
                        } else {
                            $scope.details.investment = obj.requiredAmt;
                        }
                        $scope.ages = CommonService.getAges(index);
                        $scope.details.age = CommonService.getAge(index);
                        $scope.nameOfAge = CommonService.getTypeName(index);

                        if ($scope.data !== null) {
                            $scope.dobObj.dob = (($scope.data.dobDate !== null && $scope.data.dobDate !== undefined) && $scope.data.dobDate.length === 1 ? '0' : '') + $scope.data.dobDate + "-" + (($scope.data.dobMonth !== null && $scope.data.dobMonth !== undefined) && $scope.data.dobMonth.length === 1 ? '0' : '') + $scope.data.dobMonth + "-" + $scope.data.dobYear;
                        }
                        $scope.details.yourAge = CommonService.getYourAge($scope.dobObj.dob);
                        $scope.planSelected = true;

                        $scope.plans = angular.copy(plans);
                        if (index === 1) {
                            $scope.plans[1].selected = true;
                        } else if (index === 2) {
                            $scope.plans[0].selected = true;
                        } else if (index === 3) {
                            $scope.plans[2].selected = true;

                        }
                        if ($scope.details.yourAge !== undefined && $scope.details.yourAge.toString().length == 2) {
                            $scope.checkForError($scope.details.yourAge);
                        }
                        fillValues();

                    }).error(function (data) { });

                }).error(function (data) { });

            }
            $scope.details.CountryID = 392;
            $scope.IsOTPRequired = true;
            function fillValuesByOthers() {
                $scope.details.CustomerName = $location.$$search.name || '';
                $scope.details.Email = $location.$$search.email || '';
                $scope.details.MobileNo = $location.$$search.mobileNo || '';
                $scope.IsOTPRequired = $location.$$search.mobileNo ?  false:true ;
            }
            function isMobileNumberChanged(){
            var obj = {};
                if ($localStorage.customerDetails === undefined) {
                    $scope.IsOTPRequired = true;
                    return;
                }
                obj = $localStorage.customerDetails;
                $scope.IsOTPRequired = $scope.details.MobileNo !== obj.MobileNo ;
                return;
            }
            
            function loadCustomerDetails(){         
                var obj = {};
                if ($localStorage.customerDetails !== undefined) {
                    obj = $localStorage.customerDetails;
                }               
                NewInvestmentService.getCity().success(function (data) {
                    $scope.city = CommonService.getCity(data.Data.CityList);
                    $scope.selectedCityItem = CommonService.findCityFromId($scope.city, obj.CityID) || {};
                    $scope.details.CustomerName = obj.CustomerName;
                    $scope.details.Email = obj.Email;
                    $scope.details.MobileNo = obj.MobileNo || '';
                    $scope.details.gender = obj.GenderID;   
                    $scope.details.CountryID = obj.CountryID || 392;
                }).error(function (data) {
                });
            }
            
            
            
            function onLoad() {
                ///$scope.details.annual = defaultIncome;
                $("div.zopim").hide();
                fillValuesByOthers();
                var obj = $localStorage.searchDetails;
                if (!CommonService.isEmptyObject(obj)) {
                    index = obj.investmentFor > 0 ? obj.investmentFor : 1;                    
                    if ($location.$$search.planType) {
                     index = parseInt($location.$$search.planType);
                    }
                    $scope.index = index;
                    $scope.details.gender = obj.gender;
                    $scope.details.yourAge = obj.yourAge;
                    $scope.details.age = index === 1 ? obj.payTerm : index === 2 ? obj.retirementAge : obj.childAge;
                    chosenChildValues.childAge = index === 3 ? obj.childAge : '';
                    chosenChildValues.childPayTerm = index === 3 ? obj.payTerm : 20;
                    $scope.plans = angular.copy(plans);
                    if (index === 1) {
                        $scope.plans[1].selected = true;
                    } else if (index === 2) {
                        $scope.plans[0].selected = true;
                    } else {
                        $scope.plans[2].selected = true;
                    }
                    $scope.data = {};
                    $scope.data.dobDate = obj.dobDate;
                    $scope.data.dobMonth = obj.dobMonth;
                    $scope.data.dobYear = obj.dobYear;
                    if ($scope.data !== null) {
                        $scope.dobObj.dob = (($scope.data.dobDate !== null && $scope.data.dobDate !== undefined) && $scope.data.dobDate.length === 1 ? '0' : '') + $scope.data.dobDate + "-" + (($scope.data.dobMonth !== null && $scope.data.dobMonth !== undefined) && $scope.data.dobMonth.length === 1 ? '0' : '') + $scope.data.dobMonth + "-" + $scope.data.dobYear;
                    }
                    $scope.details.yourAge = CommonService.getYourAge($scope.dobObj.dob);
                    $scope.planSelected = true;
                    $scope.ages = CommonService.getAges(index);
                    $scope.nameOfAge = CommonService.getTypeName(index);
                    if (index === 1) {   
                        var x =parseInt(obj.investment.toString().replace(/,/g,''));
                        $scope.details.investment = Math.round(x * 12 / 1000) * 1000; 
                    } else {
                        $scope.details.investment = obj.requiredAmt;
                    }

                    $scope.details.annual = obj.annualIncome;
                    fillValues();
                    if ($scope.details.yourAge !== undefined && $scope.details.yourAge.toString().length == 2) {
                        $scope.checkForError($scope.details.yourAge);
                        /*$scope.monthlyPayoutChange();*/
                    }
                }
                loadCustomerDetails();
                if ($rootScope.isLanding) {
                    //$scope.isAnyPlanSelected(parseInt($location.$$search.offerid));
                    $scope.isAnyPlanSelected(parseInt($location.$$search.planType));
                }
                else if ($location.$$search.planType) {
                    $scope.isAnyPlanSelected(parseInt($location.$$search.planType));
                    /*$location.search("planType",null);*/
                }
                else if (index === undefined) {
                    $scope.isAnyPlanSelected(1);
                }
            }

            $scope.selectedCityItem = {};

            $scope.blurObject = { customerNameBlurOut: false, emailBlurOut: false, mobileBlurOut: false, countryBlurOut: false, cityBlurOut: false, investmentBlurOut: false, annualBlurOut: false };

            $scope.trueThat = function (value) {
                if (value === 'customer') {
                    $scope.blurObject.customerNameBlurOut = true;
                } else if (value === 'email') {
                    $scope.blurObject.emailBlurOut = true;
                } else if (value === 'mobile') {
                    $scope.blurObject.mobileBlurOut = true;
                } else if (value === 'country') {
                    $scope.blurObject.countryBlurOut = true;
                } else if (value === 'city') {
                    $scope.blurObject.cityBlurOut = true;
                } else if (value === 'investment') {
                    $scope.blurObject.investmentBlurOut = true;
                } else if (value === 'annual') {
                    $scope.blurObject.annualBlurOut = true;
                }
            };

            $rootScope.leave = false;
            $scope.citySearchText = "";
            $scope.errorPresent = false;
            $scope.details = {};
            
            $scope.countryCodes = {};
            $scope.countryCodes.output = [];
            $scope.details.IsAssistanceReq = false;
            $scope.customerProceed = false;
            $scope.custFormSubmitName = "View Quotes";
            $scope.maxNum = 10;
            $scope.minNum = 10;
            $scope.buttonName = "View Quotes";
            var enquiryId = CommonService.decode($location.$$search.enquiryId) || $localStorage.equiryid;
            NewInvestmentService.getCountryCodes().success(function (data) {
                $scope.countryCodes = data;
            }).error(function (data) {

            });
            $scope.changeMaxLength = function (countryId) {
                if (countryId == 392) {
                    $scope.maxNum = 10;
                    $scope.minNum = 10;
                } else {
                    $scope.errorMessageHide = false;
                    $scope.maxNum = 18;
                    $scope.minNum = 0;
                }
            };

            // validation for city // PayTm Controller
            var checkForCity = function (isvalid, searchForm) {
                if ($scope.selectedCityItem === undefined || $scope.selectedCityItem === null || Object.keys($scope.selectedCityItem).length < 1) {
                    $scope.selectedCityItem = CommonService.findCityFromText($scope.city, $scope.citySearchText);
                    if ($scope.selectedCityItem === null || Object.keys($scope.selectedCityItem).length < 1) {
                        searchForm.cityDetails.$invalid = true;
                        $scope.errorPresent = true;
                        $scope.cityErrorMessage = "Enter city name";
                    }
                }
                return !searchForm.cityDetails.$invalid;
            };

            $scope.selectedCityItemChange = function (cityItem) {
               if(!CommonService.isEmptyObject(cityItem)){
                    $scope.errorPresent = false;
                }
                $scope.selectedCityItem = cityItem;
            };

            $scope.searchCityTextChange = function (citySearchText) {
                if (citySearchText != $scope.citySearchText) {
                    $scope.selectedCityItem = {};
                    $scope.citySearchText = citySearchText;
                }
                $scope.trueThat('city');
            };
            onLoad();
            $timeout(function () {
                $("input[name='cityDetails']").bind("keypress", function (e) { if (this.value === $scope.citywaterMark) { this.value = ''; } });
                $("input[name='cityDetails']").bind("blur", function (e) { if (this.value === "") { this.value = $scope.citywaterMark; } });
            });
        }
    ]);


/**
 * Created by Prashants on 12/3/2015.
 */
app.controller('PreQuotesController',
    [
        '$route','$scope', '$rootScope','CommonService','NewInvestmentService','$location','$cookies','$timeout','$localStorage','$routeParams',
        function ($route,$scope, $rootScope, CommonService,NewInvestmentService,$location,$cookies,$timeout,$localStorage,$routeParams) {
            'use strict';
            var index;
            $scope.privacy = true;
            $scope.dontShowMessage = false;
            $scope.submitted = false;
            $scope.data = {};
            $scope.dobObj = {};
            $scope.mainObj = {};
            $scope.mainObj.cityItem = {};
            $scope.mainObj.selectedCityItem = {};
            $scope.cityChangesObj = {};
            $scope.selectedCityItem = {};
            $scope.cityChangesObj = CommonService.prefillCityChangeObject($scope.cityChangesObj);
            CommonService.visitCreation();

            $scope.head = {};
            $scope.head.planHeading = "";

            CommonService.retrieveCustIdFromCookie(true,$scope.head.planHeading).then(function(data){
                $scope.head.planHeading = data;
            });

            triggerPageParamEvents('pre-quotes');
            $scope.discountsAndPremium = [];
            $rootScope.slideFlag = angular.isDefined($rootScope.slideFlag) ? $rootScope.slideFlag : 'right';
            $scope.focusObj = {};
            $scope.hideObject = {};
            $scope.hideObject.dropDownNotSeen = true;
            $scope.hideObject.investmentErrorMessageHide = true;
            $scope.hideObject.investmentErrorMessageHideForStep = true;
            $scope.hideObject.errorPresent = false;
            $scope.hideObject.showModalPopUp = false;

            $scope.checkIfInvestmentAmountIsCorrect = function(){
                var x = 0;
                x = CommonService.getInvestmentAmountFromCommas(x,$scope.details);
                $scope.hideObject.investmentErrorMessageForNoValue = (!isNaN(parseInt(x)) && x>=0);
                $scope.hideObject.investmentErrorMessageHide = index===3?(x>=500000 && x<=50000000):(x>=10000 && x<=200000);
                if(x!==undefined && x>=0) {
                    $scope.hideObject.investmentErrorMessageHideForStep = x% 1000 === 0 ;
                }

                if(index===1){
                    $scope.hideObject.investmentErrorMessage = "Investment amount should be between";
                }else if(index===2){
                    $scope.hideObject.investmentErrorMessage = "Pension amount should be between";
                }else if(index===3){
                    $scope.hideObject.investmentErrorMessage = "Amount you need for your child should be between";
                }

                if($scope.hideObject.investmentErrorMessageForNoValue && $scope.hideObject.investmentErrorMessageHide && $scope.hideObject.investmentErrorMessageHideForStep){
                    $scope.changeInvestmentAmount();
                }
            };

            $scope.focusObj.termFocus = false;
            $rootScope.loaderProceed = false;
            $scope.buttonName = "Continue";
            $scope.showDropDown = false;
            var chosenRetirementValues = {yourAge:0,retirementAge:0,amountNeeded:0};
            var chosenGrowthValues = {yourAge:0,payTerm:0,amountToInvest:0};
            var chosenChildValues = {yourAge:0,childAge:'',amountNeeded:0,childPayTerm:20};
            var previousValue = 0;
            $scope.monthlyInvestment = 0;
            $scope.sliderObj = {min:0,max:100,value:50,list:[]};
            $rootScope.pageClass = "choose";
            var plans = [{'name':'retirement','selected':false,id:2},{'name':'growth','selected':false,id:1},{'name':'child','selected':false,id:3}];

            $scope.plans = angular.copy(plans);

            if($rootScope.isLanding){
                $scope.discountsAndPremium = [];
                NewInvestmentService.getBanner().success(function(data){
                    _.each(data,function(value){
                        if(value.offerId == $location.$$search.offerid){
                            $scope.imageUrl = value.imageUrl;
                            $scope.discountsAndPremium = value.discountAndPremium;
                        }
                    });

                });
            }

            $scope.details = {};

            $scope.monthlyPayoutChange = function(){
                var investAmountModified = 0;
                investAmountModified = CommonService.getInvestmentAmountFromCommas(investAmountModified,$scope.details);

                if(!($scope.isError || $scope.isTermError)) {
                    $scope.dontShowMessage = true;
                    $timeout(function () {
                        var postObj = {};
                        if (index === 1) {
                            chosenGrowthValues.amountToInvest = investAmountModified;
                            postObj.payterm = $scope.details.age;
                            postObj.premium = investAmountModified;
                        } else if (index === 2) {
                            var lifeExpectancy = 80;
                            postObj.payterm = $scope.details.age - $scope.details.yourAge <= 30 ? $scope.details.age - $scope.details.yourAge : 30;
                            postObj.premium = investAmountModified * ($scope.details.age<80?(lifeExpectancy - $scope.details.age):1);

                            chosenRetirementValues.amountNeeded = investAmountModified;
                        } else {
                            chosenChildValues.amountNeeded = investAmountModified;
                            postObj.payterm = $scope.details.childTerm;
                            postObj.premium = investAmountModified;
                        }
                        var monthlyInvestment = 0;
                        if(index===1) {
                            CommonService.getPremiumForPreQuote(postObj, index).then(function (value) {
                                monthlyInvestment = value.maturityBenefit;
                                $scope.monthlyInvestment = CommonService.countUpValues(monthlyInvestment,$scope.monthlyInvestment,previousValue);
                            });
                        }else if(index===2){
                            monthlyInvestment = CommonService.GetRetirementCalc(investAmountModified,$scope.details.age,postObj.payterm);
                            $scope.monthlyInvestment = CommonService.countUpValues(monthlyInvestment,$scope.monthlyInvestment,previousValue);
                        }else  if(index===3){
                            monthlyInvestment = CommonService.compoundInterest(postObj.premium,postObj.payterm);
                            $scope.monthlyInvestment = CommonService.countUpValues(monthlyInvestment,$scope.monthlyInvestment,previousValue);
                        }

                    }, 300);
                }
            };

            $scope.triggerSelectBox = function(){
                angular.element('.trigger-class-select').trigger('click');
            };

            function fillValues(){
                var investAmountModified = 0;
                investAmountModified = CommonService.getInvestmentAmountFromCommas(investAmountModified,$scope.details);
                if(index===1){
                    $scope.maxlength = 9;
                    $scope.minlength = 1;
                    $scope.investMax = 200000;
                    $scope.investMin = 10000;
                    $scope.index = 1;
                    $scope.sliderObj.list = CommonService.getAmountList(10000,100000,125000,500000,1000,25000);
                    $scope.sliderObj.max = $scope.sliderObj.list.length-1;
                    $scope.firstText = "Amount you wish to invest yearly";

                    if(chosenGrowthValues.amountToInvest>0){
                        $scope.sliderObj.value = chosenGrowthValues.amountToInvest;
                        $scope.details.investment = $scope.sliderObj.list[chosenGrowthValues.amountToInvest];
                        $scope.details.investment = chosenGrowthValues.amountToInvest;
                        $scope.details.monthlyAmount = Math.round(investAmountModified/12);

                    }else if($scope.details.investment===undefined || (investAmountModified<=0)) {
							if(CommonService.IsMobileDevice() || $rootScope.isLanding)
							{
							$scope.details.investment ="";
							}
							else
							{
							$scope.details.investment = 50000;
							}
                    }
                    var amt = 0 ;
                    amt = CommonService.getInvestmentAmountFromCommas(amt,$scope.details);
                    if($scope.sliderObj.list.indexOf(amt)<0){
							if(CommonService.IsMobileDevice() || $rootScope.isLanding){
							$scope.details.investment ="";
							}
							else
							{
							$scope.details.investment = 50000;
							}
                    }
                    $scope.details.monthlyAmount = Math.round(investAmountModified/12);
                    $scope.sliderObj.value = $scope.sliderObj.list.indexOf(investAmountModified);
                    chosenGrowthValues.amountToInvest = $scope.sliderObj.value;
                    if(chosenGrowthValues.payTerm>0){
                        $scope.details.age = chosenGrowthValues.payTerm;
                    }

                }else if(index === 2){
                    $scope.maxlength = 9;
                    $scope.minlength = 1;
                    $scope.investMax = 200000;
                    $scope.investMin = 10000;
                    $scope.index = 2;
                    $scope.sliderObj.list = CommonService.getAmountList(10000,100000,125000,200000,5000,25000);
                    $scope.sliderObj.max = $scope.sliderObj.list.length-1;
                    $scope.firstText = "Pension you need per month";

                    if(chosenRetirementValues.amountNeeded>0){
                        $scope.sliderObj.value = chosenRetirementValues.amountNeeded;
                        $scope.details.investment = chosenRetirementValues.amountNeeded;
                    }else if((investAmountModified<=0 || $scope.details.investment===undefined) || investAmountModified>200000 ) {
                      if(CommonService.IsMobileDevice()|| $rootScope.isLanding)
							{
							$scope.details.investment ="";
							}
							else
							{
							$scope.details.investment = 50000;
							}
                    }
                    if($scope.sliderObj.list.indexOf(investAmountModified)<0){
							if(CommonService.IsMobileDevice()|| $rootScope.isLanding)
							{
							$scope.details.investment ="";
							}
							else
							{
							$scope.details.investment = 50000;
							}
                    }
                    $scope.details.monthlyAmount = Math.round(investAmountModified/12);
                    $scope.sliderObj.value = $scope.sliderObj.list.indexOf(investAmountModified);
                    chosenRetirementValues.amountNeeded = $scope.sliderObj.value;
                    if(chosenRetirementValues.retirementAge>0){
                        $scope.details.age = chosenRetirementValues.retirementAge;
                    }
                }else{
                    $scope.maxlength = 12;
                    $scope.minlength = 1;
                    $scope.investMax = 50000000;
                    $scope.investMin = 500000;
                    $scope.index = 3;
                    $scope.sliderObj.list = CommonService.getChildAmountList(500000,5000000,10000000,50000000,100000,500000,2500000);
                    $scope.sliderObj.max = $scope.sliderObj.list.length-1;
                    $scope.firstText = "Amount you need for your child";
                    if(chosenChildValues.amountNeeded>0){
                        $scope.sliderObj.value = chosenChildValues.amountNeeded;
                        $scope.details.investment = chosenChildValues.amountNeeded;
                    }else if(investAmountModified<=0 || $scope.details.investment===undefined) {
							if(CommonService.IsMobileDevice()|| $rootScope.isLanding)
							{
							$scope.details.investment ="";
							}
							else
							{
							$scope.details.investment = 2000000;
							}

                    }
                    if($scope.sliderObj.list.indexOf(investAmountModified)<0){
							if(CommonService.IsMobileDevice()|| $rootScope.isLanding)
							{
							$scope.details.investment ="";
							}
							else
							{
							$scope.details.investment = 2000000;
							}

                    }
                    $scope.details.monthlyAmount = Math.round(investAmountModified/12);
                    $scope.sliderObj.value = $scope.sliderObj.list.indexOf(investAmountModified);
                    chosenChildValues.amountNeeded = $scope.sliderObj.value;
                    if(parseInt(chosenChildValues.childAge)>=0){
                        $scope.details.age = chosenChildValues.childAge;
                    }else{
                        $scope.details.age = '';
                    }
                    $scope.details.childTerm = chosenChildValues.childPayTerm;
                }
                $scope.details.index = index;
            }

            $scope.checkForError = function(value){
                if(value!==undefined && value.toString().length==2){
                    $scope.focus = true;
                }
                $scope.isError = (value === undefined || value.length===0) || (value>60 || (index!==2 && value<18) ||(index===2 && value<20) || (index===2 && (value>$scope.details.age || $scope.details.age - value<10)) || (index===3 && (value-$scope.details.age<18)));
                if (value!==undefined && value.toString().length==2) {
                    var currentyear = new Date().getFullYear();
                    $scope.data.dobYear = currentyear - value;
                    $scope.data.dobDate = 1;
                    $scope.data.dobMonth = 1;
                    $scope.checkForTermError($scope.details.age);
                    $scope.monthlyPayoutChange();
                }
            };
            $scope.changeInYourAge = function(){
                if(index===3) {
                    if($scope.details.yourAge-$scope.details.age>=18){
                        chosenChildValues.yourAge = $scope.details.yourAge;
                    }
                }else {
                    if($scope.details.yourAge>=18){
                        if(index===1){
                            chosenGrowthValues.yourAge = $scope.details.yourAge;
                        }else{
                            chosenRetirementValues.yourAge = $scope.details.yourAge;
                        }
                    }
                }
            };

            $scope.checkForTermError = function(value){
                childTermChange();
                if(index===1){
                    $scope.isTermError = false;
                }else {
                    if (value !== undefined && value.toString().length > 0) {
                        $scope.focusObj.termFocus = true;
                    }else if(value !== undefined && value.toString().length===0){
                        $scope.focusObj.termFocus = false;
                    }
                    $scope.isTermError = (value === undefined || isNaN(value) || value.length === 0) || ((index === 3 && value > 17) || (index === 2 && (value < 50 || value > 80)) || (index === 2 && value < $scope.details.yourAge) || (index === 3 && $scope.details.yourAge - value < 18) ||(index === 2 && value - $scope.details.yourAge < 10) || (index===1 && $scope.details.age==='No. of years you want to invest for'));
                    $timeout(function(){
                        if(!$scope.isError && !$scope.isTermError){
                            $scope.monthlyPayoutChange();
                        }
                    },10);
                }
            };

            function childTermChange(){
                if(index===3) {
                    $scope.details.age = parseInt($scope.details.age);
                    changeTermChild();
                }
            }

            $scope.changeInTerm = function(isValid){
                $scope.checkForTermError($scope.details.age);
                $scope.checkForError($scope.details.yourAge);
                if(index===2){
                    chosenRetirementValues.retirementAge = $scope.details.age;
                }else if(index===3){
                    chosenChildValues.childAge = $scope.details.age;
                }
            };

            function changeTermChild(){

                $scope.details.childTerm = $scope.details.childTerm<10?10:$scope.details.childTerm<15 && $scope.details.childTerm>10?15:$scope.details.childTerm>15?20:20;
                if($scope.details.age>=14){
                    $scope.details.childTerm = 10;
                }
                if(parseInt($scope.details.childTerm)>15 && (parseInt($scope.details.age)>=9 &&parseInt($scope.details.age)<14)){
                    $scope.details.childTerm = 15;
                }
                return parseInt($scope.details.childTerm);
            }

            $scope.changeInGrowthTerm = function(isValid){
                $scope.checkForError($scope.details.yourAge,isValid);
                if(index===1){
                    chosenGrowthValues.payTerm = $scope.details.age;
                }
                if(isValid===undefined && !$scope.isError){
                    $scope.monthlyPayoutChange();
                }
            };

            $scope.isPayTermSelected = function(value){
                return !isNaN(value);
            };

            $scope.changeInChildTerm = function(){
                $scope.checkForTermError($scope.details.age);
                if(index===3){
                    chosenChildValues.childPayTerm = $scope.details.childTerm;
                }
                if(!$scope.isError && !$scope.isTermError){
                    $scope.monthlyPayoutChange();
                }
            };

            $scope.changeInvestmentAmount = function(){
                if(!$scope.isError && !$scope.isTermError){
                    $scope.monthlyPayoutChange();
                }
            };

            $scope.createEnquiry = function(isValid,form){
                $scope.blurObject = {customerNameBlurOut:true,emailBlurOut:true,mobileBlurOut:true,countryBlurOut:true,cityBlurOut:true,investmentBlurOut:true};
                $scope.checkIfInvestmentAmountIsCorrect();
                if($scope.details.yourAge <18){isValid = false;}
                isValid = isValid && !$scope.isError && !$scope.isTermError && $scope.hideObject.investmentErrorMessageHide && $scope.hideObject.investmentErrorMessageHideForStep && !isNaN($scope.details.age) && !$scope.hideObject.patternErrorPresent;
                isValid = isValid && (!isNaN($scope.cityChangesObj.countryCode));
                isValid = isValid && (!isNaN($scope.details.MobileNo)&& $scope.details.MobileNo>0);
                $scope.errorMessageHide = $scope.details.MobileNo<=0;
                if ($scope.details.CountryID*1 === 392) {
                    checkForCity(isValid, form);
                }
                if(isValid) {
                    $rootScope.slideFlag = 'right';
                    isValid = isValid && !$scope.isError && !$scope.isTermError;
                    if ($scope.details.CountryID*1 === 392) {
                        isValid = $scope.details.MobileNo <= 9999999999 && $scope.details.MobileNo >= 6666666666;
                        $scope.errorMessageHide = $scope.details.MobileNo.length != 10 || !isValid;
                        isValid = checkForCity(isValid, form) && isValid;
                    }
                    $rootScope.slideFlag = 'right';
                    var cityObj = {};
                        cityObj.CityID = $scope.details.CountryID*1 === 392?$scope.selectedCityItem.cityId:9999;
                        cityObj.City = $scope.details.CountryID*1 === 392?$scope.selectedCityItem.value:'Others';
                        cityObj.stateId = $scope.details.CountryID*1 === 392?$scope.selectedCityItem.stateId:99;
                    cityObj.countryCode =  $scope.cityChangesObj.countryCode;
                    if (isValid) {
                        $scope.loaderProceed = true;
                        $scope.buttonName = "Please Wait";
                        CommonService.saveEnquiryAndGenerateLead(index,$scope.details,$scope.monthlyInvestment,$scope.data,cityObj).then(function(data){
                            if(data){


                            }else {
                                $scope.buttonName = "Retry";
                            }
                        });
                    }
                }
            };

            $scope.isAnyPlanSelected = function(planId){
                previousValue = 0;
                $scope.blurObject = {customerNameBlurOut:false,emailBlurOut:false,mobileBlurOut:false,countryBlurOut:false,cityBlurOut:false,investmentBlurOut:false};
                angular.element(function(){
                    if($rootScope.isLanding && $rootScope.isHeadIFrame){
                        
                    }else if($rootScope.isLanding && !$rootScope.isHeadIFrame){
                        $('html,body').animate({scrollTop: 0}, 600);
                    }
                    if($rootScope.isMobile){
                        $('html,body').animate({scrollTop: 0}, 600);
                    }
                });
                if(planId !== index) {


                    $scope.plans = angular.copy(plans);
                    if (planId === 1) {
                        $scope.plans[1].selected = true;
                    } else if (planId === 2) {
                        $scope.plans[0].selected = true;
                    } else {
                        $scope.plans[2].selected = true;

                    }
                    index = CommonService.whichPlanSelected($scope.plans);

                    $scope.ages = CommonService.getAges(index);
                    /*$scope.details.age = CommonService.getAge(index);*/

					if((CommonService.IsMobileDevice()||$rootScope.isLanding) && index===1)
					{
					var ages = [];
					ages =CommonService.getAges(index);
                    if(isNaN($scope.details.age) || ages.indexOf($scope.details.age)<0){
					   ages.unshift("No. of years you want to invest for");
                       $scope.details.age="No. of years you want to invest for";
                    }
					$scope.ages = ages;
					
					}
					else if((CommonService.IsMobileDevice()||$rootScope.isLanding) && index===2)
					{
					$scope.details.age="";
					}
                    $scope.nameOfAge = CommonService.getTypeName(index);
                    fillValues();
                    $scope.checkForError($scope.details.yourAge);
                    $scope.checkForTermError($scope.details.age);
                    if ($scope.details.yourAge !== undefined && $scope.details.yourAge.toString().length == 2) {
                        $timeout(function(){
                            $scope.monthlyPayoutChange();
                        },200);
                    }

                    if ($scope.planSelected) {
                        $scope.planSelected = false;
                        $timeout(function () {
                            $scope.planSelected = true;
                        }, 50);
                    } else {
                        $scope.planSelected = true;
                    }
                }

                $scope.checkIfInvestmentAmountIsCorrect();
            };

            function fillObjectFromLead() {
                var leadID = $location.$$search.leadid !== undefined ? $location.$$search.leadid : 0;
                if (leadID === 0) { return; }
                var objLeadData = {};
                objLeadData.MatrixLeadID = leadID;
                NewInvestmentService.getCity().success(function (data) {
                    $scope.city = CommonService.getCity(data.Data.CityList);
                    NewInvestmentService.GetLeadInfoByLeadId(objLeadData).success(function (data) {
                        $scope.selectedCityItem = CommonService.findCityFromId($scope.city, parseInt(data.CityID)) || {};
                        $scope.mainObj.selectedCityItem = angular.copy($scope.selectedCityItem);
                        ///$scope.isAnyPlanSelected(parseInt(data.InvestmentTypeID));
						if(isNaN(data.InvestmentTypeID) || data.InvestmentTypeID=='0'){index = 1;}
                        else{
                            index = parseInt(data.InvestmentTypeID);
                        }
                        var uGender = data.Gender;
                        if(isNaN(uGender) || uGender=='0'){$scope.details.gender = uGender=='Male'?1:2;}
                        else{
                            $scope.details.gender = data.Gender;
                        }
                        $scope.details.CustomerName = data.Name;
                        $scope.details.Email = data.EmailID;
                        $scope.details.MobileNo = data.MobileNo;
                        var dobDt = new Date(data.DOB);
                        $scope.data.dobDate = dobDt.getDate();
                        $scope.data.dobMonth = dobDt.getMonth() + 1;
                        $scope.data.dobYear = dobDt.getFullYear();
                        if (index === 1) {
                            $scope.details.age = parseInt(data.PayTerm);
                        }
                        $scope.index = index;
                        $scope.plans = angular.copy(plans);
                        if (index === 1) {
                            $scope.details.investment = obj.investment;
                        } else {
                            $scope.details.investment = obj.requiredAmt;
                        }
                        $scope.ages = CommonService.getAges(index);
                        $scope.details.age = CommonService.getAge(index);
                        $scope.nameOfAge = CommonService.getTypeName(index);

                        if ($scope.data !== null) {
                            $scope.dobObj.dob = (($scope.data.dobDate !== null && $scope.data.dobDate !== undefined) && $scope.data.dobDate.length === 1 ? '0' : '') + $scope.data.dobDate + "-" + (($scope.data.dobMonth !== null && $scope.data.dobMonth !== undefined) && $scope.data.dobMonth.length === 1 ? '0' : '') + $scope.data.dobMonth + "-" + $scope.data.dobYear;
                        }
                        $scope.details.yourAge = CommonService.getYourAge($scope.dobObj.dob);
                        $scope.planSelected = true;

                        $scope.plans = angular.copy(plans);
                        if (index === 1) {
                            $scope.plans[1].selected = true;
                        } else if (index === 2) {
                            $scope.plans[0].selected = true;
                        } else if (index === 3) {
                            $scope.plans[2].selected = true;

                        }


                        if ($scope.details.yourAge !== undefined && $scope.details.yourAge.toString().length == 2) {
                            $scope.checkForError($scope.details.yourAge);
                        }
                        fillValues();

                    }).error(function (data) { });

                }).error(function (data) {   });

            }
            function onLoad(){
                $scope.IsHeaderHide = $location.$$search.isHead !== undefined ? $location.$$search.isHead === 'isHead' : false;
				$scope.IsHeaderHideIframe = $location.$$search.isHeadIframe !== undefined ? $location.$$search.isHeadIframe === 'isHeadIframe' : false;
                $scope.IsClickToFill = $location.$$search.c2ff !== undefined ? $location.$$search.c2ff === 'c2ff' : false;
                if ($scope.IsClickToFill) { fillObjectFromLead(); }
                else {
                    var obj = $localStorage.searchDetails;
                    if (!CommonService.isEmptyObject(obj)) {
                        index = obj.investmentFor > 0 ? obj.investmentFor : 1;
                        $scope.index = index;
                        $scope.details.yourAge = obj.yourAge;
                        $scope.details.age = index === 1 ? (Math.round(obj.payTerm / 5) * 5).toString() : index === 2 ? obj.retirementAge : obj.childAge;
                        chosenChildValues.childAge = index === 3 ? obj.childAge : '';
                        chosenChildValues.childPayTerm = index === 3 ? obj.payTerm : 20;
                        $scope.plans = angular.copy(plans);
                        if (index === 1) {
                            $scope.plans[1].selected = true;
                        } else if (index === 2) {
                            $scope.plans[0].selected = true;
                        } else {
                            $scope.plans[2].selected = true;
                        }
                        $scope.data = {};
                        $scope.data.dobDate = obj.dobDate;
                        $scope.data.dobMonth = obj.dobMonth;
                        $scope.data.dobYear = obj.dobYear;
                        if ($scope.data !== null) {
                            $scope.dobObj.dob = (($scope.data.dobDate !== null && $scope.data.dobDate !== undefined) && $scope.data.dobDate.length === 1 ? '0' : '') + $scope.data.dobDate + "-" + (($scope.data.dobMonth !== null && $scope.data.dobMonth !== undefined) && $scope.data.dobMonth.length === 1 ? '0' : '') + $scope.data.dobMonth + "-" + $scope.data.dobYear;
                        }
                        $scope.details.yourAge = CommonService.getYourAge($scope.dobObj.dob);
                        $scope.planSelected = true;
                        $scope.ages = CommonService.getAges(index);
                        $scope.nameOfAge = CommonService.getTypeName(index);
                        if (index === 1 && obj.investment!==undefined) {
                            var investAmtTemp = parseInt(obj.investment.toString().replace(/,/g, ''));
						    $scope.details.investment = $localStorage.monthlyStatus*1===2?((Math.round(investAmtTemp*12/1000))*1000):obj.investment;
                        } else {
                            $scope.details.investment = obj.requiredAmt;
                        }

                        $scope.details.investment = CommonService.AddCommas($scope.details.investment);
                        fillValues();
                        if ($scope.details.yourAge !== undefined && $scope.details.yourAge.toString().length == 2) {
                            $scope.checkForError($scope.details.yourAge);
                            /*$scope.monthlyPayoutChange();*/
                        }
                    }
                }

                if($rootScope.isLanding){
                    //$scope.isAnyPlanSelected(parseInt($location.$$search.offerid));
                    $scope.isAnyPlanSelected(parseInt($location.$$search.planType));
                }
                else if($location.$$search.planType){
                    $scope.isAnyPlanSelected(parseInt($location.$$search.planType));
                    /*$location.search("planType",null);*/
                }
                else if(index === undefined){
                    $scope.isAnyPlanSelected(1);
                }
                $scope.checkIfInvestmentAmountIsCorrect();
            }

            $scope.blurObject = {customerNameBlurOut:false,emailBlurOut:false,mobileBlurOut:false,countryBlurOut:false,cityBlurOut:false,investmentBlurOut:false};



            $scope.trueThat = function(value){
                $scope.blurObject = CommonService.getBlurObjectChange(value,$scope.blurObject);
            };

            $scope.checkForMobile = function(){
                var isValid = !isNaN($scope.details.MobileNo);
                if ($scope.details.CountryID*1 === 392) {
                    isValid = isValid && $scope.details.MobileNo <= 9999999999 && $scope.details.MobileNo >= 6666666666;
                    $scope.errorMessageHide = $scope.details.MobileNo!==undefined?($scope.details.MobileNo.length != 10 || !isValid):true;
                }
                $scope.errorMessageHide = $scope.details.MobileNo<=0;
                return $scope.errorMessageHide;
            };

            $scope.checkForCode = function(){
                return !isNaN($scope.cityChangesObj.countryCode);
            };

            $rootScope.leave = false;
            var obj = {};
            if($localStorage.customerDetails!==undefined) {
                obj = $localStorage.customerDetails;
            }
            NewInvestmentService.getCity().success(function(data){
                $scope.selectedCityItem = {};
                var newData = _.uniq(data.Data.CityList,function(a){
                    return a.CityId;
                });

                var newDelhiObject = {"CityId":"551","CityName":"New Delhi","StateId":"35","state":"Delhi"};
                newData.unshift(newDelhiObject);
                $scope.city = CommonService.getCity(newData);
                if(obj.CountryID*1===392) {
                    $scope.selectedCityItem = CommonService.findCityFromId($scope.city, obj.CityID) || {};
                    $scope.mainObj.selectedCityItem = angular.copy($scope.selectedCityItem);
                    $scope.mainObj.cityItem = angular.copy($scope.selectedCityItem);
                }
                if(obj.CountryID*1!==392){
                    $scope.cityChangesObj.citySearchText = "Others";
                    $scope.cityChangesObj.cityPlaceHolder = "Others";
                }else if(CommonService.isEmptyObject($scope.selectedCityItem)){
                    $scope.cityChangesObj.citySearchText = "";
                    $scope.cityChangesObj.cityPlaceHolder = "Enter City";
                }
                $scope.changeMaxLength($scope.details.CountryID);
            }).error(function(data){

            });

            $scope.details = {};
            CommonService.prefillCustomerEntityObject($scope.details,obj);

            $scope.countryCodes = {};

            $scope.maxNum = 10;
            $scope.minNum = 10;
            $scope.cityChangesObj.countryCode = obj.countryCode*1;

            NewInvestmentService.getCountryCodes().success(function(data){
                $scope.countryCodes = data;
                $scope.details.CountryID = obj.CountryID!==undefined?obj.CountryID.toString():"392";
                $scope.searchForCountryCode($scope.details.CountryID);

            }).error(function(data){

            });
             // validation for city
            var checkForCity = function(isvalid,searchForm){
                if($scope.selectedCityItem===undefined || $scope.selectedCityItem === null || Object.keys($scope.selectedCityItem).length<1){
                    $scope.selectedCityItem = CommonService.findCityFromText($scope.city,$scope.cityChangesObj.citySearchText);
                    if($scope.selectedCityItem === null || Object.keys($scope.selectedCityItem).length<1) {
                        searchForm.cityDetails.$invalid = true;
                        $scope.hideObject.errorPresent = true;
                        $scope.cityErrorMessage = "Enter city name";
                    }
                }
                return !searchForm.cityDetails.$invalid;
            };

            $scope.popularCityList = CommonService.getTopCitiesList();

            $scope.selectedCityItemChange = function(cityItem){
                $scope.hideObject.errorPresent = false;
                if(cityItem!==undefined &&cityItem!==null && $scope.selectedCityItem!==undefined && $scope.selectedCityItem!==null) {
                    if ($scope.selectedCityItem.cityId !== cityItem.cityId) {

                        $scope.hideObject.errorPresent = false;
                        if (cityItem !== undefined && cityItem !== null) {
                            $scope.selectedCityItem = angular.copy(cityItem);
                            $scope.mainObj.cityItem = angular.copy($scope.selectedCityItem);
                            $scope.mainObj.selectedCityItem = angular.copy($scope.selectedCityItem);
                            $scope.hideObject.showModalPopUp = false;
                        }
                    }
                }

           };

            $scope.selectFromTopCities = function(cityItem){
                $scope.selectedCityItem.cityId = cityItem.cityId;
                $scope.selectedCityItem.value = cityItem.value;
                $scope.selectedCityItem.stateId = cityItem.stateId;
                $scope.selectedCityItem.searchString = cityItem.name;
                $scope.selectedCityItem.display = cityItem.name;
                $scope.mainObj.selectedCityItem = angular.copy($scope.selectedCityItem);
                $scope.mainObj.cityItem = angular.copy(cityItem);
                $scope.hideObject.showModalPopUp = false;
            };

            $scope.searchCityTextChange = function(citySearchText){
                $scope.hideObject.patternErrorPresent = !CommonService.onlyAlphabetAllowed(citySearchText);
                if(citySearchText!==$scope.cityChangesObj.citySearchText) {
                    if($scope.selectedCityItem===undefined) {
                        $scope.selectedCityItem = {};
                    }
                     
                    $scope.citySearchText = citySearchText;
                }
            };

            $scope.details.CountryID = obj.CountryID!==undefined?obj.CountryID:"392";
            $scope.searchForCountryCode = function(countryId,hasChanged){
                $scope.cityChangesObj = CommonService.changeCityChangeObj($scope.countryCodes,$scope.cityChangesObj, countryId, obj);
                $scope.cityChangesObj.countryCode = CommonService.CheckForExistingCountryCodes($scope.cityChangesObj,$scope.countryCodes)?"":$scope.cityChangesObj.countryCode;
                if(hasChanged){
                    $scope.details.MobileNo = "";
                }
            };

            $scope.checkIfAvailableList = function(countryId){
                _.each($scope.countryCodes,function(val){
                    if(val.CountryCode===(countryId)*1){
                        $scope.details.CountryID = val.CountryCodeId.toString();
                        $scope.cityChangesObj.isEditable = false;
                        $scope.changeMaxLength($scope.details.CountryID);
                        $scope.searchForCountryCode($scope.details.CountryID);
                    }
                });
            };

            $scope.changeMaxLength = function(countryId){
                $scope.cityChangesObj.isNRI = false;
                $scope.cityChangesObj.minValue = 9;
                if(countryId*1 == 392){
                    $scope.cityChangesObj.isNRI = false;
                    $scope.cityChangesObj.minValue = 2;
                    $scope.cityChangesObj.cityPlaceHolder = "Enter City";
                    $scope.mainObj.cityItem = angular.copy($scope.selectedCityItem);
                    $scope.mainObj.selectedCityItem = angular.copy($scope.selectedCityItem);

                    if(CommonService.isEmptyObject($scope.selectedCityItem)){
                        $scope.cityChangesObj.citySearchText = '';
                    }

                }else{
                    $scope.cityChangesObj.minValue = 9;
                    $scope.hideObject.errorPresent = false;
                    $scope.cityChangesObj.citySearchText = 'Others';
                    $scope.cityChangesObj.cityPlaceHolder = "Others";
                    $scope.cityChangesObj.isNRI = true;
                    $scope.errorMessageHide=false;
                    $scope.maxNum = 11;
                    $scope.minNum = 8;
                    $scope.mainObj.cityItem = {};
                }
                _.each($scope.countryCodes,function(val){
                    if(countryId===val.CountryCodeId){
                        $scope.maxNum = val.MAX;
                        $scope.minNum = val.MIN;
                    }
                });
            };

            $scope.showPopUp = function(val){
                if(!$scope.cityChangesObj.isNRI) {
                    $scope.hideObject.showModalPopUp = val;
                }
                if(!val){
                    $scope.selectedCityItem = CommonService.findCityFromText($scope.city,$scope.cityChangesObj.citySearchText);
                    /*if(obj!==null && obj!==undefined && !CommonService.isEmptyObject(obj)){*/
                        /*$scope.selectedCityItem = angular.copy(obj);*/
                       $scope.mainObj.cityItem = angular.copy($scope.selectedCityItem);
                       $scope.mainObj.selectedCityItem = angular.copy($scope.selectedCityItem); 
                    /*}*/
                    if(!CommonService.onlyAlphabetAllowed($scope.cityChangesObj.citySearchText)){
                            $scope.cityChangesObj.citySearchText = "";
                    }
                }
            };

            $scope.isCityNameEntered = function(){
                return !$scope.cityChangesObj.isNRI && !$scope.mainObj.cityItem.cityId && ($scope.hideObject.errorPresent || $scope.blurObject.cityBlurOut );
            };

            $scope.checkIfAgeIsCorrect = function(){
                return !isNaN($scope.details.age);
            };

            onLoad();
        }
    ]);

/**
 * Created by Prashants on 8/11/2015.
 */

app.controller('QuotesController',
    [
        '$scope', '$rootScope','$cookies','$location','CommonService','$timeout','NewInvestmentService','$interval','$mdToast','$localStorage','$q','OmnitureService',
        function($scope, $rootScope,$cookies,$location,CommonService,$timeout,NewInvestmentService,$interval,$mdToast,$localStorage,$q,OmnitureService) {
            $scope.changeSlideFlag = function(flag) {
                $rootScope.slideFlag = flag;
            };
            $scope.disableObject = {};
            $scope.disableObject.redirectDisable = false;
            $scope.disableObject.quotesShown = false;
            $scope.chat = {};
            $scope.chat.chatStarted  = false;
            var custId = 0;
            var custCookieObj ={};
            custCookieObj.key = 'CustProfileCookie';
            var leadId = CommonService.decode($location.$$search.leadId,false);

                $scope.details = {};
                $scope.hideAfterCall = false;
                $scope.filterObject = {};
                $scope.hideQuotes = true;

                $scope.items = [{id:0,name:'Traditional'},{id:2,name:'Debt/Government Securities'},{id:4,name:'Equity/Market Linked'}];
                $scope.sortItems = [{id:1,name:'Returns'},{id:3,name:'Features'}];
                $scope.predItems = [{id:1,name:'Returns'},{id:2,name:'Features'}];

                var displayValueQuotes = [];

                var lowest = 0;
                /*$('#toolsModalId').modal({ keyboard: true });*/
                $scope.viewMore = "View More";
                $rootScope.slideFlag = angular.isDefined($rootScope.slideFlag) ? $rootScope.slideFlag : 'right';

                var allComboList = [];

                $scope.filterObject.filterId = $location.$$search.filt||$localStorage.filt||1;
                $scope.filterObject.prediction = $location.$$search.pred||$localStorage.pred||'2';
                $localStorage.pred = $scope.filterObject.prediction;
                $localStorage.filt = $scope.filterObject.filterId;
                $scope.isPayTermError = false;
                $scope.isPolicyTermError = false;
                $scope.hideObject = {payFocus:false,policyFocus:false,editButtonHide:false};
                $scope.hideObject.viewMore = true;
                $scope.hideObject.showFeatureDetails = false;
                $scope.hideObject.loadcomparetools = false;

                var greatest = 0;
                var quotes = {},dummyQuotes = [];
                var previousDetails = {};
                var mobileNo = 0;
                var toastPosition = {
                    bottom: false,
                    top: true,
                    left: false,
                    right: true
                    };
            

            var enqId = parseInt(CommonService.decode($location.$$search.enquiryId,true));
            $scope.planFilters = [];
            //Goto home page
            $scope.goBackToHome = function(){
                $scope.changeSlideFlag('left');
                $location.path('/customer');
            };
            $rootScope.pageClass = 'quote';
            var toolId = 2;
            if($localStorage.toolId!==undefined){
                toolId = $localStorage.toolId.toString();
            }
            $scope.filterObject.toolId = !$rootScope.isOnlyTraditional?($location.$$search.tool||toolId||2):0;
            /*$scope.filterObject.toolId = 0;*/
            $localStorage.toolId = $scope.filterObject.toolId;
            $scope.filterObject.payoutType = $location.$$search.payouttype||$localStorage.payouttype||3;
            $localStorage.payoutType = $scope.filterObject.payoutType;
            delete $location.$$search.tool;
            delete $location.$$search.payouttype;
            delete $location.$$search.filt;
            delete $location.$$search.pred;

            var previousPayTerm = 0,previousPolicyTerm = 0,previousToolId = 0;

            var details = {};

            $scope.changeInvestmentType = function(){
                if(!$scope.isError) {
                    var x = parseInt($scope.details.investment.toString().replace(/,/g, ''));
                    if ($scope.details.investmentType == 1) {
                        $scope.details.investmentType = 2;
                        $scope.details.investment = Math.round(x / 12);
                        $scope.typeName = "Change To yearly";
                        $scope.monthlyName = 'Monthly';
                    } else {
                        $scope.details.investment = Math.round(x * 12 / 100) * 100;
                        $scope.details.investmentType = 1;
                        $scope.typeName = "Change To Monthly";
                        $scope.monthlyName = 'Yearly';
                    }
                    $scope.details.investment = CommonService.AddCommas($scope.details.investment);
                    /*previousDetails.investment = angular.copy($scope.details.investment);*/
                    previousDetails.investmentType = angular.copy($scope.details.investmentType);
                    $scope.details.spaninvestment = CommonService.AmountToString($scope.details.investment);
                }
            };

            $scope.showWithCommas = function(){
                $scope.details.investment = CommonService.AddCommas($scope.details.investment);
            };

            $scope.focusOut = function(){
                $scope.details.amountFocusLost = !$scope.details.amountFocusLost;
                var x = parseInt($scope.details.investment.replace(/,/g, ''))||0;
                if(parseInt($scope.details.investmentType)===1) {
                    if (x >= 10000 && x <= 200000) {
                        $scope.details.spaninvestment = $scope.details.investment;
                        $scope.details.spaninvestment = CommonService.AmountToString($scope.details.spaninvestment);
                    } else {
                        $scope.details.investment = angular.copy(previousDetails.investment);
                    }
                }else{
                    if (x >= 833 && x <= 16667) {
                        $scope.details.spaninvestment = $scope.details.investment;
                        $scope.details.spaninvestment = CommonService.AmountToString($scope.details.spaninvestment);
                    }else{
                        $scope.details.investment = angular.copy(Math.round(previousDetails.investment.replace(/,/g, '')/12));
                    }
                }



                $scope.checkForError();

            };

                $scope.$on('chatEvent', function (event, data) {
                    $scope.chat.chatStarted = true; // 'Data to send'
                });

                $scope.chat = function(){
                    $zopim.livechat.window.show();
                };

            var getQuoteList = function(){
                $scope.list = [];
                CommonService.addHistoricalData(quotes,previousDetails.payTerm);
                dummyQuotes = angular.copy(quotes);
                if(parseInt($scope.filterObject.prediction)===1 && parseInt($scope.filterObject.toolId) === 0){
                    $scope.filterObject.prediction = '2';
                }else if(parseInt($scope.filterObject.toolId) !== 0){
                    $scope.filterObject.prediction = $localStorage.pred;
                }
                if(dummyQuotes.length>0) {
                    $scope.changeSortArray();
                    $scope.applyAllFilters();
                }else{
                    $scope.list = [];
                }
                $scope.applyFeatures();

            };

            function populateFilterObject() {
                var sortParam = "",filterParam = "";

                var filterArrayObject = [];
                if ($scope.filterObject.filterId == 1) {
                    sortParam = 'Retruns';
                } else {
                    sortParam = 'Features';
                }
                filterArrayObject.push("Sort" + sortParam);
                if (parseInt($scope.filterObject.toolId) === 0) {
                    filterParam = "traditional";
                } else if (parseInt($scope.filterObject.toolId) === 2) {
                    filterParam = "Debt securities";
                } else {
                    filterParam = "Equity";
                }
                filterArrayObject.push(filterParam);
                var payoutparam = "";
                if (parseInt($scope.filterObject.toolId) === 0) {
                    if (parseInt($scope.filterObject.payoutType) === 0) {
                        payoutparam = "Regular Interval";
                    } else if (parseInt($scope.filterObject.payoutType) === 1) {
                        payoutparam = "One Time";
                    } else {
                        payoutparam = "Both";
                    }
                    filterArrayObject.push(payoutparam);
                }
                return filterArrayObject;
            }

            $scope.changeSortArray = function(){
                $scope.hideQuotes = false;
                if($scope.filterObject.filterId==1){
                    if(parseInt($scope.filterObject.prediction)===2) {
                        if (parseInt($scope.filterObject.toolId) === 4) {
                            $scope.filterArray = ['-TotalReturnEquity', '-QuotePlan.AllPlanPriority'];
                        } else {
                            $scope.filterArray = ['-TotalReturn', '-QuotePlan.AllPlanPriority'];
                        }
                    }else if(parseInt($scope.filterObject.filterId)===1){
                        if (parseInt($scope.filterObject.toolId) === 4) {
                            $scope.filterArray = ['-historicalReturnsEquity', '-QuotePlan.AllPlanPriority'];
                        } else {
                            $scope.filterArray = ['-historicalReturns', '-QuotePlan.AllPlanPriority'];
                        }
                    }

                }else if($scope.filterObject.filterId==2) {
                    if(parseInt($scope.filterObject.prediction)===2) {
                        $scope.filterArray = ['-IRR', '-QuotePlan.AllPlanPriority'];
                    }
                }else {
                    if($scope.isAnyFeatureSelected) {
                        if(parseInt($scope.filterObject.prediction)===2) {
                            if (parseInt($scope.filterObject.toolId) === 4) {
                                $scope.filterArray = ['-percentageWeightage', '-TotalReturnEquity'];
                            } else {
                                $scope.filterArray = ['-percentageWeightage', '-TotalReturn'];
                            }
                        }else if(parseInt($scope.filterObject.filterId)===1){
                            if (parseInt($scope.filterObject.toolId) === 4) {
                                $scope.filterArray = ['-percentageWeightage', '-historicalReturnsEquity'];
                            } else {
                                $scope.filterArray = ['-percentageWeightage', '-historicalReturns'];
                            }
                        }
                    }else{
                        $scope.filterObject.filterId=1;
                        $scope.changeSortArray();
                    }
                }
                $localStorage.filt =  $scope.filterObject.filterId;
                if($scope.list!==undefined && $scope.list!==null ) {
                    CommonService.updateProductDetails($scope.list, leadId, $scope.filterObject, $scope.details.investmentFor);
                }
                $timeout(function(){
                    $scope.hideQuotes = true;
                },100);

                var filterArrayObject = populateFilterObject();
                if($scope.list!==undefined && $scope.list.length>0) {
                    var omniObject = CommonService.getOmnitureObject($scope.filterArray, $scope.list, $scope.filterObject.toolId);
                    triggerPageFilterEvents(OmnitureService.getProduct(omniObject),custId,leadId,filterArrayObject);
                }

            };

            $scope.changePrediction = function(prediction){
                $scope.hideQuotes = false;
                changeReturnName(prediction);
                $localStorage.pred =  prediction;
                getQuoteList();
                $timeout(function(){
                    $scope.hideQuotes = true;
                },100);
            };

            function changeReturnName(prediction){
                if(parseInt(prediction)===2) {
                    $scope.returnsName = "Fund Growth @ 8%";
                }else{
                    $scope.returnsName = "Had you invested 24 years ago";
                }
            }

            changeReturnName($scope.filterObject.prediction);

            $scope.selectPlanredirection = function(quote){
                $scope.disableObject.redirectDisable = true;
                $scope.generatedBIUrl = null;
                $scope.planid = quote.PlanID;
                $scope.filterObject.isChartLoaded = false;

                $scope.exclusionObject = [];
                $scope.features = [];
                    $scope.mapObject = [];



                        NewInvestmentService.getPlanRiders({PlanID: quote.PlanID}).success(function (data) {
                            $scope.PlanRiders = data.Riders;
                            $scope.Funds = data.Funds;
                            $scope.viewPlanQuote = quote;
                            if(quote.QuotePlan.ProductBrief!==null) {
                                $scope.exclusionObject = CommonService.getExclusionObj(quote.QuotePlan.ProductBrief);
                            }

                            $scope.features = CommonService.getExclusionObj(quote.QuotePlan.KeyFeatures);
                            NewInvestmentService.getChartData().success(function(data){
                                if($scope.filterObject.toolId*1===2) {
                                    $scope.mapObject = data[0].debt;
                                    $scope.heading = "Government Bond vs Fixed Deposit performance";
                                }else{
                                    $scope.mapObject = data[1].equity;
                                    $scope.heading = "Market Performance";
                                }
                                $scope.filterObject.isChartLoaded = true;
                                $('#viewModalId').modal('show');



                                $timeout(function(){
                                    calcNewWeightage(CommonService.calculateTheWeightage(displayValueQuotes,$scope.filterObject.toolId,$scope.viewPlanQuote), $scope.viewPlanQuote);
                                    $scope.disableObject.redirectDisable = false;
                                },100);
                            });
                            
                            if(configObj.planIds.indexOf(quote.PlanID)>-1) {
                                CommonService.populateBIObject(leadId, custId, quote, $localStorage.customerDetails, $localStorage.searchDetails, $scope.filterObject.toolId).then(function (data) {
                                    if($scope.planid ==quote.PlanID)
                                    {
                                    if(!data.d.HasErrror) {
                                        if ($scope.generatedBIUrl === null) {
                                            $scope.generatedBIUrl = data.d.RetrunValue;
                                        }
                                    }
                                }
                                });
                            }

                            triggerQuoteSelectionEvent(enqId,custId,leadId,OmnitureService.getSelectedProduct(quote));
                        });
                   // RedirectToPostQuote(quote,leadId);



            };

            $scope.checkForError = function(){
                var x = angular.copy($scope.details.investment);
                x = x!==undefined?parseInt(x.toString().replace(/,/g, '')):0;
                if($scope.details.investmentType===2){
                    x = x*12;
                }
                $scope.isError = Math.ceil(x)<10000 || Math.ceil(x)>200000;
            };

            $scope.ApplyFilters = function(isValid,hide){
                if(!hide) {
                    $scope.details.amountFocusLost = true;
                    $scope.checkErrorForPay();
                    $scope.checkErrorForPolicy();
                    if(!$scope.isError && !$scope.isPayTermError && !$scope.isPolicyTermError) {
                        $scope.hideObject.editButtonHide = false;
                        previousDetails = angular.copy($scope.details);
                        if($scope.details.investmentType!==null) {
                            $localStorage.monthlyStatus = $scope.details.investmentType;
                        }
                        getQuotes();
                    }else{
                        $scope.details.investment = angular.copy(previousDetails.investment);
                        $scope.details.payTerm = angular.copy(previousDetails.payTerm);
                        $scope.details.policyTerm = angular.copy(previousDetails.policyTerm);
                    }
                }
            };

            $scope.undoChanges = function(){
                $scope.details.amountFocusLost = true;
                $scope.hideObject.payFocus = false;
                $scope.hideObject.policyFocus = false;
                $scope.hideObject.editButtonHide = false;
                if($scope.details.investmentType===1) {
                    $scope.details.investment = angular.copy(previousDetails.investment);
                }else{
                    $scope.details.investment = angular.copy(Math.round(parseInt(previousDetails.investment.toString().replace(/,/g, ''))/12));
                }
                var invest = parseInt($scope.details.investment.toString().replace(/,/g, ''));
                $scope.details.spaninvestment = CommonService.AmountToString(invest);
                $scope.details.payTerm = angular.copy(previousDetails.payTerm);
                $scope.details.payOutStart = angular.copy(previousDetails.payOutStart);
                $scope.checkErrorForPay();
                $scope.checkErrorForPolicy();
            };
			$scope.addLeadingZero = function(value) {
				if(value < 10) {
					value = "0" + value;
				}
				return value;
			};        
        
			$scope.currentDate = function() {
				var currentTime = new Date(),
				month = currentTime.getMonth() + 1,
				day = currentTime.getDate(),
				year = currentTime.getFullYear();
					  
				return ($scope.addLeadingZero(year) + '' + $scope.addLeadingZero(month) + '' + $scope.addLeadingZero(day));
			};

			$scope.currentTime = function() {
					var currentTime = new Date(),
					hour = currentTime.getHours(),
					minute = currentTime.getMinutes();
						  
				return ($scope.addLeadingZero(hour) + '' + $scope.addLeadingZero(minute));
			 };

           
            NewInvestmentService.GetHttpValues(custCookieObj).success(function(response){
                if( typeof response.d !== 'undefined' && response.d!=="") {
                    var custProfileData = JSON.parse(response.d);
                    custId = custProfileData.CustID;
                }
                 
            (function updateEnquiry(){
                if(enqId>0) {
                    if($localStorage.searchDetails===undefined){
                        $localStorage.searchDetails = {};
                    }
                    details = $localStorage.searchDetails;
                    NewInvestmentService.getEnquiryDetails(enqId).success(function (data) {
                        leadId = data.MatrixLeadId;
                        details = CommonService.prefillValuesFromService(details, data);
                        $scope.planFilters = CommonService.populateFilterList(data.InvestmentTypeId);
                        $scope.typeName = "Change To Monthly";
                        $scope.monthlyName = 'Yearly';
                        NewInvestmentService.getAllCombinations().success(function (data) {
                            if (parseInt(details.investmentFor) === 3) {
                                allComboList = data[1].child;
                            }else {
                                allComboList = data[0].notChild;
                            }
                            $scope.payTermList = CommonService.GetPayTermList(details,allComboList);
                            $scope.policyTermList = CommonService.getPolicyTermList(details,details.payTerm,allComboList);
                            $scope.details = CommonService.initializeValues(details);
                            previousPayTerm = $scope.details.payTerm;
                            previousPolicyTerm = $scope.details.payOutStart;
                            previousToolId = $scope.filterObject.toolId;
                            if(parseInt($scope.filterObject.toolId)===0){
                                $scope.details.payTerm = 10;
                                /*$scope.payTermList = CommonService.GetPayTermList(details,allComboList);*/
                                $scope.payTermList = [10];
                                $scope.policyTermList = [{'payOutStart':20}];
                                $scope.details.payOutStart = 20;
                                /*if($scope.payTermList.indexOf(20)==-1){
                                    $scope.details.payTerm = 10;
                                    $scope.details.payOutStart = 10;
                                }*/
                            }
                            previousDetails = angular.copy($scope.details);
                            getQuotes();
                        });
                        if(custId===null ||custId===undefined || custId===0){
                            custId = data.CustId;
                           // $cookies.put('CustId',custId);
                        }

                        CommonService.populateCustDetails(custId,$scope.planFilters,leadId,data.LeadSource).then(function(data){
                            mobileNo = data.MobileNo;
                        });
                        triggerPageParamEvents('quotes');
                    });
                }else{
                    $location.path('/');
                }
              
            })();
            });

//            function hdfcPlans() {
//                var hdfcPlans = "142,153,161,166,167,180,183,184,188,192,194,202,203,216,227,228";
//                return hdfcPlans.split(',');
//            }

            function HdfcImageTagging(quote, eventType) {
                var hdfcPlans = "142,153,161,166,167,180,183,184,188,192,194,202,203,216,227,228";
                var hdfcPlanIDs = hdfcPlans.split(',');
                var currentPlanID = quote.PlanID;
                var isHDFCPlan = false;
                var data = $scope.list;
                var hdfcQuotes = $.grep(hdfcPlanIDs, function (element, index) {
                    return element == currentPlanID;
                });
                isHDFCPlan = hdfcQuotes.length > 0;

                if (isHDFCPlan === false) { return; }
                var product = quote.QuotePlan.PlanName;
                var todaysDate = $scope.currentDate();
                var currentTime = $scope.currentTime();
                var imgID = eventType == 'click' ? 'imgHdfcClick_' : 'imgHdfcImp_';
                var divTagg = eventType == 'click' ? $('#divDropHdfcookieClick') : $('#divDropHdfcookieImp');
                imgID = imgID + product;
                var hdfcImageSrc = 'http://hdfclife.demdex.net/event?c_partner=policybazaar&c_event=' + eventType + '&c_product=' + product + '&c_category=' + 'investment' + '&c_device=' + 'desktop' + '&c_date=' + todaysDate + '&c_time=' + currentTime + '&c_content-banner&customerID=' + leadId;
                var imgImp = '<img id="' + imgID + '" src="' + hdfcImageSrc + '" />';
                divTagg.append(imgImp);
            }

            function HDFCImpressionTagging(eventType) {
                var data = $scope.list;
                // var hdfcQuotes = $.grep(data, function (element, index) {
                //     ids = hdfcPlans();
                //     qry = '';
                //     for (xi = 0; xi < ids.length; xi++) { qry = qry ? qry + ' || element.PlanID == ' + ids[xi] : 'element.PlanID == ' + ids[xi]; }
                //     return eval(qry);
                // });
                _.each(data, function (value, key) {
                    HdfcImageTagging(value, 'imp');
                });
            }

            function getQuotes() {
                var postObj = CommonService.populateObjForQuotes(previousDetails, details, $scope.filterObject.toolId);
                NewInvestmentService.GetQuotes(postObj).success(function (data) {
                    $scope.disableObject.quotesShown = true;
                    quotes = data.Data.InvestmentQuoteList;
                    getQuoteList();
                   /* HDFCImpressionTagging();*/
                });
                $localStorage.searchDetails.investment = previousDetails.investment;
                $localStorage.searchDetails.payTerm  = previousDetails.payTerm;
                $localStorage.searchDetails.payOutStart = previousDetails.payOutStart;
                if(window.$zopim && $zopim.livechat!==undefined) {
                    CommonService.setChatTags($scope.planFilters, leadId, custId);
                }
            }

            function calcWeightage(x, value) {
                    var duration = 0;
                    if (x > 0) {
                        duration = Math.round(100 * 8 / x);
                    } else {
                        duration = 1;
                        x = 1;
                    }
                    value.weightage = 0;
                    $interval(function () {
                        value.weightage++;
                    }, duration, x);
            }

            function calcNewWeightage(x, value) {
                var duration = 0;
                if (x > 0) {
                    duration = Math.round(100 * 8 / x);
                } else {
                    duration = 1;
                    x = 1;
                }
                value.newWeightage = 0;
                $interval(function () {
                    value.newWeightage++;

                }, duration, x);
            }

            $scope.applyAllFilters = function(){
                dummyQuotes = CommonService.addIRRValues(dummyQuotes);

                _.each(dummyQuotes,function(value,key){
                    value.isDisplay = false;
                    var isDisplay = true;
                    isDisplay = isDisplay && CommonService.applyFilters(value,$scope.filterObject);
                    value.isDisplay = isDisplay;
                });

                $scope.list = angular.copy(dummyQuotes);
                var dummyDisplayedValues = $scope.list.filter(function(ele){
                   return ele.isDisplay;
                });

                displayValueQuotes = angular.copy(dummyDisplayedValues);

                if(parseInt($scope.filterObject.prediction)===2) {
                    if (parseInt($scope.filterObject.toolId) === 4) {

                        dummyDisplayedValues = dummyDisplayedValues.sortByProp('TotalReturnEquity').reverse();
                    } else {
                        dummyDisplayedValues = dummyDisplayedValues.sortByProp('TotalReturn').reverse();
                    }
                    if(dummyDisplayedValues.length>0) {
                        greatest = parseInt($scope.filterObject.toolId) == 4 ? dummyDisplayedValues[0].TotalReturnEquity : dummyDisplayedValues[0].TotalReturn;
                        lowest = (parseInt($scope.filterObject.toolId) == 4 ? CommonService.getMinLastNonZeroValue(dummyDisplayedValues, 'TotalReturnEquity') : CommonService.getMinLastNonZeroValue(dummyDisplayedValues, 'TotalReturn')) || 0;
                    }
                }else{
                    if(parseInt($scope.filterObject.toolId)===4) {
                        dummyDisplayedValues = dummyDisplayedValues.sortByProp('historicalReturnsEquity').reverse();

                    }else{
                        dummyDisplayedValues = dummyDisplayedValues.sortByProp('historicalReturns').reverse();

                    }
                    greatest = parseInt($scope.filterObject.toolId)==4?dummyDisplayedValues[0].historicalReturnsEquity:dummyDisplayedValues[0].historicalReturns;
                    if(dummyDisplayedValues.length===1){
                        lowest = 0;
                    }else {
                        lowest = (parseInt($scope.filterObject.toolId)==4?CommonService.getMinLastNonZeroValue(dummyDisplayedValues,'historicalReturnsEquity'):CommonService.getMinLastNonZeroValue(dummyDisplayedValues,'historicalReturns'))||0;
                    }
                }
                _.each($scope.list, function (value, key) {
                    var x = 0;
                    if(parseInt($scope.filterObject.prediction)===2) {
                        if (parseInt($scope.filterObject.toolId) === 4) {
                            x = CommonService.calculatePerc(greatest,lowest,value.TotalReturnEquity);/*parseInt(((value.TotalReturnEquity-lowest)/(greatest-lowest) * 100), 10);*/
                        } else {
                            x = CommonService.calculatePerc(greatest,lowest,value.TotalReturn);/*parseInt(((value.TotalReturn-lowest)/(greatest-lowest) * 100), 10);*/
                        }
                    }else{
                        if(greatest>0 && lowest>=0) {
                            if (parseInt($scope.filterObject.toolId) === 4) {
                                x = CommonService.calculatePerc(greatest,lowest,value.historicalReturnsEquity);/*parseInt(((value.historicalReturnsEquity-lowest) / (greatest-lowest) * 100), 10);*/
                            } else {
                                x = CommonService.calculatePerc(greatest,lowest,value.historicalReturns);/*parseInt(((value.historicalReturns-lowest) / (greatest-lowest) * 100), 10);*/
                            }
                        }else{
                            x=0;
                        }
                    }

                    calcWeightage(x, value);
                });
                var filterArrayObject = populateFilterObject();
                var omnitureObject = CommonService.getOmnitureObject($scope.filterArray,$scope.list,$scope.filterObject.toolId);
                triggerPageFilterEvents(OmnitureService.getProduct(omnitureObject),custId,leadId,filterArrayObject);
            };

            $scope.applyFeatures = function(){
                $scope.isAnyFeatureSelected = CommonService.getFiltersSelectedArray($scope.planFilters).length>0;
                CommonService.applyFeatureWeightage($scope.planFilters,$scope.list);
                if($scope.list!==undefined && $scope.list!==null) {
                    CommonService.updateProductDetails($scope.list, leadId, $scope.filterObject, $scope.details.investmentFor);
                }
            };

            $scope.changeInToolId = function(){
                $scope.hideQuotes = false;
                if(parseInt($scope.filterObject.toolId)===0){
                    $scope.details.payTerm = 10;
                    //$scope.payTermList = CommonService.GetPayTermList(details,allComboList);
                    $scope.payTermList = [10];
                    $scope.policyTermList = [{'payOutStart':20}];
                                $scope.details.payOutStart = 20;
                    /*$scope.changePolicyTermList();*/
                    previousDetails = angular.copy($scope.details);
                    getQuotes();

                }else{
                    if(parseInt(previousToolId)===0){
                        $scope.payTermList = CommonService.GetPayTermList(details,allComboList);
                        $scope.details.payTerm = previousPayTerm;

                        $scope.changePolicyTermList();
                        /*$scope.ApplyFilters();*/
                        previousDetails = angular.copy($scope.details);
                        getQuotes();
                    }
                }
                previousToolId = $scope.filterObject.toolId;
                $localStorage.toolId = $scope.filterObject.toolId;
                getQuoteList();
                $timeout(function(){
                    $scope.hideQuotes = true;
                },100);
            };

            $scope.changeInPayoutType = function(){
                if(parseInt($scope.filterObject.toolId)===0){
                    $localStorage.payouttype = $scope.filterObject.payoutType;
                    $scope.applyAllFilters();
                    $scope.applyFeatures();
                }

            };

            $scope.checkErrorForPay = function(){

                $scope.isPayTermError = parseInt($scope.details.payTerm)<5 || parseInt($scope.details.payTerm)>30 || parseInt($scope.details.payTerm)>parseInt($scope.details.payOutStart);
                $scope.checkErrorForPolicy();
            };

            $scope.checkErrorForPolicy = function(){
                $scope.isPolicyTermError = parseInt($scope.details.payOutStart)<5 || parseInt($scope.details.payOutStart)>30|| parseInt($scope.details.payTerm)>parseInt($scope.details.payOutStart);

            };

            $scope.emailPlans = function(){
                var emailObj = {};
                var emailQuoteList = [];
                _.each($scope.list,function(value){
                    if(emailQuoteList.length<3) {
                        var obj = {};
                        obj.PlanID = value.PlanID;
                        obj.PlanName = value.QuotePlan.PlanName;
                        obj.AnnualPremium = value.AnnualPremium;
                        obj.InsurerID = value.QuotePlan.InsurerID;
                        obj.InsurerFullName = value.QuotePlan.PlanInsurer.InsurerName;
                        obj.InsurerLogoImage = value.QuotePlan.PlanInsurer.InsurerLogoImage;
                        emailQuoteList.push(obj);
                    }
                });
                emailObj.QuoteList = emailQuoteList;
                emailObj.EnquiryID = enqId;

                emailObj.RedirectURL = window.location.href+'&filt='+$localStorage.filt+'&tool='+$localStorage.toolId+'&pred='+$localStorage.pred+'&payouttype='+$localStorage.payouttype;

                showToast('Email has been sent successfully!');

                NewInvestmentService.emailQuotes(emailObj).success(function(data){

                });
            };

            $scope.clickToCall = function(){
                var obj = {};
                obj.Number = mobileNo;
                obj.Group1 = "NTM_IB";
                obj.Group2 = "";
                obj.LeadSource = "PB";
                obj.LeadID = leadId;
                $scope.hideAfterCall = true;
                showToast('Thank You! We will call you back shortly');
                NewInvestmentService.clickToCall(obj).success(function(){

                });
            };

            var getToastPosition = function() {
                return Object.keys(toastPosition)
                    .filter(function(pos) { return toastPosition[pos]; })
                    .join(' ');
            };

            $scope.directToPostQuotes = function (quote) {
                triggerBuyEvent(enqId, custId, leadId, OmnitureService.getSelectedProduct(quote));
                RedirectToPostQuote(quote, leadId);
                product = quote.QuotePlan.PlanName;
                /*HdfcImageTagging(quote,'click');*/
            };

            function RedirectToPostQuote(quote, userInfoId) {
                var obj = {};
                obj.planId = quote.PlanID;

                obj.premium = quote.AnnualPremium;
                obj.leadId = leadId;
                NewInvestmentService.saveSelection(obj).success(function(res) {
                    var redirectionUrl = '/life-insurance/life-investment-detail.aspx';
                    var url = "";
                    if (quote.QuotePlan.RedirectURL !== null && quote.QuotePlan.RedirectURL !== undefined && typeof(quote.QuotePlan.RedirectURL) !== "undefined" && quote.QuotePlan.RedirectURL !== '') {
                        if (quote.QuotePlan.RedirectURL.indexOf('http') !== -1) {
                            url = quote.QuotePlan.RedirectURL;
                        } else {
                            if (quote.QuotePlan.RedirectURL == "xyz") {
                                url = configObj.getredirectionUrl(redirectionUrl);
                            }
                            else {
                                url = configObj.getredirectionUrl(quote.QuotePlan.RedirectURL);
                            }
                        }
                    } else {
                        url = configObj.getredirectionUrl(redirectionUrl);
                    }
                    
                    if($scope.list!==undefined && $scope.list!==null) {
                        CommonService.updateProductDetails($scope.list, leadId, $scope.filterObject, $scope.details.investmentFor,quote);
                    }
                    $timeout(function(){
                    var form = $('<form action="' + url + '" method="post">' +
                        '<input type="hidden" id="enquiryID" name="enquiryID" value=' + CommonService.encode(enqId,true) + ' />' +
                        '<input type="hidden" name="VisitorProductID" value=' + userInfoId + ' />' +
                        '<input type="hidden" name="PlanID" value=' + quote.PlanID + ' />' +
                        '<input type="hidden" name="Premium" value=' + quote.AnnualPremium + ' />' +
                        '<input type="hidden" name="WebsitePremium" value=' + quote.AnnualPremium + ' />' +
                        '<input type="hidden" name="SumAssured" value=' + quote.SumInsured + ' />' +
                        '<input type="hidden" name="Payterm" value=' + quote.Payterm + ' />' +
                        '<input type="hidden" name="MoreOptionDone" value="false" />' +
                        '<input type="hidden" name="PlanPolicyTerm" value=' + quote.PolicyTerm + ' />' +
                        '<input type="hidden" id="quoteID" name="quoteID" value=' + quote.QuoteID + ' />' +
                        '<input type="hidden" id="lifeCover" name="lifeCover" value=' + quote.LifeCover + ' />' +
                        '</form>');
                    $('body').append(form);
                    $(form).submit();
                    },200);
                });
            }

            function triggerOmnitureAndPostData(){
                var deferred = $q.defer();

            }

            function showToast(message){
                $mdToast.show(
                    $mdToast.simple()
                        .content(message)
                        .position(getToastPosition())
                        .hideDelay(3000)
                );
            }

            $scope.viewMoreFeatures = function(){

                $scope.hideObject.viewMore = !$scope.hideObject.viewMore;
                if($scope.hideObject.viewMore){
                    $scope.viewMore = "View More";
                }else{
                    $scope.viewMore = "View Less";
                }
            };

            $scope.changeFeatureList = function(value){
                $scope.hideObject.showFeatureDetails = !value;
            };

            $scope.changePolicyTermList = function(){
                $scope.policyTermList = CommonService.getPolicyTermList(details,$scope.details.payTerm,allComboList);
                $scope.details.payOutStart = angular.copy($scope.details.payTerm);
            };

            $scope.showModalPanel = function(){
                showTheModal().then(function(){
                    $timeout(function(){
                    showModal();
                    },300);
                });
            };

            function showModal(){
                $('#toolsModalId').modal('show');
            }

            function showTheModal(){
                var deferred = $q.defer();
                $scope.hideObject.loadcomparetools = true;
                deferred.resolve(true);

                return deferred.promise;

            }

            function mapNames(obj){
                var allData = obj;
                var callback = function (allData) {
                    return allData.QuotePlan.PlanName;
                };
                return allData.map(callback);
            }

       


        }

    ]);

angular.element(document).ready(function () {
    if(screen.width >= 768){
        var elem = angular.element('.filter-head'),
            leftFilterTop = 340;

        angular.element(window).scroll(function(){
            if(angular.element(window).scrollTop() >= leftFilterTop){
                angular.element('.filter-head').addClass('fixed-quote-head').removeClass('fadeOut').addClass('fadeIn');
            } else {
                angular.element('.filter-head').removeClass('fixed-quote-head').removeClass('fadeIn');
            }
        });
    }
});

/**
 * Created by Prashants on 8/12/2015.
 */

app.controller('ToolsController',
    [
        '$scope','$rootScope', 'CommonService','NewInvestmentService','$cookies','$location','$timeout','$localStorage',
        function ($scope, $rootScope,CommonService,NewInvestmentService,$cookies,$location,$timeout,$localStorage) {


            $rootScope.leave = false;
            $scope.isError = false;

            $scope.addDummyclass = false;

            $timeout(function(){
                $scope.addDummyclass = true;
            },900);
           // $scope.pageClass='compare';
            $scope.compare = {};
            $scope.compare.param = 2;
            $scope.viewMore = '+ More Details';
            $scope.plans = [];
            $scope.viewMoreDetails = true;
            $scope.obj = {};
            $scope.obj.amountFocusLost = false;
            var investment = 100000;
            if($localStorage.searchDetails!==undefined){
                if($localStorage.searchDetails.investment!==undefined && !Number($localStorage.searchDetails.investment)){
                    if( $localStorage.searchDetails.investment.replace(/,/g, '')>0){
                        var dumInvest = $localStorage.searchDetails.investment.replace(/,/g, '');
                        if($localStorage.monthlyStatus==2) {
                            investment = Math.round(dumInvest * 12 / 100) * 100;
                        }else{
                            investment = dumInvest;
                        }
                    }
                }else if($localStorage.searchDetails.investment>0){
                    if($localStorage.monthlyStatus==2) {
                        investment = $localStorage.searchDetails.investment*12;
                    }else{
                        investment = $localStorage.searchDetails.investment;
                    }
                }
            }
            $scope.obj.investment = investment;
            var payTerm = ($localStorage.searchDetails!==undefined && $localStorage.searchDetails.payTerm>0)?$localStorage.searchDetails.payTerm:10;
            $scope.obj.saveValueInvestment = angular.copy($scope.obj.investment);
                $scope.obj.investment = CommonService.AddCommas($scope.obj.investment);

            $scope.checkForError = function(){
                var x = angular.copy($scope.obj.investment);
                x = parseInt(x.toString().replace(/,/g, ''))||0;
                $scope.isError = x<10000 || x>5000000;
            };

            $scope.checkForError();

            $scope.obj.doHide = false;
            var obj0 = {id:0,name:'Traditional InsurancePlans',payoutBeforeTaxMin:'',payoutBeforeTaxMax:'',taxPayableMin:'',taxPayableMax:'',amountInvested:$scope.obj.investment,annualReturnsMin:3.1,annualReturnsMax:5.7,quarterly:1,tax:0,afterTaxReturnsMin:'',afterTaxReturnsMax:'',afterTaxReturnRateMin:'',afterTaxReturnRateMax:'',risk:'Assured rate of return',lifeCover:true,tenure:5,selected:$localStorage.toolId*1===0};
            var obj1 = {id:1,name:'Tax Free Fixed Deposits (Debt)',payoutBeforeTaxMin:'',payoutBeforeTaxMax:'',taxPayableMin:'',taxPayableMax:'',amountInvested:$scope.obj.investment,annualReturnsMin:8,annualReturnsMax:8.5,quarterly:4,tax:30,afterTaxReturnsMin:'',afterTaxReturnsMax:'',afterTaxReturnRateMin:'',afterTaxReturnRateMax:'',risk:'Assured rate of return',lifeCover:false,tenure:5,selected:false};
            var obj2 = {id:2,name:'ULIP (Debt / Government Security)',payoutBeforeTaxMin:'',payoutBeforeTaxMax:'',taxPayableMin:'',taxPayableMax:'',amountInvested:$scope.obj.investment,annualReturnsMin:7.5,annualReturnsMax:9.5,quarterly:1,tax:0,afterTaxReturnsMin:'',afterTaxReturnsMax:'',afterTaxReturnRateMin:'',afterTaxReturnRateMax:'',risk:'Low Risk',lifeCover:true,tenure:5,selected:$localStorage.toolId*1===2};
            var obj3 = {id:3,name:'Tax Free Mutual Funds  (Equity)',payoutBeforeTaxMin:'',payoutBeforeTaxMax:'',taxPayableMin:'',taxPayableMax:'',amountInvested:$scope.obj.investment,annualReturnsMin:8,annualReturnsMax:19,quarterly:1,tax:0,afterTaxReturnsMin:'',afterTaxReturnsMax:'',afterTaxReturnRateMin:'',afterTaxReturnRateMax:'',risk:'Not Guaranteed',lifeCover:false,tenure:3,selected:false};
            var obj4 = {id:4,name:'ULIP (Equity / Market  Linked)',payoutBeforeTaxMin:'',payoutBeforeTaxMax:'',taxPayableMin:'',taxPayableMax:'',amountInvested:$scope.obj.investment,annualReturnsMin:12,annualReturnsMax:24,quarterly:1,tax:0,afterTaxReturnsMin:'',afterTaxReturnsMax:'',afterTaxReturnRateMin:'',afterTaxReturnRateMax:'',risk:'Not Guaranteed',lifeCover:true,tenure:5,selected:$localStorage.toolId*1===4};


            $scope.changeTaxValues = function(){
                    var x = parseInt($scope.obj.investment.replace(/,/g, ''))||0;
                    if(x>=10000 && x<=5000000){
                    $scope.taxSavedOnInvestment = Math.round(x *0.3)||0;
                    _.each($scope.plans, function(value,key){
                        value.payoutBeforeTaxMin = CommonService.getPayoutBeforeTaxMin(value.annualReturnsMin,value.quarterly,x)||0;
                        value.payoutBeforeTaxMax = CommonService.getPayoutBeforeTaxMax(value.annualReturnsMax,value.quarterly,x)||0;
                        value.taxPayableMin = Math.round((value.tax/100)*(value.payoutBeforeTaxMin-x))||0;
                        value.taxPayableMax = Math.round((value.tax/100)*(value.payoutBeforeTaxMax-x))||0;
                        value.afterTaxReturnsMin=value.payoutBeforeTaxMin-value.taxPayableMin||0;
                        value.afterTaxReturnsMax=value.payoutBeforeTaxMax-value.taxPayableMax||0;

                        if(value.id===1) {
                            value.afterTaxReturnRateMin = CommonService.getAfterTaxReturnRateMin(value.afterTaxReturnsMin,x)||0;
                            value.afterTaxReturnRateMax = CommonService.getAfterTaxReturnRateMax(value.afterTaxReturnsMax, x) || 0;
                        }else{
                            value.afterTaxReturnRateMin = value.annualReturnsMin;
                            value.afterTaxReturnRateMax = value.annualReturnsMax;
                        }
                    });

                        $scope.tenxValue = 10*x;
                }
            };

            /*$scope.planSelectedRedirection = function(id){
                var x = 0;
                if(!Number($scope.obj.investment)) {
                    x = $scope.obj.investment.replace(/,/g, '');
                }
                if(x>=10000 && x <=5000000) {
                    $scope.changeSlideFlag('right');
                    if(id===undefined){
                        id = obj0.selected?0:obj2.selected?2:obj4.selected?4:2;
                    }
                    if (id === 0 || id === 2 || id === 4) {
                        $localStorage.toolId = id;
                        if($localStorage.searchDetails===undefined ||$localStorage.searchDetails===null){
                            $localStorage.searchDetails = {};
                        }
                        var obj = $localStorage.searchDetails;
                        obj.investment = $scope.obj.investment.replace(/,/g, '');

                        
                            var custId = $cookies.get('CustId');
                        if(custId) {
                            NewInvestmentService.getCustomerDetails(custId).success(function (data) {
                                $localStorage.customerDetails = data;
                                obj.gender = data.GenderID;
                                $localStorage.searchDetails = obj;
                                $location.$$search.enquiryId = CommonService.encode($localStorage.equiryid, false);
                                $location.path('/customer');
                            });
                        }else{
                            $location.$$search.enquiryId = CommonService.encode($localStorage.equiryid, false);
                            $location.path('/customer');
                        }
                    }
                }
            };*/

            $scope.focusOut = function(){
                var x = parseInt($scope.obj.investment.toString().replace(/,/g, ''))||0;
                if(x>=10000 && x<=5000000){
                    $scope.obj.saveValueInvestment = angular.copy($scope.obj.investment);
                }else{
                    $scope.obj.investment = angular.copy($scope.obj.saveValueInvestment);
                }
                $scope.obj.investment = CommonService.AddCommas($scope.obj.investment);
                $scope.checkForError();
            };



            $scope.compareParamsChanged = function(){
                $scope.plans = [];
                if(parseInt($scope.compare.param)===0){
                    obj2.selected = true;
                    $scope.plans.push(obj0);
                    $scope.plans.push(obj1);
                    $scope.plans.push(obj2);
                }else if(parseInt($scope.compare.param)===1){
                    obj4.selected = true;
                    $scope.plans.push(obj3);
                    $scope.plans.push(obj4);
                }else{
                    obj0.selected = $localStorage.toolId*1===0;
                    obj2.selected = $localStorage.toolId*1===2;
                    obj4.selected = $localStorage.toolId*1===4;
                    $scope.plans.push(obj0);
                    $scope.plans.push(obj1);
                    $scope.plans.push(obj2);
                    $scope.plans.push(obj3);
                    $scope.plans.push(obj4);
                }                
            };

            $scope.compareParamsChanged();

            $scope.changeTaxValues();

            $scope.focusEdit = function(){
                angular.element(function(){
                $('.num-add-comp').focus();
                });
            };

            angular.element(function(){
                $('#toolsModalId').on('hidden.bs.modal', function (e) {
                    $scope.hideObject.loadcomparetools = false;
                });
            });
    }]);

/**
 * Created by Prashants on 6/29/2015.
 */

    app.directive('onlyNum', function() {
        return function(scope, element, attrs) {

            var keyCode = [8, 9, 37, 39, 46,48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105];
            element.bind("keydown", function(event) {
                if ( !_.contains(keyCode, event.which) || event.shiftKey) {
                    scope.$apply(function() {
                        scope.$eval(attrs.onlyNum);
                        event.preventDefault();
                    });
                    event.preventDefault();
                }

            });
            element.on( "copy cut paste drop", function() {
                return false;
            });
        };
    });

app.directive('onlyNumAddCommas', ["$filter", "CommonService", function($filter,CommonService) {
    var keyCode = [8, 9, 37, 39, 46, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105];
    return {
        restrict: 'E',
        scope: {
            det: '=',
            addClass: '@',
            minLength:'@',
            maxLength:'@',
            changeValues:'&',
            getFocusOut:'&',
            placeText:'@'
        },
        link: function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (!_.contains(keyCode, event.which)|| event.shiftKey) {
                    scope.$apply(function () {
                        event.preventDefault();
                    });
                    event.preventDefault();
                } else {
                    $filter('addCommas')(scope.investment);
                }

            });

            element.on( "copy cut paste drop", function() {
                return false;
            });

            element.bind("keyup", function (event) {
                var x = angular.copy(scope.det.investment);
                if(x!==undefined){
                    x = x.replace(/,/g, '');
                }
                scope.$apply(function(){
                    scope.det.investment =  CommonService.AddCommas(x);
                });
            });


        },

        template:'<span class="rupee-sign"></span><input id="select_1355" type="tel" class="{{addClass}}" name="invest" minlength="{{minLength}}" maxlength="{{maxLength}}" placeholder="{{placeText}}" ng-model="det.investment"  ng-focus="!det.amountFocusLost" ng-blur="getFocusOut()" ng-change="changeValues()" data-ng-if="!det.amountFocusLost" />'
    };
}]);

app.directive('sliderDrag', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            function changeAmountValues() {
                scope.details.amount = scope.sliderObj.list[scope.sliderObj.value];
                scope.details.monthlyAmount = Math.round(scope.details.amount/12);
            }

            element.on('keydown', function(){
                changeAmountValues();
                scope.monthlyPayoutChange();
            });

            element.on('$md.pressup', function(){
                changeAmountValues();
                scope.monthlyPayoutChange();
            });

            element.on('$md.drag', function() {
                changeAmountValues();
            });

            element.on('$md.dragend', function() {
                changeAmountValues();
                scope.monthlyPayoutChange();
            });
        }
    };
});

/*
 This directive allows us to pass a function in on an enter key to do what we want.
 */
app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

app.directive('focusMe', ["$timeout", "$parse", function($timeout, $parse) {
      return {
        //scope: true,   // optionally create a child scope
        link: function(scope, element, attrs) {
          var model = $parse(attrs.focusMe);
          scope.$watch(model, function(value) {
            if(value === true) { 
              $timeout(function() {
                console.log(attrs);
                element[0].firstElementChild.firstElementChild.focus(); 
              });
            }
          });
          // to address @blesh's comment, set attribute value to 'false'
          // on blur event:
          element.bind('blur', function() {
             scope.$apply(model.assign(scope, false));
          });
        }
      };
}]);

app.directive('ghVisualization', ["CommonService", function (CommonService) {

    return {
        restrict: 'E',
        scope: {
            mapValue: '=',
            filtObject:'='
        },
        templateUrl:"./templates/chart-window.html",
        compile: function(tElem, tAttrs){
            includeJs("http://cdn.policybazaar.com/Investment/scripts/common-scripts/d3.v3.min.js");
            return {
                pre: function(scope, element, attributes, controller, transcludeFn){

                },
                post: function(scope, element, attributes, controller, transcludeFn){
                    scope.radio = {};

                    if(scope.filtObject.toolId*1===2){
                        scope.legend1 = "Government Bond";
                        scope.legend2 = "Fixed Deposit";
                        scope.radio.check = 1;
                    }else{
                        scope.legend1 = "Sensex";
                        scope.legend2 = "Nifty";
                        scope.heading = "Market Performance";
                        scope.radio.check = 2;
                    }
                    var mapObject = angular.copy(scope.mapValue);
                    var vis = d3.select(element[0]).append("svg").attr("width", 500).attr("height", 300),
                        WIDTH = 500,
                        HEIGHT = 300,
                        MARGINS = {
                            top: 20,
                            right: 30,
                            bottom: 20,
                            left: 50
                        };
                    // Define the div for the tooltip
                    var div = d3.select('gh-visualization').append("div")
                        .attr("class", "tooltip")
                        .style("opacity", 0);
                    var xScale = d3.scale.ordinal().rangeBands([MARGINS.left, WIDTH - MARGINS.right]);
                    var yScale = d3.scale.linear().range([HEIGHT - MARGINS.top, MARGINS.bottom]);

                    var lineGen = d3.svg.line().interpolate("cardinal ")
                        .x(function (d) {
                            return xScale(d.Year);
                        })
                        .y(function (d) {
                            if (scope.filtObject.toolId * 1 === 2) {
                                return yScale(d.fixedDeposit);
                            }else{
                                return yScale(d.Nifty);
                            }
                        });

                    var newLineGen = d3.svg.line().interpolate("cardinal ")
                        .x(function (d) {
                            return xScale(d.Year);
                        })
                        .y(function (d) {
                            if (scope.filtObject.toolId * 1 === 2) {
                                return yScale(d.governmentBondYields);
                            }else{
                                return yScale(d.Sensex);
                            }
                        });
                    xScale.domain(mapObject.map(function (d) {
                        return d.Year;
                    }));
                    var highest = CommonService.getHighest(mapObject,scope.filtObject.toolId);
                    var lowest = CommonService.getLowest(mapObject,scope.filtObject.toolId);
                    var lengthOfMap = mapObject.length;
                    var divisors = Math.round(lengthOfMap/5);
                    var xAxis = d3.svg.axis().scale(xScale).tickValues([mapObject[0].Year, mapObject[divisors].Year, mapObject[divisors*2].Year, mapObject[divisors*3].Year, mapObject[divisors*4].Year, mapObject[mapObject.length-1].Year]);

                    yScale.domain([0, highest]);
                    var yAxis = d3.svg.axis()
                        .scale(yScale)
                        .orient("left");

                    vis.append("svg:g").attr("class", "xaxis").attr("transform", "translate(0," + (HEIGHT - MARGINS.bottom) + ")").call(xAxis);

                    vis.append("svg:g")
                        .attr("class", "yaxis").attr("transform", "translate(" + (MARGINS.left) + ",0)")
                        .call(yAxis);
                    vis.append('svg:path')
                        .attr('d', lineGen(mapObject))
                        .attr('stroke', 'green')
                        .attr("class", "line1")
                        .attr('stroke-width', 2)
                        .attr('fill', 'none');

                    vis.append('svg:path')
                        .attr('d', newLineGen(mapObject))
                        .attr('stroke', 'blue')
                        .attr("class", "line2")
                        .attr('stroke-width', 2)
                        .attr('fill', 'none');

                    // Add the scatterplot
                    vis.selectAll(".dot")
                        .data(mapObject)
                        .enter().append("circle")
                        .attr("r", 3)
                        .attr("class","dot").style("fill","blue")
                        .attr("cx", function(d) { return xScale(d.Year);})
                        .attr("cy", function(d) { if(scope.filtObject.toolId*1===4){return yScale(d.Sensex);}else{return yScale(d.governmentBondYields);} })
                        .on("mouseover", function(d) {
                            div.transition()
                                .duration(200)
                                .style("opacity", 0.9);
                            var value =  scope.filtObject.toolId*1===4? d.Sensex: d.governmentBondYields;
                            div.html("Year:"+' '+d.Year + "<br/>"+"Value:"+' '+value)
                                .style("left", d3.select(this).attr("cx") + "px")
                                .style("top", d3.select(this).attr("cy") + "px");
                        })
                        .on("mouseout", function(d) {
                            div.transition()
                                .duration(500)
                                .style("opacity", 0);
                        });

                    vis.selectAll(".anotherDot")
                        .data(mapObject)
                        .enter().append("circle")
                        .attr("r", 3)
                        .attr("class","anotherDot").style("fill","green")
                        .attr("cx", function(d) { return xScale(d.Year);})
                        .attr("cy", function(d) { if(scope.filtObject.toolId*1===4){return yScale(d.Nifty);}else{return yScale(d.fixedDeposit);} })
                        .on("mouseover", function(d) {
                            div.transition()
                                .duration(200)
                                .style("opacity", 0.9);
                            var value =  scope.filtObject.toolId*1===4? d.Nifty: d.fixedDeposit;
                            div.html("Year:"+' ' +d.Year + "<br/>"  + "Value:"+' '+value)
                                .style("left", d3.select(this).attr("cx") + "px")
                                .style("top", d3.select(this).attr("cy") + "px");
                        })
                        .on("mouseout", function(d) {
                            div.transition()
                                .duration(500)
                                .style("opacity", 0);
                        });

                    function drawChart() {

                        xScale.domain(mapObject.map(function (d) {
                            return d.Year;
                        }));
                        highest = CommonService.getHighest(mapObject,scope.filtObject.toolId);
                        lowest = CommonService.getLowest(mapObject,scope.filtObject.toolId);
                        yScale.domain([0, highest]);

                        var svg = d3.select("gh-visualization").transition();

                        svg.select(".line2")   // change the line
                            .duration(750)
                            .attr("d", newLineGen(mapObject));
                        svg.select(".line1")   // change the line
                            .duration(750)
                            .attr("d", lineGen(mapObject));
                        svg.select(".xaxis") // change the x axis
                            .duration(750)
                            .call(xAxis);
                        svg.select(".yaxis") // change the y axis
                            .duration(750)
                            .call(yAxis);
                        svg.selectAll(".dot").duration(750).attr("r", 3).style("fill","blue").attr("cx", function(d) { return xScale(d.Year);})
                            .attr("cy", function(d) { if(scope.filtObject.toolId*1===4){return yScale(d.Sensex);}else{return yScale(d.governmentBondYields);} });
                        svg.selectAll(".anotherDot").duration(750).attr("r", 3).style("fill","green").attr("cx", function(d) { return xScale(d.Year);})
                            .attr("cy", function(d) { if(scope.filtObject.toolId*1===4){return yScale(d.Nifty);}else{return yScale(d.fixedDeposit);} });
                    }

                    scope.changeMap = function(val){
                        if(val!==undefined) {
                            mapObject = angular.copy(scope.mapValue.slice(scope.mapValue.length - val, scope.mapValue.length));
                        }
                        drawChart();
                    };

                    if(scope.filtObject.toolId*1===4){
                        scope.changeMap(15);
                    }else{
                        scope.changeMap(10);
                    }

                }
            };
        }
        /*link: function (scope, element, attrs) {


        }*/
    };
}]);



app.filter('trimNameSpace', function () {
    return function (value) {
    		var arrString = value.split(" ");
        var nameString;
    		if (arrString[0].length == 1 || arrString[0].length == 2) {
    			if(arrString[1] !== undefined) {
    				nameString = arrString[0] + " " + arrString[1];
    			} else {
    				nameString = arrString[0];
    			}
    		} else {
    			nameString = arrString[0];
    		}
        return nameString;
    };
  });


app.filter('searchCity', ["CommonService", function (CommonService) {
    return function (value,searchText) {
        var arr = [];
        if(searchText.search!==null && searchText.search.length>1){
            if(value!==undefined || value!==null ) {
                arr = searchText.search ? CommonService.createFilterForFirstValues(value,searchText.search) : [];
            }
        }
        return arr;
    };
}]);

  app.filter('spaceToUnderscore', function () {
    return function (value) {
        if(value!==undefined) {
            var convertString = value.split(' ').join('_');
            return convertString;
        }
        return;
    };
  });

    app.filter('yesNo', function () {
        return function (value) {
            if(value!==undefined) {
                if(value){
                return 'Yes';
                }else{
                    return 'No';
                }
            }
        };
    });

    app.filter('customDateFormat',function(){
        var months = ['Jan','Feb','Mar','Apr','May','June','July','Aug','Sept','Oct','Nov','Dec'];
        var dateForm = [];
       return function(value){
          if(value!==undefined){
              var x = value.indexOf('T');
              dateForm = value.substr(0,x).split('-');
              return dateForm[2]+" "+months[dateForm[1]-1]+" "+dateForm[0];
          }
       } ;
    });

    app.filter('amountConvertString', ["CommonService", function (CommonService) {
    return function (value) {
        if(value!==undefined) {
            return CommonService.AmountToString(value);
        }
        return "";
    };
  }]);

    app.filter('addCommas', ["CommonService", function (CommonService) {
        return function (value) {
            if(value!==undefined) {
                if (value.toString().indexOf(',') != -1) {
                    value = value.replace(/,/g, '');
                }
                return CommonService.AddCommas(value);
            }

        };
    }]);

    app.filter('removeTrailingZeros',function(){
       return function(value){
         if(value!==undefined ||Number(value)){
             value = parseFloat(value);
             return value.toString();
         }else{
             return 0;
         }
       };
    });

    app.filter('truncated', function () {
        return function (value) {
            if(value!==undefined) {
                value = value.toTitleCase();
                if(value.length>37){
                    value = value.substr(0,37)+'...';
                }
                return value;
            }else{
                return "";
            }
        };
    });

    app.filter('titleCase', function () {
        return function (value) {
            if(value!==undefined) {
                return value.toTitleCase();
            }else{
                return '';
            }
        };
    });

    app.filter('showDifferences', ["$rootScope", function($rootScope) {
        return function( items, difference) {
            var filtered = [];

            var filterFeatures = function(items){
                var filtered = [];
                if(!angular.isUndefined(items) && items.length){
                    for(var i=0; i<items.length; i++){
                        var item = items[i];
                        filtered[i] = item;
                        for(var j=0; j<item.Features.length; j++){
                            var innerItem = item.Features[j];
                            filtered[i].Features[j] = innerItem;
                            innerItem.isDisplay = true;
                            if($rootScope.showDifferences){
                                var response = changeArrayFormat(innerItem.Data);
                                var isIdentical = isValuesIdentical(response);
                                if(isIdentical){
                                    innerItem.isDisplay = false;
                                }
                            }
                        }
                    }
                }else{
                    filtered = items;
                }
                return filtered;
            };

            var isValuesIdentical = function(values){
                var len = values.length - 1;
                while(len){
                    if(values[len--].toUpperCase() !== values[len].toUpperCase())
                        return false;
                }
                return true;
            };

            var changeArrayFormat = function(input){
                var response = [];
                angular.forEach(input, function(item) {
                    response.push(item.Text);
                });
                return response;
            };

            filtered = filterFeatures(items);
            return filtered;

        };
    }]);


/**
 * Created by Prashants on 8/12/2015.
 */
app.service('CommonService', ["$rootScope", "$location", "NewInvestmentService", "$cookies", "$mdToast", "$q", "$localStorage", "$routeParams", "$filter", "$timeout","isMobile", function ($rootScope, $location, NewInvestmentService, $cookies, $mdToast, $q, $localStorage, $routeParams,$filter,$timeout,isMobile) {


    'use strict';
    var CommonService = {};

    var enquiry = '', details = {}, toolId = 0, cityItem = {}, customerDetails = {},oldObj={};

    var referer = [{name:'google'},{name:'yahoo'},{name:'bing'}];


    CommonService.IsMobileDevice = function () {
        var isMobile;
        isMobile = {
            /**
             * @return {boolean}
             */
            Android: function () {
                return navigator.userAgent.match(/Android/i) !== null;
            },
            /**
             * @return {boolean}
             */
            BlackBerry: function () {
                return navigator.userAgent.match(/BlackBerry/i) !== null;
            },
            iOS: function () {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i) !== null;
            },
            /**
             * @return {boolean}
             */
            Opera: function () {
                return navigator.userAgent.match(/Opera Mini/i) !== null;
            },
            /**
             * @return {boolean}
             */
            Windows: function () {
                return navigator.userAgent.match(/IEMobile/i) !== null;
            },
            any: function () {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };

        return isMobile.any();
    };

    CommonService.onlyAlphabetAllowed = function(val){
        var regex = /^[0-9a-zA-Z() ]*$/;
        return regex.test(val);
    };

    CommonService.visitCreation = function(){
        var newObj = {};
        newObj.key = "CustProfileCookie";
        NewInvestmentService.GetHttpValues(newObj).success(function(data){
            CommonService.createVisit(data);
        });
    };

    CommonService.createFilterForFirstValues = function(value,query) {
        var lowercaseQuery = angular.lowercase(query);
        var arr = [];
        arr = value.filter(function(ele){
            return (ele.searchString.toLowerCase().substr(0,lowercaseQuery.length).indexOf(lowercaseQuery) != -1);
        });
        return arr.slice(0, 5);
    };

    CommonService.retrieveCustIdFromCookie = function(isPlanHeadingReqd){
        var planHeading = "";
        var deferred = $q.defer();
        CommonService.GetCustIDFromCustProfileCookie().then(function(data){
            var custIDJson = {};
            if(data && data!=="") {
                custIDJson = JSON.parse(data);
            }

            var custIDd = custIDJson.CustID || 0;
            var planType;
            if(isPlanHeadingReqd) {
                if ($location.$$search.planType) {
                    planType = parseInt($location.$$search.planType);
                }
                if (planType * 1 === 1) {
                    planHeading = "Growth";
                } else if (planType * 1 === 2) {
                    planHeading = "Pension For Old Age";
                } else if (planType * 1 === 3) {
                    planHeading = "Plan Higher Education";
                }
            }else{
                if($location.$$search.planType){
                    planType =  $localStorage.searchDetails?parseInt($localStorage.searchDetails.investmentFor):parseInt($location.$$search.planType);
                }
            }
            triggerEnquiryFormStartEvent(custIDd,planType);
            deferred.resolve(planHeading);
        });
        return deferred.promise;
    };

    CommonService.whichPlanSelected = function (plans) {
        if (plans[0].selected) {
            return 2;
        } else if (plans[1].selected) {
            return 1;
        } else {
            return 3;
        }
    };

    CommonService.getAges = function (index) {
        var lowerLimit, upperLimit;
        if (index === 1) {
            lowerLimit = 5;
            upperLimit = 30;
        }
        var ages = [];
        for (var i = lowerLimit; i <= upperLimit;) {
            ages.push(i);
            i = i + 5;
        }
        if (index == 2) {
            ages = [50, 55, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 75, 80];
        }
        return ages;
    };

    CommonService.getAge = function (index) {
        if (index === 2) {
            return 65;
        } else if (index === 1) {
            return 10;
        }
    };


    CommonService.getYourAge = function(dateString){
        dateString = this.changeDateTommddYY(dateString);
        var today = new Date(),
            birthDate = new Date(dateString),
            age = today.getFullYear() - birthDate.getFullYear(),
            m = today.getMonth() - birthDate.getMonth();

        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    };

    CommonService.changeDateTommddYY = function(dateString){
        var arrDateString = dateString.split("-");
        return arrDateString[1] + "/" + arrDateString[0] + "/" + arrDateString[2];
    };


    CommonService.isValidDateOfBirth = function(dob) {
        if(angular.isUndefined(dob) || !dob) {
            return false;
        }
        var dobRegX = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;

        if(dob.match(dobRegX)) {
            var dobArray = dob.split('-'),
                date = parseInt(dobArray[0]),
                month  = parseInt(dobArray[1]),
                year = parseInt(dobArray[2]),
                listOfDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

            if (month === 1 || month > 2) {
                if (date > listOfDays[month-1]) {
                    return false;
                }
            }
            if (month === 2) {
                var leapYear = false;
                if ( ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0))  {
                    leapYear = true;
                }
                if ((leapYear === false) && (date >= 29)) {
                    return false;
                }
                if ((leapYear === true) && (date > 29)) {
                    return false;
                }
            }
            return true;
        }  else  {
            return false;
        }
    };

    CommonService.getDateList = function() {
        var dateList = [];
        for(var i=1; i<=31; i++){
            if(i <= 9) {
                dateList.push('0' + i);
            } else {
                dateList.push(i);
            }
        }
        return dateList;
    };

    CommonService.getYearList = function() {
        var currentYear = new Date().getFullYear(),
            min = currentYear - 62,
            max = currentYear - 18,
            yearList = [];

        for(var i = min; i<=max; i++){
            yearList.push(i);
        }
        return yearList;
    };

    CommonService.extractDateMonthYearFromDOB = function(dobString, index){
        var dobArray = dobString.split("-");
        return dobArray[index];
    };

    CommonService.getCorpusAmount = function (amount, payterm) {
        var array = [104.94, 105.74, 106.02, 106.18, 106.26, 106.32, 106.38, 106.43, 106.45, 106.47, 106.49, 106.59, 106.68, 106.75, 106.77, 106.80, 106.81, 106.83, 106.84, 106.85, 106.97, 107.10, 107.22, 107.35, 107.49, 107.6, 107.8, 107.9, 108.0, 108.2];
        var x = 0;
        for (var i = 0; i < payterm; i++) {
            x = Math.round(((x + amount) * array[i]) / 100);
        }
        return x;

    };

    CommonService.getInvestmentAmount = function (amount, payterm) {
        var array = [109.34, 109.3, 109.27, 109.26, 109.26, 109.27, 109.3, 109.36, 109.43, 109.52, 109.64, 109.90, 110.21, 110.55, 110.96, 111.40, 111.92, 112.50, 113.17, 113.99, 115.097, 116.495, 118.308, 120.718, 124.106, 129.23, 137.8, 154.939, 206.497, 104.94];
        var x = amount;
        for (var i = array.length - payterm; i < array.length; i++) {
            x = x / (array[i] / 100);
        }
        return x / 12;

    };

    CommonService.getTypeName = function (index) {
        if (index === 2) {
            return isMobile?'Age at which you want to retire':'Retirement Age';
        } else if (index === 1) {
            return 'No. of Years';
        } else {
            return isMobile?"Your Child's Age":"Child's Age";
        }
    };

    CommonService.getAmountList = function (firstMin, firstMax, secondMin, secondMax, firstStep, secondStep) {
        var arr = [];
        for (var i = firstMin; i <= firstMax;) {
            arr.push(i);
            i = i + firstStep;
        }
        for (var j = secondMin; j <= secondMax;) {
            arr.push(j);
            j = j + secondStep;
        }
        return arr;
    };

    // Annual investment compounding
    CommonService.compoundInterest = function (corpus, payTerm) {
        var factor = 0.07124;
        var divisor = ((Math.pow((1.07124), (payTerm)))) - 1;
        var amt = corpus * factor / divisor;
        return Math.round(amt / 12);
    };

    // Retirement Calculation
    CommonService.GetRetirementCalc = function (monthlyPension, age, payTerm) {
        var power = -12 * (age < 80 ? (80 - age) : 1);
        var divisor = 0.00667;
        var P = monthlyPension;
        var multiplier = 1 - Math.pow(1.00667, power);
        var corpus = (P * multiplier) / divisor;
        return CommonService.compoundInterest(corpus, payTerm);
    };

    CommonService.getChildAmountList = function (firstMin, firstMax, secondMax, thirdMax, firstStep, secondStep, thirdStep) {
        var arr = [];
        for (var i = firstMin; i < firstMax;) {
            arr.push(i);
            i = i + firstStep;
        }
        for (var j = firstMax; j <= secondMax;) {
            arr.push(j);
            j = j + secondStep;
        }

        for (var k = secondMax; k <= thirdMax;) {
            arr.push(k);
            k = k + thirdStep;
        }
        return arr;
    };

    CommonService.populateEnquiryDetails = function (index, details, monthlyInvestment,dob) {
        var deferred = $q.defer();
        var obj ={};
        populateForAll(index, details, monthlyInvestment,dob).then(function(data) {
            obj = data;
            obj.DesiredMonthlyPayoutRet = index === 2 ? details.amount : '';
            obj.DesiredLumpSumChild = index === 3 ? details.amount : '';
            obj.PremiumPaidGrowth = index === 1 ? Math.round(details.amount / 1000) * 1000 : '';
            deferred.resolve(obj);
        });
        return  deferred.promise;
    };

    CommonService.populateEnquiryDetailsForMob = function (index, details, monthlyInvestment,dob) {
        var deferred = $q.defer();
        var obj ={};
        populateForAll(index, details, monthlyInvestment,dob).then(function(data){
            obj = data;
            obj.DesiredMonthlyPayoutRet = index === 2 ? details.investment : '';
            obj.DesiredLumpSumChild = index === 3 ? details.investment : '';
            obj.PremiumPaidGrowth = index === 1 ? Math.round(details.investment / 1000) * 1000 : '';
            obj.LeadSource = isMobile? "PBMobile":"PB";
            obj.LeadSourceID = 15;
            deferred.resolve(obj);
        });
        return deferred.promise;
    };

    function populateForAll(index, details, monthlyInvestment,dob){
        var obj = {};
		var deferred = $q.defer();
        if ($routeParams === null || $routeParams === undefined) {
            $routeParams = {};
        }
        
        CommonService.GetCustIDFromCustProfileCookie().then(function(data){
		
            obj.UtmSource = $routeParams.utm_source || $cookies.get('pborganic')||"";
            obj.UtmTerm = $routeParams.utm_term || "";
            obj.VisitorToken = $cookies.get('Cookie_VisitToken') || "";
            obj.LandingPageName = "investmentprequote";
            obj.LeadSourceID = "15";
            obj.LeadSource = isMobile?"PBMobile":"PB";
            obj.UtmCampaign = $routeParams.utm_campaign || "";
            obj.UtmMedium = $routeParams.utm_medium || "";
            obj.ReferalUrl = document.referrer || "";
            obj.VisitID = CommonService.GetVisitIDFromVisitProfileCookie();
            // obj.VisitID = $cookies.get('visitId');
            obj.AgentId = $location.$$search.aid || "";
            obj.InvestmentTypeId = index;
            obj.Age = details.yourAge;
            obj.PayTerm = index === 1 ? details.age : index === 3 ? details.childTerm : index === 2 ? details.age - details.yourAge : '';
            if (index === 3 && (obj.PayTerm < 25 && obj.PayTerm > 20)||(obj.PayTerm < 30 && obj.PayTerm > 25) ) {
                obj.PayTerm = Math.round(obj.PayTerm / 5) * 5;
            }
            obj.RetirementAge = index === 2 ? details.age : '';
            obj.ChildAge = index === 3 ? details.age : '';
            obj.DOB = (dob.dobDate!==null && dob.dobDate.toString().length===1?'0':'') + dob.dobDate + "-" + (dob.dobMonth!==null && dob.dobMonth.toString().length===1?'0':'')+dob.dobMonth + "-" + dob.dobYear;

            obj.PremiumPaidRet = index === 2 ? (Math.round((parseInt(monthlyInvestment) * 12) / 1000) * 1000) : '';
            obj.PremiumPaidChild = index === 3 ? (Math.round((parseInt(monthlyInvestment) * 12) / 1000) * 1000) : '';
            obj.CustId =0;
            var custIDJson = {};
            if(data && data!=="") {
                custIDJson = JSON.parse(data);
            }
            obj.CustId = custIDJson.CustID || 0;
            obj.EnquiryNeed = {};
            obj.EnquiryNeed.NeedRefID = 0;
            obj.EnquiryNeed.InvestTypeId = index;
            obj.EnquiryNeed.Age = details.yourAge;
            obj.EnquiryNeed.RetirmentAge = index === 2 ? details.age : '';
            obj.EnquiryNeed.ChildAge = index === 3 ? details.age : '';
            obj.EnquiryNeed.PayTerm = index === 1 ? details.age : index === 3 ? details.childTerm : index === 2 ? details.age - details.yourAge : '';
            deferred.resolve(obj);
        });
        //obj.CustId = $cookies.get('CustId') || -1;
        

        return deferred.promise;
    }

    CommonService.getPremiumForPreQuote = function (obj, index) {
        var deferred = $q.defer();
        var newObj = {"payterm": obj.payterm, "premium": obj.premium};
        if (index === 1) {
            NewInvestmentService.getGrowthMaturity(newObj).success(function (data) {
                if (!CommonService.isEmptyObject(data)) {
                    deferred.resolve(data);
                }
            });
            return deferred.promise;
        }
    };

    CommonService.saveSearchDetails = function (index, details, monthlyInvestment,dobObj) {
        var obj = saveForAll(index, details, monthlyInvestment,dobObj);
        var amt = (index === 1 ? Math.round(details.amount / 1000) * 1000 : (Math.round((monthlyInvestment * 12) / 1000) * 1000));
        obj.investment = amt > 5000000 ? 5000000 : amt;
        obj.requiredAmt = (index !== 1 ? Math.round(details.amount / 1000) * 1000 : 0);
        return obj;
    };

    CommonService.saveSearchDetailsForMob = function(index, details, monthlyInvestment,dobObj) {
        var obj = saveForAll(index, details, monthlyInvestment,dobObj);
        var number = 0;
        if(details.investment!==undefined && details.investment !==null ){
            number = isNaN(details.investment)?details.investment.replace(/,/g, ''):details.investment;
        }
        var amt = (index === 1 ? Math.round(number/ 1000) * 1000 : (Math.round((monthlyInvestment * 12) / 1000) * 1000));
        obj.investment = amt > 5000000 ? 5000000 : amt;
        obj.requiredAmt = (index !== 1 ? Math.round(number / 1000) * 1000 : 0);
        return obj;
    };

    function saveForAll(index, details, monthlyInvestment,dobObj){
        var obj = {};

        var term = index === 1 ? details.age : index === 2 ? details.age - details.yourAge : details.childTerm;
        if (index === 2 && (term < 25 && term > 20)||(term < 30 && term > 25) ) {
            term = Math.round(term / 5) * 5;
        }
        obj.payTerm = term <= 30 ? term : 30;
        obj.childAge = index === 3 ? details.age : 0;
        obj.retirementAge = index === 2 ? details.age : 65;
        obj.investmentFor = index;
        obj.yourAge = details.yourAge;
        obj.requiredAmt = (index !== 1 ? Math.round(details.amount / 1000) * 1000 : 0);
        obj.dobDate = dobObj.dobDate;
        obj.dobMonth = dobObj.dobMonth;
        obj.dobYear = dobObj.dobYear;
        obj.DOB = (dobObj.dobDate!==null && dobObj.dobDate!==undefined && dobObj.dobDate.length===1?'0':'') + dobObj.dobDate + "-" + (dobObj.dobMonth!==null && dobObj.dobMonth!==undefined && dobObj.dobMonth.length===1?'0':'')+dobObj.dobMonth + "-" + dobObj.dobYear;
        obj.annualIncome = details.annual;
        return obj;
    }

    function checkReferer(){
        for(var refer in referer) {
            if (document.referrer.indexOf(refer.name)){
                return ;
            }
        }
    }

    CommonService.createVisit = function (custprofileCookie) {
        var obj = {}, visitId;
        if ($routeParams === null || $routeParams === undefined) {
            $routeParams = {};
        }
        var isMobile = CommonService.IsMobileDevice();
        obj.UtmSource = $routeParams.utm_source ||checkReferer() ||"";
        obj.UtmTerm = $routeParams.utm_term || "";
        obj.VisitorToken = $cookies.get('Cookie_VisitToken') || "";
        obj.LandingPageName = "investmentprequote";
        obj.LeadSourceID = "15";
        obj.LeadSource = isMobile? "PBMobile":"PB";
        obj.UtmCampaign = $routeParams.utm_campaign || "";
        obj.UtmMedium = $routeParams.utm_medium || "";
        obj.ReferalUrl = document.referrer || "";
        obj.VisitID = 0;
        var visitToken = "";
        if(!$cookies.get('VisitProfileCookie'))
        {
            NewInvestmentService.createVisit(obj).success(function (data) {
                if (!data.Data.ResponseDetails.HasError) {
                    var visitCookieobject ={};
                      visitCookieobject.UtmCampaign = data.Data.UtmCampaign;
                      visitCookieobject.UtmMedium= data.Data.UtmMedium;
                      visitCookieobject.UtmSource= data.Data.UtmSource;
                      visitCookieobject.UtmTerm= data.Data.UtmTerm;
                      visitCookieobject.visitorToken= data.Data.VisitorToken;
                      visitCookieobject.visitId= data.Data.VisitID;
                   // visitId = data.Data.VisitID;
                      visitToken = data.Data.VisitorToken;
                   // var now1 = new Date();
                    // this will set the expiration to 24 hours
                   // var exp1 = new Date(now1.getFullYear(), now1.getMonth(), now1.getDate() + 1);
                   var date = new Date();
                  
                   var exp1 = data.Data.ResponseDetails.ReturnValue;
                   date.setTime(date.getTime() + (exp1 * 60 * 1000));
                   var jsonResult = JSON.stringify(visitCookieobject);
                   //CommonService.setCookies('VisitProfileCookie',jsonResult);
                        $cookies.put('VisitProfileCookie', jsonResult, {
                        expires: date,
                        domain: configObj.getCookieDomain()
                        });
                    var objCust = {};
                    if(custprofileCookie.d===""){

                        objCust.VisitorToken=visitToken;
                        objCust.CustID = 0;
                        objCust.SocialProfileID = 0;
                        var value ="'"+ JSON.stringify(objCust)+"'";
                        var dataToSend = '{"key":"CustProfileCookie","value":'+value+',"expires":-1}';
                        NewInvestmentService.SetHttpValues(dataToSend);
                    } else{
                        var custCookieObj = {};
                        custCookieObj.key = 'CustProfileCookie';
                        NewInvestmentService.GetHttpValues(custCookieObj).success(function(response){
                        var custProfileData = JSON.parse(response.d);
                        objCust.VisitorToken=custProfileData.VisitorToken;
                        objCust.CustID = custProfileData.CustID;
                        objCust.SocialProfileID = custProfileData.SocialProfileID || 0;       
                        var value ="'"+ JSON.stringify(objCust)+"'";
                        var dataToSend = '{"key":"CustProfileCookie","value":'+value+',"expires":-1}';
                        NewInvestmentService.SetHttpValues(dataToSend);  
						visitCookieobject.visitorToken = custProfileData.VisitorToken;
                         var jsonResult = JSON.stringify(visitCookieobject);
                   //CommonService.setCookies('VisitProfileCookie',jsonResult);
                        $cookies.put('VisitProfileCookie', jsonResult, {
                        expires: date,
                        domain: configObj.getCookieDomain()
                        });
                     });
                     
                     
                    }
                // //CommonService.setCookies('visitId', visitId);
                } else {
                }
            }).error(function (data) {

        });
    }
    else
    {
               var objVisitProfileCookie = JSON.parse($cookies.get('VisitProfileCookie'));
                var objCust = {};
                if(custprofileCookie.d==="")
                {
                    objCust.VisitorToken=objVisitProfileCookie.visitorToken;
                    //objCust.VisitorToken=data.Data.VisitorToken;
                    objCust.CustID = 0;
                    objCust.SocialProfileID = 0;
                    var value ="'"+ JSON.stringify(objCust)+"'";
                    var dataToSend = '{"key":"CustProfileCookie","value":'+value+',"expires":-1}';
                    NewInvestmentService.SetHttpValues(dataToSend);
                }
                else
                {
                 var custCookieObj = {};
                        custCookieObj.key = 'CustProfileCookie';
                        NewInvestmentService.GetHttpValues(custCookieObj).success(function(response){
                        var custProfileData = JSON.parse(response.d);
                        objCust.VisitorToken=objVisitProfileCookie.visitorToken;  
                       // objCust.VisitorToken=data.Data.VisitorToken;  
                        objCust.CustID = custProfileData.CustID;
                        objCust.SocialProfileID = custProfileData.SocialProfileID || 0;       
                        var value ="'"+ JSON.stringify(objCust)+"'";
                        var dataToSend = '{"key":"CustProfileCookie","value":'+value+',"expires":-1}';
                        NewInvestmentService.SetHttpValues(dataToSend);  
                     });
                }
    }

    };

    function checkIfDOBChangedAndCookieExists(dob,index,details) {
        var redirect = false;
        if($localStorage.searchDetails!==undefined) {
            if ($localStorage.searchDetails.investmentFor*1 === index*1) {
                redirect = false;
                if(index*1===1){
                    redirect = details.age*1 === $localStorage.searchDetails.payTerm*1;
                }else if(index*1 === 2){
                    redirect = details.age*1 === $localStorage.searchDetails.retirementAge*1;
                }else if(index*1 === 3){
                    redirect = details.age*1 === $localStorage.searchDetails.childAge*1;
                }
            }

            redirect  = redirect && (dob === $localStorage.searchDetails.DOB && (angular.isDefined($cookies.get("shouldPageSkip")) && $cookies.get("shouldPageSkip")));

        }

        return redirect;

    }

    CommonService.saveEnquiry = function (index, details, monthlyInvestment,dateObj) {
        var redirectionToQuotes = false;
        var deferred = $q.defer();
        CommonService.populateEnquiryDetails(index, details, monthlyInvestment,dateObj).then(function(obj){
        NewInvestmentService.AddEnquiryAndNeed(obj).success(function (data) {
            if (!data.Data.ResponseDetails.HasError) {
                $localStorage.equiryid = data.Data.EnquiryId;
                CommonService.saveEnquiryId(data.Data.EnquiryId);
                $location.$$search.enquiryId = CommonService.encode(data.Data.EnquiryId, false);
                //triggerEnquiryGenerationEvent(data.Data.EnquiryId,$cookies.get('CustId'));obj
                triggerEnquiryGenerationEvent(data.Data.EnquiryId,obj.CustId,index);
                var dateOfBirth = (dateObj.dobDate!==null && dateObj.dobDate.length===1?'0':'') + dateObj.dobDate + "-" + (dateObj.dobMonth!==null && dateObj.dobMonth.length===1?'0':'')+dateObj.dobMonth + "-" + dateObj.dobYear;
                redirectionToQuotes = checkIfDOBChangedAndCookieExists(dateOfBirth,index,details);
                $localStorage.searchDetails = CommonService.saveSearchDetails(index, details, monthlyInvestment,dateObj);
                $localStorage.monthlyStatus = null;

                $rootScope.slideFlag = 'right';
                var payTerm = ($localStorage.searchDetails!==undefined && $localStorage.searchDetails.payTerm>0)?$localStorage.searchDetails.payTerm:10;
                if(payTerm<8){
                    $localStorage.toolId=2;
                }else {
                    $localStorage.toolId=4;
                }
                var queryString = "?";
                if($location.$$search.utm_source!=="" && $location.$$search.utm_source!==null &&$location.$$search.utm_source!==undefined){
                    queryString = queryString+"utm_source="+$location.$$search.utm_source+'&';
                }

                if($location.$$search.utm_medium!=="" && $location.$$search.utm_medium!==null &&$location.$$search.utm_medium!==undefined){
                    queryString = queryString+"utm_medium="+$location.$$search.utm_medium+'&';
                }
                if($location.$$search.utm_term!=="" && $location.$$search.utm_term!==null &&$location.$$search.utm_term!==undefined){
                    queryString = queryString+"utm_term="+$location.$$search.utm_term+'&';
                }

                if($location.$$search.utm_campaign!=="" && $location.$$search.utm_campaign!==null &&$location.$$search.utm_campaign!==undefined){
                    queryString = queryString+"utm_campaign="+$location.$$search.utm_campaign+'&';
                }

                if($location.$$search.enquiryId!=="" && $location.$$search.enquiryId!==null &&$location.$$search.enquiryId!==undefined){
                    queryString = queryString+"enquiryId="+$location.$$search.enquiryId+'&';
                }

                if($rootScope.isIFrame) {
                    window.open(configObj.getCustomerPageUrl(queryString), "_parent");
                    $rootScope.isIFrame = false;
                }else {
                    NewInvestmentService.getCustomerDetails(obj.CustId).success(function(data){
                        $localStorage.customerDetails = CommonService.setCustomerValuesToLocalStorage(data);

                        if(redirectionToQuotes){
                            $location.$$search.leadId =
                            $location.path('/quote', true);
                        }else {
                            $location.path('/customer', true);
                        }
                    });
                     
                    // populate customer details based on cust id cookie
                    
                }
            } else {
                CommonService.showQuoteToast();
            }
            deferred.resolve(true);
        }).error(function () {
            CommonService.showQuoteToast();
            $rootScope.loaderProceed = false;
            deferred.reject(false);
        });

		});
        return deferred.promise;
		};
   
	


    CommonService.saveEnquiryAndGenerateLead = function(index, details, monthlyInvestment,dateObj,cityObj){
        var deferred = $q.defer();
      //  var obj = CommonService.populateEnquiryDetailsForMob(index, details, monthlyInvestment,dateObj);
        CommonService.populateEnquiryDetailsForMob(index, details, monthlyInvestment,dateObj).then(function(obj){
        NewInvestmentService.AddEnquiryAndNeed(obj).success(function (data) {
            if (!data.Data.ResponseDetails.HasError) {
                $localStorage.equiryid = data.Data.EnquiryId;
                CommonService.saveEnquiryId(data.Data.EnquiryId);
                $location.$$search.enquiryId = CommonService.encode(data.Data.EnquiryId, true);
                //triggerEnquiryGenerationEvent(data.Data.EnquiryId,$cookies.get('CustId'));
                //triggerEnquiryGenerationEvent(data.Data.EnquiryId,obj.CustId,index);
                $localStorage.searchDetails = CommonService.saveSearchDetailsForMob(index, details, monthlyInvestment,dateObj);
                $rootScope.slideFlag = 'right';
                var payTerm = ($localStorage.searchDetails!==undefined && $localStorage.searchDetails.payTerm>0)?$localStorage.searchDetails.payTerm:10;
                if(payTerm<8){
                    $localStorage.toolId=2;
                }else {
                    $localStorage.toolId=4;
                }
                var queryString = "?";
                if($location.$$search.utm_source!=="" && $location.$$search.utm_source!==null &&$location.$$search.utm_source!==undefined){
                    queryString = queryString+"utm_source="+$location.$$search.utm_source+'&';
                }

                if($location.$$search.utm_medium!=="" && $location.$$search.utm_medium!==null &&$location.$$search.utm_medium!==undefined){
                    queryString = queryString+"utm_medium="+$location.$$search.utm_medium+'&';
                }
                if($location.$$search.utm_term!=="" && $location.$$search.utm_term!==null &&$location.$$search.utm_term!==undefined){
                    queryString = queryString+"utm_term="+$location.$$search.utm_term+'&';
                }

                if($location.$$search.utm_campaign!=="" && $location.$$search.utm_campaign!==null &&$location.$$search.utm_campaign!==undefined){
                    queryString = queryString+"utm_campaign="+$location.$$search.utm_campaign+'&';
                }

                if($location.$$search.enquiryId!=="" && $location.$$search.enquiryId!==null &&$location.$$search.enquiryId!==undefined){
                    queryString = queryString+"enquiryId="+$location.$$search.enquiryId+'&';
                }
                CommonService.generateLeadOnFirst(index, details, monthlyInvestment,dateObj,cityObj,data.Data.EnquiryId,queryString);

            } else {
                CommonService.showQuoteToast();
            }
            deferred.resolve(true);
        }).error(function () {
            CommonService.showQuoteToast();
            $rootScope.loaderProceed = false;
            deferred.reject(false);
        });

    });
        return deferred.promise;
    };

    CommonService.generateLeadOnFirst = function(index, details, monthlyInvestment,dateObj,cityObj,enqId,queryString){
        var obj = {};
        obj.CustomerName = details.CustomerName;
        obj.MobileNo = details.MobileNo;
        obj.Email = details.Email;
        obj.CountryID = details.CountryID;
        obj.GenderID = details.gender||1;
        obj.CityID = cityObj.CityID;
        obj.City = cityObj.City;
        obj.stateId = cityObj.stateId;
        obj.countryCode = cityObj.countryCode;
        $localStorage.customerDetails = obj;
        CommonService.saveCustomerDetails(obj);

        var searchObj = $localStorage.searchDetails||{};
        searchObj.gender = details.gender;
        CommonService.setSearchDetails(searchObj);
        $localStorage.searchDetails = searchObj;

		$localStorage.monthlyStatus = null;
		CommonService.GetCustIDFromCustProfileCookie().then(function(data){
                        var SocialProfileID =0;
                            var custIDJson = {};
                            if(data!=="" && data) {
                                custIDJson = JSON.parse(data);
                            }
                        SocialProfileID = custIDJson.SocialProfileID || 0;
        NewInvestmentService.RegisterCustomer(populateCustomerData(details,cityObj,enqId)).success(function (data) {
            if (!data.ResponseDetails.HasError) {
				var custObj  ={};
                                var objCust ={};
                                custObj.key ="CustProfileCookie";
                                NewInvestmentService.GetHttpValues(custObj).success(function(response){
                                    var customerProfileData = {};
                                    if(response.d && response.d!=="") {
                                        customerProfileData = JSON.parse(response.d);
                                    }
                                objCust.CustID=data.CustId;
                                objCust.VisitorToken=customerProfileData.VisitorToken;
                                objCust.SocialProfileID=customerProfileData.SocialProfileID || "";           
                                var value ="'"+ JSON.stringify(objCust)+"'";
                                var dataToSend = '{"key":"CustProfileCookie","value":'+value+',"expires":-1}';
                                NewInvestmentService.SetHttpValues(dataToSend);  
                                });
                CommonService.goToQuotesPage(enqId,queryString);
            } else {
                CommonService.showQuoteToast();
            }
        }).error(function () {
            CommonService.showQuoteToast();
        });
});
    };

    function populateCustomerData(details,cityObj,enquiryId){
        var obj = {};
        obj.IsAssistanceReq = !details.IsAssistanceReq;
        obj.CountryID = details.CountryID;
        obj.CustomerName = details.CustomerName;
        obj.Email = details.Email;
        obj.MobileNo = details.MobileNo;
        obj.GenderID = details.gender;
        obj.IsNRI = details.CountryID*1!=392;
        obj.Title = details.gender===1?'Mr':'Ms/Mrs';
        obj.CityId = cityObj.CityID;
        obj.StateId = cityObj.stateId;
        obj.countryCode = cityObj.countryCode;
        obj.EnquiryId = enquiryId;
        return obj;
    }
CommonService.setCustomerValuesToStorage = function(customerEntity,selectedCityItem,countryCode){
        var obj = {};
        obj.CustomerName = customerEntity.CustomerName;
        obj.MobileNo = customerEntity.MobileNo;
        obj.Email = customerEntity.Email;
        obj.CountryID = customerEntity.CountryID;
        obj.GenderID = customerEntity.gender || customerEntity.GenderID;
        if(customerEntity.CountryID*1!==392){
            obj.CityID = 9999;
            obj.City = 999;
            obj.stateId = 999;
        }else {
            obj.CityID = selectedCityItem !== undefined ? selectedCityItem.cityId : customerEntity.CityID;
            obj.City = selectedCityItem !== undefined ? selectedCityItem.value : customerEntity.CityId;
            obj.stateId = selectedCityItem !== undefined ? selectedCityItem.stateId : customerEntity.StateID;
        }
        obj.countryCode = countryCode;

        return obj;
    };

    CommonService.setCustomerValuesToLocalStorage = function(customerEntity){
        var obj = {};
        obj.CustomerName = customerEntity.CustomerName;
        obj.MobileNo = customerEntity.MobileNo;
        obj.Email = customerEntity.Email;
        obj.CountryID = $localStorage.customerDetails!==undefined?$localStorage.customerDetails.CountryID:"392";
        obj.GenderID = customerEntity.gender || customerEntity.GenderID;
        if(customerEntity.CountryID*1!==392){
            obj.CityID = 9999;
            obj.City = 999;
            obj.stateId = 999;
        }else {
            obj.CityID = customerEntity.CityID;
            obj.City = customerEntity.CityId;
            obj.stateId = customerEntity.StateID;
        }

        return obj;
    };

    CommonService.getTopCitiesList = function() {
        var topCities = [
            {cityId: 551, name: "Delhi", stateId: 35, state: "Delhi",value:"Delhi(Delhi)"},
            {cityId: 302, name: "Mumbai", stateId: 20, state: "Maharashtra",value:"Mumbai(Maharashtra)"},
            {cityId: 207, name: "Bengaluru", stateId: 16, state: "Karnataka",value:"Bengaluru(Karnataka)"},

            {cityId: 309, name: "Pune", stateId: 20, state: "Maharashtra",value:"Pune(Maharashtra)"},
            {cityId: 837, name: "Hyderabad", stateId: 37, state: "Telangana",value:"Hyderabad(Telangana)"},
            {cityId: 541, name: "Kolkata", stateId: 34, state: "West Bengal",value:"Kolkata(West Bengal)"},
            {cityId: 420, name: "Chennai", stateId: 30, state: "Tamil Nadu",value:"Chennai(Tamil Nadu)"},
            {cityId: 555, name: "Gurgaon", stateId: 36, state: "NCR",value:"Gurgaon(NCR)"},
            {cityId: 103, name: "Ahmedabad", stateId: 11, state: "Gujarat",value:"Ahmedabad(Gujarat)"},
            {cityId: 553, name: "Noida", stateId: 36, state: "NCR",value:"Noida(NCR)"},
            {cityId: 316, name: "Thane", stateId: 20, state: "Maharashtra",value:"Thane(Maharashtra)"},
            {cityId: 411, name: "Jaipur", stateId: 28, state: "Rajasthan",value:"Jaipur(Rajasthan)"},
            {cityId: 666, name: "Navi Mumbai", stateId: 20, state: "Maharashtra",value:"Navi Numbai(Maharashtra)"}];
        return topCities;

        //return topCities.sort(this.dynamicSort("name"));
    };

    CommonService.getSearchDetails = function () {
        return details;
    };

    CommonService.setSearchDetails = function (searhDetails) {
        details = searhDetails;
    };

    CommonService.getEnquiryId = function () {
        return enquiry;
    };

    CommonService.saveEnquiryId = function (enquiryid) {
        enquiry = enquiryid;
    };

    CommonService.getSelectedTool = function () {
        return toolId;
    };

    CommonService.saveSelectedTool = function (tool) {
        toolId = tool;
    };

    CommonService.saveCityDetails = function (city) {
        cityItem = city;
    };

    CommonService.getCityDetails = function () {
        return cityItem;
    };

    CommonService.saveCustomerDetails = function (customer) {
        customerDetails = customer;
    };

    CommonService.getCustomerDetails = function () {
        return customerDetails;
    };

    CommonService.prefillCityChangeObject = function(cityChangeObject){
        cityChangeObject.isNRI = false;
        cityChangeObject.minValue = 2;
        cityChangeObject.isEditable = false;
        cityChangeObject.citySearchText = "Others";
        return cityChangeObject;
    };

    CommonService.prefillCustomerEntityObject = function(customerEntity,obj){
        customerEntity.CustomerName = obj.CustomerName;
        customerEntity.Email = obj.Email;
        customerEntity.MobileNo = obj.MobileNo||'';
        customerEntity.gender = obj.GenderID;
        customerEntity.IsAssistanceReq = false;
    };

    CommonService.getInvestmentAmountFromCommas = function (x,val) {
        if (val !== undefined) {
            if (val.investment !== undefined) {
                if (isNaN(val.investment)) {
                    x = val.investment.replace(/,/g, '');
                } else {
                    x = val.investment;
                }
            }
        }
        return x*1;
    };

    CommonService.getBlurObjectChange = function (value,blurObj){
        if (value === 'customer') {
            blurObj.customerNameBlurOut = true;
        } else if (value === 'email') {
            blurObj.emailBlurOut = true;
        } else if (value === 'mobile') {
            blurObj.mobileBlurOut = true;
        } else if (value === 'country') {
            blurObj.countryBlurOut = true;
        } else if (value === 'city') {
            blurObj.cityBlurOut = true;
        } else if (value === 'investment') {
            blurObj.investmentBlurOut = true;
        }

        return blurObj;
    };

    CommonService.countUpValues = function(value,monthlyInvestment,previousValue) {
        if (value> 0) {
            monthlyInvestment = value;
            var options = {
                useEasing: true,
                useGrouping: true,
                separator: ',',
                decimal: '.',
                prefix: 'Rs.',
                suffix: ''
            };
            $timeout(function () {
                var demo = new CountUp("myTargetElement", previousValue, value, 0, 1.5, options);
                demo.start(function () {
                    /*$scope.$apply(function () {

                     });*/
                });
                previousValue = value;
            }, 10);

        }
        return monthlyInvestment;
    };

    CommonService.changeCityChangeObj = function(countryCodes,cityChangesObj, countryId, obj) {
        _.each(countryCodes, function (val) {
            if (val.CountryCodeId * 1 === countryId * 1) {
                if (val.CountryCode !== '') {
                    cityChangesObj.isEditable = false;
                    cityChangesObj.countryCode = val.CountryCode;
                } else {
                    cityChangesObj.isEditable = true;
                    cityChangesObj.countryCode = obj.countryCode * 1 || "";
                }
            }
        });

        return cityChangesObj;
    };

    CommonService.CheckForExistingCountryCodes = function(cityChangesObj,countryCodes){
        var hasToChange = false;
        var  obj = {};
        if(cityChangesObj.isEditable){
            obj = _.find(countryCodes, function(val){
                return (val.CountryCode * 1 === cityChangesObj.countryCode * 1);
            });
            hasToChange = !CommonService.isEmptyObject(obj);
        }
        return hasToChange;
    };

    CommonService.getPayoutBeforeTaxMin = function (annualReturnsMin, quarterly, amount) {
        var interestToBeMultiplied = 1 + (annualReturnsMin / (100 * quarterly));
        var multiplicationFactor = Math.pow(interestToBeMultiplied, (5 * quarterly));
        return Math.round(amount * multiplicationFactor);
    };

    CommonService.getPayoutBeforeTaxMax = function (annualReturnsMax, quarterly, amount) {
        var interestToBeMultiplied = 1 + (annualReturnsMax / (100 * quarterly));
        var multiplicationFactor = Math.pow(interestToBeMultiplied, (5 * quarterly));
        return Math.round(amount * multiplicationFactor);
    };

    CommonService.getAfterTaxReturnRateMin = function (value, amount) {
        var amt = value / amount;
        var x = Math.pow(amt, 1 / 5);
        return ((x - 1) * 100).toFixed(2) || 0;
    };

    CommonService.getAfterTaxReturnRateMax = function (value, amount) {
        var amt = value / amount;
        var x = Math.pow(amt, 1 / 5);
        return ((x - 1) * 100).toFixed(2) || 0;
    };

    // Check if an object is empty
    CommonService.isEmptyObject = function (obj) {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop))
                return false;
        }
        return true;
    };

    CommonService.AddCommas = function (value) {
    if (value !== undefined) {
        value = value.toString().replace(/,/g, '');
        value += '';
        var x = value.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var lastThree = x1.substring(x1.length - 3);

        var otherNumbers = x1.substring(0, x1.length - 3);
        if (otherNumbers !== '')
            lastThree = ',' + lastThree;
        var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
        return res + x2;
    } else {
        return "";
    }
    };

    CommonService.AmountToString = function (value) {
        if (value !== undefined && value !== null) {
            value = value.toString().replace(/,/g, '');
            var num = Math.floor(value).toString(),
                numLength = num.length,
                displayNum = '',
                suffix = '';
            if (numLength < 4) {
                displayNum = num;
            } else if (numLength == 4) {
                displayNum = num / 1000;
                suffix = (displayNum > 1) ? "K" : "K";
            } else if (numLength == 5) {
                displayNum = num / 1000;
                suffix = (displayNum > 1) ? "K" : "K";
            } else if (numLength == 6) {
                displayNum = num / 100000;
                suffix = (displayNum > 1) ? "Lac" : "Lac";
            } else if (numLength == 7) {
                displayNum = num / 100000;
                suffix = (displayNum > 1) ? "Lac" : "Lac";
            } else if (numLength == 8) {
                displayNum = num / 10000000;
                suffix = (displayNum > 1) ? "Cr" : "Cr";
            } else if (numLength >= 9) {
                displayNum = num / 10000000;
                suffix = (displayNum > 1) ? "Cr" : "Cr";
            }
            if (parseInt(displayNum) > 9) {
                displayNum = Math.floor(10 * displayNum) / 10;
            } else {
                displayNum = Math.floor(100 * displayNum) / 100;
            }
            return displayNum + " " + suffix;
        } else {
            return 0;
        }
    };

    CommonService.getCity = function (data) {
        var allData = data;
        var callback = function (allData) {
            var displayData = allData.CityName;
            var disValue = allData.CityName;
            var searchString = allData.CityName;

            return {
                display: displayData,
                value: disValue,
                searchString: searchString,
                stateId: allData.StateId,
                cityId: allData.CityId
            };
        };
        return allData.map(callback);
    };

    CommonService.findCityFromText = function (city, citySearchText) {
        var obj = {};
        if(citySearchText!==null && citySearchText!==undefined && citySearchText!=='') {
            citySearchText = citySearchText.replace(/ /g, '');
            if (citySearchText.length > 0) {
                _.each(city, function (val, key) {
                    var txt = val.value;
                    var index = val.value.indexOf('(');
                    txt = txt.substr(0, index).replace(/ /g, '');
                    if (citySearchText.toLowerCase() === txt.toLowerCase()) {
                        obj = val;

                    }
                });
            }
        }
        return obj;
    };

    CommonService.findCityFromId = function (city, cityId) {
        var obj = {};
        _.each(city, function (value, key) {
            var id = value.cityId;
            if (id === cityId) {
                obj = value;

            }
        });
        return obj;
    };

    CommonService.addHistoricalData = function (data, term) {
        _.each(data, function (value) {
            value.historicalReturns = CommonService.calculateHistoricalReturn(value.AnnualPremium, value.DebtFactorValue, term);
            value.historicalReturnsEquity = CommonService.calculateHistoricalReturn(value.AnnualPremium, value.EquityFactorValue, term);
        });
    };

    CommonService.calculateHistoricalReturn = function (a, r, n) {
        if (r !== null) {
            var rate = (100 + r) / 100;
            var x = (1 - Math.pow(rate, (n)));
            var y = 1 - rate;
            var tr = Math.round((a * x) / y);
            return Math.round(tr + tr * r / 100);
        } else {
            return 0;
        }
    };

    CommonService.setCookies = function (key, value) {
        var now = new Date(),
        // this will set the expiration to 12 months
            exp = new Date(now.getFullYear() + 1, now.getMonth(), now.getDate());

        $cookies.put(key, value, {
            expires: exp
        });
    };

    CommonService.showQuoteToast = function () {
        $mdToast.show(
            $mdToast.simple()
                .content('Something went wrong, Please try in sometime.')
                .position(getToastPosition())
                .hideDelay(3 * 1000)
        );
    };

    var toastPosition = {
        bottom: false,
        top: true,
        left: false,
        right: true
    };

    var getToastPosition = function () {
        return Object.keys(toastPosition)
            .filter(function (pos) {
                return toastPosition[pos];
            })
            .join(' ');
    };

    CommonService.encode = function (value, hasToUrlEncode) {
        if (value !== undefined) {
            value = value.toString();
            var encodedValue = Base64.encode(value);
            if (hasToUrlEncode) {
                encodedValue = encodeURIComponent(encodedValue);
            }
            return encodedValue;
        }
    };

    CommonService.decode = function (value, urlDecode) {
        if (value !== undefined) {
            value = value.toString();
            if (urlDecode) {
                value = decodeURIComponent(value);
            }

            return Base64.decode(value);
        }
    };

    CommonService.prefillValuesFromService = function (details, data) {
        if ((details === null || CommonService.isEmptyObject(details))) {
            if (!Number(details.investment)) {
                if (details.investment === undefined || (!Number(details.investment.replace(/,/g, '')) )) {
                    details.investment = data.InvestmentTypeId === 1 ? data.PremiumPaidGrowth : data.InvestmentTypeId === 2 ? data.PremiumPaidRet : data.PremiumPaidChild;
                } else {
                    details.investment = data.InvestmentTypeId === 1 ? data.PremiumPaidGrowth : data.InvestmentTypeId === 2 ? data.PremiumPaidRet : data.PremiumPaidChild;
                }
            }
        } else {

            if($localStorage.monthlyStatus!==null){
                if($localStorage.monthlyStatus*1===2){
                    details.investment = details.investment.toString().replace(/,/g, '') *12;
                }
            }

        }

        if (details.investment < 10000) {
            details.investment = 10000;
        } else if (details.investment > 200000) {
            details.investment = 200000;
        }


        if($localStorage.monthlyStatus!==null){
            if($localStorage.monthlyStatus*1===2){
                details.investment = details.investment.toString().replace(/,/g, '') *1;
            }
        }

        details.payTerm = details.payTerm || data.PayTerm || 20;
        if (details.payTerm > 30) {
            details.payTerm = 30;
        }
        details.gender = details.gender || 1;
        details.investmentFor = data.InvestmentTypeId;
        details.payOutStart = details.payOutStart || angular.copy(details.payTerm);
        details.yourAge = data.Age;
        details.age = data.ChildAge;
        return details;
    };

    CommonService.initializeValues = function (details) {
        var scopeDetails = angular.copy(details);

        scopeDetails.investmentType = 1;

        //scopeDetails.investmentType = 2;
        var numInvestment = details.investment.toString().replace(/,/g, '') *1;
        /*scopeDetails.investment = Math.round(numInvestment / 12);*/

        scopeDetails.payOutStart = details.payOutStart;
        scopeDetails.investment = CommonService.AddCommas(scopeDetails.investment);
        scopeDetails.spaninvestment = scopeDetails.investment;
        scopeDetails.spaninvestment = CommonService.AmountToString(scopeDetails.spaninvestment);
        scopeDetails.amountFocusLost = true;
        return scopeDetails;
    };

    CommonService.createChildTermList = function (value) {
        var l = [];
        value = parseInt(value);
        l.push(value + 10);
        l.push(value + 15);
        l.push(value + 20);

        return l;
    };

    CommonService.populateObjForQuotes = function (scopeDetails, details, id) {
        var postObj = {};
        var investment = parseInt(scopeDetails.investment.toString().replace(/,/g, ''));
        postObj.Age = details.yourAge;
        postObj.ChildAge = details.investmentFor === 3 ? details.age : '';
        postObj.GenderID = details.gender;
        postObj.PlanCategoryID = details.investmentFor;
        var amt = scopeDetails.investmentType === 2 ? (investment * 12) : investment;
        if (amt < 100000) {
            amt = (Math.round(amt / 1000)) * 1000;
        } else if (amt > 100000) {
            amt = (Math.round(amt / 10000)) * 10000;
        }
        postObj.AnnualPremium = amt;
        postObj.PayTerm = scopeDetails.payTerm;
        postObj.PolicyTerm = scopeDetails.payOutStart;
        return postObj;
    };


    CommonService.addIRRValues = function (dummyQuotes) {
        var list = [];
        list = angular.copy(dummyQuotes);
        _.each(list, function (value, key) {
            var IRR = parseInt(value.IRR * 100);
            var IRREquity = parseInt(value.IRREquity * 100);
            value.totalCost = (800 - IRR) / 100;
            value.totalCostEquity = (800 - IRREquity) / 100;
            value.isDisplay = false;
        });
        return list;
    };

    CommonService.applyFilters = function (item, object) {
        var isDisplay = true;
        object.toolId = parseInt(object.toolId);
        if (object.toolId !== 0) {
            isDisplay = isDisplay && item.isUlip;
        } else if (object.toolId === 0) {
            isDisplay = isDisplay && !item.isUlip;
        }

        if (object.payoutType !== undefined || object.payoutType !== null) {
            object.payoutType = parseInt(object.payoutType);
            if (object.toolId === 0 && object.payoutType !== 3) {
                isDisplay = isDisplay && object.payoutType === item.QuotePlan.MoneyReceivingWayID;
            }
        }

        if (parseInt(object.toolId) === 2) {
            isDisplay = isDisplay && (item.TotalReturn !== null && item.TotalReturn > 0);
        } else if (parseInt(object.toolId) === 4) {
            isDisplay = isDisplay && (item.TotalReturnEquity !== null && item.TotalReturnEquity > 0);
        }
        return isDisplay;
    };

    // If filter is selected the push the filter name and weightage attached to it to an array
    CommonService.getFiltersSelectedArray = function (planFilters) {
        var filterArray = [];
        angular.forEach(planFilters, function (value, key) {
            if (planFilters[key].checked) {
                filterArray.push([planFilters[key].name, planFilters[key].weight]);
            }
        });
        return filterArray;
    };

    // If addons exists add the key to an array
    function seeIfAddOnsExist(item) {
        var addOnArray = [];
        angular.forEach(item.AddOnFeatures, function (addOnValue, addOnKey) {
            if (addOnValue) {
                addOnArray.push(addOnKey);
            }
        });

        return addOnArray;
    }

    CommonService.goToQuotesPage = function (enqId,queryString) {
	    var matrixLeadID=0;
        if (Number(enqId) && parseInt(enqId) > 0) {
            NewInvestmentService.UpdateEnquiry({"enquiryEntityID": enqId}).success(function (data) {
                if (data.MatrixLeadId === null || parseInt(data.MatrixLeadId) === 0) {
                    CommonService.showQuoteToast();
                } else {
                    $location.$$search.enquiryId = CommonService.encode(enqId, true);
                    $location.$$search.leadId = CommonService.encode(data.MatrixLeadId, false);

					matrixLeadID = data.MatrixLeadId;
                    try {
						var planType,offerId;
						 planType = data.InvestmentTypeId;
						if($location.$$search.offerid){
							offerId = $location.$$search.offerid;
						}
                        executed: dataLayer.push({'LeadID':data.MatrixLeadId});
                        executed: dataLayer.push({'event':'investmentLead'});
						if((data.Age >= 25)&& planType ==2 && offerId ==2 ){
							executed: dataLayer.push({'event':'InvestmentQuote'});
						}
                    }
                    catch(e) {
                    }
                    CommonService.GetCustIDFromCustProfileCookie().then(function(data){
                        var CustId =0;
                         var custIDJson = {};
						if(data!==""){
							 custIDJson = JSON.parse(data);
						}
                        CustId = custIDJson.CustID;
                        var leadObj = {};
                        if($localStorage.searchDetails!==undefined && $localStorage.searchDetails!==null){
                            leadObj.age = $localStorage.searchDetails.yourAge;
                        }
                        if($localStorage.customerDetails!==undefined && $localStorage.customerDetails!==null){
                            leadObj.gender = $localStorage.customerDetails.GenderID==1?'Male':'Female';
                            leadObj.city = $localStorage.customerDetails.City;
                        }
                        triggerLeadGenerationEvent(matrixLeadID,enqId,CustId,planType,leadObj);
                        if($rootScope.isHeadIFrame) {
                            queryString = queryString+'leadId='+(CommonService.encode(matrixLeadID, false));
                        $timeout(function(){
                         window.open(configObj.getQuotePageUrl(queryString), "_parent");
                        },200);
                         //$rootScope.isHeadIFrame = false;
                         }else {
                            $location.path('/quote');
                         }

                });
                }
            }).error(function () {
                CommonService.showQuoteToast();
            });
        } else {
            $location.path('/');
        }

    };

    CommonService.applyFeatureWeightage = function (planFilters, quotes) {
        var selectedFilters = CommonService.getFiltersSelectedArray(planFilters);
        var totalWeightage = 0;

        angular.forEach(quotes, function (value, key) {
            value.featureWeightage = 0;
            var arrayOfAddonAllowed = seeIfAddOnsExist(value);
            angular.forEach(selectedFilters, function (filterValue) {
                if (arrayOfAddonAllowed.indexOf(filterValue[0]) != -1) {
                    value.featureWeightage += filterValue[1];
                }
            });
        });

        angular.forEach(selectedFilters, function (filterValue) {
            totalWeightage += filterValue[1];
        });

        angular.forEach(quotes, function (value, key) {
            value.percentageWeightage = Math.round((value.featureWeightage / totalWeightage) * 100);
        });
    };

    CommonService.populateFilterList = function (id) {
        var filters = [];
        var restOfPerc = 10 / 180 * 100;
        if (parseInt(id) === 1) {

            filters.push({
                name: 'LoyaltyAdditions',
                checked: true,
                'weight': (40 / 180) * 100,
                fullName: 'Loyalty Additions',
                description: ''
            });
            filters.push({
                name: 'Switches', checked: true, 'weight': (30 / 180) * 100,
                fullName: 'Free Switches',
                description: 'Using free switches, the policyholders can move their investments between various asset classes like debt,equity & money market, depending on their risk appetite and financial goals'
            });
            filters.push({
                name: 'AutomaticRebalancing',
                checked: true,
                'weight': (20 / 180) * 100,
                fullName: 'Automatic Rebalancing',
                description: 'Equity investments automatically spread out into regular amounts rather than investing at one go combat market fluctuations'
            });
            filters.push({
                name: 'CapitalProtection',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Capital Protection'
            });
            filters.push({name: 'IncomeBenefit', checked: false, 'weight': restOfPerc, fullName: 'Income Benefit'});
            filters.push({
                name: 'WaiverofPremium',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Waiver of Premium on Death'
            });
            filters.push({
                name: 'WOPOnCriticalIllness',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Waiver of Premium On Critical Illness'
            });
            filters.push({
                name: 'PremiumRedirecton',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Free Premium Redirecton'
            });
            filters.push({
                name: 'SystematicTransfer',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Systematic Transfer'
            });
            filters.push({name: 'TopUp', checked: false, 'weight': restOfPerc, fullName: 'Free Top-Up'});
            filters.push({
                name: 'LifeStageProtection',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Life Stage Protection'
            });
            filters.push({
                name: 'FreePartialWithdrawl',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Free Partial Withdrawal'
            });
        } else if (parseInt(id) === 2) {
            filters.push({
                name: 'IncomeBenefit',
                checked: true,
                'weight': (40 / 180) * 100,
                fullName: 'Income Benefit'
            });
            filters.push({
                name: 'WaiverofPremium',
                checked: true,
                'weight': (30 / 180) * 100,
                fullName: 'Waiver of Premium on Death'
            });
            filters.push({
                name: 'CapitalProtection',
                checked: true,
                'weight': (20 / 180) * 100,
                fullName: 'Capital Protection'
            });
            filters.push({
                name: 'LoyaltyAdditions',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Loyalty Additions'
            });
            filters.push({
                name: 'Switches', checked: false, 'weight': restOfPerc,
                fullName: 'Free Switches',
                description: 'Using free switches, the policyholders can move their investments between various asset classes like debt,equity & money market, depending on their risk appetite and financial goals'
            });
            filters.push({
                name: 'AutomaticRebalancing',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Automatic Rebalancing',
                description: 'Equity investments automatically spread out into regular amounts rather than investing at one go combat market fluctuations'
            });
            filters.push({
                name: 'WOPOnCriticalIllness',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Waiver of Premium On Critical Illness'
            });
            filters.push({
                name: 'PremiumRedirecton',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Free Premium Redirecton'
            });
            filters.push({
                name: 'SystematicTransfer',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Systematic Transfer'
            });
            filters.push({name: 'TopUp', checked: false, 'weight': restOfPerc, fullName: 'Free Top-Up'});
            filters.push({
                name: 'LifeStageProtection',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Life Stage Protection'
            });
            filters.push({
                name: 'FreePartialWithdrawl',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Free Partial Withdrawal'
            });
        } else if (parseInt(id) === 3) {
            filters.push({
                name: 'WaiverofPremium',
                checked: true,
                'weight': (40 / 180) * 100,
                fullName: 'Waiver of Premium on Death'
            });
            filters.push({
                name: 'WOPOnCriticalIllness',
                checked: true,
                'weight': (30 / 180) * 100,
                fullName: 'Waiver of Premium On Critical Illness'
            });
            filters.push({
                name: 'CapitalProtection',
                checked: true,
                'weight': (20 / 180) * 100,
                fullName: 'Capital Protection'
            });
            filters.push({
                name: 'LoyaltyAdditions',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Loyalty Additions'
            });
            filters.push({
                name: 'Switches', checked: false, 'weight': restOfPerc,
                fullName: 'Free Switches',
                description: 'Using free switches, the policyholders can move their investments between various asset classes like debt,equity & money market, depending on their risk appetite and financial goals'

            });
            filters.push({
                name: 'AutomaticRebalancing',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Automatic Rebalancing',
                description: 'Equity investments automatically spread out into regular amounts rather than investing at one go combat market fluctuations'

            });
            filters.push({name: 'IncomeBenefit', checked: false, 'weight': restOfPerc, fullName: 'Income Benefit'});
            filters.push({
                name: 'PremiumRedirecton',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Free Premium Redirecton'
            });
            filters.push({
                name: 'SystematicTransfer',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Systematic Transfer'
            });
            filters.push({name: 'TopUp', checked: false, 'weight': restOfPerc, fullName: 'Free Top-Up'});
            filters.push({
                name: 'LifeStageProtection',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Life Stage Protection'
            });
            filters.push({
                name: 'FreePartialWithdrawl',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Free Partial Withdrawal'
            });
        }
        return filters;
    };

    function sortByProps(list, by, thenby) {
        return list.sort(function (first, second) {
            if (first[by] == second[by]) {
                return first[thenby] - second[thenby];
            }
            return second[by] - first[by];
        });
    }

    function getFirstThreePlans(quotes, filterObj) {
        var trimmedQuotesNames = [];
        var factor = "";

        if (parseInt(filterObj.filterId) === 1) {
            if (parseInt(filterObj.prediction) === 2) {
                if (parseInt(filterObj.toolId) === 4) {
                    factor = 'TotalReturnEquity';
                } else {
                    factor = 'TotalReturn';
                }
            } else if (parseInt(filterObj.prediction) === 1) {
                if (parseInt(filterObj.toolId) === 4) {
                    factor = 'historicalReturnsEquity';
                } else {
                    factor = 'historicalReturns';
                }
            }

        } else {
            factor = "TotalCost";
        }
        var q = angular.copy(quotes);
        q = q.sortByProp(factor).reverse();

        _.each(q, function (value) {
            if (value.isDisplay) {
                if (trimmedQuotesNames.length < 3) {
                    trimmedQuotesNames.push(value.QuotePlan.PlanName);
                } else {
                    return false;
                }
            }
        });

        return trimmedQuotesNames;
    }



    CommonService.updateProductDetails = function (quotes, leadId, filterObj, investmentTypeId, quote) {
        var obj = {};

        var arr = getFirstThreePlans(quotes, filterObj);
        obj.LeadID = leadId;
        obj.InvestmentTypeId = investmentTypeId;
        obj.PlanID = quote ? quote.PlanID : '';
        obj.PlanName = quote ? quote.QuotePlan.PlanName : '';
        obj.SumAssured = quote ? quote.SumInsured : '';
        obj.Premium = quote ? quote.AnnualPremium : '';
        obj.Term = quote ? quote.PolicyTerm : '';
        obj.Plan1 = arr[0];
        obj.Plan2 = arr[1];
        obj.Plan3 = arr[2];
        obj.SupplierId = quote ? quote.QuotePlan.PlanInsurer.InsurerID : '';
        obj.SupplierName = quote ? quote.QuotePlan.PlanInsurer.InsurerName : '';
        obj.TypeOfPolicy = "";
        obj.CustomerPrefID = "";
        obj.PayTerm = quote ? quote.PayTerm : '';

        NewInvestmentService.updateProductDetails(obj).success(function () {
        });
    };

    CommonService.calculatePerc = function (max, min, value) {
        var factor = 50 / (max - min);
        var diff = max - value;

        return 100 - (factor * diff);

    };

    CommonService.getMinLastNonZeroValue = function (dummyQuoteValues, someReturnValue) {
        var min = 0;
        for (var i = dummyQuoteValues.length - 1; i > 0; i--) {
            if (dummyQuoteValues[i][someReturnValue] > 0) {
                min = dummyQuoteValues[i][someReturnValue];
                break;
            }
        }
        return min;
    };

    CommonService.GetPayTermList = function (details, comboList) {
        var list = [];
        if (parseInt(details.investmentFor) === 3) {
            if (details.age < 8) {
                _.each(comboList, function (value, key) {
                    list.push(value.term);
                });
            } else if (details.age >= 8 && details.age < 13) {
                _.each(comboList, function (value, key) {
                    if (parseInt(value.term) <= 15) {
                        list.push(value.term);
                    }
                });
            } else if (details.age >= 13 && details.age < 18) {
                _.each(comboList, function (value, key) {
                    if (parseInt(value.term) < 15) {
                        list.push(value.term);
                    }
                });
            }
        } else {
            _.each(comboList, function (value, key) {
                list.push(value.term);
            });
        }

        return list;
    };

    CommonService.getPolicyTermList = function (details, payTerm, comboList) {
        var list = [];
        if (parseInt(details.investmentFor) === 3) {
            list = populateListForChild(details, payTerm, comboList);
        } else {
            _.each(comboList, function (value, key) {
                if (parseInt(payTerm) === parseInt(value.term)) {
                    for (var i = 0; i < value.policyList.length; i++) {
                        list.push({
                            'payOutStart': value.policyList[i].polTerm,
                            'noOfPlans': value.policyList[i].noOfPlans
                        });
                    }
                }
            });
        }
        return list;
    };

    function populateListForChild(details, payTerm, comboList) {
        var tempList = [];
        if (parseInt(details.age) <= 8) {
            if (parseInt(payTerm) === 10) {
                _.each(comboList, function (value, key) {
                    if (parseInt(payTerm) === parseInt(value.term)) {
                        for (var i = 0; i < value.policyList.length; i++) {
                            tempList.push({
                                'payOutStart': value.policyList[i].polTerm,
                                'noOfPlans': value.policyList[i].noOfPlans
                            });
                        }
                    }
                });
            } else if (parseInt(payTerm) === 15) {
                _.each(comboList, function (value, key) {
                    if (parseInt(payTerm) === parseInt(value.term)) {
                        for (var i = 0; i < value.policyList.length; i++) {
                            if (parseInt(value.policyList[i].polTerm) <= 15) {
                                tempList.push({
                                    'payOutStart': value.policyList[i].polTerm,
                                    'noOfPlans': value.policyList[i].noOfPlans
                                });
                            }
                        }
                    }
                });
            } else if (parseInt(payTerm) === 20) {
                _.each(comboList, function (value, key) {
                    if (parseInt(payTerm) === parseInt(value.term)) {
                        for (var i = 0; i < value.policyList.length; i++) {
                            if (parseInt(value.policyList[i].polTerm) > 15) {
                                tempList.push({
                                    'payOutStart': value.policyList[i].polTerm,
                                    'noOfPlans': value.policyList[i].noOfPlans
                                });
                            }
                        }
                    }
                });
            }
        } else if (details.age > 8 && details.age < 14) {
            if (parseInt(payTerm) === 10) {
                _.each(comboList, function (value, key) {
                    if (parseInt(payTerm) === parseInt(value.term)) {
                        for (var i = 0; i < value.policyList.length; i++) {
                            if (parseInt(value.policyList[i].polTerm) <= 15) {
                                tempList.push({
                                    'payOutStart': value.policyList[i].polTerm,
                                    'noOfPlans': value.policyList[i].noOfPlans
                                });
                            }
                        }
                    }
                });
            } else if (parseInt(payTerm) === 15) {
                _.each(comboList, function (value, key) {
                    if (parseInt(payTerm) === parseInt(value.term)) {
                        for (var i = 0; i < value.policyList.length; i++) {
                            if (parseInt(value.policyList[i].polTerm) === 15) {
                                tempList.push({
                                    'payOutStart': value.policyList[i].polTerm,
                                    'noOfPlans': value.policyList[i].noOfPlans
                                });
                            }
                        }
                    }
                });
            }
        } else if (details.age >= 14 && details.age < 18) {
            _.each(comboList, function (value, key) {
                if (parseInt(payTerm) === parseInt(value.term)) {
                    for (var i = 0; i < value.policyList.length; i++) {
                        if (parseInt(value.policyList[i].polTerm) === 10) {
                            tempList.push({
                                'payOutStart': value.policyList[i].polTerm,
                                'noOfPlans': value.policyList[i].noOfPlans
                            });
                        }
                    }
                }
            });
        }
        return tempList;
    }

    CommonService.getOmnitureObject = function(filterArray,dummyDisplayedValues,toolId){
        var obj = [];
        obj = dummyDisplayedValues.filter(function(ele){
            return ele.isDisplay;
        });

        var sortedObj = $filter('orderBy')(obj, filterArray, false);
        return sortedObj;

    };

    CommonService.calculateTheWeightage = function(dummyDisplayedValues,toolId,quote){
        var greatest = 0;
        var lowest = 0;
        var x = 0;
        if (parseInt(toolId) === 4) {

            dummyDisplayedValues = dummyDisplayedValues.sortByProp('TotalReturnEquity').reverse();
        } else {
            dummyDisplayedValues = dummyDisplayedValues.sortByProp('TotalReturn').reverse();
        }
        greatest = parseInt(toolId)==4?dummyDisplayedValues[0].TotalReturnEquity:dummyDisplayedValues[0].TotalReturn;
        lowest = (parseInt(toolId)==4?CommonService.getMinLastNonZeroValue(dummyDisplayedValues,'TotalReturnEquity'):CommonService.getMinLastNonZeroValue(dummyDisplayedValues,'TotalReturn'))||0;
        if (parseInt(toolId) === 4) {
            x = CommonService.calculatePerc(greatest,lowest,quote.TotalReturnEquity);/*parseInt(((value.TotalReturnEquity-lowest)/(greatest-lowest) * 100), 10);*/
        } else {
            x = CommonService.calculatePerc(greatest,lowest,quote.TotalReturn);/*parseInt(((value.TotalReturn-lowest)/(greatest-lowest) * 100), 10);*/
        }

        return x;
    };

	CommonService.populateCustDetails = function (custId, filters,leadId, leadSource) {
            var defer = $q.defer();
            var mobileNo = 0;
            NewInvestmentService.getCustomerDetails(custId).success(function (data) {
                mobileNo = data.MobileNo;
                if ($localStorage.customerDetails === undefined || $localStorage.customerDetails === null) {
                    var obj = {};
                    obj.CustomerName = data.CustomerName;
                    obj.MobileNo = data.MobileNo;
                    obj.Email = data.Email;
                    obj.CountryID = data.CountryID;
                    obj.GenderID = data.GenderID;
                    obj.CityID = data.CityID;
                    $localStorage.customerDetails = obj;
                }
                defer.resolve(data);
                var age = 0;
                if ($localStorage.searchDetails !== undefined) {
                    age = $localStorage.searchDetails.yourAge;
                }
                angular.element(function () {

                    enableChatting(data,filters,leadId,age, mobileNo,custId);
                });
            });
            return defer.promise;
        };

        function enableChatting(data,filters,lead,age, mobileNo,custId) {
		    $("div.zopim").hide();
            if(mobileNo == 9777777777){	return ;}
            NewInvestmentService.CheckInternalIP().success(function(data){
                 var isInternalIP = false;
                if(configObj.isInternalDisabled){isInternalIP=!JSON.parse(data.toLowerCase());}
                else{isInternalIP=true;}
                NewInvestmentService.getChatDetails({ MatrixLeadID: lead }).success(function (data) {
                    $rootScope.workingHrs = CommonService.isWorkingHours(data.ChatTime, configObj.callEndTime, configObj.callStartTime);
                    var workingHrs = CommonService.isWorkingHours(data.ChatTime, configObj.endTime, configObj.startTime);
                    if (isInternalIP && (workingHrs && mobileNo != 9777777777 )){
                        if (data.LastVisitURL !== null) {
                            $localStorage.ExitPointUrl = data.LastVisitURL + '&leadId=' + CommonService.encode(data.MatrixLeadID);
                        } else {
                            $localStorage.ExitPointUrl = configObj.getQuotePageUrl("leadId=" + CommonService.encode(data.MatrixLeadID) + '&enquiryId=' + CommonService.encode($localStorage.enquiryId));
                        }
                        $localStorage.FirstVisitURL = data.FirstVisitURL + '&leadId=' + CommonService.encode(data.MatrixLeadID);
                        $timeout(function () {
                            zopimService($cookies, $localStorage, lead, filters, CommonService, data.ParentMatrixLeadID, workingHrs,custId);
                        }, 0);
                    }
                
                });
             
            });
        };

    CommonService.isChatStarted = function(){
        $rootScope.$broadcast('chatEvent', {
            someProp: true // send whatever you want
        });
    };

    CommonService.getExclusionObj = function(val){
        var x = val.split('$$');
        var z = [];
        for(var i  =0;i<x.length;i++){
            var sub_string = x[i].split('::');
            var key = sub_string[0];
            var value = sub_string[1];
            z.push({1:key,2:value});
        }

        return z;

    };

    CommonService.setChatTags = function(filters,leadId,custId){
        var payTerm = 0;
        var policyTerm = 0;
        var investment = 100000;
        var investmentType = 1;
        var age = 0;
        var retirementAge = 0;
        var gender = "";
        var isMonthly = "";
        var city = "";
        var features = "";
        var firstVisitUrl = "";
        var lastVisitUrl = "";
        if ($localStorage.searchDetails !== undefined) {
            payTerm = $localStorage.searchDetails.payTerm;
            policyTerm = $localStorage.searchDetails.payOutStart;
            investment = 100000;
            investmentType = $localStorage.searchDetails.investmentFor*1===1?'Growth':$localStorage.searchDetails.investmentFor*1===2?'Retirement':'Child';
            age = $localStorage.searchDetails.yourAge;
            retirementAge = $localStorage.searchDetails.investmentFor*1===2?$localStorage.searchDetails.retirementAge:0;
            gender = $localStorage.customerDetails.GenderID==1?'Male':'Female';
            isMonthly = $localStorage.monthlyStatus*1===1?'Yearly':'Monthly';
            if($localStorage.customerDetails!==undefined) {
                city = $localStorage.customerDetails.City;
            }
            var  featuresList = [];

            _.each(filters,function(value){
                if(value.checked) {
                    featuresList.push(value.fullName);
                }
            });
            features = featuresList.join(',');
            firstVisitUrl = $localStorage.FirstVisitURL+'&AgentId=Mw==';
            lastVisitUrl = $localStorage.ExitPointUrl+'&AgentId=Mw==';

            if (!Number($localStorage.searchDetails.investment)) {
                if ($localStorage.searchDetails.investment.replace(/,/g, '') > 0) {
                    var dumInvest = $localStorage.searchDetails.investment.replace(/,/g, '');
                    if ($localStorage.monthlyStatus == 2) {
                        investment = dumInvest;
                    } else {
                        investment = dumInvest;
                    }
                }
            } else if ($localStorage.searchDetails.investment > 0) {
                if ($localStorage.monthlyStatus == 2) {
                    investment = $localStorage.searchDetails.investment;
                } else {
                    investment = $localStorage.searchDetails.investment;
                }
            }
        }
        $zopim.livechat.removeTags("LeadID: " + oldObj.leadId, "CustomerID: " + oldObj.custId, "InvestmentAmount: " + oldObj.investment, "PayTerm: " + oldObj.payTerm, "Maturity Period:"+oldObj.policyTerm,"InvestmentType: " + oldObj.investmentType,"Gender:"+oldObj.gender,"Age:" + oldObj.age,'City:'+oldObj.CityName,'Yearly/ Monthly Frequency:'+oldObj.isMonthly,'Retirement Age: '+oldObj.retirementAge,'Features Chosen:'+oldObj.features,"First Visit URL:"+oldObj.firstVisitUrl,"last Visit URL:"+oldObj.lastVisitUrl);
        $zopim.livechat.addTags("LeadID: " + leadId, "CustomerID: " + custId, "InvestmentAmount: " + investment, "PayTerm: " + payTerm, "Maturity Period:"+ policyTerm, "InvestmentType: " + investmentType,"Gender:"+gender,"Age:" + age,"City:"+city,'Yearly/ Monthly Frequency:'+isMonthly,'Retirement Age: '+retirementAge,'Features Chosen:'+features,"First Visit URL:"+firstVisitUrl,"last Visit URL:"+lastVisitUrl);
        oldObj.leadId = leadId;
        oldObj.custId = custId;
        oldObj.investment = investment;
        oldObj.payTerm = payTerm;
        oldObj.policyTerm = policyTerm;
        oldObj.investmentType = investmentType;
        oldObj.gender = gender;
        oldObj.age = age;
        oldObj.isMonthly = isMonthly;
        oldObj.retirementAge = retirementAge;
        oldObj.features = features;
        oldObj.firstVisitUrl = firstVisitUrl;
        oldObj.lastVisitUrl = lastVisitUrl;
        oldObj.CityName = city;
    };

    function getHighestForEquity(myArray, highest) {
        var tmp;
        for (var i = myArray.length - 1; i >= 0; i--) {
            tmp = myArray[i].Sensex * 1;
            if (tmp > highest) highest = tmp;
        }
        return highest;
    }

    function getHighestForDebt(myArray, highest) {
        var tmp;
        var highestGovtBond = Number.NEGATIVE_INFINITY;
        var highestFixedDeposit = Number.NEGATIVE_INFINITY;
        for (var i = myArray.length - 1; i >= 0; i--) {
            tmp = myArray[i].governmentBondYields * 1;
            if (tmp > highestGovtBond) highestGovtBond = tmp;
        }

        for (var j = myArray.length - 1; j >= 0; j--) {
            tmp = myArray[j].fixedDeposit * 1;
            if (tmp > highestFixedDeposit) highestFixedDeposit = tmp;
        }

        if(highestGovtBond>highestFixedDeposit){
            highest = highestGovtBond;
        }else{
            highest = highestFixedDeposit;
        }
        return highest;
    }

    CommonService.getHighest = function(myArray,toolId){
        var highest = Number.NEGATIVE_INFINITY;

        if (toolId * 1 === 4) {
            highest = getHighestForEquity(myArray, highest);
        }else{
            highest = getHighestForDebt(myArray,highest);
        }

        return highest;
    };

    CommonService.getLowest = function(myArray,toolId){
        var lowest = Number.POSITIVE_INFINITY;
        var tmp;
        for (var i=myArray.length-1; i>=0; i--) {
            if(toolId*1===4) {
                tmp = myArray[i].Nifty * 1;
            }else{
                tmp = myArray[i].fixedDeposit * 1;
            }
            if (tmp < lowest) lowest = tmp;
        }
        return lowest;
    };

    // determine if the current hours are within 10 am and 7 pm
    CommonService.isWorkingHours = function(time,endTime,startTime){
      var startTimeDate = '01/01/2011'+' '+startTime,
            endTimeDate = '01/01/2011'+' '+endTime,
            currentTimeDate = '01/01/2011'+' '+time;
            return Date.parse(currentTimeDate)>Date.parse(startTimeDate) && Date.parse(currentTimeDate) < Date.parse(endTimeDate);
    };

    CommonService.populateBIObject = function(leadId,customerId,quote,customer,searchDetails,tooId){
    var defered = $q.defer();
    var obj = {};
      var GeneralDataBI = {};
        GeneralDataBI.RequestProposerPlanDetails = {};
        GeneralDataBI.RequestProposerPlanDetails.LeadID = leadId;
        GeneralDataBI.RequestProposerPlanDetails.PlanID = quote.PlanID;
        GeneralDataBI.RequestProposerPlanDetails.PayTerm = quote.Payterm;
        GeneralDataBI.RequestProposerPlanDetails.PolicyTerm = quote.PolicyTerm;
        GeneralDataBI.RequestProposerPlanDetails.SumAssured = quote.SumInsured;
        GeneralDataBI.RequestProposerPlanDetails.LifeCover = quote.LifeCover;
        GeneralDataBI.RequestProposerPlanDetails.GuranteedReturn = quote.GuranteedReturns;
        GeneralDataBI.RequestProposerPlanDetails.BasePremium = quote.AnnualPremium;
        GeneralDataBI.RequestProposerPlanDetails.AnnualPremium = quote.AnnualPremium;
        GeneralDataBI.RequestProposerPlanDetails.PremiumFrequency = 1;
        GeneralDataBI.RequestProposerPlanDetails.PremiumPaid = quote.AnnualPremium;
        GeneralDataBI.RequestProposerPlanDetails.IsPayment = false;
        GeneralDataBI.RequestProposerPlanDetails.PaymentIntegrationResp = false;
        GeneralDataBI.RequestProposerPlanDetails.RiderDetailsJSON = null;
        GeneralDataBI.RequestProposerPlanDetails.BookingIPAddress = "::1";
        GeneralDataBI.RequestProposerPlanDetails.ProposalNo = "12345";
        GeneralDataBI.PersonalData = {};
        GeneralDataBI.PersonalData.CustomerName = customer.CustomerName;
        GeneralDataBI.PersonalData.DateOfBirth = searchDetails.DOB;
        GeneralDataBI.PersonalData.EmailID = customer.Email;
        GeneralDataBI.PersonalData.Gender = customer.GenderID;
        GeneralDataBI.PersonalData.MobileNo = customer.MobileNo;
        GeneralDataBI.PersonalData.CityID = customer.CityID;
        GeneralDataBI.PersonalData.CityName = customer.City;
        GeneralDataBI.PersonalData.StateId = customer.stateId;
        GeneralDataBI.PersonalData.CustomerID = customerId;
        GeneralDataBI.PersonalData.PlanName = quote.QuotePlan.PlanName;
        GeneralDataBI.PersonalData.Child1 = null;
        GeneralDataBI.PersonalData.ChildName = null;
        GeneralDataBI.PersonalData.IsSummaryBI = true;
        GeneralDataBI.PersonalData.PensionAge = searchDetails.retirementAge;
        GeneralDataBI.PersonalData.PlanNature = parseInt(tooId)===2?"DebtGovernmentSecurities":parseInt(tooId)===4?"EquityMarketLinked":"Traditional";
        obj.GeneralDataBI = GeneralDataBI;

        NewInvestmentService.generateBI(obj).success(function(data){
            defered.resolve(data);
        });

        return defered.promise;

    };

CommonService.GetVisitIDFromVisitProfileCookie=function()
{

      var objVisitProfileCookie = JSON.parse($cookies.get('VisitProfileCookie'));
      var visitId = objVisitProfileCookie.visitId;
      return visitId;
};
CommonService.GetCustIDFromCustProfileCookie=function()
{
        var deferred = $q.defer();
        var CustprofileCookie="";
        var newObj = {};
        newObj.key ="CustProfileCookie";
        NewInvestmentService.GetHttpValues(newObj).success(function(data){ 
            deferred.resolve(data.d);
        });
        return deferred.promise;
};

CommonService.setShouldPageSkipCookie = function() {
    var now = new Date(),
        time = now.getTime();

    time += 3600 * 1000;
    now.setTime(time);
    $cookies.put('shouldPageSkip', true, {'expires': now.toUTCString()});
};


    return CommonService;

}]);

/**
 * Created by Prashants on 8/12/2015.
 */
app.service('NewInvestmentService', ["$rootScope", "$http", function ($rootScope,$http) {
    'use strict';

    var NewInvestmentService = {};

    var GetCountryCodesUrl = './json/country.json',
        GetComboUrl = "./json/comboList.json",
        GetInvestmentQuoteListUrl = './json/QuoteList.json',
        GetInvestmentPreQuoteUrl = './json/growth.json',
        GetBannerFromOfferId = './json/investment_offer.json',
        GetChartDatUrl = './json/Chart.json',
        CreateVisitUrl = configObj.getBaseServiceURL('prequote/CreateVisit'),
        AddEnquiryAndNeedUrl = configObj.getBaseServiceURL('prequote/AddEnquiryAndNeed'),
        RegisterCustomerUrl = configObj.getBaseServiceURL('prequote/RegisterUpdateCustomer'),
        GetCustomerDetailsUrl = configObj.getBaseServiceURL('Prequote/GetCustomerProfile?customerID='),
        UpdateEnquiryUrl = configObj.getBaseServiceURL('Prequote/UpdateEnquiryDetails'),
        GetQuotesUrl = configObj.getBaseServiceURL('Quote/GetCachedQuotes'),
        GetCityUrl = configObj.getBaseServiceURL('Data/GetCompleteCityList'),
        GetGrowthMaturityBenefitUrl = configObj.getBaseServiceURL('Data/GetGrowthMaturityBenefit'),
        GetDataFromEnquiryUrl = configObj.getBaseServiceURL('Prequote/GetEnquiryInfo?enquiryID='),
        SaveQuoteSelectionUrl = configObj.getBaseServiceURL('Quote/QuotesSelection'),
        ClickToCallUrl = configObj.getBaseServiceURL('Quote/ClickToCall'),
        EmailQuotesUrl =  configObj.getBaseServiceURL('Quote/EmailTopQuotes'),
        UpdateProductDetailsUrl = configObj.getBaseServiceURL('Quote/InsertUpdateProductDetails'),
        getInvestDataForLeadUrl = configObj.getBaseServiceURL('Prequote/CopyInvestmentData'),
        getPlanRiders = configObj.getBaseServiceURL('Quote/GetPlanFundPerformanceData'),
        GetChatDetailsUrl = configObj.getBaseServiceURL('Quote/GetChatLeadDetails'),
        GenerateBIUrl = configObj.getNewServiceURL('GenerateBusinessIllustration'),
		GetLeadInfoByLeadIdUrl = configObj.getBaseServiceURL('prequote/GetLeadInfoByLeadId'),
        HandlerURL = configObj.getHandlerURL(),
        SendMobileOTPUrl = configObj.getBaseServiceURL('prequote/SendMobileOTP'),
        VerifyMobileOTPUrl = configObj.getBaseServiceURL('prequote/VerifyMobileOTP'),
        CheckInternalIPUrl='./chatsupport.aspx',
		CheckOneTwoNineIPURL='./IsTraditional.aspx';
		
    NewInvestmentService.getCountryCodes = function(){
        var request = $http ({
            method : "GET",
            params: { 'v': new Date().getTime() },
            url : GetCountryCodesUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.getInvestmentQuoteList = function(){
        var request = $http ({
            method : "GET",
            params: { 'v': new Date().getTime() },
            url : GetInvestmentQuoteListUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.getInvestmentPreQuoteList = function(){
        var request = $http ({
            method : "GET",
            params: { 'v': new Date().getTime() },
            url : GetInvestmentPreQuoteUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.getCity = function(){
        var request = $http ({
            method : "GET",
            params: { 'v': new Date().getTime() },
            url : GetCityUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.createVisit = function(data){
        var request = $http ({
            method : "POST",
            data:data,
            url : CreateVisitUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.AddEnquiryAndNeed = function(data){
        var request = $http ({
            method : "POST",
            data:data,
            url : AddEnquiryAndNeedUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.RegisterCustomer = function(data){
        var request = $http ({
            method : "POST",
            data:data,
            url : RegisterCustomerUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.UpdateEnquiry = function(data){
        var request = $http ({
            method : "POST",
            data:data,
            url : UpdateEnquiryUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.GetQuotes = function(data){
        var request = $http ({
            method : "POST",
            data:data,
            url : GetQuotesUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.getCustomerDetails = function(custId){
        var url = GetCustomerDetailsUrl+custId;
        var request = $http ({
            method : "GET",
            params: { 'v': new Date().getTime() },
            url : url
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.getGrowthMaturity = function(obj){
        var request = $http ({
            method : "POST",
            data:obj,
            url : GetGrowthMaturityBenefitUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.getEnquiryDetails = function(obj){
        var url = GetDataFromEnquiryUrl+obj;
        var request = $http ({
            method : "GET",
            params: { 'v': new Date().getTime() },
            url : url
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.saveSelection = function(obj){
        var request = $http ({
            method : "POST",
            data:obj,
            url : SaveQuoteSelectionUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.clickToCall = function(obj){
        var request = $http ({
            method : "POST",
            data:obj,
            url : ClickToCallUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.emailQuotes = function(obj){
        var request = $http ({
            method : "POST",
            data:obj,
            url : EmailQuotesUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.updateProductDetails = function(obj){
        var request = $http ({
            method : "POST",
            data:obj,
            url : UpdateProductDetailsUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.getAllCombinations = function(){
        var request = $http ({
            method : "GET",
            params: { 'v': new Date().getTime() },
            url : GetComboUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.getPlanRiders = function(obj){
        var request = $http ({
            method : "POST",
            data:obj,
            url : getPlanRiders
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.getBanner = function(){
        var request = $http ({
            method : "GET",
            url : GetBannerFromOfferId
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.getInvestDataForLead = function(obj) {
        var request = $http({
            method: "POST",
            data: obj,
            url: getInvestDataForLeadUrl
        });
        return request.success(
            function (data, status, headers, config) {
            }).error(function (data, status, headers, config) {
            });
    };

    NewInvestmentService.getChatDetails = function(obj) {
        var request = $http({
            method: "POST",
            data: obj,
            url: GetChatDetailsUrl
        });
        return request.success(
            function (data, status, headers, config) {
            }).error(function (data, status, headers, config) {
            });
    };
    NewInvestmentService.generateBI = function(obj) {
        var request = $http({
            method: "POST",
            data: obj,
            url: GenerateBIUrl
        });
        return request.success(
            function (data, status, headers, config) {
            }).error(function (data, status, headers, config) {
            });
    };

    NewInvestmentService.getChartData = function(){
        var request = $http ({
            method : "GET",
            url : GetChartDatUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };
     NewInvestmentService.SetHttpValues = function(obj) {
        var request = $http({
            method: "POST",
            data: obj,
            url: HandlerURL+"/SetHttpValues"
        });
        return request.success(
            function (data, status, headers, config) {
            }).error(function (data, status, headers, config) {
            });
    };
    NewInvestmentService.GetHttpValues = function(obj) {
        var request = $http({
            method: "POST",
            data: obj,
            url: HandlerURL+"/GetHttpValues"
        });
        return request.success(
            function (data, status, headers, config) {
            }).error(function (data, status, headers, config) {
            });
    };
	NewInvestmentService.GetLeadInfoByLeadId = function (obj) {
        var request = $http({
            method: "POST",
            data: obj,
            url: GetLeadInfoByLeadIdUrl
        });
        return request.success(
            function (data, status, headers, config) {
            }).error(function (data, status, headers, config) {
            });
    };
    NewInvestmentService.SendMobileOTP = function (obj) {
        var request = $http({
            method: "POST",
            data: obj,
            url: SendMobileOTPUrl
        });
        return request.success(
            function (data, status, headers, config) {
            }).error(function (data, status, headers, config) { });
    };
    NewInvestmentService.VerifyMobileOTP = function (obj) {
        var request = $http({
            method: "POST",
            data: obj,
            url: VerifyMobileOTPUrl
        });
        return request.success(
            function (data, status, headers, config) {
            }).error(function (data, status, headers, config) { });
    };
	NewInvestmentService.CheckInternalIP = function(){
		var request = $http({
		method:"GET",
		url:CheckInternalIPUrl
	});
	return request.success(
		function (data,status,headers,config) {
	 }).error(function(data,status,headers,config){ });
	};
	NewInvestmentService.CheckOneTwoNineIP = function(){
		var request = $http({
		method:"GET",
		url:CheckOneTwoNineIPURL
	});
	return request.success(
		function (data,status,headers,config) {
	 }).error(function(data,status,headers,config){ });
	};
	return NewInvestmentService;
}]);

 app.service('OmnitureService', ["$http", "$rootScope", function ($http, $rootScope) {
	    'use strict';
	    
	    var OmnitureService = {};
	    OmnitureService.getOmnitureSelectedPlansString = function(comparePlansSummary){
	    	var selectedPlan = '';
	    	angular.forEach(comparePlansSummary, function(value, key){
	    		selectedPlan+=value.insurerName + "-" + value.policyName + "|";
	    	});
	    	selectedPlan = selectedPlan + '#';
    		selectedPlan = selectedPlan.replace('|#', '');
	    	return selectedPlan;
	    };	    

	    OmnitureService.getProduct = function(quotes){
	    	// This is sample data for reference
	    	//var product = ';ABC;;;;eVar16=Price_ABC,;DEF;;;;eVar16=Price_DEF,;XYZ;;;;eVar16=Price_XYZ';

	    	var product = "";
	    	if(quotes.length){
	    		angular.forEach(quotes, function(value, key){
	    			product+=value.QuotePlan.PlanName;
	    			product+="-";
	    			product+=value.QuotePlan.PlanInsurer.InsurerName;
	    			product+=";;;;eVar16=";
	    			product+=value.AnnualPremium;
	    			product+=",;";
		    	});
		    	product = product + '#';
    			product = product.replace(',;#', '');
	    	}
	    	return product;
	    };

		OmnitureService.getSelectedProduct = function(quotes){
			// This is sample data for reference
			//var product = ';ABC;;;;eVar16=Price_ABC,;DEF;;;;eVar16=Price_DEF,;XYZ;;;;eVar16=Price_XYZ';

			var product = "";
			if(quotes!==undefined){

				product+=quotes.QuotePlan.PlanName;
				product+="-";
				product+=quotes.QuotePlan.PlanInsurer.InsurerName;
				product+=";;;;eVar16=";
				product+=quotes.AnnualPremium;
				product+=",;";
				product = product + '#';
				product = product.replace(',;#', '');
			}
			return product;
		};


	    return OmnitureService;
    }]);

/**
 * Created by Prashants on 1/7/2016.
 */

/**
 * Created by Prashants on 8/12/2015.
 */
app.service('PreQuoteService', ["$rootScope", "$location", "NewInvestmentService", "$cookies", "$mdToast", "$q", "$localStorage", "$routeParams", "$filter", "$timeout","isMobile", function ($rootScope, $location, NewInvestmentService, $cookies, $mdToast, $q, $localStorage, $routeParams,$filter,$timeout,isMobile) {

}]);