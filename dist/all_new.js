
    var app = angular.module('investmentApp', ['pbHeaderFooter','ngAnimate','ngRoute', 'ngCookies','ngSanitize','ngAria','ngMaterial','ngMessages','ngStorage','Analytic.services']);
    app.constant('isMobile',isMobileDevice());
    function isMobileDevice(){
        var isMobile;
        isMobile = {
            /**
             * @return {boolean}
             */
            Android: function () {
                return navigator.userAgent.match(/Android/i) !== null;
            },
            /**
             * @return {boolean}
             */
            BlackBerry: function () {
                return navigator.userAgent.match(/BlackBerry/i) !== null;
            },
            iOS: function () {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i) !== null;
            },
            /**
             * @return {boolean}
             */
            Opera: function () {
                return navigator.userAgent.match(/Opera Mini/i) !== null;
            },
            /**
             * @return {boolean}
             */
            Windows: function () {
                return navigator.userAgent.match(/IEMobile/i) !== null;
            },
            any: function () {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };

        return isMobile.any();
    }

    function includeJs(jsFilePath) {
        var js = document.createElement("script");

        js.type = "text/javascript";
        js.src = jsFilePath;

        document.body.appendChild(js);
    }

    String.prototype.toTitleCase = function(){
        return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    };

    Array.prototype.sortByProp = function(p){
        return this.sort(function(a,b){
            return (a[p] > b[p]) ? 1 : (a[p] < b[p]) ? -1 : 0;
        });
    };

    Array.prototype.unique = function(){
        var n = {},r=[];
        for(var i = 0; i < this.length; i++){
            if (!n[this[i]]){
                n[this[i]] = true;
                r.push(this[i]);
            }
        }
        return r;
    };

    

    Array.prototype.insert = function (index, item) {
        this.splice(index, 0, item);
    };

    app.run(['$route', '$rootScope', '$location','$cookies','GoogleTagManager','isMobile','NewInvestmentService', function ($route, $rootScope, $location,$cookies,GoogleTagManager,isMobile,NewInvestmentService) {
        var original = $location.path;
        $rootScope.isIFrame = false;
        $rootScope.isOnlyTraditional = false;
        /*NewInvestmentService.CheckOneTwoNineIP().success(function(data){
            if(data.toUpperCase()==="TRUE"){
                $rootScope.isOnlyTraditional = true;
            }
        }).error(function(data){

        });*/
        $location.path = function (path, reload) {
            if (reload === false) {
                var lastRoute = $route.current;
                var un = $rootScope.$on('$locationChangeSuccess', function () {
                    $route.current = lastRoute;
                    un();
                });
            }
            return original.apply($location, [path]);
        };

        $rootScope.$on('$routeChangeStart', function (ev,next,cur) {
            if(next.$$route!==undefined) {
                    $rootScope.isLanding = next.$$route.originalPath === '/landing';
					$rootScope.isMobile = isMobile;
					$rootScope.isLandingPC = $rootScope.isLanding && !isMobile;
                    $rootScope.isPaytm= next.$$route.originalPath === '/paytm';
            }
            $(document).ready(function () {

                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            });
        });
		$rootScope.$on('$routeChangeSuccess', function() {
            var path= $location.path(),
                absUrl = $location.absUrl(),
                virtualUrl = absUrl.substring(absUrl.indexOf(path));
            GoogleTagManager.push({ event: 'virtualPageView', virtualUrl: virtualUrl });
            if (location.hash.length > 3) {
                ga('send', 'pageview', location.hash);
            } else {
                ga('send', 'pageview', location.href.replace("#",""));
            }
        });
        if($location.$$search.iframe ){
            $rootScope.isIFrame = $location.$$search.iframe;
        }else{
            $rootScope.isIFrame = false;
        }

        if($location.$$search.isHeadIframe){
            $rootScope.isHeadIFrame = true;
        }else{
            $rootScope.isHeadIFrame = false;
        }

        $( window ).unload(function() {
            $cookies.remove('visitId');
        });



    }]);

    app.config(
        [
            '$routeProvider',
            '$locationProvider',
            '$controllerProvider',
            '$compileProvider',
            '$filterProvider',
            '$provide',
            '$animateProvider',
            '$sceDelegateProvider',
            'isMobile',

            function($routeProvider, $locationProvider, $controllerProvider, $compileProvider, $filterProvider, $provide,$animateProvider,$sceDelegateProvider,isMobile)
            {

                app.controller = $controllerProvider.register;
                app.directive  = $compileProvider.directive;
                app.filter     = $filterProvider.register;
                app.factory    = $provide.factory;
                app.service    = $provide.service;
                if(configObj.getEnv()==="live") {
                    $locationProvider.html5Mode(true);
                }else{
                    $locationProvider.html5Mode(false);
                }
                /******************/
                if(!isMobile) {
                    $routeProvider
                        .when('/', {templateUrl: 'templates/choose-plan.html', controller: 'ChooseController'})
                        .when('/landing',{templateUrl: 'templates/pre-quote-resp.html', controller: 'PreQuotesController'})
                        .when('/customer', {templateUrl: 'templates/enter-contact.html', controller: 'CustomerController'})
                        .when('/quote', {
                            templateUrl: 'templates/quotes.html',
                            controller: 'QuotesController',
                            reloadOnSearch: false
                        })
                        .when('/interim', {templateUrl: 'templates/intermediate.html', controller: 'InterController'})
						.when('/click2fill', { templateUrl: 'templates/c2fill_details.html', controller: 'ClickToFillController' })
                        .when('/paytm', { templateUrl: 'templates/paytmTemplate.html', controller: 'PaytmController' })
                        .otherwise({redirectTo: '/'});
                }else{
                    $routeProvider
                        .when('/', {templateUrl: 'templates/pre-quote-resp.html', controller: 'PreQuotesController'})
                        .when('/landing',{templateUrl: 'templates/pre-quote-resp.html', controller: 'PreQuotesController'})
                        .when('/quote', {
                            templateUrl: 'templates/quotes.html',
                            controller: 'QuotesController',
                            reloadOnSearch: false
                        })
                        .when('/interim', {templateUrl: 'templates/intermediate.html', controller: 'InterController'})
                        .otherwise({redirectTo: '/'});
                }
            /*************************/
                /*************************
                $routeProvider
                    .when('/', {templateUrl: 'templates/pre-quote-resp.html', controller: 'PreQuotesController'})
                    .when('/landing',{templateUrl: 'templates/pre-quote-resp.html', controller: 'PreQuotesController'})
                    .when('/customer', {templateUrl: 'templates/enter-contact.html', controller: 'CustomerController'})
                    .when('/quote', {
                        templateUrl: 'templates/quotes.html',
                        controller: 'QuotesController',
                        reloadOnSearch: false
                    })
                    .when('/interim', {templateUrl: 'templates/intermediate.html', controller: 'InterController'})
                    .when('/click2fill', { templateUrl: 'templates/c2fill_details.html', controller: 'ClickToFillController' })
                    .otherwise({redirectTo: '/'});
                *************************/
                $animateProvider.classNameFilter(/^(?:(?!ng-animate-disabled).)*$/);
                $sceDelegateProvider.resourceUrlWhitelist([
                    // Allow same origin resource loads.
                    'self',
                    // Allow loading from our assets domain.  Notice the difference between * and **.
                    'http://pbstatic.policybazaar.com'
                ]);

        }
    ]);




    var userType = "";

    var callOnPageBottom = function () {
        this.pageName = "";
        this.channel = "";
        this.subSection1 = "";
        this.subSection2 = "";
        this.subSection3 = "";
        this.userType = "";

        this.pageBottomEvent = function () {
            var digitData = {
                page: {
                    'pageName': this.pageName,
                    'channel': this.channel,
                    'subSection1': this.subSection1,
                    'subSection2': this.subSection2,
                    'userType': this.userType
                }
            };
            return digitData;
        };
    };

    var callOnPageParam = function () {
        this.pageName = "";
        this.channel = "";
        this.subSection1 = "";
        this.subSection2 = "";
        this.subSection3 = "";
        this.userType = "";

        this.pageParamEvent = function () {
            var digitData = {
                page: {
                    'pageName': this.pageName,
                    'channel': this.channel,
                    'subSection1': this.subSection1,
                    'subSection2': this.subSection2,
                    'subSection3': this.subSection3,
                    'userType': this.userType
                }
            };
            return digitData;
        };
    };

    function getuserType() {
        userType = "guest";
        return userType;
    }

    userType = getuserType();

    var callOnPageFilter = function () {

        this.pageName = '';
        this.channel = '';
        this.subSection1 = '';
        this.subSection2 = '';
        this.customerId = '';
        this.userType = '';
        this.filterApplied = '';
        this.product = '';
        this.leadid = '';

        this.pageFilterEvent = function () {
            var digitData = {
                page: {
                    'pageName': this.pageName,
                    'channel': this.channel,
                    'subSection1': this.subSection1,
                    'subSection2': this.subSection2,
                    'userType': this.userType,
                    'filterApplied': this.filterApplied,
                    'product': this.product,
                    'leadid': this.leadid,
                    'customerId': this.customerId

                }
            };
            return digitData;
        };
    };

    var callOnQuoteSelection = function () {

        this.pageName = '';
        this.channel = '';
        this.subSection1 = '';
        this.subSection2 = '';
        this.enquiryId = '';
        this.userType = '';
        this.customerId = '';
        this.leadid = '';
        this.planname = '';

        this.quoteSelectionEvent = function () {
            var digitData = {
                page: {
                    'pageName': this.pageName,
                    'channel': this.channel,
                    'subSection1': this.subSection1,
                    'subSection2': this.subSection2,
                    'userType': this.userType,
                    'enquiryId': this.enquiryId,
                    'product': this.product,
                    'leadid': this.leadid,
                    'customerId': this.customerId

                }
            };
            return digitData;
        };
    };	
   
	var callOnEnquiryFormStart = function () {
        this.pageName = '';
        this.channel = '';
        this.subSection1 = '';
        this.subSection2 = '';
        this.subSection3 = '';
        this.customerId = '';
        this.userType = '';
        this.formname = 'Investment-prequotes';   
        this.journeyType = '';   		
        this.enquiryFormStartEvent = function () {
            var digitData = {
                page: {
                    'pageName': this.pageName,
                    'channel': this.channel,
                    'subSection1': this.subSection1,
                    'subSection2': this.subSection2,
                    'subSection3': this.subSection3,
                    'userType': this.userType,
                    'customerId': this.customerId,
                    'formname': this.formname,
                    'journeyType':this.journeyType				
                }
            };
            return digitData;
        };
    };

    var callOnEnquiryGeneration = function () {
        this.pageName = '';
        this.channel = '';
        this.subSection1 = '';
        this.subSection2 = '';
        this.subSection3 = '';
        this.customerId = '';
        this.enquiryId = '';
        this.userType = '';
        this.formname = 'Investment-prequotes';
        this.journeyType ='';
    this.enquiryGenerationEvent = function () {
        var digitData = {
            page: {
                'pageName': this.pageName,
                'channel': this.channel,
                'subSection1': this.subSection1,
                'subSection2': this.subSection2,
                'subSection3': this.subSection3,
                'userType': this.userType,
                'customerId': this.customerId,
                'enquiryId': this.enquiryId,
                'formname': this.formname,
                'journeyType': this.journeyType
            }
        };
        return digitData;
    };
};

var callOnLeadGeneration = function () {
    this.pageName = '';
    this.channel = '';
    this.subSection1 = '';
    this.subSection2 = '';
    this.leadid = '';
    this.customerId = '';
    this.enquiryId = '';
    this.userType = '';
    this.age = '';
    this.gender = '';
    this.city = '';
    this.formname = 'Investment-prequotes';
    this.journeyType = '';

    this.leadGenerationEvent = function () {
        var digitData = {
            page: {
                'pageName': this.pageName,
                'channel': this.channel,
                'subSection1': this.subSection1,
                'subSection2': this.subSection2,
                'leadid': this.leadid,
                'userType': this.userType,
                'customerId': this.customerId,
                'enquiryId': this.enquiryId,
                'formname': this.formname,
                'journeyType': this.journeyType,
                'age': this.age,
                'gender': this.gender,
                'city': this.city
            }
        };
        return digitData;
    };
};

    var omnitureCallBackEvents = function () {
        this.filterUsed = function () {
            _satellite.track('filterUsedCLEID');
        };
        this.compared = function () {
            _satellite.track('compared');
        };
        this.leadGeneration = function () {
            _satellite.track('enqlead-generated');
        };

        this.formStart = function () {
            _satellite.track('formstart');
        };
        this.enquiryGeneration = function () {
            _satellite.track('FormStepOneEnq');
        };
        this.pageBottom = function () {
            _satellite.pageBottom();
        };
        this.pageParam = function () {
            _satellite.track('Pageparam');
        };
        this.productSelection = function () {
            _satellite.track('productSelection');
        };
        this.formstep1Complete = function () {
            _satellite.track('formstep-1-Complete');
        };
        this.formSubmitted = function () {
            _satellite.track('form-submitted');
        };
        this.buyPlanClick = function () {
            _satellite.track('continue-btn');
        };
    };

    triggerPageBottomEvents = function(pageName) {
        try {
            var ominiObj = new callOnPageBottom();
            ominiObj.pageName = 'ac:investment-insurance-' + pageName;
            ominiObj.channel = 'Investment Insurance';
            ominiObj.subSection1 = 'Home';
            ominiObj.subSection2 = 'Investment Insurance';
            ominiObj.userType = getuserType();
            digitalData = ominiObj.pageBottomEvent();
            var OminiEvent = new omnitureCallBackEvents();
            OminiEvent.pageBottom();
        }
        catch (e) {
        }
    };

    triggerPageParamEvents = function(pageName) {
        try {
            var ominiObj = new callOnPageParam();
            ominiObj.pageName = 'pb:investment-insurance-' + pageName;
            ominiObj.channel = 'Investment Insurance';
            ominiObj.subSection1 = 'Home';
            ominiObj.subSection2 = 'Investment Insurance';
            ominiObj.subSection3 = 'Investment Insurance';
            ominiObj.userType = getuserType();
            digitalData = ominiObj.pageParamEvent();
            var OminiEvent = new omnitureCallBackEvents();
            OminiEvent.pageParam();
        }
        catch (e) {
        }
    };

    triggerPageFilterEvents = function(OmniData, customerid, leadid, filters) {
        try {
            var ominiObj = new callOnPageFilter();
            ominiObj.pageName = 'pb:investment-insurance-quotes';
            ominiObj.channel = 'Investment Insurance';
            ominiObj.subSection1 = 'Home';
            ominiObj.subSection2 = 'Investment Insurance';
            ominiObj.userType = getuserType();
            if (filters !== undefined) {
                ominiObj.filterApplied = filters.join('|');
            } else {
                ominiObj.filterApplied = '';
            }
            ominiObj.product = ';' + convertSpecialCharacterToHTML(OmniData);
            ominiObj.customerId = customerid;
            ominiObj.leadid = leadid;
            digitalData = ominiObj.pageFilterEvent();
            var OminiEvent = new omnitureCallBackEvents();
            OminiEvent.filterUsed();
        }
        catch (e) {
        }
    };

triggerLeadGenerationEvent = function (leadId, enquiryId, customerId, investmentTypeID,leadObj) {
    try {
        var ominiObj = new callOnLeadGeneration();
        investmentTypeID = investmentTypeID * 1;
        var investmentType = "Growth";
        if (investmentTypeID == 2) { investmentType = "Retirement"; }
        else if (investmentTypeID == 3) { investmentType = "Child"; }
        ominiObj.pageName = 'pb:investment-insurance-quotes';
        ominiObj.channel = 'Investment Insurance';
        ominiObj.subSection1 = 'Home';
        ominiObj.subSection2 = 'Investment Insurance';
        ominiObj.leadid = leadId;
        ominiObj.userType = 'guest';
        ominiObj.customerId = customerId;
        ominiObj.enquiryId = enquiryId;
        ominiObj.formname = 'Investment-preQuotes';
        ominiObj.journeyType = investmentType;
        ominiObj.age = leadObj.age;
        ominiObj.gender = leadObj.gender;
        ominiObj.city = leadObj.city;
        digitalData = ominiObj.leadGenerationEvent();
        var OminiEvent = new omnitureCallBackEvents();
        OminiEvent.leadGeneration();
       // OminiEvent.formSubmitted();
        return digitalData;
    }
    catch (e) {
    }
};

triggerEnquiryGenerationEvent = function (enquiryId, customerId, investmentTypeID) {
    try {
        var ominiObj = new callOnEnquiryGeneration();
        investmentTypeID = investmentTypeID * 1;
        var investmentType = "Growth";
        if (investmentTypeID == 2) { investmentType = "Retirement"; }
        else if (investmentTypeID == 3) { investmentType = "Child"; }
        ominiObj.pageName = 'pb:investment-insurance-quotes';
        ominiObj.channel = 'Investment Insurance';
        ominiObj.subSection1 = 'Home';
        ominiObj.subSection2 = 'Investment Insurance';
        ominiObj.userType = 'guest';
        ominiObj.customerId = customerId;
        ominiObj.enquiryId = enquiryId;
        ominiObj.formname = 'Investment-preQuotes';
        ominiObj.journeyType = investmentType;
        digitalData = ominiObj.enquiryGenerationEvent();
        var OminiEvent = new omnitureCallBackEvents();
        //OminiEvent.formStart();
        OminiEvent.enquiryGeneration();
        return digitalData;
    } catch (e) {
    }
};

triggerEnquiryFormStartEvent = function (customerId,investmentTypeID) {
    try {
        var ominiObj = new callOnEnquiryFormStart();
		if(investmentTypeID !== undefined){
		investmentTypeID = investmentTypeID * 1;
		}
        var investmentType = "Growth";
		if(investmentTypeID == 1) { investmentType = "Growth"; }
        else if (investmentTypeID == 2) { investmentType = "Retirement"; }
        else if (investmentTypeID == 3) { investmentType = "Child"; }
        ominiObj.pageName = 'pb:investment-insurance-prequotes';
        ominiObj.channel = 'Investment Insurance';
        ominiObj.subSection1 = 'Home';
        ominiObj.subSection2 = 'Investment Insurance';
        ominiObj.userType = 'guest';
        ominiObj.customerId = customerId;
        ominiObj.formname = 'Investment-preQuotes';
		ominiObj.journeyType = investmentType;
        digitalData = ominiObj.enquiryFormStartEvent();
        var OminiEvent = new omnitureCallBackEvents();
        OminiEvent.formStart();
        return digitalData;
    } catch (e) {
    }
};

triggerQuoteSelectionEvent = function (enquiryId, customerId, leadId, omniData) {
    try {
        var ominiObj = new callOnQuoteSelection();
        ominiObj.pageName = 'pb:investment-insurance-quotes';
        ominiObj.channel = 'Investment Insurance';
        ominiObj.subSection1 = 'Home';
        ominiObj.subSection2 = 'Investment Insurance';
        ominiObj.userType = getuserType();
        ominiObj.product = ';' + convertSpecialCharacterToHTML(omniData);
        ominiObj.customerId = customerId;
        ominiObj.leadid = leadId;
        ominiObj.enquiryId = enquiryId;
        digitalData = ominiObj.quoteSelectionEvent();
        var OminiEvent = new omnitureCallBackEvents();
        OminiEvent.productSelection();
    }
    catch (e) {
    }
};

    triggerBuyEvent = function(enquiryId, customerId, leadId,omniData) {
        try {
            var ominiObj = new callOnQuoteSelection();
            ominiObj.pageName = 'pb:investment-insurance-quotes';
            ominiObj.channel = 'Investment Insurance';
            ominiObj.subSection1 = 'Home';
            ominiObj.subSection2 = 'Investment Insurance';
            ominiObj.userType = getuserType();
            ominiObj.product = ';' + convertSpecialCharacterToHTML(omniData);
            ominiObj.customerId = customerId;
            ominiObj.leadid = leadId;
            ominiObj.enquiryId = enquiryId;
            digitalData = ominiObj.quoteSelectionEvent();
            var OminiEvent = new omnitureCallBackEvents();
            OminiEvent.buyPlanClick();
        }
        catch (e) {
        }
    };


    function convertSpecialCharacterToHTML(str) {
        return str.replace(/&/g, " and ").replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;");
    }


//function zopimService() {
//app.service('zopimService', function () {

function zopimService($cookies,$localStorage,lead,filters,CommonService,parentLead,workingHrs,custID){
    var timer1 = '';
    var timer2 = '';
    var currentdate = new Date();
    var showDelayTime = 30000;
    var hideDelayTime = 5000;
    var parentLeadId = parentLead||0;
    var custId = 0;
   
   
        custId=custID;
        var leadId = lead;
        var notWorkingHrs = workingHrs;

        var mm = (currentdate.getMonth() + 1);
        if (mm < 10) {
            mm = "0" + (currentdate.getMonth() + 1);
        }

        var dd = (currentdate.getDate());
        if (dd < 10) {
            dd = "0" + (currentdate.getDate());
        }

        var datetime = mm + "/" + dd + "/" + currentdate.getFullYear() + " " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();

        function SetDateFormat(dateToBeFormatted) {
            var monthNames = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
            var date = new Date(dateToBeFormatted);
            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();
            if (day < 10) {
                day = "0" + day;
            }
            return monthNames[monthIndex] + "/" + day + "/" + year;
        }

        if (jQuery(window).width() < 769 || (jQuery(window).width() > 769)) { // Screen Offset Width

            if(configObj.getEnv()!=="live") {
                window.$zopim || (function (d, s) {
                    var z = $zopim = function (c) {
                        z._.push(c);
                    }, $ = z.s =
                        d.createElement(s), e = d.getElementsByTagName(s)[0];
                    z.set = function (o) {
                        z.set.
                            _.push(o);
                    };
                    z._ = [];
                    z.set._ = [];
                    $.async = !0;
                    $.setAttribute("charset", "utf-8");
                    $.src = "//v2.zopim.com/?3P2eHhKkEfkNyi838oYo2kid6kJBoQ2k";
                    z.t = +new Date();
                    $.
                        type = "text/javascript";
                    e.parentNode.insertBefore($, e)
                })(document, "script");
            }else {
                //Live
                window.$zopim || (function (d, s) {
                    var z = $zopim = function (c) {
                        z._.push(c);
                    }, $ = z.s = d.createElement(s), e = d.getElementsByTagName(s)[0];
                    z.set = function (o) {
                        z.set.
                            _.push(o);
                    };
                    z._ = [];
                    z.set._ = [];
                    $.async = !0;
                    $.setAttribute("charset", "utf-8");
                    $.src = "//v2.zopim.com/?3DJInKwWllGioUhQv0QUosu2y0bcNNM3";
                    z.t = +new Date();
                    $.type = "text/javascript";
                    e.parentNode.insertBefore($, e);
                })(document, "script");
            }

            $zopim(function () {
                $zopim.livechat.setName($localStorage.customerDetails.CustomerName);
                $zopim.livechat.setEmail($localStorage.customerDetails.Email);
                CommonService.isChatStarted();

                $zopim.livechat.setOnConnected(function() {
                    $zopim.livechat.departments.setVisitorDepartment('Investment Plans');
                });

                $zopim.livechat.setOnChatStart(setAttrOnChatStart);
                $zopim.livechat.setOnChatEnd(setAttrOnChatEnd);

                if ($zopim.livechat.isChatting()) {
                    clearTimeout(timer1);
                    clearTimeout(timer2);
                }
                if(!CommonService.IsMobileDevice()) {
                    if (!$zopim.livechat.isChatting()) {
                        timer1 = setTimeout(function () {
                            $zopim.livechat.window.show();
                            minimizeWindowPopup();
                        }, showDelayTime);
                    } else {
                        $zopim.livechat.window.show();
                    }
                }else{
                    CommonService.isChatStarted();
                }

            function minimizeWindowPopup() {
                timer2 = setTimeout(function () {
                    if (!$zopim.livechat.isChatting()) {
                    $zopim.livechat.window.hide();
                    $zopim.livechat.badge.hide();
                    }
                }, hideDelayTime);
            }

            function setAttrOnChatStart() {
            var dataToPost = '';
            clearTimeout(timer1);
            clearTimeout(timer2);

            //Product information should be pass as parameters in removeTags and addTags API
            if ((($zopim.livechat.isChatting()))) {

                CommonService.setChatTags(filters,leadId,custId);
            }
            if(parentLeadId === ""||parentLeadId === null||parentLeadId === 0){
                $zopim.livechat.say("ChatID: " + leadId);
                dataToPost = '{"item":{"Data":{"LeadId":"' + leadId + '","CustId":"' + custId + '","StartDate":"' + datetime + '","IntractionType":"1"}}}';
            } else {
                $zopim.livechat.say("ChatID: " + parentLeadId + "_" + leadId);
                dataToPost = '{"item":{"Data":{"LeadId":"' + parentLeadId + '","CustId":"' + custId + '","StartDate":"' + datetime + '","IntractionType":"1"}}}';

            }

            // Service for Matrix to set chat start flag.
            $.ajax({
                url: 'http://offers.policybazaar.com/ChatService/SetCustomerInteraction',
                headers: { 'Authorization': 'cG9saWN5 YmF6YWFy' },
                type: 'POST',
                data: dataToPost,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (res) {
                    //do work on success, if required.
                },
                error: function (res) {
                    //alert("Oops! error encountered");
                }
            });
            }

            function setAttrOnChatEnd() {
            var dataToPost = '';
            var endDate = new Date();

            var endmm = (endDate.getMonth() + 1);
            if (endmm < 10) {
                endmm = "0" + (endDate.getMonth() + 1);
            }

            var enddd = (endDate.getDate());
            if (enddd < 10) {
                enddd = "0" + (endDate.getDate());
            }

            var enddatetime = endmm + "/" + enddd + "/" + endDate.getFullYear() + " " + endDate.getHours() + ":" + endDate.getMinutes() + ":" + endDate.getSeconds();

            if(parentLeadId === ""||parentLeadId === null||parentLeadId === 0) {
                dataToPost = '{"item":{"Data":{"LeadId":"' + leadId + '","CustId":"' + custId + '","StartDate":"' + datetime + '","EndDate":"' + enddatetime + '","IntractionType":"2"}}}';
            } else {
                dataToPost = '{"item":{"Data":{"LeadId":"' + parentLeadId + '","CustId":"' + custId + '","StartDate":"' + datetime + '","EndDate":"' + enddatetime + '","IntractionType":"2"}}}';
            }

            // Service for Matrix to set chat end flag.
            $.ajax({
                url: 'http://offers.policybazaar.com/ChatService/SetCustomerInteraction',
                headers: { 'Authorization': 'cG9saWN5 YmF6YWFy' },
                type: 'POST',
                data: dataToPost,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (res) {
                    //do work on success, if required.
                },
                error: function (res) {
                    //alert("Oops! error encountered");
                }
            });
            }
            }); //Zopim Script End
            }
  
}

//});

//}