/**
 * Created by Prashants on 6/29/2015.
 */

    app.directive('onlyNum', function() {
        return function(scope, element, attrs) {

            var keyCode = [8, 9, 37, 39, 46,48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105];
            element.bind("keydown", function(event) {
                if ( !_.contains(keyCode, event.which) || event.shiftKey) {
                    scope.$apply(function() {
                        scope.$eval(attrs.onlyNum);
                        event.preventDefault();
                    });
                    event.preventDefault();
                }

            });
            element.on( "copy cut paste drop", function() {
                return false;
            });
        };
    });

app.directive('onlyNumAddCommas', ["$filter", "CommonService", function($filter,CommonService) {
    var keyCode = [8, 9, 37, 39, 46, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105];
    return {
        restrict: 'E',
        scope: {
            det: '=',
            addClass: '@',
            minLength:'@',
            maxLength:'@',
            changeValues:'&',
            getFocusOut:'&',
            placeText:'@'
        },
        link: function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (!_.contains(keyCode, event.which)|| event.shiftKey) {
                    scope.$apply(function () {
                        event.preventDefault();
                    });
                    event.preventDefault();
                } else {
                    $filter('addCommas')(scope.investment);
                }

            });

            element.on( "copy cut paste drop", function() {
                return false;
            });

            element.bind("keyup", function (event) {
                var x = angular.copy(scope.det.investment);
                if(x!==undefined){
                    x = x.replace(/,/g, '');
                }
                scope.$apply(function(){
                    scope.det.investment =  CommonService.AddCommas(x);
                });
            });


        },

        template:'<span class="rupee-sign"></span><input id="select_1355" type="tel" class="{{addClass}}" name="invest" minlength="{{minLength}}" maxlength="{{maxLength}}" placeholder="{{placeText}}" ng-model="det.investment"  ng-focus="!det.amountFocusLost" ng-blur="getFocusOut()" ng-change="changeValues()" data-ng-if="!det.amountFocusLost" />'
    };
}]);

app.directive('sliderDrag', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            function changeAmountValues() {
                scope.details.amount = scope.sliderObj.list[scope.sliderObj.value];
                scope.details.monthlyAmount = Math.round(scope.details.amount/12);
            }

            element.on('keydown', function(){
                changeAmountValues();
                scope.monthlyPayoutChange();
            });

            element.on('$md.pressup', function(){
                changeAmountValues();
                scope.monthlyPayoutChange();
            });

            element.on('$md.drag', function() {
                changeAmountValues();
            });

            element.on('$md.dragend', function() {
                changeAmountValues();
                scope.monthlyPayoutChange();
            });
        }
    };
});

/*
 This directive allows us to pass a function in on an enter key to do what we want.
 */
app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

app.directive('focusMe', function($timeout, $parse) {
      return {
        //scope: true,   // optionally create a child scope
        link: function(scope, element, attrs) {
          var model = $parse(attrs.focusMe);
          scope.$watch(model, function(value) {
            if(value === true) { 
              $timeout(function() {
                console.log(attrs);
                element[0].firstElementChild.firstElementChild.focus(); 
              });
            }
          });
          // to address @blesh's comment, set attribute value to 'false'
          // on blur event:
          element.bind('blur', function() {
             scope.$apply(model.assign(scope, false));
          });
        }
      };
});

app.directive('ghVisualization', ["CommonService", function (CommonService) {

    return {
        restrict: 'E',
        scope: {
            mapValue: '=',
            filtObject:'='
        },
        templateUrl:"./templates/chart-window.html",
        compile: function(tElem, tAttrs){
            includeJs("http://cdn.policybazaar.com/Investment/scripts/common-scripts/d3.v3.min.js");
            return {
                pre: function(scope, element, attributes, controller, transcludeFn){

                },
                post: function(scope, element, attributes, controller, transcludeFn){
                    scope.radio = {};

                    if(scope.filtObject.toolId*1===2){
                        scope.legend1 = "Government Bond";
                        scope.legend2 = "Fixed Deposit";
                        scope.radio.check = 1;
                    }else{
                        scope.legend1 = "Sensex";
                        scope.legend2 = "Nifty";
                        scope.heading = "Market Performance";
                        scope.radio.check = 2;
                    }
                    var mapObject = angular.copy(scope.mapValue);
                    var vis = d3.select(element[0]).append("svg").attr("width", 500).attr("height", 300),
                        WIDTH = 500,
                        HEIGHT = 300,
                        MARGINS = {
                            top: 20,
                            right: 30,
                            bottom: 20,
                            left: 50
                        };
                    // Define the div for the tooltip
                    var div = d3.select('gh-visualization').append("div")
                        .attr("class", "tooltip")
                        .style("opacity", 0);
                    var xScale = d3.scale.ordinal().rangeBands([MARGINS.left, WIDTH - MARGINS.right]);
                    var yScale = d3.scale.linear().range([HEIGHT - MARGINS.top, MARGINS.bottom]);

                    var lineGen = d3.svg.line().interpolate("cardinal ")
                        .x(function (d) {
                            return xScale(d.Year);
                        })
                        .y(function (d) {
                            if (scope.filtObject.toolId * 1 === 2) {
                                return yScale(d.fixedDeposit);
                            }else{
                                return yScale(d.Nifty);
                            }
                        });

                    var newLineGen = d3.svg.line().interpolate("cardinal ")
                        .x(function (d) {
                            return xScale(d.Year);
                        })
                        .y(function (d) {
                            if (scope.filtObject.toolId * 1 === 2) {
                                return yScale(d.governmentBondYields);
                            }else{
                                return yScale(d.Sensex);
                            }
                        });
                    xScale.domain(mapObject.map(function (d) {
                        return d.Year;
                    }));
                    var highest = CommonService.getHighest(mapObject,scope.filtObject.toolId);
                    var lowest = CommonService.getLowest(mapObject,scope.filtObject.toolId);
                    var lengthOfMap = mapObject.length;
                    var divisors = Math.round(lengthOfMap/5);
                    var xAxis = d3.svg.axis().scale(xScale).tickValues([mapObject[0].Year, mapObject[divisors].Year, mapObject[divisors*2].Year, mapObject[divisors*3].Year, mapObject[divisors*4].Year, mapObject[mapObject.length-1].Year]);

                    yScale.domain([0, highest]);
                    var yAxis = d3.svg.axis()
                        .scale(yScale)
                        .orient("left");

                    vis.append("svg:g").attr("class", "xaxis").attr("transform", "translate(0," + (HEIGHT - MARGINS.bottom) + ")").call(xAxis);

                    vis.append("svg:g")
                        .attr("class", "yaxis").attr("transform", "translate(" + (MARGINS.left) + ",0)")
                        .call(yAxis);
                    vis.append('svg:path')
                        .attr('d', lineGen(mapObject))
                        .attr('stroke', 'green')
                        .attr("class", "line1")
                        .attr('stroke-width', 2)
                        .attr('fill', 'none');

                    vis.append('svg:path')
                        .attr('d', newLineGen(mapObject))
                        .attr('stroke', 'blue')
                        .attr("class", "line2")
                        .attr('stroke-width', 2)
                        .attr('fill', 'none');

                    // Add the scatterplot
                    vis.selectAll(".dot")
                        .data(mapObject)
                        .enter().append("circle")
                        .attr("r", 3)
                        .attr("class","dot").style("fill","blue")
                        .attr("cx", function(d) { return xScale(d.Year);})
                        .attr("cy", function(d) { if(scope.filtObject.toolId*1===4){return yScale(d.Sensex);}else{return yScale(d.governmentBondYields);} })
                        .on("mouseover", function(d) {
                            div.transition()
                                .duration(200)
                                .style("opacity", 0.9);
                            var value =  scope.filtObject.toolId*1===4? d.Sensex: d.governmentBondYields;
                            div.html("Year:"+' '+d.Year + "<br/>"+"Value:"+' '+value)
                                .style("left", d3.select(this).attr("cx") + "px")
                                .style("top", d3.select(this).attr("cy") + "px");
                        })
                        .on("mouseout", function(d) {
                            div.transition()
                                .duration(500)
                                .style("opacity", 0);
                        });

                    vis.selectAll(".anotherDot")
                        .data(mapObject)
                        .enter().append("circle")
                        .attr("r", 3)
                        .attr("class","anotherDot").style("fill","green")
                        .attr("cx", function(d) { return xScale(d.Year);})
                        .attr("cy", function(d) { if(scope.filtObject.toolId*1===4){return yScale(d.Nifty);}else{return yScale(d.fixedDeposit);} })
                        .on("mouseover", function(d) {
                            div.transition()
                                .duration(200)
                                .style("opacity", 0.9);
                            var value =  scope.filtObject.toolId*1===4? d.Nifty: d.fixedDeposit;
                            div.html("Year:"+' ' +d.Year + "<br/>"  + "Value:"+' '+value)
                                .style("left", d3.select(this).attr("cx") + "px")
                                .style("top", d3.select(this).attr("cy") + "px");
                        })
                        .on("mouseout", function(d) {
                            div.transition()
                                .duration(500)
                                .style("opacity", 0);
                        });

                    function drawChart() {

                        xScale.domain(mapObject.map(function (d) {
                            return d.Year;
                        }));
                        highest = CommonService.getHighest(mapObject,scope.filtObject.toolId);
                        lowest = CommonService.getLowest(mapObject,scope.filtObject.toolId);
                        yScale.domain([0, highest]);

                        var svg = d3.select("gh-visualization").transition();

                        svg.select(".line2")   // change the line
                            .duration(750)
                            .attr("d", newLineGen(mapObject));
                        svg.select(".line1")   // change the line
                            .duration(750)
                            .attr("d", lineGen(mapObject));
                        svg.select(".xaxis") // change the x axis
                            .duration(750)
                            .call(xAxis);
                        svg.select(".yaxis") // change the y axis
                            .duration(750)
                            .call(yAxis);
                        svg.selectAll(".dot").duration(750).attr("r", 3).style("fill","blue").attr("cx", function(d) { return xScale(d.Year);})
                            .attr("cy", function(d) { if(scope.filtObject.toolId*1===4){return yScale(d.Sensex);}else{return yScale(d.governmentBondYields);} });
                        svg.selectAll(".anotherDot").duration(750).attr("r", 3).style("fill","green").attr("cx", function(d) { return xScale(d.Year);})
                            .attr("cy", function(d) { if(scope.filtObject.toolId*1===4){return yScale(d.Nifty);}else{return yScale(d.fixedDeposit);} });
                    }

                    scope.changeMap = function(val){
                        if(val!==undefined) {
                            mapObject = angular.copy(scope.mapValue.slice(scope.mapValue.length - val, scope.mapValue.length));
                        }
                        drawChart();
                    };

                    if(scope.filtObject.toolId*1===4){
                        scope.changeMap(15);
                    }else{
                        scope.changeMap(10);
                    }

                }
            };
        }
        /*link: function (scope, element, attrs) {


        }*/
    };
}]);


