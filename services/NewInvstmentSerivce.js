/**
 * Created by Prashants on 8/12/2015.
 */
app.service('NewInvestmentService', ["$rootScope", "$http", function ($rootScope,$http) {
    'use strict';

    var NewInvestmentService = {};

    var GetCountryCodesUrl = './json/country.json',
        GetComboUrl = "./json/comboList.json",
        GetInvestmentQuoteListUrl = './json/QuoteList.json',
        GetInvestmentPreQuoteUrl = './json/growth.json',
        GetBannerFromOfferId = './json/investment_offer.json',
        GetChartDatUrl = './json/Chart.json',
        CreateVisitUrl = configObj.getBaseServiceURL('prequote/CreateVisit'),
        AddEnquiryAndNeedUrl = configObj.getBaseServiceURL('prequote/AddEnquiryAndNeed'),
        RegisterCustomerUrl = configObj.getBaseServiceURL('prequote/RegisterUpdateCustomer'),
        GetCustomerDetailsUrl = configObj.getBaseServiceURL('Prequote/GetCustomerProfile?customerID='),
        UpdateEnquiryUrl = configObj.getBaseServiceURL('Prequote/UpdateEnquiryDetails'),
        GetQuotesUrl = configObj.getBaseServiceURL('Quote/GetCachedQuotes'),
        GetCityUrl = configObj.getBaseServiceURL('Data/GetCompleteCityList'),
        GetGrowthMaturityBenefitUrl = configObj.getBaseServiceURL('Data/GetGrowthMaturityBenefit'),
        GetDataFromEnquiryUrl = configObj.getBaseServiceURL('Prequote/GetEnquiryInfo?enquiryID='),
        SaveQuoteSelectionUrl = configObj.getBaseServiceURL('Quote/QuotesSelection'),
        ClickToCallUrl = configObj.getBaseServiceURL('Quote/ClickToCall'),
        EmailQuotesUrl =  configObj.getBaseServiceURL('Quote/EmailTopQuotes'),
        UpdateProductDetailsUrl = configObj.getBaseServiceURL('Quote/InsertUpdateProductDetails'),
        getInvestDataForLeadUrl = configObj.getBaseServiceURL('Prequote/CopyInvestmentData'),
        getPlanRiders = configObj.getBaseServiceURL('Quote/GetPlanFundPerformanceData'),
        GetChatDetailsUrl = configObj.getBaseServiceURL('Quote/GetChatLeadDetails'),
        GenerateBIUrl = configObj.getNewServiceURL('GenerateBusinessIllustration'),
		GetLeadInfoByLeadIdUrl = configObj.getBaseServiceURL('prequote/GetLeadInfoByLeadId'),
        HandlerURL = configObj.getHandlerURL(),
        SendMobileOTPUrl = configObj.getBaseServiceURL('prequote/SendMobileOTP'),
        VerifyMobileOTPUrl = configObj.getBaseServiceURL('prequote/VerifyMobileOTP'),
        CheckInternalIPUrl='./chatsupport.aspx',
		CheckOneTwoNineIPURL='./IsTraditional.aspx';
		
    NewInvestmentService.getCountryCodes = function(){
        var request = $http ({
            method : "GET",
            params: { 'v': new Date().getTime() },
            url : GetCountryCodesUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.getInvestmentQuoteList = function(){
        var request = $http ({
            method : "GET",
            params: { 'v': new Date().getTime() },
            url : GetInvestmentQuoteListUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.getInvestmentPreQuoteList = function(){
        var request = $http ({
            method : "GET",
            params: { 'v': new Date().getTime() },
            url : GetInvestmentPreQuoteUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.getCity = function(){
        var request = $http ({
            method : "GET",
            params: { 'v': new Date().getTime() },
            url : GetCityUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.createVisit = function(data){
        var request = $http ({
            method : "POST",
            data:data,
            url : CreateVisitUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.AddEnquiryAndNeed = function(data){
        var request = $http ({
            method : "POST",
            data:data,
            url : AddEnquiryAndNeedUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.RegisterCustomer = function(data){
        var request = $http ({
            method : "POST",
            data:data,
            url : RegisterCustomerUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.UpdateEnquiry = function(data){
        var request = $http ({
            method : "POST",
            data:data,
            url : UpdateEnquiryUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.GetQuotes = function(data){
        var request = $http ({
            method : "POST",
            data:data,
            url : GetQuotesUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.getCustomerDetails = function(custId){
        var url = GetCustomerDetailsUrl+custId;
        var request = $http ({
            method : "GET",
            params: { 'v': new Date().getTime() },
            url : url
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.getGrowthMaturity = function(obj){
        var request = $http ({
            method : "POST",
            data:obj,
            url : GetGrowthMaturityBenefitUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.getEnquiryDetails = function(obj){
        var url = GetDataFromEnquiryUrl+obj;
        var request = $http ({
            method : "GET",
            params: { 'v': new Date().getTime() },
            url : url
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.saveSelection = function(obj){
        var request = $http ({
            method : "POST",
            data:obj,
            url : SaveQuoteSelectionUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.clickToCall = function(obj){
        var request = $http ({
            method : "POST",
            data:obj,
            url : ClickToCallUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.emailQuotes = function(obj){
        var request = $http ({
            method : "POST",
            data:obj,
            url : EmailQuotesUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.updateProductDetails = function(obj){
        var request = $http ({
            method : "POST",
            data:obj,
            url : UpdateProductDetailsUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.getAllCombinations = function(){
        var request = $http ({
            method : "GET",
            params: { 'v': new Date().getTime() },
            url : GetComboUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.getPlanRiders = function(obj){
        var request = $http ({
            method : "POST",
            data:obj,
            url : getPlanRiders
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.getBanner = function(){
        var request = $http ({
            method : "GET",
            url : GetBannerFromOfferId
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };

    NewInvestmentService.getInvestDataForLead = function(obj) {
        var request = $http({
            method: "POST",
            data: obj,
            url: getInvestDataForLeadUrl
        });
        return request.success(
            function (data, status, headers, config) {
            }).error(function (data, status, headers, config) {
            });
    };

    NewInvestmentService.getChatDetails = function(obj) {
        var request = $http({
            method: "POST",
            data: obj,
            url: GetChatDetailsUrl
        });
        return request.success(
            function (data, status, headers, config) {
            }).error(function (data, status, headers, config) {
            });
    };
    NewInvestmentService.generateBI = function(obj) {
        var request = $http({
            method: "POST",
            data: obj,
            url: GenerateBIUrl
        });
        return request.success(
            function (data, status, headers, config) {
            }).error(function (data, status, headers, config) {
            });
    };

    NewInvestmentService.getChartData = function(){
        var request = $http ({
            method : "GET",
            url : GetChartDatUrl
        });
        return request.success(
            function(data, status, headers, config) { }).error(function(data, status, headers, config) { });
    };
     NewInvestmentService.SetHttpValues = function(obj) {
        var request = $http({
            method: "POST",
            data: obj,
            url: HandlerURL+"/SetHttpValues"
        });
        return request.success(
            function (data, status, headers, config) {
            }).error(function (data, status, headers, config) {
            });
    };
    NewInvestmentService.GetHttpValues = function(obj) {
        var request = $http({
            method: "POST",
            data: obj,
            url: HandlerURL+"/GetHttpValues"
        });
        return request.success(
            function (data, status, headers, config) {
            }).error(function (data, status, headers, config) {
            });
    };
	NewInvestmentService.GetLeadInfoByLeadId = function (obj) {
        var request = $http({
            method: "POST",
            data: obj,
            url: GetLeadInfoByLeadIdUrl
        });
        return request.success(
            function (data, status, headers, config) {
            }).error(function (data, status, headers, config) {
            });
    };
    NewInvestmentService.SendMobileOTP = function (obj) {
        var request = $http({
            method: "POST",
            data: obj,
            url: SendMobileOTPUrl
        });
        return request.success(
            function (data, status, headers, config) {
            }).error(function (data, status, headers, config) { });
    };
    NewInvestmentService.VerifyMobileOTP = function (obj) {
        var request = $http({
            method: "POST",
            data: obj,
            url: VerifyMobileOTPUrl
        });
        return request.success(
            function (data, status, headers, config) {
            }).error(function (data, status, headers, config) { });
    };
	NewInvestmentService.CheckInternalIP = function(){
		var request = $http({
		method:"GET",
		url:CheckInternalIPUrl
	});
	return request.success(
		function (data,status,headers,config) {
	 }).error(function(data,status,headers,config){ });
	};
	NewInvestmentService.CheckOneTwoNineIP = function(){
		var request = $http({
		method:"GET",
		url:CheckOneTwoNineIPURL
	});
	return request.success(
		function (data,status,headers,config) {
	 }).error(function(data,status,headers,config){ });
	};
	return NewInvestmentService;
}]);
