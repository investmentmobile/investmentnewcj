/**
 * Created by Prashants on 8/12/2015.
 */
app.service('CommonService', ["$rootScope", "$location", "NewInvestmentService", "$cookies", "$mdToast", "$q", "$localStorage", "$routeParams", "$filter", "$timeout","isMobile", function ($rootScope, $location, NewInvestmentService, $cookies, $mdToast, $q, $localStorage, $routeParams,$filter,$timeout,isMobile) {


    'use strict';
    var CommonService = {};

    var enquiry = '', details = {}, toolId = 0, cityItem = {}, customerDetails = {},oldObj={};

    var referer = [{name:'google'},{name:'yahoo'},{name:'bing'}];


    CommonService.IsMobileDevice = function () {
        var isMobile;
        isMobile = {
            /**
             * @return {boolean}
             */
            Android: function () {
                return navigator.userAgent.match(/Android/i) !== null;
            },
            /**
             * @return {boolean}
             */
            BlackBerry: function () {
                return navigator.userAgent.match(/BlackBerry/i) !== null;
            },
            iOS: function () {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i) !== null;
            },
            /**
             * @return {boolean}
             */
            Opera: function () {
                return navigator.userAgent.match(/Opera Mini/i) !== null;
            },
            /**
             * @return {boolean}
             */
            Windows: function () {
                return navigator.userAgent.match(/IEMobile/i) !== null;
            },
            any: function () {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };

        return isMobile.any();
    };

    CommonService.onlyAlphabetAllowed = function(val){
        var regex = /^[0-9a-zA-Z() ]*$/;
        return regex.test(val);
    };

    CommonService.visitCreation = function(){
        var newObj = {};
        newObj.key = "CustProfileCookie";
        NewInvestmentService.GetHttpValues(newObj).success(function(data){
            CommonService.createVisit(data);
        });
    };

    CommonService.createFilterForFirstValues = function(value,query) {
        var lowercaseQuery = angular.lowercase(query);
        var arr = [];
        arr = value.filter(function(ele){
            return (ele.searchString.toLowerCase().substr(0,lowercaseQuery.length).indexOf(lowercaseQuery) != -1);
        });
        return arr.slice(0, 5);
    };

    CommonService.retrieveCustIdFromCookie = function(isPlanHeadingReqd){
        var planHeading = "";
        var deferred = $q.defer();
        CommonService.GetCustIDFromCustProfileCookie().then(function(data){
            var custIDJson = {};
            if(data && data!=="") {
                custIDJson = JSON.parse(data);
            }

            var custIDd = custIDJson.CustID || 0;
            var planType;
            if(isPlanHeadingReqd) {
                if ($location.$$search.planType) {
                    planType = parseInt($location.$$search.planType);
                }
                if (planType * 1 === 1) {
                    planHeading = "Growth";
                } else if (planType * 1 === 2) {
                    planHeading = "Pension For Old Age";
                } else if (planType * 1 === 3) {
                    planHeading = "Plan Higher Education";
                }
            }else{
                if($location.$$search.planType){
                    planType =  $localStorage.searchDetails?parseInt($localStorage.searchDetails.investmentFor):parseInt($location.$$search.planType);
                }
            }
            triggerEnquiryFormStartEvent(custIDd,planType);
            deferred.resolve(planHeading);
        });
        return deferred.promise;
    };

    CommonService.whichPlanSelected = function (plans) {
        if (plans[0].selected) {
            return 2;
        } else if (plans[1].selected) {
            return 1;
        } else {
            return 3;
        }
    };

    CommonService.getAges = function (index) {
        var lowerLimit, upperLimit;
        if (index === 1) {
            lowerLimit = 5;
            upperLimit = 30;
        }
        var ages = [];
        for (var i = lowerLimit; i <= upperLimit;) {
            ages.push(i);
            i = i + 5;
        }
        if (index == 2) {
            ages = [50, 55, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 75, 80];
        }
        return ages;
    };

    CommonService.getAge = function (index) {
        if (index === 2) {
            return 65;
        } else if (index === 1) {
            return 10;
        }
    };


    CommonService.getYourAge = function(dateString){
        dateString = this.changeDateTommddYY(dateString);
        var today = new Date(),
            birthDate = new Date(dateString),
            age = today.getFullYear() - birthDate.getFullYear(),
            m = today.getMonth() - birthDate.getMonth();

        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    };

    CommonService.changeDateTommddYY = function(dateString){
        var arrDateString = dateString.split("-");
        return arrDateString[1] + "/" + arrDateString[0] + "/" + arrDateString[2];
    };


    CommonService.isValidDateOfBirth = function(dob) {
        if(angular.isUndefined(dob) || !dob) {
            return false;
        }
        var dobRegX = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;

        if(dob.match(dobRegX)) {
            var dobArray = dob.split('-'),
                date = parseInt(dobArray[0]),
                month  = parseInt(dobArray[1]),
                year = parseInt(dobArray[2]),
                listOfDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

            if (month === 1 || month > 2) {
                if (date > listOfDays[month-1]) {
                    return false;
                }
            }
            if (month === 2) {
                var leapYear = false;
                if ( ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0))  {
                    leapYear = true;
                }
                if ((leapYear === false) && (date >= 29)) {
                    return false;
                }
                if ((leapYear === true) && (date > 29)) {
                    return false;
                }
            }
            return true;
        }  else  {
            return false;
        }
    };

    CommonService.getDateList = function() {
        var dateList = [];
        for(var i=1; i<=31; i++){
            if(i <= 9) {
                dateList.push('0' + i);
            } else {
                dateList.push(i);
            }
        }
        return dateList;
    };

    CommonService.getYearList = function() {
        var currentYear = new Date().getFullYear(),
            min = currentYear - 62,
            max = currentYear - 18,
            yearList = [];

        for(var i = min; i<=max; i++){
            yearList.push(i);
        }
        return yearList;
    };

    CommonService.extractDateMonthYearFromDOB = function(dobString, index){
        var dobArray = dobString.split("-");
        return dobArray[index];
    };

    CommonService.getCorpusAmount = function (amount, payterm) {
        var array = [104.94, 105.74, 106.02, 106.18, 106.26, 106.32, 106.38, 106.43, 106.45, 106.47, 106.49, 106.59, 106.68, 106.75, 106.77, 106.80, 106.81, 106.83, 106.84, 106.85, 106.97, 107.10, 107.22, 107.35, 107.49, 107.6, 107.8, 107.9, 108.0, 108.2];
        var x = 0;
        for (var i = 0; i < payterm; i++) {
            x = Math.round(((x + amount) * array[i]) / 100);
        }
        return x;

    };

    CommonService.getInvestmentAmount = function (amount, payterm) {
        var array = [109.34, 109.3, 109.27, 109.26, 109.26, 109.27, 109.3, 109.36, 109.43, 109.52, 109.64, 109.90, 110.21, 110.55, 110.96, 111.40, 111.92, 112.50, 113.17, 113.99, 115.097, 116.495, 118.308, 120.718, 124.106, 129.23, 137.8, 154.939, 206.497, 104.94];
        var x = amount;
        for (var i = array.length - payterm; i < array.length; i++) {
            x = x / (array[i] / 100);
        }
        return x / 12;

    };

    CommonService.getTypeName = function (index) {
        if (index === 2) {
            return isMobile?'Age at which you want to retire':'Retirement Age';
        } else if (index === 1) {
            return 'No. of Years';
        } else {
            return isMobile?"Your Child's Age":"Child's Age";
        }
    };

    CommonService.getAmountList = function (firstMin, firstMax, secondMin, secondMax, firstStep, secondStep) {
        var arr = [];
        for (var i = firstMin; i <= firstMax;) {
            arr.push(i);
            i = i + firstStep;
        }
        for (var j = secondMin; j <= secondMax;) {
            arr.push(j);
            j = j + secondStep;
        }
        return arr;
    };

    // Annual investment compounding
    CommonService.compoundInterest = function (corpus, payTerm) {
        var factor = 0.07124;
        var divisor = ((Math.pow((1.07124), (payTerm)))) - 1;
        var amt = corpus * factor / divisor;
        return Math.round(amt / 12);
    };

    // Retirement Calculation
    CommonService.GetRetirementCalc = function (monthlyPension, age, payTerm) {
        var power = -12 * (age < 80 ? (80 - age) : 1);
        var divisor = 0.00667;
        var P = monthlyPension;
        var multiplier = 1 - Math.pow(1.00667, power);
        var corpus = (P * multiplier) / divisor;
        return CommonService.compoundInterest(corpus, payTerm);
    };

    CommonService.getChildAmountList = function (firstMin, firstMax, secondMax, thirdMax, firstStep, secondStep, thirdStep) {
        var arr = [];
        for (var i = firstMin; i < firstMax;) {
            arr.push(i);
            i = i + firstStep;
        }
        for (var j = firstMax; j <= secondMax;) {
            arr.push(j);
            j = j + secondStep;
        }

        for (var k = secondMax; k <= thirdMax;) {
            arr.push(k);
            k = k + thirdStep;
        }
        return arr;
    };

    CommonService.populateEnquiryDetails = function (index, details, monthlyInvestment,dob) {
        var deferred = $q.defer();
        var obj ={};
        populateForAll(index, details, monthlyInvestment,dob).then(function(data) {
            obj = data;
            obj.DesiredMonthlyPayoutRet = index === 2 ? details.amount : '';
            obj.DesiredLumpSumChild = index === 3 ? details.amount : '';
            obj.PremiumPaidGrowth = index === 1 ? Math.round(details.amount / 1000) * 1000 : '';
            deferred.resolve(obj);
        });
        return  deferred.promise;
    };

    CommonService.populateEnquiryDetailsForMob = function (index, details, monthlyInvestment,dob) {
        var deferred = $q.defer();
        var obj ={};
        populateForAll(index, details, monthlyInvestment,dob).then(function(data){
            obj = data;
            obj.DesiredMonthlyPayoutRet = index === 2 ? details.investment : '';
            obj.DesiredLumpSumChild = index === 3 ? details.investment : '';
            obj.PremiumPaidGrowth = index === 1 ? Math.round(details.investment / 1000) * 1000 : '';
            obj.LeadSource = isMobile? "PBMobile":"PB";
            obj.LeadSourceID = 15;
            deferred.resolve(obj);
        });
        return deferred.promise;
    };

    function populateForAll(index, details, monthlyInvestment,dob){
        var obj = {};
		var deferred = $q.defer();
        if ($routeParams === null || $routeParams === undefined) {
            $routeParams = {};
        }
        
        CommonService.GetCustIDFromCustProfileCookie().then(function(data){
		
            obj.UtmSource = $routeParams.utm_source || $cookies.get('pborganic')||"";
            obj.UtmTerm = $routeParams.utm_term || "";
            obj.VisitorToken = $cookies.get('Cookie_VisitToken') || "";
            obj.LandingPageName = "investmentprequote";
            obj.LeadSourceID = "15";
            obj.LeadSource = isMobile?"PBMobile":"PB";
            obj.UtmCampaign = $routeParams.utm_campaign || "";
            obj.UtmMedium = $routeParams.utm_medium || "";
            obj.ReferalUrl = document.referrer || "";
            obj.VisitID = CommonService.GetVisitIDFromVisitProfileCookie();
            // obj.VisitID = $cookies.get('visitId');
            obj.AgentId = $location.$$search.aid || "";
            obj.InvestmentTypeId = index;
            obj.Age = details.yourAge;
            obj.PayTerm = index === 1 ? details.age : index === 3 ? details.childTerm : index === 2 ? details.age - details.yourAge : '';
            if (index === 3 && (obj.PayTerm < 25 && obj.PayTerm > 20)||(obj.PayTerm < 30 && obj.PayTerm > 25) ) {
                obj.PayTerm = Math.round(obj.PayTerm / 5) * 5;
            }
            obj.RetirementAge = index === 2 ? details.age : '';
            obj.ChildAge = index === 3 ? details.age : '';
            obj.DOB = (dob.dobDate!==null && dob.dobDate.toString().length===1?'0':'') + dob.dobDate + "-" + (dob.dobMonth!==null && dob.dobMonth.toString().length===1?'0':'')+dob.dobMonth + "-" + dob.dobYear;

            obj.PremiumPaidRet = index === 2 ? (Math.round((parseInt(monthlyInvestment) * 12) / 1000) * 1000) : '';
            obj.PremiumPaidChild = index === 3 ? (Math.round((parseInt(monthlyInvestment) * 12) / 1000) * 1000) : '';
            obj.CustId =0;
            var custIDJson = {};
            if(data && data!=="") {
                custIDJson = JSON.parse(data);
            }
            obj.CustId = custIDJson.CustID || 0;
            obj.EnquiryNeed = {};
            obj.EnquiryNeed.NeedRefID = 0;
            obj.EnquiryNeed.InvestTypeId = index;
            obj.EnquiryNeed.Age = details.yourAge;
            obj.EnquiryNeed.RetirmentAge = index === 2 ? details.age : '';
            obj.EnquiryNeed.ChildAge = index === 3 ? details.age : '';
            obj.EnquiryNeed.PayTerm = index === 1 ? details.age : index === 3 ? details.childTerm : index === 2 ? details.age - details.yourAge : '';
            deferred.resolve(obj);
        });
        //obj.CustId = $cookies.get('CustId') || -1;
        

        return deferred.promise;
    }

    CommonService.getPremiumForPreQuote = function (obj, index) {
        var deferred = $q.defer();
        var newObj = {"payterm": obj.payterm, "premium": obj.premium};
        if (index === 1) {
            NewInvestmentService.getGrowthMaturity(newObj).success(function (data) {
                if (!CommonService.isEmptyObject(data)) {
                    deferred.resolve(data);
                }
            });
            return deferred.promise;
        }
    };

    CommonService.saveSearchDetails = function (index, details, monthlyInvestment,dobObj) {
        var obj = saveForAll(index, details, monthlyInvestment,dobObj);
        var amt = (index === 1 ? Math.round(details.amount / 1000) * 1000 : (Math.round((monthlyInvestment * 12) / 1000) * 1000));
        obj.investment = amt > 5000000 ? 5000000 : amt;
        obj.requiredAmt = (index !== 1 ? Math.round(details.amount / 1000) * 1000 : 0);
        return obj;
    };

    CommonService.saveSearchDetailsForMob = function(index, details, monthlyInvestment,dobObj) {
        var obj = saveForAll(index, details, monthlyInvestment,dobObj);
        var number = 0;
        if(details.investment!==undefined && details.investment !==null ){
            number = isNaN(details.investment)?details.investment.replace(/,/g, ''):details.investment;
        }
        var amt = (index === 1 ? Math.round(number/ 1000) * 1000 : (Math.round((monthlyInvestment * 12) / 1000) * 1000));
        obj.investment = amt > 5000000 ? 5000000 : amt;
        obj.requiredAmt = (index !== 1 ? Math.round(number / 1000) * 1000 : 0);
        return obj;
    };

    function saveForAll(index, details, monthlyInvestment,dobObj){
        var obj = {};

        var term = index === 1 ? details.age : index === 2 ? details.age - details.yourAge : details.childTerm;
        if (index === 2 && (term < 25 && term > 20)||(term < 30 && term > 25) ) {
            term = Math.round(term / 5) * 5;
        }
        obj.payTerm = term <= 30 ? term : 30;
        obj.childAge = index === 3 ? details.age : 0;
        obj.retirementAge = index === 2 ? details.age : 65;
        obj.investmentFor = index;
        obj.yourAge = details.yourAge;
        obj.requiredAmt = (index !== 1 ? Math.round(details.amount / 1000) * 1000 : 0);
        obj.dobDate = dobObj.dobDate;
        obj.dobMonth = dobObj.dobMonth;
        obj.dobYear = dobObj.dobYear;
        obj.DOB = (dobObj.dobDate!==null && dobObj.dobDate!==undefined && dobObj.dobDate.length===1?'0':'') + dobObj.dobDate + "-" + (dobObj.dobMonth!==null && dobObj.dobMonth!==undefined && dobObj.dobMonth.length===1?'0':'')+dobObj.dobMonth + "-" + dobObj.dobYear;
        obj.annualIncome = details.annual;
        return obj;
    }

    function checkReferer(){
        for(var refer in referer) {
            if (document.referrer.indexOf(refer.name)){
                return ;
            }
        }
    }

    CommonService.createVisit = function (custprofileCookie) {
        var obj = {}, visitId;
        if ($routeParams === null || $routeParams === undefined) {
            $routeParams = {};
        }
        var isMobile = CommonService.IsMobileDevice();
        obj.UtmSource = $routeParams.utm_source ||checkReferer() ||"";
        obj.UtmTerm = $routeParams.utm_term || "";
        obj.VisitorToken = $cookies.get('Cookie_VisitToken') || "";
        obj.LandingPageName = "investmentprequote";
        obj.LeadSourceID = "15";
        obj.LeadSource = isMobile? "PBMobile":"PB";
        obj.UtmCampaign = $routeParams.utm_campaign || "";
        obj.UtmMedium = $routeParams.utm_medium || "";
        obj.ReferalUrl = document.referrer || "";
        obj.VisitID = 0;
        var visitToken = "";
        if(!$cookies.get('VisitProfileCookie'))
        {
            NewInvestmentService.createVisit(obj).success(function (data) {
                if (!data.Data.ResponseDetails.HasError) {
                    var visitCookieobject ={};
                      visitCookieobject.UtmCampaign = data.Data.UtmCampaign;
                      visitCookieobject.UtmMedium= data.Data.UtmMedium;
                      visitCookieobject.UtmSource= data.Data.UtmSource;
                      visitCookieobject.UtmTerm= data.Data.UtmTerm;
                      visitCookieobject.visitorToken= data.Data.VisitorToken;
                      visitCookieobject.visitId= data.Data.VisitID;
                   // visitId = data.Data.VisitID;
                      visitToken = data.Data.VisitorToken;
                   // var now1 = new Date();
                    // this will set the expiration to 24 hours
                   // var exp1 = new Date(now1.getFullYear(), now1.getMonth(), now1.getDate() + 1);
                   var date = new Date();
                  
                   var exp1 = data.Data.ResponseDetails.ReturnValue;
                   date.setTime(date.getTime() + (exp1 * 60 * 1000));
                   var jsonResult = JSON.stringify(visitCookieobject);
                   //CommonService.setCookies('VisitProfileCookie',jsonResult);
                        $cookies.put('VisitProfileCookie', jsonResult, {
                        expires: date,
                        domain: configObj.getCookieDomain()
                        });
                    var objCust = {};
                    if(custprofileCookie.d===""){

                        objCust.VisitorToken=visitToken;
                        objCust.CustID = 0;
                        objCust.SocialProfileID = 0;
                        var value ="'"+ JSON.stringify(objCust)+"'";
                        var dataToSend = '{"key":"CustProfileCookie","value":'+value+',"expires":-1}';
                        NewInvestmentService.SetHttpValues(dataToSend);
                    } else{
                        var custCookieObj = {};
                        custCookieObj.key = 'CustProfileCookie';
                        NewInvestmentService.GetHttpValues(custCookieObj).success(function(response){
                        var custProfileData = JSON.parse(response.d);
                        objCust.VisitorToken=custProfileData.VisitorToken;
                        objCust.CustID = custProfileData.CustID;
                        objCust.SocialProfileID = custProfileData.SocialProfileID || 0;       
                        var value ="'"+ JSON.stringify(objCust)+"'";
                        var dataToSend = '{"key":"CustProfileCookie","value":'+value+',"expires":-1}';
                        NewInvestmentService.SetHttpValues(dataToSend);  
						visitCookieobject.visitorToken = custProfileData.VisitorToken;
                         var jsonResult = JSON.stringify(visitCookieobject);
                   //CommonService.setCookies('VisitProfileCookie',jsonResult);
                        $cookies.put('VisitProfileCookie', jsonResult, {
                        expires: date,
                        domain: configObj.getCookieDomain()
                        });
                     });
                     
                     
                    }
                // //CommonService.setCookies('visitId', visitId);
                } else {
                }
            }).error(function (data) {

        });
    }
    else
    {
               var objVisitProfileCookie = JSON.parse($cookies.get('VisitProfileCookie'));
                var objCust = {};
                if(custprofileCookie.d==="")
                {
                    objCust.VisitorToken=objVisitProfileCookie.visitorToken;
                    //objCust.VisitorToken=data.Data.VisitorToken;
                    objCust.CustID = 0;
                    objCust.SocialProfileID = 0;
                    var value ="'"+ JSON.stringify(objCust)+"'";
                    var dataToSend = '{"key":"CustProfileCookie","value":'+value+',"expires":-1}';
                    NewInvestmentService.SetHttpValues(dataToSend);
                }
                else
                {
                 var custCookieObj = {};
                        custCookieObj.key = 'CustProfileCookie';
                        NewInvestmentService.GetHttpValues(custCookieObj).success(function(response){
                        var custProfileData = JSON.parse(response.d);
                        objCust.VisitorToken=objVisitProfileCookie.visitorToken;  
                       // objCust.VisitorToken=data.Data.VisitorToken;  
                        objCust.CustID = custProfileData.CustID;
                        objCust.SocialProfileID = custProfileData.SocialProfileID || 0;       
                        var value ="'"+ JSON.stringify(objCust)+"'";
                        var dataToSend = '{"key":"CustProfileCookie","value":'+value+',"expires":-1}';
                        NewInvestmentService.SetHttpValues(dataToSend);  
                     });
                }
    }

    };

    function checkIfDOBChangedAndCookieExists(dob,index,details) {
        var redirect = false;
        if($localStorage.searchDetails!==undefined) {
            if ($localStorage.searchDetails.investmentFor*1 === index*1) {
                redirect = false;
                if(index*1===1){
                    redirect = details.age*1 === $localStorage.searchDetails.payTerm*1;
                }else if(index*1 === 2){
                    redirect = details.age*1 === $localStorage.searchDetails.retirementAge*1;
                }else if(index*1 === 3){
                    redirect = details.age*1 === $localStorage.searchDetails.childAge*1;
                }
            }

            redirect  = redirect && (dob === $localStorage.searchDetails.DOB && (angular.isDefined($cookies.get("shouldPageSkip")) && $cookies.get("shouldPageSkip")));

        }

        return redirect;

    }

    CommonService.saveEnquiry = function (index, details, monthlyInvestment,dateObj) {
        var redirectionToQuotes = false;
        var deferred = $q.defer();
        CommonService.populateEnquiryDetails(index, details, monthlyInvestment,dateObj).then(function(obj){
        NewInvestmentService.AddEnquiryAndNeed(obj).success(function (data) {
            if (!data.Data.ResponseDetails.HasError) {
                $localStorage.equiryid = data.Data.EnquiryId;
                CommonService.saveEnquiryId(data.Data.EnquiryId);
                $location.$$search.enquiryId = CommonService.encode(data.Data.EnquiryId, false);
                //triggerEnquiryGenerationEvent(data.Data.EnquiryId,$cookies.get('CustId'));obj
                triggerEnquiryGenerationEvent(data.Data.EnquiryId,obj.CustId,index);
                var dateOfBirth = (dateObj.dobDate!==null && dateObj.dobDate.length===1?'0':'') + dateObj.dobDate + "-" + (dateObj.dobMonth!==null && dateObj.dobMonth.length===1?'0':'')+dateObj.dobMonth + "-" + dateObj.dobYear;
                redirectionToQuotes = checkIfDOBChangedAndCookieExists(dateOfBirth,index,details);
                $localStorage.searchDetails = CommonService.saveSearchDetails(index, details, monthlyInvestment,dateObj);
                $localStorage.monthlyStatus = null;

                $rootScope.slideFlag = 'right';
                var payTerm = ($localStorage.searchDetails!==undefined && $localStorage.searchDetails.payTerm>0)?$localStorage.searchDetails.payTerm:10;
                if(payTerm<8){
                    $localStorage.toolId=2;
                }else {
                    $localStorage.toolId=4;
                }
                var queryString = "?";
                if($location.$$search.utm_source!=="" && $location.$$search.utm_source!==null &&$location.$$search.utm_source!==undefined){
                    queryString = queryString+"utm_source="+$location.$$search.utm_source+'&';
                }

                if($location.$$search.utm_medium!=="" && $location.$$search.utm_medium!==null &&$location.$$search.utm_medium!==undefined){
                    queryString = queryString+"utm_medium="+$location.$$search.utm_medium+'&';
                }
                if($location.$$search.utm_term!=="" && $location.$$search.utm_term!==null &&$location.$$search.utm_term!==undefined){
                    queryString = queryString+"utm_term="+$location.$$search.utm_term+'&';
                }

                if($location.$$search.utm_campaign!=="" && $location.$$search.utm_campaign!==null &&$location.$$search.utm_campaign!==undefined){
                    queryString = queryString+"utm_campaign="+$location.$$search.utm_campaign+'&';
                }

                if($location.$$search.enquiryId!=="" && $location.$$search.enquiryId!==null &&$location.$$search.enquiryId!==undefined){
                    queryString = queryString+"enquiryId="+$location.$$search.enquiryId+'&';
                }

                if($rootScope.isIFrame) {
                    window.open(configObj.getCustomerPageUrl(queryString), "_parent");
                    $rootScope.isIFrame = false;
                }else {
                    NewInvestmentService.getCustomerDetails(obj.CustId).success(function(data){
                        $localStorage.customerDetails = CommonService.setCustomerValuesToLocalStorage(data);

                        if(redirectionToQuotes){
                            $location.$$search.leadId =
                            $location.path('/quote', true);
                        }else {
                            $location.path('/customer', true);
                        }
                    });
                     
                    // populate customer details based on cust id cookie
                    
                }
            } else {
                CommonService.showQuoteToast();
            }
            deferred.resolve(true);
        }).error(function () {
            CommonService.showQuoteToast();
            $rootScope.loaderProceed = false;
            deferred.reject(false);
        });

		});
        return deferred.promise;
		};
   
	


    CommonService.saveEnquiryAndGenerateLead = function(index, details, monthlyInvestment,dateObj,cityObj){
        var deferred = $q.defer();
      //  var obj = CommonService.populateEnquiryDetailsForMob(index, details, monthlyInvestment,dateObj);
        CommonService.populateEnquiryDetailsForMob(index, details, monthlyInvestment,dateObj).then(function(obj){
        NewInvestmentService.AddEnquiryAndNeed(obj).success(function (data) {
            if (!data.Data.ResponseDetails.HasError) {
                $localStorage.equiryid = data.Data.EnquiryId;
                CommonService.saveEnquiryId(data.Data.EnquiryId);
                $location.$$search.enquiryId = CommonService.encode(data.Data.EnquiryId, true);
                //triggerEnquiryGenerationEvent(data.Data.EnquiryId,$cookies.get('CustId'));
                //triggerEnquiryGenerationEvent(data.Data.EnquiryId,obj.CustId,index);
                $localStorage.searchDetails = CommonService.saveSearchDetailsForMob(index, details, monthlyInvestment,dateObj);
                $rootScope.slideFlag = 'right';
                var payTerm = ($localStorage.searchDetails!==undefined && $localStorage.searchDetails.payTerm>0)?$localStorage.searchDetails.payTerm:10;
                if(payTerm<8){
                    $localStorage.toolId=2;
                }else {
                    $localStorage.toolId=4;
                }
                var queryString = "?";
                if($location.$$search.utm_source!=="" && $location.$$search.utm_source!==null &&$location.$$search.utm_source!==undefined){
                    queryString = queryString+"utm_source="+$location.$$search.utm_source+'&';
                }

                if($location.$$search.utm_medium!=="" && $location.$$search.utm_medium!==null &&$location.$$search.utm_medium!==undefined){
                    queryString = queryString+"utm_medium="+$location.$$search.utm_medium+'&';
                }
                if($location.$$search.utm_term!=="" && $location.$$search.utm_term!==null &&$location.$$search.utm_term!==undefined){
                    queryString = queryString+"utm_term="+$location.$$search.utm_term+'&';
                }

                if($location.$$search.utm_campaign!=="" && $location.$$search.utm_campaign!==null &&$location.$$search.utm_campaign!==undefined){
                    queryString = queryString+"utm_campaign="+$location.$$search.utm_campaign+'&';
                }

                if($location.$$search.enquiryId!=="" && $location.$$search.enquiryId!==null &&$location.$$search.enquiryId!==undefined){
                    queryString = queryString+"enquiryId="+$location.$$search.enquiryId+'&';
                }
                CommonService.generateLeadOnFirst(index, details, monthlyInvestment,dateObj,cityObj,data.Data.EnquiryId,queryString);

            } else {
                CommonService.showQuoteToast();
            }
            deferred.resolve(true);
        }).error(function () {
            CommonService.showQuoteToast();
            $rootScope.loaderProceed = false;
            deferred.reject(false);
        });

    });
        return deferred.promise;
    };

    CommonService.generateLeadOnFirst = function(index, details, monthlyInvestment,dateObj,cityObj,enqId,queryString){
        var obj = {};
        obj.CustomerName = details.CustomerName;
        obj.MobileNo = details.MobileNo;
        obj.Email = details.Email;
        obj.CountryID = details.CountryID;
        obj.GenderID = details.gender||1;
        obj.CityID = cityObj.CityID;
        obj.City = cityObj.City;
        obj.stateId = cityObj.stateId;
        obj.countryCode = cityObj.countryCode;
        $localStorage.customerDetails = obj;
        CommonService.saveCustomerDetails(obj);

        var searchObj = $localStorage.searchDetails||{};
        searchObj.gender = details.gender;
        CommonService.setSearchDetails(searchObj);
        $localStorage.searchDetails = searchObj;

		$localStorage.monthlyStatus = null;
		CommonService.GetCustIDFromCustProfileCookie().then(function(data){
                        var SocialProfileID =0;
                            var custIDJson = {};
                            if(data!=="" && data) {
                                custIDJson = JSON.parse(data);
                            }
                        SocialProfileID = custIDJson.SocialProfileID || 0;
        NewInvestmentService.RegisterCustomer(populateCustomerData(details,cityObj,enqId)).success(function (data) {
            if (!data.ResponseDetails.HasError) {
				var custObj  ={};
                                var objCust ={};
                                custObj.key ="CustProfileCookie";
                                NewInvestmentService.GetHttpValues(custObj).success(function(response){
                                    var customerProfileData = {};
                                    if(response.d && response.d!=="") {
                                        customerProfileData = JSON.parse(response.d);
                                    }
                                objCust.CustID=data.CustId;
                                objCust.VisitorToken=customerProfileData.VisitorToken;
                                objCust.SocialProfileID=customerProfileData.SocialProfileID || "";           
                                var value ="'"+ JSON.stringify(objCust)+"'";
                                var dataToSend = '{"key":"CustProfileCookie","value":'+value+',"expires":-1}';
                                NewInvestmentService.SetHttpValues(dataToSend);  
                                });
                CommonService.goToQuotesPage(enqId,queryString);
            } else {
                CommonService.showQuoteToast();
            }
        }).error(function () {
            CommonService.showQuoteToast();
        });
});
    };

    function populateCustomerData(details,cityObj,enquiryId){
        var obj = {};
        obj.IsAssistanceReq = !details.IsAssistanceReq;
        obj.CountryID = details.CountryID;
        obj.CustomerName = details.CustomerName;
        obj.Email = details.Email;
        obj.MobileNo = details.MobileNo;
        obj.GenderID = details.gender;
        obj.IsNRI = details.CountryID*1!=392;
        obj.Title = details.gender===1?'Mr':'Ms/Mrs';
        obj.CityId = cityObj.CityID;
        obj.StateId = cityObj.stateId;
        obj.countryCode = cityObj.countryCode;
        obj.EnquiryId = enquiryId;
        return obj;
    }
CommonService.setCustomerValuesToStorage = function(customerEntity,selectedCityItem,countryCode){
        var obj = {};
        obj.CustomerName = customerEntity.CustomerName;
        obj.MobileNo = customerEntity.MobileNo;
        obj.Email = customerEntity.Email;
        obj.CountryID = customerEntity.CountryID;
        obj.GenderID = customerEntity.gender || customerEntity.GenderID;
        if(customerEntity.CountryID*1!==392){
            obj.CityID = 9999;
            obj.City = 999;
            obj.stateId = 999;
        }else {
            obj.CityID = selectedCityItem !== undefined ? selectedCityItem.cityId : customerEntity.CityID;
            obj.City = selectedCityItem !== undefined ? selectedCityItem.value : customerEntity.CityId;
            obj.stateId = selectedCityItem !== undefined ? selectedCityItem.stateId : customerEntity.StateID;
        }
        obj.countryCode = countryCode;

        return obj;
    };

    CommonService.setCustomerValuesToLocalStorage = function(customerEntity){
        var obj = {};
        obj.CustomerName = customerEntity.CustomerName;
        obj.MobileNo = customerEntity.MobileNo;
        obj.Email = customerEntity.Email;
        obj.CountryID = $localStorage.customerDetails!==undefined?$localStorage.customerDetails.CountryID:"392";
        obj.GenderID = customerEntity.gender || customerEntity.GenderID;
        if(customerEntity.CountryID*1!==392){
            obj.CityID = 9999;
            obj.City = 999;
            obj.stateId = 999;
        }else {
            obj.CityID = customerEntity.CityID;
            obj.City = customerEntity.CityId;
            obj.stateId = customerEntity.StateID;
        }

        return obj;
    };

    CommonService.getTopCitiesList = function() {
        var topCities = [
            {cityId: 551, name: "Delhi", stateId: 35, state: "Delhi",value:"Delhi(Delhi)"},
            {cityId: 302, name: "Mumbai", stateId: 20, state: "Maharashtra",value:"Mumbai(Maharashtra)"},
            {cityId: 207, name: "Bengaluru", stateId: 16, state: "Karnataka",value:"Bengaluru(Karnataka)"},

            {cityId: 309, name: "Pune", stateId: 20, state: "Maharashtra",value:"Pune(Maharashtra)"},
            {cityId: 837, name: "Hyderabad", stateId: 37, state: "Telangana",value:"Hyderabad(Telangana)"},
            {cityId: 541, name: "Kolkata", stateId: 34, state: "West Bengal",value:"Kolkata(West Bengal)"},
            {cityId: 420, name: "Chennai", stateId: 30, state: "Tamil Nadu",value:"Chennai(Tamil Nadu)"},
            {cityId: 555, name: "Gurgaon", stateId: 36, state: "NCR",value:"Gurgaon(NCR)"},
            {cityId: 103, name: "Ahmedabad", stateId: 11, state: "Gujarat",value:"Ahmedabad(Gujarat)"},
            {cityId: 553, name: "Noida", stateId: 36, state: "NCR",value:"Noida(NCR)"},
            {cityId: 316, name: "Thane", stateId: 20, state: "Maharashtra",value:"Thane(Maharashtra)"},
            {cityId: 411, name: "Jaipur", stateId: 28, state: "Rajasthan",value:"Jaipur(Rajasthan)"},
            {cityId: 666, name: "Navi Mumbai", stateId: 20, state: "Maharashtra",value:"Navi Numbai(Maharashtra)"}];
        return topCities;

        //return topCities.sort(this.dynamicSort("name"));
    };

    CommonService.getSearchDetails = function () {
        return details;
    };

    CommonService.setSearchDetails = function (searhDetails) {
        details = searhDetails;
    };

    CommonService.getEnquiryId = function () {
        return enquiry;
    };

    CommonService.saveEnquiryId = function (enquiryid) {
        enquiry = enquiryid;
    };

    CommonService.getSelectedTool = function () {
        return toolId;
    };

    CommonService.saveSelectedTool = function (tool) {
        toolId = tool;
    };

    CommonService.saveCityDetails = function (city) {
        cityItem = city;
    };

    CommonService.getCityDetails = function () {
        return cityItem;
    };

    CommonService.saveCustomerDetails = function (customer) {
        customerDetails = customer;
    };

    CommonService.getCustomerDetails = function () {
        return customerDetails;
    };

    CommonService.prefillCityChangeObject = function(cityChangeObject){
        cityChangeObject.isNRI = false;
        cityChangeObject.minValue = 2;
        cityChangeObject.isEditable = false;
        cityChangeObject.citySearchText = "Others";
        return cityChangeObject;
    };

    CommonService.prefillCustomerEntityObject = function(customerEntity,obj){
        customerEntity.CustomerName = obj.CustomerName;
        customerEntity.Email = obj.Email;
        customerEntity.MobileNo = obj.MobileNo||'';
        customerEntity.gender = obj.GenderID;
        customerEntity.IsAssistanceReq = false;
    };

    CommonService.getInvestmentAmountFromCommas = function (x,val) {
        if (val !== undefined) {
            if (val.investment !== undefined) {
                if (isNaN(val.investment)) {
                    x = val.investment.replace(/,/g, '');
                } else {
                    x = val.investment;
                }
            }
        }
        return x*1;
    };

    CommonService.getBlurObjectChange = function (value,blurObj){
        if (value === 'customer') {
            blurObj.customerNameBlurOut = true;
        } else if (value === 'email') {
            blurObj.emailBlurOut = true;
        } else if (value === 'mobile') {
            blurObj.mobileBlurOut = true;
        } else if (value === 'country') {
            blurObj.countryBlurOut = true;
        } else if (value === 'city') {
            blurObj.cityBlurOut = true;
        } else if (value === 'investment') {
            blurObj.investmentBlurOut = true;
        }

        return blurObj;
    };

    CommonService.countUpValues = function(value,monthlyInvestment,previousValue) {
        if (value> 0) {
            monthlyInvestment = value;
            var options = {
                useEasing: true,
                useGrouping: true,
                separator: ',',
                decimal: '.',
                prefix: 'Rs.',
                suffix: ''
            };
            $timeout(function () {
                var demo = new CountUp("myTargetElement", previousValue, value, 0, 1.5, options);
                demo.start(function () {
                    /*$scope.$apply(function () {

                     });*/
                });
                previousValue = value;
            }, 10);

        }
        return monthlyInvestment;
    };

    CommonService.changeCityChangeObj = function(countryCodes,cityChangesObj, countryId, obj) {
        _.each(countryCodes, function (val) {
            if (val.CountryCodeId * 1 === countryId * 1) {
                if (val.CountryCode !== '') {
                    cityChangesObj.isEditable = false;
                    cityChangesObj.countryCode = val.CountryCode;
                } else {
                    cityChangesObj.isEditable = true;
                    cityChangesObj.countryCode = obj.countryCode * 1 || "";
                }
            }
        });

        return cityChangesObj;
    };

    CommonService.CheckForExistingCountryCodes = function(cityChangesObj,countryCodes){
        var hasToChange = false;
        var  obj = {};
        if(cityChangesObj.isEditable){
            obj = _.find(countryCodes, function(val){
                return (val.CountryCode * 1 === cityChangesObj.countryCode * 1);
            });
            hasToChange = !CommonService.isEmptyObject(obj);
        }
        return hasToChange;
    };

    CommonService.getPayoutBeforeTaxMin = function (annualReturnsMin, quarterly, amount) {
        var interestToBeMultiplied = 1 + (annualReturnsMin / (100 * quarterly));
        var multiplicationFactor = Math.pow(interestToBeMultiplied, (5 * quarterly));
        return Math.round(amount * multiplicationFactor);
    };

    CommonService.getPayoutBeforeTaxMax = function (annualReturnsMax, quarterly, amount) {
        var interestToBeMultiplied = 1 + (annualReturnsMax / (100 * quarterly));
        var multiplicationFactor = Math.pow(interestToBeMultiplied, (5 * quarterly));
        return Math.round(amount * multiplicationFactor);
    };

    CommonService.getAfterTaxReturnRateMin = function (value, amount) {
        var amt = value / amount;
        var x = Math.pow(amt, 1 / 5);
        return ((x - 1) * 100).toFixed(2) || 0;
    };

    CommonService.getAfterTaxReturnRateMax = function (value, amount) {
        var amt = value / amount;
        var x = Math.pow(amt, 1 / 5);
        return ((x - 1) * 100).toFixed(2) || 0;
    };

    // Check if an object is empty
    CommonService.isEmptyObject = function (obj) {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop))
                return false;
        }
        return true;
    };

    CommonService.AddCommas = function (value) {
    if (value !== undefined) {
        value = value.toString().replace(/,/g, '');
        value += '';
        var x = value.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var lastThree = x1.substring(x1.length - 3);

        var otherNumbers = x1.substring(0, x1.length - 3);
        if (otherNumbers !== '')
            lastThree = ',' + lastThree;
        var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
        return res + x2;
    } else {
        return "";
    }
    };

    CommonService.AmountToString = function (value) {
        if (value !== undefined && value !== null) {
            value = value.toString().replace(/,/g, '');
            var num = Math.floor(value).toString(),
                numLength = num.length,
                displayNum = '',
                suffix = '';
            if (numLength < 4) {
                displayNum = num;
            } else if (numLength == 4) {
                displayNum = num / 1000;
                suffix = (displayNum > 1) ? "K" : "K";
            } else if (numLength == 5) {
                displayNum = num / 1000;
                suffix = (displayNum > 1) ? "K" : "K";
            } else if (numLength == 6) {
                displayNum = num / 100000;
                suffix = (displayNum > 1) ? "Lac" : "Lac";
            } else if (numLength == 7) {
                displayNum = num / 100000;
                suffix = (displayNum > 1) ? "Lac" : "Lac";
            } else if (numLength == 8) {
                displayNum = num / 10000000;
                suffix = (displayNum > 1) ? "Cr" : "Cr";
            } else if (numLength >= 9) {
                displayNum = num / 10000000;
                suffix = (displayNum > 1) ? "Cr" : "Cr";
            }
            if (parseInt(displayNum) > 9) {
                displayNum = Math.floor(10 * displayNum) / 10;
            } else {
                displayNum = Math.floor(100 * displayNum) / 100;
            }
            return displayNum + " " + suffix;
        } else {
            return 0;
        }
    };

    CommonService.getCity = function (data) {
        var allData = data;
        var callback = function (allData) {
            var displayData = allData.CityName;
            var disValue = allData.CityName;
            var searchString = allData.CityName;

            return {
                display: displayData,
                value: disValue,
                searchString: searchString,
                stateId: allData.StateId,
                cityId: allData.CityId
            };
        };
        return allData.map(callback);
    };

    CommonService.findCityFromText = function (city, citySearchText) {
        var obj = {};
        if(citySearchText!==null && citySearchText!==undefined && citySearchText!=='') {
            citySearchText = citySearchText.replace(/ /g, '');
            if (citySearchText.length > 0) {
                _.each(city, function (val, key) {
                    var txt = val.value;
                    var index = val.value.indexOf('(');
                    txt = txt.substr(0, index).replace(/ /g, '');
                    if (citySearchText.toLowerCase() === txt.toLowerCase()) {
                        obj = val;

                    }
                });
            }
        }
        return obj;
    };

    CommonService.findCityFromId = function (city, cityId) {
        var obj = {};
        _.each(city, function (value, key) {
            var id = value.cityId;
            if (id === cityId) {
                obj = value;

            }
        });
        return obj;
    };

    CommonService.addHistoricalData = function (data, term) {
        _.each(data, function (value) {
            value.historicalReturns = CommonService.calculateHistoricalReturn(value.AnnualPremium, value.DebtFactorValue, term);
            value.historicalReturnsEquity = CommonService.calculateHistoricalReturn(value.AnnualPremium, value.EquityFactorValue, term);
        });
    };

    CommonService.calculateHistoricalReturn = function (a, r, n) {
        if (r !== null) {
            var rate = (100 + r) / 100;
            var x = (1 - Math.pow(rate, (n)));
            var y = 1 - rate;
            var tr = Math.round((a * x) / y);
            return Math.round(tr + tr * r / 100);
        } else {
            return 0;
        }
    };

    CommonService.setCookies = function (key, value) {
        var now = new Date(),
        // this will set the expiration to 12 months
            exp = new Date(now.getFullYear() + 1, now.getMonth(), now.getDate());

        $cookies.put(key, value, {
            expires: exp
        });
    };

    CommonService.showQuoteToast = function () {
        $mdToast.show(
            $mdToast.simple()
                .content('Something went wrong, Please try in sometime.')
                .position(getToastPosition())
                .hideDelay(3 * 1000)
        );
    };

    var toastPosition = {
        bottom: false,
        top: true,
        left: false,
        right: true
    };

    var getToastPosition = function () {
        return Object.keys(toastPosition)
            .filter(function (pos) {
                return toastPosition[pos];
            })
            .join(' ');
    };

    CommonService.encode = function (value, hasToUrlEncode) {
        if (value !== undefined) {
            value = value.toString();
            var encodedValue = Base64.encode(value);
            if (hasToUrlEncode) {
                encodedValue = encodeURIComponent(encodedValue);
            }
            return encodedValue;
        }
    };

    CommonService.decode = function (value, urlDecode) {
        if (value !== undefined) {
            value = value.toString();
            if (urlDecode) {
                value = decodeURIComponent(value);
            }

            return Base64.decode(value);
        }
    };

    CommonService.prefillValuesFromService = function (details, data) {
        if ((details === null || CommonService.isEmptyObject(details))) {
            if (!Number(details.investment)) {
                if (details.investment === undefined || (!Number(details.investment.replace(/,/g, '')) )) {
                    details.investment = data.InvestmentTypeId === 1 ? data.PremiumPaidGrowth : data.InvestmentTypeId === 2 ? data.PremiumPaidRet : data.PremiumPaidChild;
                } else {
                    details.investment = data.InvestmentTypeId === 1 ? data.PremiumPaidGrowth : data.InvestmentTypeId === 2 ? data.PremiumPaidRet : data.PremiumPaidChild;
                }
            }
        } else {

            if($localStorage.monthlyStatus!==null){
                if($localStorage.monthlyStatus*1===2){
                    details.investment = details.investment.toString().replace(/,/g, '') *12;
                }
            }

        }

        if (details.investment < 10000) {
            details.investment = 10000;
        } else if (details.investment > 200000) {
            details.investment = 200000;
        }


        if($localStorage.monthlyStatus!==null){
            if($localStorage.monthlyStatus*1===2){
                details.investment = details.investment.toString().replace(/,/g, '') *1;
            }
        }

        details.payTerm = details.payTerm || data.PayTerm || 20;
        if (details.payTerm > 30) {
            details.payTerm = 30;
        }
        details.gender = details.gender || 1;
        details.investmentFor = data.InvestmentTypeId;
        details.payOutStart = details.payOutStart || angular.copy(details.payTerm);
        details.yourAge = data.Age;
        details.age = data.ChildAge;
        return details;
    };

    CommonService.initializeValues = function (details) {
        var scopeDetails = angular.copy(details);

        scopeDetails.investmentType = 1;

        //scopeDetails.investmentType = 2;
        var numInvestment = details.investment.toString().replace(/,/g, '') *1;
        /*scopeDetails.investment = Math.round(numInvestment / 12);*/

        scopeDetails.payOutStart = details.payOutStart;
        scopeDetails.investment = CommonService.AddCommas(scopeDetails.investment);
        scopeDetails.spaninvestment = scopeDetails.investment;
        scopeDetails.spaninvestment = CommonService.AmountToString(scopeDetails.spaninvestment);
        scopeDetails.amountFocusLost = true;
        return scopeDetails;
    };

    CommonService.createChildTermList = function (value) {
        var l = [];
        value = parseInt(value);
        l.push(value + 10);
        l.push(value + 15);
        l.push(value + 20);

        return l;
    };

    CommonService.populateObjForQuotes = function (scopeDetails, details, id) {
        var postObj = {};
        var investment = parseInt(scopeDetails.investment.toString().replace(/,/g, ''));
        postObj.Age = details.yourAge;
        postObj.ChildAge = details.investmentFor === 3 ? details.age : '';
        postObj.GenderID = details.gender;
        postObj.PlanCategoryID = details.investmentFor;
        var amt = scopeDetails.investmentType === 2 ? (investment * 12) : investment;
        if (amt < 100000) {
            amt = (Math.round(amt / 1000)) * 1000;
        } else if (amt > 100000) {
            amt = (Math.round(amt / 10000)) * 10000;
        }
        postObj.AnnualPremium = amt;
        postObj.PayTerm = scopeDetails.payTerm;
        postObj.PolicyTerm = scopeDetails.payOutStart;
        return postObj;
    };


    CommonService.addIRRValues = function (dummyQuotes) {
        var list = [];
        list = angular.copy(dummyQuotes);
        _.each(list, function (value, key) {
            var IRR = parseInt(value.IRR * 100);
            var IRREquity = parseInt(value.IRREquity * 100);
            value.totalCost = (800 - IRR) / 100;
            value.totalCostEquity = (800 - IRREquity) / 100;
            value.isDisplay = false;
        });
        return list;
    };

    CommonService.applyFilters = function (item, object) {
        var isDisplay = true;
        object.toolId = parseInt(object.toolId);
        if (object.toolId !== 0) {
            isDisplay = isDisplay && item.isUlip;
        } else if (object.toolId === 0) {
            isDisplay = isDisplay && !item.isUlip;
        }

        if (object.payoutType !== undefined || object.payoutType !== null) {
            object.payoutType = parseInt(object.payoutType);
            if (object.toolId === 0 && object.payoutType !== 3) {
                isDisplay = isDisplay && object.payoutType === item.QuotePlan.MoneyReceivingWayID;
            }
        }

        if (parseInt(object.toolId) === 2) {
            isDisplay = isDisplay && (item.TotalReturn !== null && item.TotalReturn > 0);
        } else if (parseInt(object.toolId) === 4) {
            isDisplay = isDisplay && (item.TotalReturnEquity !== null && item.TotalReturnEquity > 0);
        }
        return isDisplay;
    };

    // If filter is selected the push the filter name and weightage attached to it to an array
    CommonService.getFiltersSelectedArray = function (planFilters) {
        var filterArray = [];
        angular.forEach(planFilters, function (value, key) {
            if (planFilters[key].checked) {
                filterArray.push([planFilters[key].name, planFilters[key].weight]);
            }
        });
        return filterArray;
    };

    // If addons exists add the key to an array
    function seeIfAddOnsExist(item) {
        var addOnArray = [];
        angular.forEach(item.AddOnFeatures, function (addOnValue, addOnKey) {
            if (addOnValue) {
                addOnArray.push(addOnKey);
            }
        });

        return addOnArray;
    }

    CommonService.goToQuotesPage = function (enqId,queryString) {
	    var matrixLeadID=0;
        if (Number(enqId) && parseInt(enqId) > 0) {
            NewInvestmentService.UpdateEnquiry({"enquiryEntityID": enqId}).success(function (data) {
                if (data.MatrixLeadId === null || parseInt(data.MatrixLeadId) === 0) {
                    CommonService.showQuoteToast();
                } else {
                    $location.$$search.enquiryId = CommonService.encode(enqId, true);
                    $location.$$search.leadId = CommonService.encode(data.MatrixLeadId, false);

					matrixLeadID = data.MatrixLeadId;
                    try {
						var planType,offerId;
						 planType = data.InvestmentTypeId;
						if($location.$$search.offerid){
							offerId = $location.$$search.offerid;
						}
                        executed: dataLayer.push({'LeadID':data.MatrixLeadId});
                        executed: dataLayer.push({'event':'investmentLead'});
						if((data.Age >= 25)&& planType ==2 && offerId ==2 ){
							executed: dataLayer.push({'event':'InvestmentQuote'});
						}
                    }
                    catch(e) {
                    }
                    CommonService.GetCustIDFromCustProfileCookie().then(function(data){
                        var CustId =0;
                         var custIDJson = {};
						if(data!==""){
							 custIDJson = JSON.parse(data);
						}
                        CustId = custIDJson.CustID;
                        var leadObj = {};
                        if($localStorage.searchDetails!==undefined && $localStorage.searchDetails!==null){
                            leadObj.age = $localStorage.searchDetails.yourAge;
                        }
                        if($localStorage.customerDetails!==undefined && $localStorage.customerDetails!==null){
                            leadObj.gender = $localStorage.customerDetails.GenderID==1?'Male':'Female';
                            leadObj.city = $localStorage.customerDetails.City;
                        }
                        triggerLeadGenerationEvent(matrixLeadID,enqId,CustId,planType,leadObj);
                        if($rootScope.isHeadIFrame) {
                            queryString = queryString+'leadId='+(CommonService.encode(matrixLeadID, false));
                        $timeout(function(){
                         window.open(configObj.getQuotePageUrl(queryString), "_parent");
                        },200);
                         //$rootScope.isHeadIFrame = false;
                         }else {
                            $location.path('/quote');
                         }

                });
                }
            }).error(function () {
                CommonService.showQuoteToast();
            });
        } else {
            $location.path('/');
        }

    };

    CommonService.applyFeatureWeightage = function (planFilters, quotes) {
        var selectedFilters = CommonService.getFiltersSelectedArray(planFilters);
        var totalWeightage = 0;

        angular.forEach(quotes, function (value, key) {
            value.featureWeightage = 0;
            var arrayOfAddonAllowed = seeIfAddOnsExist(value);
            angular.forEach(selectedFilters, function (filterValue) {
                if (arrayOfAddonAllowed.indexOf(filterValue[0]) != -1) {
                    value.featureWeightage += filterValue[1];
                }
            });
        });

        angular.forEach(selectedFilters, function (filterValue) {
            totalWeightage += filterValue[1];
        });

        angular.forEach(quotes, function (value, key) {
            value.percentageWeightage = Math.round((value.featureWeightage / totalWeightage) * 100);
        });
    };

    CommonService.populateFilterList = function (id) {
        var filters = [];
        var restOfPerc = 10 / 180 * 100;
        if (parseInt(id) === 1) {

            filters.push({
                name: 'LoyaltyAdditions',
                checked: true,
                'weight': (40 / 180) * 100,
                fullName: 'Loyalty Additions',
                description: ''
            });
            filters.push({
                name: 'Switches', checked: true, 'weight': (30 / 180) * 100,
                fullName: 'Free Switches',
                description: 'Using free switches, the policyholders can move their investments between various asset classes like debt,equity & money market, depending on their risk appetite and financial goals'
            });
            filters.push({
                name: 'AutomaticRebalancing',
                checked: true,
                'weight': (20 / 180) * 100,
                fullName: 'Automatic Rebalancing',
                description: 'Equity investments automatically spread out into regular amounts rather than investing at one go combat market fluctuations'
            });
            filters.push({
                name: 'CapitalProtection',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Capital Protection'
            });
            filters.push({name: 'IncomeBenefit', checked: false, 'weight': restOfPerc, fullName: 'Income Benefit'});
            filters.push({
                name: 'WaiverofPremium',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Waiver of Premium on Death'
            });
            filters.push({
                name: 'WOPOnCriticalIllness',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Waiver of Premium On Critical Illness'
            });
            filters.push({
                name: 'PremiumRedirecton',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Free Premium Redirecton'
            });
            filters.push({
                name: 'SystematicTransfer',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Systematic Transfer'
            });
            filters.push({name: 'TopUp', checked: false, 'weight': restOfPerc, fullName: 'Free Top-Up'});
            filters.push({
                name: 'LifeStageProtection',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Life Stage Protection'
            });
            filters.push({
                name: 'FreePartialWithdrawl',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Free Partial Withdrawal'
            });
        } else if (parseInt(id) === 2) {
            filters.push({
                name: 'IncomeBenefit',
                checked: true,
                'weight': (40 / 180) * 100,
                fullName: 'Income Benefit'
            });
            filters.push({
                name: 'WaiverofPremium',
                checked: true,
                'weight': (30 / 180) * 100,
                fullName: 'Waiver of Premium on Death'
            });
            filters.push({
                name: 'CapitalProtection',
                checked: true,
                'weight': (20 / 180) * 100,
                fullName: 'Capital Protection'
            });
            filters.push({
                name: 'LoyaltyAdditions',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Loyalty Additions'
            });
            filters.push({
                name: 'Switches', checked: false, 'weight': restOfPerc,
                fullName: 'Free Switches',
                description: 'Using free switches, the policyholders can move their investments between various asset classes like debt,equity & money market, depending on their risk appetite and financial goals'
            });
            filters.push({
                name: 'AutomaticRebalancing',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Automatic Rebalancing',
                description: 'Equity investments automatically spread out into regular amounts rather than investing at one go combat market fluctuations'
            });
            filters.push({
                name: 'WOPOnCriticalIllness',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Waiver of Premium On Critical Illness'
            });
            filters.push({
                name: 'PremiumRedirecton',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Free Premium Redirecton'
            });
            filters.push({
                name: 'SystematicTransfer',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Systematic Transfer'
            });
            filters.push({name: 'TopUp', checked: false, 'weight': restOfPerc, fullName: 'Free Top-Up'});
            filters.push({
                name: 'LifeStageProtection',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Life Stage Protection'
            });
            filters.push({
                name: 'FreePartialWithdrawl',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Free Partial Withdrawal'
            });
        } else if (parseInt(id) === 3) {
            filters.push({
                name: 'WaiverofPremium',
                checked: true,
                'weight': (40 / 180) * 100,
                fullName: 'Waiver of Premium on Death'
            });
            filters.push({
                name: 'WOPOnCriticalIllness',
                checked: true,
                'weight': (30 / 180) * 100,
                fullName: 'Waiver of Premium On Critical Illness'
            });
            filters.push({
                name: 'CapitalProtection',
                checked: true,
                'weight': (20 / 180) * 100,
                fullName: 'Capital Protection'
            });
            filters.push({
                name: 'LoyaltyAdditions',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Loyalty Additions'
            });
            filters.push({
                name: 'Switches', checked: false, 'weight': restOfPerc,
                fullName: 'Free Switches',
                description: 'Using free switches, the policyholders can move their investments between various asset classes like debt,equity & money market, depending on their risk appetite and financial goals'

            });
            filters.push({
                name: 'AutomaticRebalancing',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Automatic Rebalancing',
                description: 'Equity investments automatically spread out into regular amounts rather than investing at one go combat market fluctuations'

            });
            filters.push({name: 'IncomeBenefit', checked: false, 'weight': restOfPerc, fullName: 'Income Benefit'});
            filters.push({
                name: 'PremiumRedirecton',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Free Premium Redirecton'
            });
            filters.push({
                name: 'SystematicTransfer',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Systematic Transfer'
            });
            filters.push({name: 'TopUp', checked: false, 'weight': restOfPerc, fullName: 'Free Top-Up'});
            filters.push({
                name: 'LifeStageProtection',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Life Stage Protection'
            });
            filters.push({
                name: 'FreePartialWithdrawl',
                checked: false,
                'weight': restOfPerc,
                fullName: 'Free Partial Withdrawal'
            });
        }
        return filters;
    };

    function sortByProps(list, by, thenby) {
        return list.sort(function (first, second) {
            if (first[by] == second[by]) {
                return first[thenby] - second[thenby];
            }
            return second[by] - first[by];
        });
    }

    function getFirstThreePlans(quotes, filterObj) {
        var trimmedQuotesNames = [];
        var factor = "";

        if (parseInt(filterObj.filterId) === 1) {
            if (parseInt(filterObj.prediction) === 2) {
                if (parseInt(filterObj.toolId) === 4) {
                    factor = 'TotalReturnEquity';
                } else {
                    factor = 'TotalReturn';
                }
            } else if (parseInt(filterObj.prediction) === 1) {
                if (parseInt(filterObj.toolId) === 4) {
                    factor = 'historicalReturnsEquity';
                } else {
                    factor = 'historicalReturns';
                }
            }

        } else {
            factor = "TotalCost";
        }
        var q = angular.copy(quotes);
        q = q.sortByProp(factor).reverse();

        _.each(q, function (value) {
            if (value.isDisplay) {
                if (trimmedQuotesNames.length < 3) {
                    trimmedQuotesNames.push(value.QuotePlan.PlanName);
                } else {
                    return false;
                }
            }
        });

        return trimmedQuotesNames;
    }



    CommonService.updateProductDetails = function (quotes, leadId, filterObj, investmentTypeId, quote) {
        var obj = {};

        var arr = getFirstThreePlans(quotes, filterObj);
        obj.LeadID = leadId;
        obj.InvestmentTypeId = investmentTypeId;
        obj.PlanID = quote ? quote.PlanID : '';
        obj.PlanName = quote ? quote.QuotePlan.PlanName : '';
        obj.SumAssured = quote ? quote.SumInsured : '';
        obj.Premium = quote ? quote.AnnualPremium : '';
        obj.Term = quote ? quote.PolicyTerm : '';
        obj.Plan1 = arr[0];
        obj.Plan2 = arr[1];
        obj.Plan3 = arr[2];
        obj.SupplierId = quote ? quote.QuotePlan.PlanInsurer.InsurerID : '';
        obj.SupplierName = quote ? quote.QuotePlan.PlanInsurer.InsurerName : '';
        obj.TypeOfPolicy = "";
        obj.CustomerPrefID = "";
        obj.PayTerm = quote ? quote.PayTerm : '';

        NewInvestmentService.updateProductDetails(obj).success(function () {
        });
    };

    CommonService.calculatePerc = function (max, min, value) {
        var factor = 50 / (max - min);
        var diff = max - value;

        return 100 - (factor * diff);

    };

    CommonService.getMinLastNonZeroValue = function (dummyQuoteValues, someReturnValue) {
        var min = 0;
        for (var i = dummyQuoteValues.length - 1; i > 0; i--) {
            if (dummyQuoteValues[i][someReturnValue] > 0) {
                min = dummyQuoteValues[i][someReturnValue];
                break;
            }
        }
        return min;
    };

    CommonService.GetPayTermList = function (details, comboList) {
        var list = [];
        if (parseInt(details.investmentFor) === 3) {
            if (details.age < 8) {
                _.each(comboList, function (value, key) {
                    list.push(value.term);
                });
            } else if (details.age >= 8 && details.age < 13) {
                _.each(comboList, function (value, key) {
                    if (parseInt(value.term) <= 15) {
                        list.push(value.term);
                    }
                });
            } else if (details.age >= 13 && details.age < 18) {
                _.each(comboList, function (value, key) {
                    if (parseInt(value.term) < 15) {
                        list.push(value.term);
                    }
                });
            }
        } else {
            _.each(comboList, function (value, key) {
                list.push(value.term);
            });
        }

        return list;
    };

    CommonService.getPolicyTermList = function (details, payTerm, comboList) {
        var list = [];
        if (parseInt(details.investmentFor) === 3) {
            list = populateListForChild(details, payTerm, comboList);
        } else {
            _.each(comboList, function (value, key) {
                if (parseInt(payTerm) === parseInt(value.term)) {
                    for (var i = 0; i < value.policyList.length; i++) {
                        list.push({
                            'payOutStart': value.policyList[i].polTerm,
                            'noOfPlans': value.policyList[i].noOfPlans
                        });
                    }
                }
            });
        }
        return list;
    };

    function populateListForChild(details, payTerm, comboList) {
        var tempList = [];
        if (parseInt(details.age) <= 8) {
            if (parseInt(payTerm) === 10) {
                _.each(comboList, function (value, key) {
                    if (parseInt(payTerm) === parseInt(value.term)) {
                        for (var i = 0; i < value.policyList.length; i++) {
                            tempList.push({
                                'payOutStart': value.policyList[i].polTerm,
                                'noOfPlans': value.policyList[i].noOfPlans
                            });
                        }
                    }
                });
            } else if (parseInt(payTerm) === 15) {
                _.each(comboList, function (value, key) {
                    if (parseInt(payTerm) === parseInt(value.term)) {
                        for (var i = 0; i < value.policyList.length; i++) {
                            if (parseInt(value.policyList[i].polTerm) <= 15) {
                                tempList.push({
                                    'payOutStart': value.policyList[i].polTerm,
                                    'noOfPlans': value.policyList[i].noOfPlans
                                });
                            }
                        }
                    }
                });
            } else if (parseInt(payTerm) === 20) {
                _.each(comboList, function (value, key) {
                    if (parseInt(payTerm) === parseInt(value.term)) {
                        for (var i = 0; i < value.policyList.length; i++) {
                            if (parseInt(value.policyList[i].polTerm) > 15) {
                                tempList.push({
                                    'payOutStart': value.policyList[i].polTerm,
                                    'noOfPlans': value.policyList[i].noOfPlans
                                });
                            }
                        }
                    }
                });
            }
        } else if (details.age > 8 && details.age < 14) {
            if (parseInt(payTerm) === 10) {
                _.each(comboList, function (value, key) {
                    if (parseInt(payTerm) === parseInt(value.term)) {
                        for (var i = 0; i < value.policyList.length; i++) {
                            if (parseInt(value.policyList[i].polTerm) <= 15) {
                                tempList.push({
                                    'payOutStart': value.policyList[i].polTerm,
                                    'noOfPlans': value.policyList[i].noOfPlans
                                });
                            }
                        }
                    }
                });
            } else if (parseInt(payTerm) === 15) {
                _.each(comboList, function (value, key) {
                    if (parseInt(payTerm) === parseInt(value.term)) {
                        for (var i = 0; i < value.policyList.length; i++) {
                            if (parseInt(value.policyList[i].polTerm) === 15) {
                                tempList.push({
                                    'payOutStart': value.policyList[i].polTerm,
                                    'noOfPlans': value.policyList[i].noOfPlans
                                });
                            }
                        }
                    }
                });
            }
        } else if (details.age >= 14 && details.age < 18) {
            _.each(comboList, function (value, key) {
                if (parseInt(payTerm) === parseInt(value.term)) {
                    for (var i = 0; i < value.policyList.length; i++) {
                        if (parseInt(value.policyList[i].polTerm) === 10) {
                            tempList.push({
                                'payOutStart': value.policyList[i].polTerm,
                                'noOfPlans': value.policyList[i].noOfPlans
                            });
                        }
                    }
                }
            });
        }
        return tempList;
    }

    CommonService.getOmnitureObject = function(filterArray,dummyDisplayedValues,toolId){
        var obj = [];
        obj = dummyDisplayedValues.filter(function(ele){
            return ele.isDisplay;
        });

        var sortedObj = $filter('orderBy')(obj, filterArray, false);
        return sortedObj;

    };

    CommonService.calculateTheWeightage = function(dummyDisplayedValues,toolId,quote){
        var greatest = 0;
        var lowest = 0;
        var x = 0;
        if (parseInt(toolId) === 4) {

            dummyDisplayedValues = dummyDisplayedValues.sortByProp('TotalReturnEquity').reverse();
        } else {
            dummyDisplayedValues = dummyDisplayedValues.sortByProp('TotalReturn').reverse();
        }
        greatest = parseInt(toolId)==4?dummyDisplayedValues[0].TotalReturnEquity:dummyDisplayedValues[0].TotalReturn;
        lowest = (parseInt(toolId)==4?CommonService.getMinLastNonZeroValue(dummyDisplayedValues,'TotalReturnEquity'):CommonService.getMinLastNonZeroValue(dummyDisplayedValues,'TotalReturn'))||0;
        if (parseInt(toolId) === 4) {
            x = CommonService.calculatePerc(greatest,lowest,quote.TotalReturnEquity);/*parseInt(((value.TotalReturnEquity-lowest)/(greatest-lowest) * 100), 10);*/
        } else {
            x = CommonService.calculatePerc(greatest,lowest,quote.TotalReturn);/*parseInt(((value.TotalReturn-lowest)/(greatest-lowest) * 100), 10);*/
        }

        return x;
    };

	CommonService.populateCustDetails = function (custId, filters,leadId, leadSource) {
            var defer = $q.defer();
            var mobileNo = 0;
            NewInvestmentService.getCustomerDetails(custId).success(function (data) {
                mobileNo = data.MobileNo;
                if ($localStorage.customerDetails === undefined || $localStorage.customerDetails === null) {
                    var obj = {};
                    obj.CustomerName = data.CustomerName;
                    obj.MobileNo = data.MobileNo;
                    obj.Email = data.Email;
                    obj.CountryID = data.CountryID;
                    obj.GenderID = data.GenderID;
                    obj.CityID = data.CityID;
                    $localStorage.customerDetails = obj;
                }
                defer.resolve(data);
                var age = 0;
                if ($localStorage.searchDetails !== undefined) {
                    age = $localStorage.searchDetails.yourAge;
                }
                angular.element(function () {

                    enableChatting(data,filters,leadId,age, mobileNo,custId);
                });
            });
            return defer.promise;
        };

        function enableChatting(data,filters,lead,age, mobileNo,custId) {
		    $("div.zopim").hide();
            if(mobileNo == 9777777777){	return ;}
            NewInvestmentService.CheckInternalIP().success(function(data){
                 var isInternalIP = false;
                if(configObj.isInternalDisabled){isInternalIP=!JSON.parse(data.toLowerCase());}
                else{isInternalIP=true;}
                NewInvestmentService.getChatDetails({ MatrixLeadID: lead }).success(function (data) {
                    $rootScope.workingHrs = CommonService.isWorkingHours(data.ChatTime, configObj.callEndTime, configObj.callStartTime);
                    var workingHrs = CommonService.isWorkingHours(data.ChatTime, configObj.endTime, configObj.startTime);
                    if (isInternalIP && (workingHrs && mobileNo != 9777777777 )){
                        if (data.LastVisitURL !== null) {
                            $localStorage.ExitPointUrl = data.LastVisitURL + '&leadId=' + CommonService.encode(data.MatrixLeadID);
                        } else {
                            $localStorage.ExitPointUrl = configObj.getQuotePageUrl("leadId=" + CommonService.encode(data.MatrixLeadID) + '&enquiryId=' + CommonService.encode($localStorage.enquiryId));
                        }
                        $localStorage.FirstVisitURL = data.FirstVisitURL + '&leadId=' + CommonService.encode(data.MatrixLeadID);
                        $timeout(function () {
                            zopimService($cookies, $localStorage, lead, filters, CommonService, data.ParentMatrixLeadID, workingHrs,custId);
                        }, 0);
                    }
                
                });
             
            });
        };

    CommonService.isChatStarted = function(){
        $rootScope.$broadcast('chatEvent', {
            someProp: true // send whatever you want
        });
    };

    CommonService.getExclusionObj = function(val){
        var x = val.split('$$');
        var z = [];
        for(var i  =0;i<x.length;i++){
            var sub_string = x[i].split('::');
            var key = sub_string[0];
            var value = sub_string[1];
            z.push({1:key,2:value});
        }

        return z;

    };

    CommonService.setChatTags = function(filters,leadId,custId){
        var payTerm = 0;
        var policyTerm = 0;
        var investment = 100000;
        var investmentType = 1;
        var age = 0;
        var retirementAge = 0;
        var gender = "";
        var isMonthly = "";
        var city = "";
        var features = "";
        var firstVisitUrl = "";
        var lastVisitUrl = "";
        if ($localStorage.searchDetails !== undefined) {
            payTerm = $localStorage.searchDetails.payTerm;
            policyTerm = $localStorage.searchDetails.payOutStart;
            investment = 100000;
            investmentType = $localStorage.searchDetails.investmentFor*1===1?'Growth':$localStorage.searchDetails.investmentFor*1===2?'Retirement':'Child';
            age = $localStorage.searchDetails.yourAge;
            retirementAge = $localStorage.searchDetails.investmentFor*1===2?$localStorage.searchDetails.retirementAge:0;
            gender = $localStorage.customerDetails.GenderID==1?'Male':'Female';
            isMonthly = $localStorage.monthlyStatus*1===1?'Yearly':'Monthly';
            if($localStorage.customerDetails!==undefined) {
                city = $localStorage.customerDetails.City;
            }
            var  featuresList = [];

            _.each(filters,function(value){
                if(value.checked) {
                    featuresList.push(value.fullName);
                }
            });
            features = featuresList.join(',');
            firstVisitUrl = $localStorage.FirstVisitURL+'&AgentId=Mw==';
            lastVisitUrl = $localStorage.ExitPointUrl+'&AgentId=Mw==';

            if (!Number($localStorage.searchDetails.investment)) {
                if ($localStorage.searchDetails.investment.replace(/,/g, '') > 0) {
                    var dumInvest = $localStorage.searchDetails.investment.replace(/,/g, '');
                    if ($localStorage.monthlyStatus == 2) {
                        investment = dumInvest;
                    } else {
                        investment = dumInvest;
                    }
                }
            } else if ($localStorage.searchDetails.investment > 0) {
                if ($localStorage.monthlyStatus == 2) {
                    investment = $localStorage.searchDetails.investment;
                } else {
                    investment = $localStorage.searchDetails.investment;
                }
            }
        }
        $zopim.livechat.removeTags("LeadID: " + oldObj.leadId, "CustomerID: " + oldObj.custId, "InvestmentAmount: " + oldObj.investment, "PayTerm: " + oldObj.payTerm, "Maturity Period:"+oldObj.policyTerm,"InvestmentType: " + oldObj.investmentType,"Gender:"+oldObj.gender,"Age:" + oldObj.age,'City:'+oldObj.CityName,'Yearly/ Monthly Frequency:'+oldObj.isMonthly,'Retirement Age: '+oldObj.retirementAge,'Features Chosen:'+oldObj.features,"First Visit URL:"+oldObj.firstVisitUrl,"last Visit URL:"+oldObj.lastVisitUrl);
        $zopim.livechat.addTags("LeadID: " + leadId, "CustomerID: " + custId, "InvestmentAmount: " + investment, "PayTerm: " + payTerm, "Maturity Period:"+ policyTerm, "InvestmentType: " + investmentType,"Gender:"+gender,"Age:" + age,"City:"+city,'Yearly/ Monthly Frequency:'+isMonthly,'Retirement Age: '+retirementAge,'Features Chosen:'+features,"First Visit URL:"+firstVisitUrl,"last Visit URL:"+lastVisitUrl);
        oldObj.leadId = leadId;
        oldObj.custId = custId;
        oldObj.investment = investment;
        oldObj.payTerm = payTerm;
        oldObj.policyTerm = policyTerm;
        oldObj.investmentType = investmentType;
        oldObj.gender = gender;
        oldObj.age = age;
        oldObj.isMonthly = isMonthly;
        oldObj.retirementAge = retirementAge;
        oldObj.features = features;
        oldObj.firstVisitUrl = firstVisitUrl;
        oldObj.lastVisitUrl = lastVisitUrl;
        oldObj.CityName = city;
    };

    function getHighestForEquity(myArray, highest) {
        var tmp;
        for (var i = myArray.length - 1; i >= 0; i--) {
            tmp = myArray[i].Sensex * 1;
            if (tmp > highest) highest = tmp;
        }
        return highest;
    }

    function getHighestForDebt(myArray, highest) {
        var tmp;
        var highestGovtBond = Number.NEGATIVE_INFINITY;
        var highestFixedDeposit = Number.NEGATIVE_INFINITY;
        for (var i = myArray.length - 1; i >= 0; i--) {
            tmp = myArray[i].governmentBondYields * 1;
            if (tmp > highestGovtBond) highestGovtBond = tmp;
        }

        for (var j = myArray.length - 1; j >= 0; j--) {
            tmp = myArray[j].fixedDeposit * 1;
            if (tmp > highestFixedDeposit) highestFixedDeposit = tmp;
        }

        if(highestGovtBond>highestFixedDeposit){
            highest = highestGovtBond;
        }else{
            highest = highestFixedDeposit;
        }
        return highest;
    }

    CommonService.getHighest = function(myArray,toolId){
        var highest = Number.NEGATIVE_INFINITY;

        if (toolId * 1 === 4) {
            highest = getHighestForEquity(myArray, highest);
        }else{
            highest = getHighestForDebt(myArray,highest);
        }

        return highest;
    };

    CommonService.getLowest = function(myArray,toolId){
        var lowest = Number.POSITIVE_INFINITY;
        var tmp;
        for (var i=myArray.length-1; i>=0; i--) {
            if(toolId*1===4) {
                tmp = myArray[i].Nifty * 1;
            }else{
                tmp = myArray[i].fixedDeposit * 1;
            }
            if (tmp < lowest) lowest = tmp;
        }
        return lowest;
    };

    // determine if the current hours are within 10 am and 7 pm
    CommonService.isWorkingHours = function(time,endTime,startTime){
      var startTimeDate = '01/01/2011'+' '+startTime,
            endTimeDate = '01/01/2011'+' '+endTime,
            currentTimeDate = '01/01/2011'+' '+time;
            return Date.parse(currentTimeDate)>Date.parse(startTimeDate) && Date.parse(currentTimeDate) < Date.parse(endTimeDate);
    };

    CommonService.populateBIObject = function(leadId,customerId,quote,customer,searchDetails,tooId){
    var defered = $q.defer();
    var obj = {};
      var GeneralDataBI = {};
        GeneralDataBI.RequestProposerPlanDetails = {};
        GeneralDataBI.RequestProposerPlanDetails.LeadID = leadId;
        GeneralDataBI.RequestProposerPlanDetails.PlanID = quote.PlanID;
        GeneralDataBI.RequestProposerPlanDetails.PayTerm = quote.Payterm;
        GeneralDataBI.RequestProposerPlanDetails.PolicyTerm = quote.PolicyTerm;
        GeneralDataBI.RequestProposerPlanDetails.SumAssured = quote.SumInsured;
        GeneralDataBI.RequestProposerPlanDetails.LifeCover = quote.LifeCover;
        GeneralDataBI.RequestProposerPlanDetails.GuranteedReturn = quote.GuranteedReturns;
        GeneralDataBI.RequestProposerPlanDetails.BasePremium = quote.AnnualPremium;
        GeneralDataBI.RequestProposerPlanDetails.AnnualPremium = quote.AnnualPremium;
        GeneralDataBI.RequestProposerPlanDetails.PremiumFrequency = 1;
        GeneralDataBI.RequestProposerPlanDetails.PremiumPaid = quote.AnnualPremium;
        GeneralDataBI.RequestProposerPlanDetails.IsPayment = false;
        GeneralDataBI.RequestProposerPlanDetails.PaymentIntegrationResp = false;
        GeneralDataBI.RequestProposerPlanDetails.RiderDetailsJSON = null;
        GeneralDataBI.RequestProposerPlanDetails.BookingIPAddress = "::1";
        GeneralDataBI.RequestProposerPlanDetails.ProposalNo = "12345";
        GeneralDataBI.PersonalData = {};
        GeneralDataBI.PersonalData.CustomerName = customer.CustomerName;
        GeneralDataBI.PersonalData.DateOfBirth = searchDetails.DOB;
        GeneralDataBI.PersonalData.EmailID = customer.Email;
        GeneralDataBI.PersonalData.Gender = customer.GenderID;
        GeneralDataBI.PersonalData.MobileNo = customer.MobileNo;
        GeneralDataBI.PersonalData.CityID = customer.CityID;
        GeneralDataBI.PersonalData.CityName = customer.City;
        GeneralDataBI.PersonalData.StateId = customer.stateId;
        GeneralDataBI.PersonalData.CustomerID = customerId;
        GeneralDataBI.PersonalData.PlanName = quote.QuotePlan.PlanName;
        GeneralDataBI.PersonalData.Child1 = null;
        GeneralDataBI.PersonalData.ChildName = null;
        GeneralDataBI.PersonalData.IsSummaryBI = true;
        GeneralDataBI.PersonalData.PensionAge = searchDetails.retirementAge;
        GeneralDataBI.PersonalData.PlanNature = parseInt(tooId)===2?"DebtGovernmentSecurities":parseInt(tooId)===4?"EquityMarketLinked":"Traditional";
        obj.GeneralDataBI = GeneralDataBI;

        NewInvestmentService.generateBI(obj).success(function(data){
            defered.resolve(data);
        });

        return defered.promise;

    };

CommonService.GetVisitIDFromVisitProfileCookie=function()
{

      var objVisitProfileCookie = JSON.parse($cookies.get('VisitProfileCookie'));
      var visitId = objVisitProfileCookie.visitId;
      return visitId;
};
CommonService.GetCustIDFromCustProfileCookie=function()
{
        var deferred = $q.defer();
        var CustprofileCookie="";
        var newObj = {};
        newObj.key ="CustProfileCookie";
        NewInvestmentService.GetHttpValues(newObj).success(function(data){ 
            deferred.resolve(data.d);
        });
        return deferred.promise;
};

CommonService.setShouldPageSkipCookie = function() {
    var now = new Date(),
        time = now.getTime();

    time += 3600 * 1000;
    now.setTime(time);
    $cookies.put('shouldPageSkip', true, {'expires': now.toUTCString()});
};


    return CommonService;

}]);
