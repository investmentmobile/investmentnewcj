 app.service('OmnitureService', ["$http", "$rootScope", function ($http, $rootScope) {
	    'use strict';
	    
	    var OmnitureService = {};
	    OmnitureService.getOmnitureSelectedPlansString = function(comparePlansSummary){
	    	var selectedPlan = '';
	    	angular.forEach(comparePlansSummary, function(value, key){
	    		selectedPlan+=value.insurerName + "-" + value.policyName + "|";
	    	});
	    	selectedPlan = selectedPlan + '#';
    		selectedPlan = selectedPlan.replace('|#', '');
	    	return selectedPlan;
	    };	    

	    OmnitureService.getProduct = function(quotes){
	    	// This is sample data for reference
	    	//var product = ';ABC;;;;eVar16=Price_ABC,;DEF;;;;eVar16=Price_DEF,;XYZ;;;;eVar16=Price_XYZ';

	    	var product = "";
	    	if(quotes.length){
	    		angular.forEach(quotes, function(value, key){
	    			product+=value.QuotePlan.PlanName;
	    			product+="-";
	    			product+=value.QuotePlan.PlanInsurer.InsurerName;
	    			product+=";;;;eVar16=";
	    			product+=value.AnnualPremium;
	    			product+=",;";
		    	});
		    	product = product + '#';
    			product = product.replace(',;#', '');
	    	}
	    	return product;
	    };

		OmnitureService.getSelectedProduct = function(quotes){
			// This is sample data for reference
			//var product = ';ABC;;;;eVar16=Price_ABC,;DEF;;;;eVar16=Price_DEF,;XYZ;;;;eVar16=Price_XYZ';

			var product = "";
			if(quotes!==undefined){

				product+=quotes.QuotePlan.PlanName;
				product+="-";
				product+=quotes.QuotePlan.PlanInsurer.InsurerName;
				product+=";;;;eVar16=";
				product+=quotes.AnnualPremium;
				product+=",;";
				product = product + '#';
				product = product.replace(',;#', '');
			}
			return product;
		};


	    return OmnitureService;
    }]);
