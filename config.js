var configObj = (function() {'use strict';
    var env = "local";
    var obj = {};
    obj.startTime = '10:00:00';
    obj.endTime = '20:00:00';
    obj.planIds = [190,217,224,225,226,208,210,206];
    obj.callStartTime = '10:00:00';
    obj.callEndTime = '19:00:00';
	obj.isInternalDisabled = true;

        obj.config = {
            "local": {
                "protocol": "http://",
                "rootApi": "qainvestment.policybazaar.com/api/enqapi",
                'newApiUrl': "qainvestment.policybazaar.com/life-insurance/CommonBIGeneration.aspx",
                "base": "",
                "redirectionUrl":"localhost:81",
                'cdnUrl': "",
                'customerUrl': "localhost:8000/InvestmentNewCJ/#/customer/",
                'quotesUrl': "localhost:8000/InvestmentNewCJ/#/quote/",
                'HandlerURL': "qainvestment.policybazaar.com/life-insurance/EnquiryHandler.aspx",
                'CookieDomain': "localhost:8000"
            },
            "qa": {
                "protocol": "http://",
                "rootApi": "qainvestment.policybazaar.com/api/enqapi",
                'newApiUrl': "qainvestment.policybazaar.com/life-insurance/CommonBIGeneration.aspx",
		"redirectionUrl":"qainvestment.policybazaar.com",
                "base": "",
                'customerUrl': "qainvestment.policybazaar.com/#/customer/",
                'quotesUrl': "qainvestment.policybazaar.com/#/quote/",
				'HandlerURL': "qainvestment.policybazaar.com/life-insurance/EnquiryHandler.aspx",
                'CookieDomain': "policybazaar.com"
            },
            "uat": {
                "protocol": "http://",
                "rootApi": "car.policybazaar.com",
                'newApiUrl': "qainvestment.policybazaar.com/life-insurance/CommonBIGeneration.aspx",
                'cdnUrl': "pbstatic.policybazaar.com/assets/Car",
                'cdnHeader': "pbstatic.policybazaar.com/pbmodule",
                "base": "",
                'customerUrl': "qainvestment.policybazaar.com/#/customer/",
                'quotesUrl': "qainvestment.policybazaar.com/#/quote/",
				'HandlerURL': "uatinvestment.policybazaar.com/life-insurance/EnquiryHandler.aspx",
                'CookieDomain': "policybazaar.com"
            },
            "live": {
                "protocol": "http://",
                "rootApi": "investmentlife.policybazaar.com/api/enqapi",
                'newApiUrl': "investmentlife.policybazaar.com/life-insurance/CommonBIGeneration.aspx",
		        "redirectionUrl":"investmentlife.policybazaar.com",
                "base": "",
                'customerUrl': "investmentlife.policybazaar.com/customer/",
                'quotesUrl': "investmentlife.policybazaar.com/quote/",
				'HandlerURL': "investmentlife.policybazaar.com/life-insurance/EnquiryHandler.aspx",
                'CookieDomain': "policybazaar.com"
            }

        };
    obj.getBaseServiceURL = function(webService) {
        return obj.config[env].protocol + obj.config[env].rootApi + "/" + webService;
    };

    obj.getNewServiceURL = function(webService) {
        return obj.config[env].protocol + obj.config[env].newApiUrl + "/" + webService;
    };

    obj.getredirectionUrl = function(webService) {
        return obj.config[env].protocol + obj.config[env].redirectionUrl +  webService;
    };

    obj.getCustomerPageUrl = function(webService){
        return obj.config[env].protocol + obj.config[env].customerUrl +  webService;
    };

    obj.getQuotePageUrl = function(webService){
        return obj.config[env].protocol + obj.config[env].quotesUrl +  webService+'?';
    };

   obj.getHandlerURL = function(){
        return obj.config[env].protocol + obj.config[env].HandlerURL;
    };
    obj.getCookieDomain = function(){
        return obj.config[env].CookieDomain;
    };

    obj.getEnv = function(){
        return env;
    };

    return obj;



})();