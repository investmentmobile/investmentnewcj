/**
 * Created by Prashants on 10/6/2015.
 */


app.controller('InterController',
    [
        'CommonService','NewInvestmentService','$location','$cookies',
        function (CommonService,NewInvestmentService,$location,$cookies) {

                var leadId = $location.$$search.leadId;

            var obj = {};
            obj.MatrixLeadID = leadId;
            obj.UtmSource = $location.$$search.utm_source||"";
            obj.UtmTerm = $location.$$search.utm_term||"";
            obj.UtmMedium = $location.$$search.utm_medium||"";
            obj.UtmCampaign = $location.$$search.utm_campaign||"";

            NewInvestmentService.getInvestDataForLead(obj).success(function(data) {
                if (data !== null && data.Data !== null && data.Data.VisitId > 0 && data.Data.CustomerId > 0 && data.Data.EnquiryId > 0){
                   // $cookies.put('visitId', data.Data.VisitId);
                    //$cookies.put('CustId', data.Data.CustomerId);
                    
                    $location.$$search.enquiryId = CommonService.encode(data.Data.EnquiryId , false);
                    $location.$$search.leadId = CommonService.encode(leadId, false);
                    $location.path('/quote');
                }else{
                    delete $location.$$search.leadId;
                    $location.path('/');
                }
            });
        }]);
