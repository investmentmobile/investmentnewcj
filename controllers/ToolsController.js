/**
 * Created by Prashants on 8/12/2015.
 */

app.controller('ToolsController',
    [
        '$scope','$rootScope', 'CommonService','NewInvestmentService','$cookies','$location','$timeout','$localStorage',
        function ($scope, $rootScope,CommonService,NewInvestmentService,$cookies,$location,$timeout,$localStorage) {


            $rootScope.leave = false;
            $scope.isError = false;

            $scope.addDummyclass = false;

            $timeout(function(){
                $scope.addDummyclass = true;
            },900);
           // $scope.pageClass='compare';
            $scope.compare = {};
            $scope.compare.param = 2;
            $scope.viewMore = '+ More Details';
            $scope.plans = [];
            $scope.viewMoreDetails = true;
            $scope.obj = {};
            $scope.obj.amountFocusLost = false;
            var investment = 100000;
            if($localStorage.searchDetails!==undefined){
                if($localStorage.searchDetails.investment!==undefined && !Number($localStorage.searchDetails.investment)){
                    if( $localStorage.searchDetails.investment.replace(/,/g, '')>0){
                        var dumInvest = $localStorage.searchDetails.investment.replace(/,/g, '');
                        if($localStorage.monthlyStatus==2) {
                            investment = Math.round(dumInvest * 12 / 100) * 100;
                        }else{
                            investment = dumInvest;
                        }
                    }
                }else if($localStorage.searchDetails.investment>0){
                    if($localStorage.monthlyStatus==2) {
                        investment = $localStorage.searchDetails.investment*12;
                    }else{
                        investment = $localStorage.searchDetails.investment;
                    }
                }
            }
            $scope.obj.investment = investment;
            var payTerm = ($localStorage.searchDetails!==undefined && $localStorage.searchDetails.payTerm>0)?$localStorage.searchDetails.payTerm:10;
            $scope.obj.saveValueInvestment = angular.copy($scope.obj.investment);
                $scope.obj.investment = CommonService.AddCommas($scope.obj.investment);

            $scope.checkForError = function(){
                var x = angular.copy($scope.obj.investment);
                x = parseInt(x.toString().replace(/,/g, ''))||0;
                $scope.isError = x<10000 || x>5000000;
            };

            $scope.checkForError();

            $scope.obj.doHide = false;
            var obj0 = {id:0,name:'Traditional InsurancePlans',payoutBeforeTaxMin:'',payoutBeforeTaxMax:'',taxPayableMin:'',taxPayableMax:'',amountInvested:$scope.obj.investment,annualReturnsMin:3.1,annualReturnsMax:5.7,quarterly:1,tax:0,afterTaxReturnsMin:'',afterTaxReturnsMax:'',afterTaxReturnRateMin:'',afterTaxReturnRateMax:'',risk:'Assured rate of return',lifeCover:true,tenure:5,selected:$localStorage.toolId*1===0};
            var obj1 = {id:1,name:'Tax Free Fixed Deposits (Debt)',payoutBeforeTaxMin:'',payoutBeforeTaxMax:'',taxPayableMin:'',taxPayableMax:'',amountInvested:$scope.obj.investment,annualReturnsMin:8,annualReturnsMax:8.5,quarterly:4,tax:30,afterTaxReturnsMin:'',afterTaxReturnsMax:'',afterTaxReturnRateMin:'',afterTaxReturnRateMax:'',risk:'Assured rate of return',lifeCover:false,tenure:5,selected:false};
            var obj2 = {id:2,name:'ULIP (Debt / Government Security)',payoutBeforeTaxMin:'',payoutBeforeTaxMax:'',taxPayableMin:'',taxPayableMax:'',amountInvested:$scope.obj.investment,annualReturnsMin:7.5,annualReturnsMax:9.5,quarterly:1,tax:0,afterTaxReturnsMin:'',afterTaxReturnsMax:'',afterTaxReturnRateMin:'',afterTaxReturnRateMax:'',risk:'Low Risk',lifeCover:true,tenure:5,selected:$localStorage.toolId*1===2};
            var obj3 = {id:3,name:'Tax Free Mutual Funds  (Equity)',payoutBeforeTaxMin:'',payoutBeforeTaxMax:'',taxPayableMin:'',taxPayableMax:'',amountInvested:$scope.obj.investment,annualReturnsMin:8,annualReturnsMax:19,quarterly:1,tax:0,afterTaxReturnsMin:'',afterTaxReturnsMax:'',afterTaxReturnRateMin:'',afterTaxReturnRateMax:'',risk:'Not Guaranteed',lifeCover:false,tenure:3,selected:false};
            var obj4 = {id:4,name:'ULIP (Equity / Market  Linked)',payoutBeforeTaxMin:'',payoutBeforeTaxMax:'',taxPayableMin:'',taxPayableMax:'',amountInvested:$scope.obj.investment,annualReturnsMin:12,annualReturnsMax:24,quarterly:1,tax:0,afterTaxReturnsMin:'',afterTaxReturnsMax:'',afterTaxReturnRateMin:'',afterTaxReturnRateMax:'',risk:'Not Guaranteed',lifeCover:true,tenure:5,selected:$localStorage.toolId*1===4};


            $scope.changeTaxValues = function(){
                    var x = parseInt($scope.obj.investment.replace(/,/g, ''))||0;
                    if(x>=10000 && x<=5000000){
                    $scope.taxSavedOnInvestment = Math.round(x *0.3)||0;
                    _.each($scope.plans, function(value,key){
                        value.payoutBeforeTaxMin = CommonService.getPayoutBeforeTaxMin(value.annualReturnsMin,value.quarterly,x)||0;
                        value.payoutBeforeTaxMax = CommonService.getPayoutBeforeTaxMax(value.annualReturnsMax,value.quarterly,x)||0;
                        value.taxPayableMin = Math.round((value.tax/100)*(value.payoutBeforeTaxMin-x))||0;
                        value.taxPayableMax = Math.round((value.tax/100)*(value.payoutBeforeTaxMax-x))||0;
                        value.afterTaxReturnsMin=value.payoutBeforeTaxMin-value.taxPayableMin||0;
                        value.afterTaxReturnsMax=value.payoutBeforeTaxMax-value.taxPayableMax||0;

                        if(value.id===1) {
                            value.afterTaxReturnRateMin = CommonService.getAfterTaxReturnRateMin(value.afterTaxReturnsMin,x)||0;
                            value.afterTaxReturnRateMax = CommonService.getAfterTaxReturnRateMax(value.afterTaxReturnsMax, x) || 0;
                        }else{
                            value.afterTaxReturnRateMin = value.annualReturnsMin;
                            value.afterTaxReturnRateMax = value.annualReturnsMax;
                        }
                    });

                        $scope.tenxValue = 10*x;
                }
            };

            /*$scope.planSelectedRedirection = function(id){
                var x = 0;
                if(!Number($scope.obj.investment)) {
                    x = $scope.obj.investment.replace(/,/g, '');
                }
                if(x>=10000 && x <=5000000) {
                    $scope.changeSlideFlag('right');
                    if(id===undefined){
                        id = obj0.selected?0:obj2.selected?2:obj4.selected?4:2;
                    }
                    if (id === 0 || id === 2 || id === 4) {
                        $localStorage.toolId = id;
                        if($localStorage.searchDetails===undefined ||$localStorage.searchDetails===null){
                            $localStorage.searchDetails = {};
                        }
                        var obj = $localStorage.searchDetails;
                        obj.investment = $scope.obj.investment.replace(/,/g, '');

                        
                            var custId = $cookies.get('CustId');
                        if(custId) {
                            NewInvestmentService.getCustomerDetails(custId).success(function (data) {
                                $localStorage.customerDetails = data;
                                obj.gender = data.GenderID;
                                $localStorage.searchDetails = obj;
                                $location.$$search.enquiryId = CommonService.encode($localStorage.equiryid, false);
                                $location.path('/customer');
                            });
                        }else{
                            $location.$$search.enquiryId = CommonService.encode($localStorage.equiryid, false);
                            $location.path('/customer');
                        }
                    }
                }
            };*/

            $scope.focusOut = function(){
                var x = parseInt($scope.obj.investment.toString().replace(/,/g, ''))||0;
                if(x>=10000 && x<=5000000){
                    $scope.obj.saveValueInvestment = angular.copy($scope.obj.investment);
                }else{
                    $scope.obj.investment = angular.copy($scope.obj.saveValueInvestment);
                }
                $scope.obj.investment = CommonService.AddCommas($scope.obj.investment);
                $scope.checkForError();
            };



            $scope.compareParamsChanged = function(){
                $scope.plans = [];
                if(parseInt($scope.compare.param)===0){
                    obj2.selected = true;
                    $scope.plans.push(obj0);
                    $scope.plans.push(obj1);
                    $scope.plans.push(obj2);
                }else if(parseInt($scope.compare.param)===1){
                    obj4.selected = true;
                    $scope.plans.push(obj3);
                    $scope.plans.push(obj4);
                }else{
                    obj0.selected = $localStorage.toolId*1===0;
                    obj2.selected = $localStorage.toolId*1===2;
                    obj4.selected = $localStorage.toolId*1===4;
                    $scope.plans.push(obj0);
                    $scope.plans.push(obj1);
                    $scope.plans.push(obj2);
                    $scope.plans.push(obj3);
                    $scope.plans.push(obj4);
                }                
            };

            $scope.compareParamsChanged();

            $scope.changeTaxValues();

            $scope.focusEdit = function(){
                angular.element(function(){
                $('.num-add-comp').focus();
                });
            };

            angular.element(function(){
                $('#toolsModalId').on('hidden.bs.modal', function (e) {
                    $scope.hideObject.loadcomparetools = false;
                });
            });
    }]);
