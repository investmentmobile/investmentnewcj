/**
 * Created by Prashants on 8/12/2015.
 */
app.controller('ChooseController',
    [
        '$route','$scope', '$rootScope','CommonService','NewInvestmentService','$location','$cookies','$timeout','$localStorage','$routeParams',
        function ($route,$scope, $rootScope, CommonService,NewInvestmentService,$location,$cookies,$timeout,$localStorage,$routeParams) {
            'use strict';
            var index;
            $scope.dontShowMessage = false;
            CommonService.visitCreation();

            CommonService.retrieveCustIdFromCookie(false);

        
            triggerPageParamEvents('pre-quotes');
            $scope.discountsAndPremium = [];
            $rootScope.slideFlag = angular.isDefined($rootScope.slideFlag) ? $rootScope.slideFlag : 'right';
            $scope.focusObj = {};
            $scope.hideObject = {};
            $scope.hideObject.dropDownNotSeen = true;
            $scope.dobObj = {};
            $scope.dob = {
                date: CommonService.getDateList(),
                month: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
                year: CommonService.getYearList()
            };


            $scope.focusObj.termFocus = false;
            $rootScope.loaderProceed = false;
            $scope.buttonName = "Continue";
            $scope.showDropDown = false;
            var chosenRetirementValues = {yourAge:0,retirementAge:0,amountNeeded:0};
            var chosenGrowthValues = {yourAge:0,payTerm:0,amountToInvest:0};
            var chosenChildValues = {yourAge:0,childAge:'',amountNeeded:0,childPayTerm:20};
            var previousValue = 0;
            $scope.monthlyInvestment = 0;
            $scope.sliderObj = {min:0,max:100,value:50,list:[]};
            $rootScope.pageClass = "choose";
            var plans = [{'name':'retirement','selected':false,id:2},{'name':'growth','selected':false,id:1},{'name':'child','selected':false,id:3}];

            $scope.plans = angular.copy(plans);

            if($rootScope.isLanding){
                $scope.discountsAndPremium = [];
                NewInvestmentService.getBanner().success(function(data){
                    _.each(data,function(value){
                        if(value.offerId == $location.$$search.offerid){
                            $scope.imageUrl = value.imageUrl;
                            $scope.discountsAndPremium = value.discountAndPremium;
                        }
                    });

                });
            }


            $scope.details = {};
            function countUpValues(value) {
                if (value> 0) {
                    $scope.monthlyInvestment = value;
                    var options = {
                        useEasing: true,
                        useGrouping: true,
                        separator: ',',
                        decimal: '.',
                        prefix: 'Rs.',
                        suffix: ''
                    };
                    $timeout(function () {
                        var demo = new CountUp("myTargetElement", previousValue, value, 0, 1.5, options);
                        demo.start(function () {
                            /*$scope.$apply(function () {

                            });*/
                        });
                        previousValue = value;
                    }, 10);

                }
            }

            $scope.monthlyPayoutChange = function(){
                if(!($scope.isError || $scope.isTermError)) {
                    $scope.dontShowMessage = true;
                    $timeout(function () {
                        var postObj = {};
                        if (index === 1) {
                            chosenGrowthValues.amountToInvest = $scope.sliderObj.list.indexOf($scope.details.amount);
                            postObj.payterm = $scope.details.age;
                            postObj.premium = $scope.details.amount;
                        } else if (index === 2) {
                            var lifeExpectancy = 80;
                            postObj.payterm = $scope.details.age - $scope.details.yourAge <= 30 ? $scope.details.age - $scope.details.yourAge : 30;
                            postObj.premium = $scope.details.amount * ($scope.details.age<80?(lifeExpectancy - $scope.details.age):1);

                            chosenRetirementValues.amountNeeded = $scope.sliderObj.list.indexOf($scope.details.amount);
                        } else {
                            chosenChildValues.amountNeeded = $scope.sliderObj.list.indexOf($scope.details.amount);
                            postObj.payterm = $scope.details.childTerm;
                            postObj.premium = $scope.details.amount;
                        }
                        var monthlyInvestment = 0;
                        if(index===1) {
                            CommonService.getPremiumForPreQuote(postObj, index).then(function (value) {
                                monthlyInvestment = value.maturityBenefit;
                                $scope.monthlyInvestment = CommonService.countUpValues(monthlyInvestment,$scope.monthlyInvestment,previousValue);
                            });
                        }else if(index===2){
                            monthlyInvestment = CommonService.GetRetirementCalc($scope.details.amount,$scope.details.age,postObj.payterm);
                            $scope.monthlyInvestment = CommonService.countUpValues(monthlyInvestment,$scope.monthlyInvestment,previousValue);
                        }else  if(index===3){
                           monthlyInvestment = CommonService.compoundInterest(postObj.premium,postObj.payterm);
                            $scope.monthlyInvestment = CommonService.countUpValues(monthlyInvestment,$scope.monthlyInvestment,previousValue);
                        }

                    }, 300);
                }
            };

            $scope.triggerSelectBox = function(){
              angular.element('.trigger-class-select').trigger('click');
            };

            function fillValues(){
                if(index===1){
                    $scope.index = 1;
                    $scope.sliderObj.list = CommonService.getAmountList(10000,100000,125000,500000,1000,25000);
                    $scope.sliderObj.max = $scope.sliderObj.list.length-1;
                    $scope.firstText = "Investment per year";
                    $scope.secondText = "You will invest";
                    $scope.thirdText = "You will get";
                    $scope.finalText = "per year";
                    $scope.perText = "per month";
                    $scope.inYearsText = " in";
                    if(chosenGrowthValues.amountToInvest>0){
                        $scope.sliderObj.value = chosenGrowthValues.amountToInvest;
                        $scope.details.amount = $scope.sliderObj.list[chosenGrowthValues.amountToInvest];
                        $scope.details.monthlyAmount = Math.round($scope.details.amount/12);

                    }else if(parseInt($scope.details.amount)<=0 || $scope.details.amount===undefined) {
                        $scope.details.amount = 50000;
                    }
                    if($scope.sliderObj.list.indexOf($scope.details.amount)<0){
                        $scope.details.amount = 50000;
                    }
                    $scope.details.monthlyAmount = Math.round($scope.details.amount/12);
                    $scope.sliderObj.value = $scope.sliderObj.list.indexOf($scope.details.amount);
                    chosenGrowthValues.amountToInvest = $scope.sliderObj.value;
                    if(chosenGrowthValues.payTerm>0){
                        $scope.details.age = chosenGrowthValues.payTerm;
                    }

                }else if(index === 2){
                    $scope.index = 2;
                    $scope.sliderObj.list = CommonService.getAmountList(10000,100000,125000,200000,5000,25000);
                    $scope.sliderObj.max = $scope.sliderObj.list.length-1;
                    $scope.firstText = "Pension Needed per Month";
                    $scope.secondText = "You will get";
                    $scope.thirdText = "You need to invest ";
                    $scope.finalText = "per month";
                    /*$scope.perText = "per month";*/
                    $scope.inYearsText = "per month";
                    if(chosenRetirementValues.amountNeeded>0){
                        $scope.sliderObj.value = chosenRetirementValues.amountNeeded;
                        $scope.details.amount = $scope.sliderObj.list[chosenRetirementValues.amountNeeded];
                    }else if((parseInt($scope.details.amount)<=0 || $scope.details.amount===undefined) || parseInt($scope.details.amount)>500000 ) {
                        $scope.details.amount = 50000;
                    }
                    if($scope.sliderObj.list.indexOf($scope.details.amount)<0){
                        $scope.details.amount = 50000;
                    }
                    $scope.details.monthlyAmount = Math.round($scope.details.amount/12);
                    $scope.sliderObj.value = $scope.sliderObj.list.indexOf($scope.details.amount);
                    chosenRetirementValues.amountNeeded = $scope.sliderObj.value;
                    if(chosenRetirementValues.retirementAge>0){
                        $scope.details.age = chosenRetirementValues.retirementAge;
                    }
                }else{
                    $scope.index = 3;
                    $scope.sliderObj.list = CommonService.getChildAmountList(500000,5000000,10000000,50000000,100000,500000,2500000);
                    $scope.sliderObj.max = $scope.sliderObj.list.length-1;
                    $scope.firstText = "Amount you Need";
                    $scope.secondText = "Your child gets";
                    $scope.thirdText = "You need to invest";
                    $scope.inYearsText = "per month";
                    if(chosenChildValues.amountNeeded>0){
                        $scope.sliderObj.value = chosenChildValues.amountNeeded;
                        $scope.details.amount = $scope.sliderObj.list[chosenChildValues.amountNeeded];
                    }else if(parseInt($scope.details.amount)<=0 || $scope.details.amount===undefined) {
                        $scope.details.amount = 3000000;
                    }
                    if($scope.sliderObj.list.indexOf($scope.details.amount)<0){
                        $scope.details.amount = 3000000;
                    }
                    $scope.details.monthlyAmount = Math.round($scope.details.amount/12);
                    $scope.sliderObj.value = $scope.sliderObj.list.indexOf($scope.details.amount);
                    chosenChildValues.amountNeeded = $scope.sliderObj.value;
                    if(parseInt(chosenChildValues.childAge)>=0){
                        $scope.details.age = chosenChildValues.childAge;
                    }else{
                        $scope.details.age = '';
                    }
                    $scope.details.childTerm = chosenChildValues.childPayTerm;
                }
                $scope.details.index = index;
            }



            $scope.checkForError = function(value){
                if(value!==undefined && value.toString().length==2){
                    $scope.focus = true;
                }
                $scope.isError = (value === undefined || value.length===0) || (value>60 || (index!==2 && value<18) ||(index===2 && value<20) || (index===2 && (value>$scope.details.age || $scope.details.age - value<10)) || (index===3 && (value-$scope.details.age<18)));
                    if (value!==undefined && value.toString().length==2) {
                        $scope.checkForTermError($scope.details.age);
                        $scope.monthlyPayoutChange();
                    }
            };

            $scope.changeInYourAge = function(){
                if(index===3) {
                    if($scope.details.yourAge-$scope.details.age>=18){
                        chosenChildValues.yourAge = $scope.details.yourAge;
                    }
                }else {
                    if($scope.details.yourAge>=18){
                        if(index===1){
                            chosenGrowthValues.yourAge = $scope.details.yourAge;
                        }else{
                            chosenRetirementValues.yourAge = $scope.details.yourAge;
                        }
                    }
                }
            };

            $scope.checkForTermError = function(value){
                childTermChange();
                if(index===1){
                    $scope.isTermError = false;
                }else {
                    if (value !== undefined && value.toString().length > 0) {
                        $scope.focusObj.termFocus = true;
                    }else if(value !== undefined && value.toString().length===0){
                        $scope.focusObj.termFocus = false;
                    }
                    $scope.isTermError = (value === undefined || value.length === 0) || ((index === 3 && value > 17) || (index === 2 && (value < 50 || value > 80)) || (index === 2 && value < $scope.details.yourAge) || (index === 3 && $scope.details.yourAge - value < 18) ||(index === 2 && value - $scope.details.yourAge < 10));
                    $timeout(function(){
                    if(!$scope.isError && !$scope.isTermError){
                        $scope.monthlyPayoutChange();
                    }
                    },10);
                }
            };

            function childTermChange(){
                if(index===3) {
                    $scope.details.age = parseInt($scope.details.age);
                    changeTermChild();
                }
            }

            $scope.changeInTerm = function(isValid){
                $scope.checkForTermError($scope.details.age);
                $scope.checkForError($scope.details.yourAge);
                if(index===2){
                    chosenRetirementValues.retirementAge = $scope.details.age;
                }else if(index===3){
                    chosenChildValues.childAge = $scope.details.age;
                }
            };

            function changeTermChild(){

                $scope.details.childTerm = $scope.details.childTerm<10?10:$scope.details.childTerm<15 && $scope.details.childTerm>10?15:$scope.details.childTerm>15?20:20;
                if($scope.details.age>=14){
                    $scope.details.childTerm = 10;
                }
                if(parseInt($scope.details.childTerm)>15 && (parseInt($scope.details.age)>=9 &&parseInt($scope.details.age)<14)){
                    $scope.details.childTerm = 15;
                }
                return parseInt($scope.details.childTerm);
            }

            $scope.changeInGrowthTerm = function(isValid){
                $scope.checkForError($scope.details.yourAge,isValid);
                if(index===1){
                    chosenGrowthValues.payTerm = $scope.details.age;
                }
                if(isValid===undefined && !$scope.isError){
                    $scope.monthlyPayoutChange();
                }
            };

            $scope.changeInChildTerm = function(){
                $scope.checkForTermError($scope.details.age);
                if(index===3){
                    chosenChildValues.childPayTerm = $scope.details.childTerm;
                }
                if(!$scope.isError && !$scope.isTermError){
                    $scope.monthlyPayoutChange();
                }
            };

            $scope.changeInvestmentAmount = function(){
                if(!$scope.isError && !$scope.isTermError){
                    $scope.monthlyPayoutChange();
                }
            };

            $scope.createEnquiry = function(isValid){

                if(isValid ||!$scope.isValidDateOfBirth()) {
                    $rootScope.slideFlag = 'right';
                    isValid = isValid && !$scope.isError && !$scope.isTermError;
                    if (isValid) {
                        $rootScope.loaderProceed = true;
                        $scope.buttonName = "Please Wait";
                        CommonService.saveEnquiry(index,$scope.details,$scope.monthlyInvestment,$scope.data).then(function(data){

                            if(data){
                                $scope.buttonName = "Continue";

                            }else {
                                $scope.buttonName = "Retry";
                            }
                        });
                    }
                }
            };

            $scope.showDropDownfunc = function(){
                $scope.showDropDown = !$scope.showDropDown;
            };
            $scope.isAnyPlanSelected = function(planId){
                previousValue = 0;
                angular.element(function(){
                    if($rootScope.isLanding){
                        angular.element('html,body').animate({scrollTop: 0}, 600);
                    }else {
                        ///$('html,body').animate({scrollTop: 255}, 600);
                    }
                });
                if(planId !== index) {
                    $scope.plans = angular.copy(plans);
                    if (planId === 1) {
                        $scope.plans[1].selected = true;
                    } else if (planId === 2) {
                        $scope.plans[0].selected = true;
                    } else {
                        $scope.plans[2].selected = true;

                    }
                    index = CommonService.whichPlanSelected($scope.plans);
                    $scope.ages = CommonService.getAges(index);
                    $scope.details.age = CommonService.getAge(index);
                    $scope.nameOfAge = CommonService.getTypeName(index);
                    fillValues();
                    $scope.checkForError($scope.details.yourAge);
                    $scope.checkForTermError($scope.details.age);
                    if ($scope.details.yourAge !== undefined && $scope.details.yourAge.toString().length == 2) {
                        $timeout(function(){
                            $scope.monthlyPayoutChange();
                        },200);
                    }

                    if ($scope.planSelected) {
                        $scope.planSelected = false;
                        $timeout(function () {
                            $scope.planSelected = true;
                        }, 500);
                    } else {
                        $scope.planSelected = true;
                    }
                }
            };

            $scope.data = {
                dobDate: null,
                dobMonth: null,
                dobYear: null
            };

            $scope.changeDate = function(model) {
                $scope.data.dobDate = model;
                $scope.changeDateOfBirth();
            };

            $scope.changeMonth = function(model) {
                $scope.data.dobMonth = model;
                $scope.changeDateOfBirth();
            };

            $scope.changeYear = function(model) {
                $scope.data.dobYear = model;
                $scope.changeDateOfBirth();
            };

            $scope.changeDateOfBirth = function(){
                $scope.dobObj.dob = ($scope.data.dobDate!==undefined && $scope.data.dobDate!==null && $scope.data.dobDate.toString().length===1?'0':'') + $scope.data.dobDate + "-" + ($scope.data.dobMonth!==undefined && $scope.data.dobMonth!==null && $scope.data.dobMonth.toString().length===1?'0':'')+$scope.data.dobMonth + "-" + $scope.data.dobYear;
                if($scope.isValidDateOfBirth()) {
                    $scope.details.yourAge = CommonService.getYourAge($scope.dobObj.dob);
                    $scope.checkForError($scope.details.yourAge);
                }
            };

            $scope.isAgeWithinRange = function() {
                return !$scope.isDOBMissing() && $scope.isValidDateOfBirth();
            };

            $scope.isAgeBelowMinimum = function() {
                return !$scope.isDOBMissing() && $scope.isValidDateOfBirth() && CommonService.getYourAge($scope.dobObj.dob) < 18;
            };

            $scope.isAgeAboveMaximum = function() {
                return !$scope.isDOBMissing() && $scope.isValidDateOfBirth() && CommonService.getYourAge($scope.dobObj.dob) > 65;
            };

            $scope.isValidAge = function() {
                return !$scope.isDOBMissing() && $scope.isValidDateOfBirth() && CommonService.getAge($scope.dobObj.dob) >= 18 && CommonService.getAge($scope.dobObj.dob) <= 65;
            };

            $scope.isYearSelected = function(index, dobYear){
                return (index*1 === $scope.dob.year.indexOf(parseInt(dobYear)));
            };

            $scope.isDOBMissing = function() {
                return (!$scope.data.dobDate || !$scope.data.dobMonth || !$scope.data.dobYear);
            };

            $scope.isValidDateOfBirth = function() {
                return $scope.dobObj.dob && CommonService.isValidDateOfBirth($scope.dobObj.dob);
            };

            function onLoad(){
                var obj = $localStorage.searchDetails;
                if(!CommonService.isEmptyObject(obj)){
                    index = obj.investmentFor>0?obj.investmentFor:1;
                    $scope.index = index;
                    $scope.details.yourAge = obj.yourAge;
                    $scope.details.age = index===1?obj.payTerm:index===2?obj.retirementAge:obj.childAge;
                    chosenChildValues.childAge = index===3?obj.childAge:'';
                    chosenChildValues.childPayTerm = index===3?obj.payTerm:20;
                    $scope.plans = angular.copy(plans);
                    if(index===1){
                        $scope.plans[1].selected = true;
                    }else if(index===2){
                        $scope.plans[0].selected = true;
                    }else{
                        $scope.plans[2].selected = true;
                    }
                    $scope.data = {};
                    $scope.data.dobDate = obj.dobDate;
                    $scope.data.dobMonth = obj.dobMonth;
                    $scope.data.dobYear = obj.dobYear;
                    if($scope.data!==null) {
                        $scope.dobObj.dob = (($scope.data.dobDate !== null && $scope.data.dobDate !== undefined) && $scope.data.dobDate.length === 1 ? '0' : '') + $scope.data.dobDate + "-" + (($scope.data.dobMonth !== null && $scope.data.dobMonth !== undefined) && $scope.data.dobMonth.length === 1 ? '0' : '') + $scope.data.dobMonth + "-" + $scope.data.dobYear;
                    }
                    $scope.details.yourAge = CommonService.getYourAge($scope.dobObj.dob);
                    $scope.planSelected = true;
                    $scope.ages = CommonService.getAges(index);
                    $scope.nameOfAge = CommonService.getTypeName(index);
                    if(index===1){
                        $scope.details.amount = obj.investment;
                    }else{
                        $scope.details.amount = obj.requiredAmt;
                    }
                    fillValues();
                    if($scope.details.yourAge!==undefined && $scope.details.yourAge.toString().length==2){
                        $scope.checkForError($scope.details.yourAge);
                        /*$scope.monthlyPayoutChange();*/
                    }
                }
                if($location.$$search.planType){
                    $scope.isAnyPlanSelected(parseInt($localStorage.searchDetails?parseInt($localStorage.searchDetails.investmentFor):parseInt($location.$$search.planType)));
                    /*$location.search("planType",null);*/
                }
            }

            onLoad();
        }
    ]);
