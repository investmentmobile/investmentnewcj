/**
 * Created by Prashants on 12/3/2015.
 */
app.controller('PreQuotesController',
    [
        '$route','$scope', '$rootScope','CommonService','NewInvestmentService','$location','$cookies','$timeout','$localStorage','$routeParams',
        function ($route,$scope, $rootScope, CommonService,NewInvestmentService,$location,$cookies,$timeout,$localStorage,$routeParams) {
            'use strict';
            var index;
            $scope.privacy = true;
            $scope.dontShowMessage = false;
            $scope.submitted = false;
            $scope.data = {};
            $scope.dobObj = {};
            $scope.mainObj = {};
            $scope.mainObj.cityItem = {};
            $scope.mainObj.selectedCityItem = {};
            $scope.cityChangesObj = {};
            $scope.selectedCityItem = {};
            $scope.cityChangesObj = CommonService.prefillCityChangeObject($scope.cityChangesObj);
            CommonService.visitCreation();

            $scope.head = {};
            $scope.head.planHeading = "";

            CommonService.retrieveCustIdFromCookie(true,$scope.head.planHeading).then(function(data){
                $scope.head.planHeading = data;
            });

            triggerPageParamEvents('pre-quotes');
            $scope.discountsAndPremium = [];
            $rootScope.slideFlag = angular.isDefined($rootScope.slideFlag) ? $rootScope.slideFlag : 'right';
            $scope.focusObj = {};
            $scope.hideObject = {};
            $scope.hideObject.dropDownNotSeen = true;
            $scope.hideObject.investmentErrorMessageHide = true;
            $scope.hideObject.investmentErrorMessageHideForStep = true;
            $scope.hideObject.errorPresent = false;
            $scope.hideObject.showModalPopUp = false;

            $scope.checkIfInvestmentAmountIsCorrect = function(){
                var x = 0;
                x = CommonService.getInvestmentAmountFromCommas(x,$scope.details);
                $scope.hideObject.investmentErrorMessageForNoValue = (!isNaN(parseInt(x)) && x>=0);
                $scope.hideObject.investmentErrorMessageHide = index===3?(x>=500000 && x<=50000000):(x>=10000 && x<=200000);
                if(x!==undefined && x>=0) {
                    $scope.hideObject.investmentErrorMessageHideForStep = x% 1000 === 0 ;
                }

                if(index===1){
                    $scope.hideObject.investmentErrorMessage = "Investment amount should be between";
                }else if(index===2){
                    $scope.hideObject.investmentErrorMessage = "Pension amount should be between";
                }else if(index===3){
                    $scope.hideObject.investmentErrorMessage = "Amount you need for your child should be between";
                }

                if($scope.hideObject.investmentErrorMessageForNoValue && $scope.hideObject.investmentErrorMessageHide && $scope.hideObject.investmentErrorMessageHideForStep){
                    $scope.changeInvestmentAmount();
                }
            };

            $scope.focusObj.termFocus = false;
            $rootScope.loaderProceed = false;
            $scope.buttonName = "Continue";
            $scope.showDropDown = false;
            var chosenRetirementValues = {yourAge:0,retirementAge:0,amountNeeded:0};
            var chosenGrowthValues = {yourAge:0,payTerm:0,amountToInvest:0};
            var chosenChildValues = {yourAge:0,childAge:'',amountNeeded:0,childPayTerm:20};
            var previousValue = 0;
            $scope.monthlyInvestment = 0;
            $scope.sliderObj = {min:0,max:100,value:50,list:[]};
            $rootScope.pageClass = "choose";
            var plans = [{'name':'retirement','selected':false,id:2},{'name':'growth','selected':false,id:1},{'name':'child','selected':false,id:3}];

            $scope.plans = angular.copy(plans);

            if($rootScope.isLanding){
                $scope.discountsAndPremium = [];
                NewInvestmentService.getBanner().success(function(data){
                    _.each(data,function(value){
                        if(value.offerId == $location.$$search.offerid){
                            $scope.imageUrl = value.imageUrl;
                            $scope.discountsAndPremium = value.discountAndPremium;
                        }
                    });

                });
            }

            $scope.details = {};

            $scope.monthlyPayoutChange = function(){
                var investAmountModified = 0;
                investAmountModified = CommonService.getInvestmentAmountFromCommas(investAmountModified,$scope.details);

                if(!($scope.isError || $scope.isTermError)) {
                    $scope.dontShowMessage = true;
                    $timeout(function () {
                        var postObj = {};
                        if (index === 1) {
                            chosenGrowthValues.amountToInvest = investAmountModified;
                            postObj.payterm = $scope.details.age;
                            postObj.premium = investAmountModified;
                        } else if (index === 2) {
                            var lifeExpectancy = 80;
                            postObj.payterm = $scope.details.age - $scope.details.yourAge <= 30 ? $scope.details.age - $scope.details.yourAge : 30;
                            postObj.premium = investAmountModified * ($scope.details.age<80?(lifeExpectancy - $scope.details.age):1);

                            chosenRetirementValues.amountNeeded = investAmountModified;
                        } else {
                            chosenChildValues.amountNeeded = investAmountModified;
                            postObj.payterm = $scope.details.childTerm;
                            postObj.premium = investAmountModified;
                        }
                        var monthlyInvestment = 0;
                        if(index===1) {
                            CommonService.getPremiumForPreQuote(postObj, index).then(function (value) {
                                monthlyInvestment = value.maturityBenefit;
                                $scope.monthlyInvestment = CommonService.countUpValues(monthlyInvestment,$scope.monthlyInvestment,previousValue);
                            });
                        }else if(index===2){
                            monthlyInvestment = CommonService.GetRetirementCalc(investAmountModified,$scope.details.age,postObj.payterm);
                            $scope.monthlyInvestment = CommonService.countUpValues(monthlyInvestment,$scope.monthlyInvestment,previousValue);
                        }else  if(index===3){
                            monthlyInvestment = CommonService.compoundInterest(postObj.premium,postObj.payterm);
                            $scope.monthlyInvestment = CommonService.countUpValues(monthlyInvestment,$scope.monthlyInvestment,previousValue);
                        }

                    }, 300);
                }
            };

            $scope.triggerSelectBox = function(){
                angular.element('.trigger-class-select').trigger('click');
            };

            function fillValues(){
                var investAmountModified = 0;
                investAmountModified = CommonService.getInvestmentAmountFromCommas(investAmountModified,$scope.details);
                if(index===1){
                    $scope.maxlength = 9;
                    $scope.minlength = 1;
                    $scope.investMax = 200000;
                    $scope.investMin = 10000;
                    $scope.index = 1;
                    $scope.sliderObj.list = CommonService.getAmountList(10000,100000,125000,500000,1000,25000);
                    $scope.sliderObj.max = $scope.sliderObj.list.length-1;
                    $scope.firstText = "Amount you wish to invest yearly";

                    if(chosenGrowthValues.amountToInvest>0){
                        $scope.sliderObj.value = chosenGrowthValues.amountToInvest;
                        $scope.details.investment = $scope.sliderObj.list[chosenGrowthValues.amountToInvest];
                        $scope.details.investment = chosenGrowthValues.amountToInvest;
                        $scope.details.monthlyAmount = Math.round(investAmountModified/12);

                    }else if($scope.details.investment===undefined || (investAmountModified<=0)) {
							if(CommonService.IsMobileDevice() || $rootScope.isLanding)
							{
							$scope.details.investment ="";
							}
							else
							{
							$scope.details.investment = 50000;
							}
                    }
                    var amt = 0 ;
                    amt = CommonService.getInvestmentAmountFromCommas(amt,$scope.details);
                    if($scope.sliderObj.list.indexOf(amt)<0){
							if(CommonService.IsMobileDevice() || $rootScope.isLanding){
							$scope.details.investment ="";
							}
							else
							{
							$scope.details.investment = 50000;
							}
                    }
                    $scope.details.monthlyAmount = Math.round(investAmountModified/12);
                    $scope.sliderObj.value = $scope.sliderObj.list.indexOf(investAmountModified);
                    chosenGrowthValues.amountToInvest = $scope.sliderObj.value;
                    if(chosenGrowthValues.payTerm>0){
                        $scope.details.age = chosenGrowthValues.payTerm;
                    }

                }else if(index === 2){
                    $scope.maxlength = 9;
                    $scope.minlength = 1;
                    $scope.investMax = 200000;
                    $scope.investMin = 10000;
                    $scope.index = 2;
                    $scope.sliderObj.list = CommonService.getAmountList(10000,100000,125000,200000,5000,25000);
                    $scope.sliderObj.max = $scope.sliderObj.list.length-1;
                    $scope.firstText = "Pension you need per month";

                    if(chosenRetirementValues.amountNeeded>0){
                        $scope.sliderObj.value = chosenRetirementValues.amountNeeded;
                        $scope.details.investment = chosenRetirementValues.amountNeeded;
                    }else if((investAmountModified<=0 || $scope.details.investment===undefined) || investAmountModified>200000 ) {
                      if(CommonService.IsMobileDevice()|| $rootScope.isLanding)
							{
							$scope.details.investment ="";
							}
							else
							{
							$scope.details.investment = 50000;
							}
                    }
                    if($scope.sliderObj.list.indexOf(investAmountModified)<0){
							if(CommonService.IsMobileDevice()|| $rootScope.isLanding)
							{
							$scope.details.investment ="";
							}
							else
							{
							$scope.details.investment = 50000;
							}
                    }
                    $scope.details.monthlyAmount = Math.round(investAmountModified/12);
                    $scope.sliderObj.value = $scope.sliderObj.list.indexOf(investAmountModified);
                    chosenRetirementValues.amountNeeded = $scope.sliderObj.value;
                    if(chosenRetirementValues.retirementAge>0){
                        $scope.details.age = chosenRetirementValues.retirementAge;
                    }
                }else{
                    $scope.maxlength = 12;
                    $scope.minlength = 1;
                    $scope.investMax = 50000000;
                    $scope.investMin = 500000;
                    $scope.index = 3;
                    $scope.sliderObj.list = CommonService.getChildAmountList(500000,5000000,10000000,50000000,100000,500000,2500000);
                    $scope.sliderObj.max = $scope.sliderObj.list.length-1;
                    $scope.firstText = "Amount you need for your child";
                    if(chosenChildValues.amountNeeded>0){
                        $scope.sliderObj.value = chosenChildValues.amountNeeded;
                        $scope.details.investment = chosenChildValues.amountNeeded;
                    }else if(investAmountModified<=0 || $scope.details.investment===undefined) {
							if(CommonService.IsMobileDevice()|| $rootScope.isLanding)
							{
							$scope.details.investment ="";
							}
							else
							{
							$scope.details.investment = 2000000;
							}

                    }
                    if($scope.sliderObj.list.indexOf(investAmountModified)<0){
							if(CommonService.IsMobileDevice()|| $rootScope.isLanding)
							{
							$scope.details.investment ="";
							}
							else
							{
							$scope.details.investment = 2000000;
							}

                    }
                    $scope.details.monthlyAmount = Math.round(investAmountModified/12);
                    $scope.sliderObj.value = $scope.sliderObj.list.indexOf(investAmountModified);
                    chosenChildValues.amountNeeded = $scope.sliderObj.value;
                    if(parseInt(chosenChildValues.childAge)>=0){
                        $scope.details.age = chosenChildValues.childAge;
                    }else{
                        $scope.details.age = '';
                    }
                    $scope.details.childTerm = chosenChildValues.childPayTerm;
                }
                $scope.details.index = index;
            }

            $scope.checkForError = function(value){
                if(value!==undefined && value.toString().length==2){
                    $scope.focus = true;
                }
                $scope.isError = (value === undefined || value.length===0) || (value>60 || (index!==2 && value<18) ||(index===2 && value<20) || (index===2 && (value>$scope.details.age || $scope.details.age - value<10)) || (index===3 && (value-$scope.details.age<18)));
                if (value!==undefined && value.toString().length==2) {
                    var currentyear = new Date().getFullYear();
                    $scope.data.dobYear = currentyear - value;
                    $scope.data.dobDate = 1;
                    $scope.data.dobMonth = 1;
                    $scope.checkForTermError($scope.details.age);
                    $scope.monthlyPayoutChange();
                }
            };
            $scope.changeInYourAge = function(){
                if(index===3) {
                    if($scope.details.yourAge-$scope.details.age>=18){
                        chosenChildValues.yourAge = $scope.details.yourAge;
                    }
                }else {
                    if($scope.details.yourAge>=18){
                        if(index===1){
                            chosenGrowthValues.yourAge = $scope.details.yourAge;
                        }else{
                            chosenRetirementValues.yourAge = $scope.details.yourAge;
                        }
                    }
                }
            };

            $scope.checkForTermError = function(value){
                childTermChange();
                if(index===1){
                    $scope.isTermError = false;
                }else {
                    if (value !== undefined && value.toString().length > 0) {
                        $scope.focusObj.termFocus = true;
                    }else if(value !== undefined && value.toString().length===0){
                        $scope.focusObj.termFocus = false;
                    }
                    $scope.isTermError = (value === undefined || isNaN(value) || value.length === 0) || ((index === 3 && value > 17) || (index === 2 && (value < 50 || value > 80)) || (index === 2 && value < $scope.details.yourAge) || (index === 3 && $scope.details.yourAge - value < 18) ||(index === 2 && value - $scope.details.yourAge < 10) || (index===1 && $scope.details.age==='No. of years you want to invest for'));
                    $timeout(function(){
                        if(!$scope.isError && !$scope.isTermError){
                            $scope.monthlyPayoutChange();
                        }
                    },10);
                }
            };

            function childTermChange(){
                if(index===3) {
                    $scope.details.age = parseInt($scope.details.age);
                    changeTermChild();
                }
            }

            $scope.changeInTerm = function(isValid){
                $scope.checkForTermError($scope.details.age);
                $scope.checkForError($scope.details.yourAge);
                if(index===2){
                    chosenRetirementValues.retirementAge = $scope.details.age;
                }else if(index===3){
                    chosenChildValues.childAge = $scope.details.age;
                }
            };

            function changeTermChild(){

                $scope.details.childTerm = $scope.details.childTerm<10?10:$scope.details.childTerm<15 && $scope.details.childTerm>10?15:$scope.details.childTerm>15?20:20;
                if($scope.details.age>=14){
                    $scope.details.childTerm = 10;
                }
                if(parseInt($scope.details.childTerm)>15 && (parseInt($scope.details.age)>=9 &&parseInt($scope.details.age)<14)){
                    $scope.details.childTerm = 15;
                }
                return parseInt($scope.details.childTerm);
            }

            $scope.changeInGrowthTerm = function(isValid){
                $scope.checkForError($scope.details.yourAge,isValid);
                if(index===1){
                    chosenGrowthValues.payTerm = $scope.details.age;
                }
                if(isValid===undefined && !$scope.isError){
                    $scope.monthlyPayoutChange();
                }
            };

            $scope.isPayTermSelected = function(value){
                return !isNaN(value);
            };

            $scope.changeInChildTerm = function(){
                $scope.checkForTermError($scope.details.age);
                if(index===3){
                    chosenChildValues.childPayTerm = $scope.details.childTerm;
                }
                if(!$scope.isError && !$scope.isTermError){
                    $scope.monthlyPayoutChange();
                }
            };

            $scope.changeInvestmentAmount = function(){
                if(!$scope.isError && !$scope.isTermError){
                    $scope.monthlyPayoutChange();
                }
            };

            $scope.createEnquiry = function(isValid,form){
                $scope.blurObject = {customerNameBlurOut:true,emailBlurOut:true,mobileBlurOut:true,countryBlurOut:true,cityBlurOut:true,investmentBlurOut:true};
                $scope.checkIfInvestmentAmountIsCorrect();
                if($scope.details.yourAge <18){isValid = false;}
                isValid = isValid && !$scope.isError && !$scope.isTermError && $scope.hideObject.investmentErrorMessageHide && $scope.hideObject.investmentErrorMessageHideForStep && !isNaN($scope.details.age) && !$scope.hideObject.patternErrorPresent;
                isValid = isValid && (!isNaN($scope.cityChangesObj.countryCode));
                isValid = isValid && (!isNaN($scope.details.MobileNo)&& $scope.details.MobileNo>0);
                $scope.errorMessageHide = $scope.details.MobileNo<=0;
                if ($scope.details.CountryID*1 === 392) {
                    checkForCity(isValid, form);
                }
                if(isValid) {
                    $rootScope.slideFlag = 'right';
                    isValid = isValid && !$scope.isError && !$scope.isTermError;
                    if ($scope.details.CountryID*1 === 392) {
                        isValid = $scope.details.MobileNo <= 9999999999 && $scope.details.MobileNo >= 6666666666;
                        $scope.errorMessageHide = $scope.details.MobileNo.length != 10 || !isValid;
                        isValid = checkForCity(isValid, form) && isValid;
                    }
                    $rootScope.slideFlag = 'right';
                    var cityObj = {};
                        cityObj.CityID = $scope.details.CountryID*1 === 392?$scope.selectedCityItem.cityId:9999;
                        cityObj.City = $scope.details.CountryID*1 === 392?$scope.selectedCityItem.value:'Others';
                        cityObj.stateId = $scope.details.CountryID*1 === 392?$scope.selectedCityItem.stateId:99;
                    cityObj.countryCode =  $scope.cityChangesObj.countryCode;
                    if (isValid) {
                        $scope.loaderProceed = true;
                        $scope.buttonName = "Please Wait";
                        CommonService.saveEnquiryAndGenerateLead(index,$scope.details,$scope.monthlyInvestment,$scope.data,cityObj).then(function(data){
                            if(data){


                            }else {
                                $scope.buttonName = "Retry";
                            }
                        });
                    }
                }
            };

            $scope.isAnyPlanSelected = function(planId){
                previousValue = 0;
                $scope.blurObject = {customerNameBlurOut:false,emailBlurOut:false,mobileBlurOut:false,countryBlurOut:false,cityBlurOut:false,investmentBlurOut:false};
                angular.element(function(){
                    if($rootScope.isLanding && $rootScope.isHeadIFrame){
                        
                    }else if($rootScope.isLanding && !$rootScope.isHeadIFrame){
                        $('html,body').animate({scrollTop: 0}, 600);
                    }
                    if($rootScope.isMobile){
                        $('html,body').animate({scrollTop: 0}, 600);
                    }
                });
                if(planId !== index) {


                    $scope.plans = angular.copy(plans);
                    if (planId === 1) {
                        $scope.plans[1].selected = true;
                    } else if (planId === 2) {
                        $scope.plans[0].selected = true;
                    } else {
                        $scope.plans[2].selected = true;

                    }
                    index = CommonService.whichPlanSelected($scope.plans);

                    $scope.ages = CommonService.getAges(index);
                    /*$scope.details.age = CommonService.getAge(index);*/

					if((CommonService.IsMobileDevice()||$rootScope.isLanding) && index===1)
					{
					var ages = [];
					ages =CommonService.getAges(index);
                    if(isNaN($scope.details.age) || ages.indexOf($scope.details.age)<0){
					   ages.unshift("No. of years you want to invest for");
                       $scope.details.age="No. of years you want to invest for";
                    }
					$scope.ages = ages;
					
					}
					else if((CommonService.IsMobileDevice()||$rootScope.isLanding) && index===2)
					{
					$scope.details.age="";
					}
                    $scope.nameOfAge = CommonService.getTypeName(index);
                    fillValues();
                    $scope.checkForError($scope.details.yourAge);
                    $scope.checkForTermError($scope.details.age);
                    if ($scope.details.yourAge !== undefined && $scope.details.yourAge.toString().length == 2) {
                        $timeout(function(){
                            $scope.monthlyPayoutChange();
                        },200);
                    }

                    if ($scope.planSelected) {
                        $scope.planSelected = false;
                        $timeout(function () {
                            $scope.planSelected = true;
                        }, 50);
                    } else {
                        $scope.planSelected = true;
                    }
                }

                $scope.checkIfInvestmentAmountIsCorrect();
            };

            function fillObjectFromLead() {
                var leadID = $location.$$search.leadid !== undefined ? $location.$$search.leadid : 0;
                if (leadID === 0) { return; }
                var objLeadData = {};
                objLeadData.MatrixLeadID = leadID;
                NewInvestmentService.getCity().success(function (data) {
                    $scope.city = CommonService.getCity(data.Data.CityList);
                    NewInvestmentService.GetLeadInfoByLeadId(objLeadData).success(function (data) {
                        $scope.selectedCityItem = CommonService.findCityFromId($scope.city, parseInt(data.CityID)) || {};
                        $scope.mainObj.selectedCityItem = angular.copy($scope.selectedCityItem);
                        ///$scope.isAnyPlanSelected(parseInt(data.InvestmentTypeID));
						if(isNaN(data.InvestmentTypeID) || data.InvestmentTypeID=='0'){index = 1;}
                        else{
                            index = parseInt(data.InvestmentTypeID);
                        }
                        var uGender = data.Gender;
                        if(isNaN(uGender) || uGender=='0'){$scope.details.gender = uGender=='Male'?1:2;}
                        else{
                            $scope.details.gender = data.Gender;
                        }
                        $scope.details.CustomerName = data.Name;
                        $scope.details.Email = data.EmailID;
                        $scope.details.MobileNo = data.MobileNo;
                        var dobDt = new Date(data.DOB);
                        $scope.data.dobDate = dobDt.getDate();
                        $scope.data.dobMonth = dobDt.getMonth() + 1;
                        $scope.data.dobYear = dobDt.getFullYear();
                        if (index === 1) {
                            $scope.details.age = parseInt(data.PayTerm);
                        }
                        $scope.index = index;
                        $scope.plans = angular.copy(plans);
                        if (index === 1) {
                            $scope.details.investment = obj.investment;
                        } else {
                            $scope.details.investment = obj.requiredAmt;
                        }
                        $scope.ages = CommonService.getAges(index);
                        $scope.details.age = CommonService.getAge(index);
                        $scope.nameOfAge = CommonService.getTypeName(index);

                        if ($scope.data !== null) {
                            $scope.dobObj.dob = (($scope.data.dobDate !== null && $scope.data.dobDate !== undefined) && $scope.data.dobDate.length === 1 ? '0' : '') + $scope.data.dobDate + "-" + (($scope.data.dobMonth !== null && $scope.data.dobMonth !== undefined) && $scope.data.dobMonth.length === 1 ? '0' : '') + $scope.data.dobMonth + "-" + $scope.data.dobYear;
                        }
                        $scope.details.yourAge = CommonService.getYourAge($scope.dobObj.dob);
                        $scope.planSelected = true;

                        $scope.plans = angular.copy(plans);
                        if (index === 1) {
                            $scope.plans[1].selected = true;
                        } else if (index === 2) {
                            $scope.plans[0].selected = true;
                        } else if (index === 3) {
                            $scope.plans[2].selected = true;

                        }


                        if ($scope.details.yourAge !== undefined && $scope.details.yourAge.toString().length == 2) {
                            $scope.checkForError($scope.details.yourAge);
                        }
                        fillValues();

                    }).error(function (data) { });

                }).error(function (data) {   });

            }
            function onLoad(){
                $scope.IsHeaderHide = $location.$$search.isHead !== undefined ? $location.$$search.isHead === 'isHead' : false;
				$scope.IsHeaderHideIframe = $location.$$search.isHeadIframe !== undefined ? $location.$$search.isHeadIframe === 'isHeadIframe' : false;
                $scope.IsClickToFill = $location.$$search.c2ff !== undefined ? $location.$$search.c2ff === 'c2ff' : false;
                if ($scope.IsClickToFill) { fillObjectFromLead(); }
                else {
                    var obj = $localStorage.searchDetails;
                    if (!CommonService.isEmptyObject(obj)) {
                        index = obj.investmentFor > 0 ? obj.investmentFor : 1;
                        $scope.index = index;
                        $scope.details.yourAge = obj.yourAge;
                        $scope.details.age = index === 1 ? (Math.round(obj.payTerm / 5) * 5).toString() : index === 2 ? obj.retirementAge : obj.childAge;
                        chosenChildValues.childAge = index === 3 ? obj.childAge : '';
                        chosenChildValues.childPayTerm = index === 3 ? obj.payTerm : 20;
                        $scope.plans = angular.copy(plans);
                        if (index === 1) {
                            $scope.plans[1].selected = true;
                        } else if (index === 2) {
                            $scope.plans[0].selected = true;
                        } else {
                            $scope.plans[2].selected = true;
                        }
                        $scope.data = {};
                        $scope.data.dobDate = obj.dobDate;
                        $scope.data.dobMonth = obj.dobMonth;
                        $scope.data.dobYear = obj.dobYear;
                        if ($scope.data !== null) {
                            $scope.dobObj.dob = (($scope.data.dobDate !== null && $scope.data.dobDate !== undefined) && $scope.data.dobDate.length === 1 ? '0' : '') + $scope.data.dobDate + "-" + (($scope.data.dobMonth !== null && $scope.data.dobMonth !== undefined) && $scope.data.dobMonth.length === 1 ? '0' : '') + $scope.data.dobMonth + "-" + $scope.data.dobYear;
                        }
                        $scope.details.yourAge = CommonService.getYourAge($scope.dobObj.dob);
                        $scope.planSelected = true;
                        $scope.ages = CommonService.getAges(index);
                        $scope.nameOfAge = CommonService.getTypeName(index);
                        if (index === 1 && obj.investment!==undefined) {
                            var investAmtTemp = parseInt(obj.investment.toString().replace(/,/g, ''));
						    $scope.details.investment = $localStorage.monthlyStatus*1===2?((Math.round(investAmtTemp*12/1000))*1000):obj.investment;
                        } else {
                            $scope.details.investment = obj.requiredAmt;
                        }

                        $scope.details.investment = CommonService.AddCommas($scope.details.investment);
                        fillValues();
                        if ($scope.details.yourAge !== undefined && $scope.details.yourAge.toString().length == 2) {
                            $scope.checkForError($scope.details.yourAge);
                            /*$scope.monthlyPayoutChange();*/
                        }
                    }
                }

                if($rootScope.isLanding){
                    //$scope.isAnyPlanSelected(parseInt($location.$$search.offerid));
                    $scope.isAnyPlanSelected(parseInt($location.$$search.planType));
                }
                else if($location.$$search.planType){
                    $scope.isAnyPlanSelected(parseInt($location.$$search.planType));
                    /*$location.search("planType",null);*/
                }
                else if(index === undefined){
                    $scope.isAnyPlanSelected(1);
                }
                $scope.checkIfInvestmentAmountIsCorrect();
            }

            $scope.blurObject = {customerNameBlurOut:false,emailBlurOut:false,mobileBlurOut:false,countryBlurOut:false,cityBlurOut:false,investmentBlurOut:false};



            $scope.trueThat = function(value){
                $scope.blurObject = CommonService.getBlurObjectChange(value,$scope.blurObject);
            };

            $scope.checkForMobile = function(){
                var isValid = !isNaN($scope.details.MobileNo);
                if ($scope.details.CountryID*1 === 392) {
                    isValid = isValid && $scope.details.MobileNo <= 9999999999 && $scope.details.MobileNo >= 6666666666;
                    $scope.errorMessageHide = $scope.details.MobileNo!==undefined?($scope.details.MobileNo.length != 10 || !isValid):true;
                }
                $scope.errorMessageHide = $scope.details.MobileNo<=0;
                return $scope.errorMessageHide;
            };

            $scope.checkForCode = function(){
                return !isNaN($scope.cityChangesObj.countryCode);
            };

            $rootScope.leave = false;
            var obj = {};
            if($localStorage.customerDetails!==undefined) {
                obj = $localStorage.customerDetails;
            }
            NewInvestmentService.getCity().success(function(data){
                $scope.selectedCityItem = {};
                var newData = _.uniq(data.Data.CityList,function(a){
                    return a.CityId;
                });

                var newDelhiObject = {"CityId":"551","CityName":"New Delhi","StateId":"35","state":"Delhi"};
                newData.unshift(newDelhiObject);
                $scope.city = CommonService.getCity(newData);
                if(obj.CountryID*1===392) {
                    $scope.selectedCityItem = CommonService.findCityFromId($scope.city, obj.CityID) || {};
                    $scope.mainObj.selectedCityItem = angular.copy($scope.selectedCityItem);
                    $scope.mainObj.cityItem = angular.copy($scope.selectedCityItem);
                }
                if(obj.CountryID*1!==392){
                    $scope.cityChangesObj.citySearchText = "Others";
                    $scope.cityChangesObj.cityPlaceHolder = "Others";
                }else if(CommonService.isEmptyObject($scope.selectedCityItem)){
                    $scope.cityChangesObj.citySearchText = "";
                    $scope.cityChangesObj.cityPlaceHolder = "Enter City";
                }
                $scope.changeMaxLength($scope.details.CountryID);
            }).error(function(data){

            });

            $scope.details = {};
            CommonService.prefillCustomerEntityObject($scope.details,obj);

            $scope.countryCodes = {};

            $scope.maxNum = 10;
            $scope.minNum = 10;
            $scope.cityChangesObj.countryCode = obj.countryCode*1;

            NewInvestmentService.getCountryCodes().success(function(data){
                $scope.countryCodes = data;
                $scope.details.CountryID = obj.CountryID!==undefined?obj.CountryID.toString():"392";
                $scope.searchForCountryCode($scope.details.CountryID);

            }).error(function(data){

            });
             // validation for city
            var checkForCity = function(isvalid,searchForm){
                if($scope.selectedCityItem===undefined || $scope.selectedCityItem === null || Object.keys($scope.selectedCityItem).length<1){
                    $scope.selectedCityItem = CommonService.findCityFromText($scope.city,$scope.cityChangesObj.citySearchText);
                    if($scope.selectedCityItem === null || Object.keys($scope.selectedCityItem).length<1) {
                        searchForm.cityDetails.$invalid = true;
                        $scope.hideObject.errorPresent = true;
                        $scope.cityErrorMessage = "Enter city name";
                    }
                }
                return !searchForm.cityDetails.$invalid;
            };

            $scope.popularCityList = CommonService.getTopCitiesList();

            $scope.selectedCityItemChange = function(cityItem){
                $scope.hideObject.errorPresent = false;
                if(cityItem!==undefined &&cityItem!==null && $scope.selectedCityItem!==undefined && $scope.selectedCityItem!==null) {
                    if ($scope.selectedCityItem.cityId !== cityItem.cityId) {

                        $scope.hideObject.errorPresent = false;
                        if (cityItem !== undefined && cityItem !== null) {
                            $scope.selectedCityItem = angular.copy(cityItem);
                            $scope.mainObj.cityItem = angular.copy($scope.selectedCityItem);
                            $scope.mainObj.selectedCityItem = angular.copy($scope.selectedCityItem);
                            $scope.hideObject.showModalPopUp = false;
                        }
                    }
                }

           };

            $scope.selectFromTopCities = function(cityItem){
                $scope.selectedCityItem.cityId = cityItem.cityId;
                $scope.selectedCityItem.value = cityItem.value;
                $scope.selectedCityItem.stateId = cityItem.stateId;
                $scope.selectedCityItem.searchString = cityItem.name;
                $scope.selectedCityItem.display = cityItem.name;
                $scope.mainObj.selectedCityItem = angular.copy($scope.selectedCityItem);
                $scope.mainObj.cityItem = angular.copy(cityItem);
                $scope.hideObject.showModalPopUp = false;
            };

            $scope.searchCityTextChange = function(citySearchText){
                $scope.hideObject.patternErrorPresent = !CommonService.onlyAlphabetAllowed(citySearchText);
                if(citySearchText!==$scope.cityChangesObj.citySearchText) {
                    if($scope.selectedCityItem===undefined) {
                        $scope.selectedCityItem = {};
                    }
                     
                    $scope.citySearchText = citySearchText;
                }
            };

            $scope.details.CountryID = obj.CountryID!==undefined?obj.CountryID:"392";
            $scope.searchForCountryCode = function(countryId,hasChanged){
                $scope.cityChangesObj = CommonService.changeCityChangeObj($scope.countryCodes,$scope.cityChangesObj, countryId, obj);
                $scope.cityChangesObj.countryCode = CommonService.CheckForExistingCountryCodes($scope.cityChangesObj,$scope.countryCodes)?"":$scope.cityChangesObj.countryCode;
                if(hasChanged){
                    $scope.details.MobileNo = "";
                }
            };

            $scope.checkIfAvailableList = function(countryId){
                _.each($scope.countryCodes,function(val){
                    if(val.CountryCode===(countryId)*1){
                        $scope.details.CountryID = val.CountryCodeId.toString();
                        $scope.cityChangesObj.isEditable = false;
                        $scope.changeMaxLength($scope.details.CountryID);
                        $scope.searchForCountryCode($scope.details.CountryID);
                    }
                });
            };

            $scope.changeMaxLength = function(countryId){
                $scope.cityChangesObj.isNRI = false;
                $scope.cityChangesObj.minValue = 9;
                if(countryId*1 == 392){
                    $scope.cityChangesObj.isNRI = false;
                    $scope.cityChangesObj.minValue = 2;
                    $scope.cityChangesObj.cityPlaceHolder = "Enter City";
                    $scope.mainObj.cityItem = angular.copy($scope.selectedCityItem);
                    $scope.mainObj.selectedCityItem = angular.copy($scope.selectedCityItem);

                    if(CommonService.isEmptyObject($scope.selectedCityItem)){
                        $scope.cityChangesObj.citySearchText = '';
                    }

                }else{
                    $scope.cityChangesObj.minValue = 9;
                    $scope.hideObject.errorPresent = false;
                    $scope.cityChangesObj.citySearchText = 'Others';
                    $scope.cityChangesObj.cityPlaceHolder = "Others";
                    $scope.cityChangesObj.isNRI = true;
                    $scope.errorMessageHide=false;
                    $scope.maxNum = 11;
                    $scope.minNum = 8;
                    $scope.mainObj.cityItem = {};
                }
                _.each($scope.countryCodes,function(val){
                    if(countryId===val.CountryCodeId){
                        $scope.maxNum = val.MAX;
                        $scope.minNum = val.MIN;
                    }
                });
            };

            $scope.showPopUp = function(val){
                if(!$scope.cityChangesObj.isNRI) {
                    $scope.hideObject.showModalPopUp = val;
                }
                if(!val){
                    $scope.selectedCityItem = CommonService.findCityFromText($scope.city,$scope.cityChangesObj.citySearchText);
                    /*if(obj!==null && obj!==undefined && !CommonService.isEmptyObject(obj)){*/
                        /*$scope.selectedCityItem = angular.copy(obj);*/
                       $scope.mainObj.cityItem = angular.copy($scope.selectedCityItem);
                       $scope.mainObj.selectedCityItem = angular.copy($scope.selectedCityItem); 
                    /*}*/
                    if(!CommonService.onlyAlphabetAllowed($scope.cityChangesObj.citySearchText)){
                            $scope.cityChangesObj.citySearchText = "";
                    }
                }
            };

            $scope.isCityNameEntered = function(){
                return !$scope.cityChangesObj.isNRI && !$scope.mainObj.cityItem.cityId && ($scope.hideObject.errorPresent || $scope.blurObject.cityBlurOut );
            };

            $scope.checkIfAgeIsCorrect = function(){
                return !isNaN($scope.details.age);
            };

            onLoad();
        }
    ]);