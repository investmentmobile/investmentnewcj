
/**
 * Created by Prashants on 8/11/2015.
 */

app.controller('QuotesController',
    [
        '$scope', '$rootScope','$cookies','$location','CommonService','$timeout','NewInvestmentService','$interval','$mdToast','$localStorage','$q','OmnitureService',
        function($scope, $rootScope,$cookies,$location,CommonService,$timeout,NewInvestmentService,$interval,$mdToast,$localStorage,$q,OmnitureService) {
            $scope.changeSlideFlag = function(flag) {
                $rootScope.slideFlag = flag;
            };
            $scope.disableObject = {};
            $scope.disableObject.redirectDisable = false;
            $scope.disableObject.quotesShown = false;
            $scope.chat = {};
            $scope.chat.chatStarted  = false;
            var custId = 0;
            var custCookieObj ={};
            custCookieObj.key = 'CustProfileCookie';
            var leadId = CommonService.decode($location.$$search.leadId,false);

                $scope.details = {};
                $scope.hideAfterCall = false;
                $scope.filterObject = {};
                $scope.hideQuotes = true;

                $scope.items = [{id:0,name:'Traditional'},{id:2,name:'Debt/Government Securities'},{id:4,name:'Equity/Market Linked'}];
                $scope.sortItems = [{id:1,name:'Returns'},{id:3,name:'Features'}];
                $scope.predItems = [{id:1,name:'Returns'},{id:2,name:'Features'}];

                var displayValueQuotes = [];

                var lowest = 0;
                /*$('#toolsModalId').modal({ keyboard: true });*/
                $scope.viewMore = "View More";
                $rootScope.slideFlag = angular.isDefined($rootScope.slideFlag) ? $rootScope.slideFlag : 'right';

                var allComboList = [];

                $scope.filterObject.filterId = $location.$$search.filt||$localStorage.filt||1;
                $scope.filterObject.prediction = $location.$$search.pred||$localStorage.pred||'2';
                $localStorage.pred = $scope.filterObject.prediction;
                $localStorage.filt = $scope.filterObject.filterId;
                $scope.isPayTermError = false;
                $scope.isPolicyTermError = false;
                $scope.hideObject = {payFocus:false,policyFocus:false,editButtonHide:false};
                $scope.hideObject.viewMore = true;
                $scope.hideObject.showFeatureDetails = false;
                $scope.hideObject.loadcomparetools = false;

                var greatest = 0;
                var quotes = {},dummyQuotes = [];
                var previousDetails = {};
                var mobileNo = 0;
                var toastPosition = {
                    bottom: false,
                    top: true,
                    left: false,
                    right: true
                    };
            

            var enqId = parseInt(CommonService.decode($location.$$search.enquiryId,true));
            $scope.planFilters = [];
            //Goto home page
            $scope.goBackToHome = function(){
                $scope.changeSlideFlag('left');
                $location.path('/customer');
            };
            $rootScope.pageClass = 'quote';
            var toolId = 2;
            if($localStorage.toolId!==undefined){
                toolId = $localStorage.toolId.toString();
            }
            $scope.filterObject.toolId = !$rootScope.isOnlyTraditional?($location.$$search.tool||toolId||2):0;
            /*$scope.filterObject.toolId = 0;*/
            $localStorage.toolId = $scope.filterObject.toolId;
            $scope.filterObject.payoutType = $location.$$search.payouttype||$localStorage.payouttype||3;
            $localStorage.payoutType = $scope.filterObject.payoutType;
            delete $location.$$search.tool;
            delete $location.$$search.payouttype;
            delete $location.$$search.filt;
            delete $location.$$search.pred;

            var previousPayTerm = 0,previousPolicyTerm = 0,previousToolId = 0;

            var details = {};

            $scope.changeInvestmentType = function(){
                if(!$scope.isError) {
                    var x = parseInt($scope.details.investment.toString().replace(/,/g, ''));
                    if ($scope.details.investmentType == 1) {
                        $scope.details.investmentType = 2;
                        $scope.details.investment = Math.round(x / 12);
                        $scope.typeName = "Change To yearly";
                        $scope.monthlyName = 'Monthly';
                    } else {
                        $scope.details.investment = Math.round(x * 12 / 100) * 100;
                        $scope.details.investmentType = 1;
                        $scope.typeName = "Change To Monthly";
                        $scope.monthlyName = 'Yearly';
                    }
                    $scope.details.investment = CommonService.AddCommas($scope.details.investment);
                    /*previousDetails.investment = angular.copy($scope.details.investment);*/
                    previousDetails.investmentType = angular.copy($scope.details.investmentType);
                    $scope.details.spaninvestment = CommonService.AmountToString($scope.details.investment);
                }
            };

            $scope.showWithCommas = function(){
                $scope.details.investment = CommonService.AddCommas($scope.details.investment);
            };

            $scope.focusOut = function(){
                $scope.details.amountFocusLost = !$scope.details.amountFocusLost;
                var x = parseInt($scope.details.investment.replace(/,/g, ''))||0;
                if(parseInt($scope.details.investmentType)===1) {
                    if (x >= 10000 && x <= 200000) {
                        $scope.details.spaninvestment = $scope.details.investment;
                        $scope.details.spaninvestment = CommonService.AmountToString($scope.details.spaninvestment);
                    } else {
                        $scope.details.investment = angular.copy(previousDetails.investment);
                    }
                }else{
                    if (x >= 833 && x <= 16667) {
                        $scope.details.spaninvestment = $scope.details.investment;
                        $scope.details.spaninvestment = CommonService.AmountToString($scope.details.spaninvestment);
                    }else{
                        $scope.details.investment = angular.copy(Math.round(previousDetails.investment.replace(/,/g, '')/12));
                    }
                }



                $scope.checkForError();

            };

                $scope.$on('chatEvent', function (event, data) {
                    $scope.chat.chatStarted = true; // 'Data to send'
                });

                $scope.chat = function(){
                    $zopim.livechat.window.show();
                };

            var getQuoteList = function(){
                $scope.list = [];
                CommonService.addHistoricalData(quotes,previousDetails.payTerm);
                dummyQuotes = angular.copy(quotes);
                if(parseInt($scope.filterObject.prediction)===1 && parseInt($scope.filterObject.toolId) === 0){
                    $scope.filterObject.prediction = '2';
                }else if(parseInt($scope.filterObject.toolId) !== 0){
                    $scope.filterObject.prediction = $localStorage.pred;
                }
                if(dummyQuotes.length>0) {
                    $scope.changeSortArray();
                    $scope.applyAllFilters();
                }else{
                    $scope.list = [];
                }
                $scope.applyFeatures();

            };

            function populateFilterObject() {
                var sortParam = "",filterParam = "";

                var filterArrayObject = [];
                if ($scope.filterObject.filterId == 1) {
                    sortParam = 'Retruns';
                } else {
                    sortParam = 'Features';
                }
                filterArrayObject.push("Sort" + sortParam);
                if (parseInt($scope.filterObject.toolId) === 0) {
                    filterParam = "traditional";
                } else if (parseInt($scope.filterObject.toolId) === 2) {
                    filterParam = "Debt securities";
                } else {
                    filterParam = "Equity";
                }
                filterArrayObject.push(filterParam);
                var payoutparam = "";
                if (parseInt($scope.filterObject.toolId) === 0) {
                    if (parseInt($scope.filterObject.payoutType) === 0) {
                        payoutparam = "Regular Interval";
                    } else if (parseInt($scope.filterObject.payoutType) === 1) {
                        payoutparam = "One Time";
                    } else {
                        payoutparam = "Both";
                    }
                    filterArrayObject.push(payoutparam);
                }
                return filterArrayObject;
            }

            $scope.changeSortArray = function(){
                $scope.hideQuotes = false;
                if($scope.filterObject.filterId==1){
                    if(parseInt($scope.filterObject.prediction)===2) {
                        if (parseInt($scope.filterObject.toolId) === 4) {
                            $scope.filterArray = ['-TotalReturnEquity', '-QuotePlan.AllPlanPriority'];
                        } else {
                            $scope.filterArray = ['-TotalReturn', '-QuotePlan.AllPlanPriority'];
                        }
                    }else if(parseInt($scope.filterObject.filterId)===1){
                        if (parseInt($scope.filterObject.toolId) === 4) {
                            $scope.filterArray = ['-historicalReturnsEquity', '-QuotePlan.AllPlanPriority'];
                        } else {
                            $scope.filterArray = ['-historicalReturns', '-QuotePlan.AllPlanPriority'];
                        }
                    }

                }else if($scope.filterObject.filterId==2) {
                    if(parseInt($scope.filterObject.prediction)===2) {
                        $scope.filterArray = ['-IRR', '-QuotePlan.AllPlanPriority'];
                    }
                }else {
                    if($scope.isAnyFeatureSelected) {
                        if(parseInt($scope.filterObject.prediction)===2) {
                            if (parseInt($scope.filterObject.toolId) === 4) {
                                $scope.filterArray = ['-percentageWeightage', '-TotalReturnEquity'];
                            } else {
                                $scope.filterArray = ['-percentageWeightage', '-TotalReturn'];
                            }
                        }else if(parseInt($scope.filterObject.filterId)===1){
                            if (parseInt($scope.filterObject.toolId) === 4) {
                                $scope.filterArray = ['-percentageWeightage', '-historicalReturnsEquity'];
                            } else {
                                $scope.filterArray = ['-percentageWeightage', '-historicalReturns'];
                            }
                        }
                    }else{
                        $scope.filterObject.filterId=1;
                        $scope.changeSortArray();
                    }
                }
                $localStorage.filt =  $scope.filterObject.filterId;
                if($scope.list!==undefined && $scope.list!==null ) {
                    CommonService.updateProductDetails($scope.list, leadId, $scope.filterObject, $scope.details.investmentFor);
                }
                $timeout(function(){
                    $scope.hideQuotes = true;
                },100);

                var filterArrayObject = populateFilterObject();
                if($scope.list!==undefined && $scope.list.length>0) {
                    var omniObject = CommonService.getOmnitureObject($scope.filterArray, $scope.list, $scope.filterObject.toolId);
                    triggerPageFilterEvents(OmnitureService.getProduct(omniObject),custId,leadId,filterArrayObject);
                }

            };

            $scope.changePrediction = function(prediction){
                $scope.hideQuotes = false;
                changeReturnName(prediction);
                $localStorage.pred =  prediction;
                getQuoteList();
                $timeout(function(){
                    $scope.hideQuotes = true;
                },100);
            };

            function changeReturnName(prediction){
                if(parseInt(prediction)===2) {
                    $scope.returnsName = "Fund Growth @ 8%";
                }else{
                    $scope.returnsName = "Had you invested 24 years ago";
                }
            }

            changeReturnName($scope.filterObject.prediction);

            $scope.selectPlanredirection = function(quote){
                $scope.disableObject.redirectDisable = true;
                $scope.generatedBIUrl = null;
                $scope.planid = quote.PlanID;
                $scope.filterObject.isChartLoaded = false;

                $scope.exclusionObject = [];
                $scope.features = [];
                    $scope.mapObject = [];



                        NewInvestmentService.getPlanRiders({PlanID: quote.PlanID}).success(function (data) {
                            $scope.PlanRiders = data.Riders;
                            $scope.Funds = data.Funds;
                            $scope.viewPlanQuote = quote;
                            if(quote.QuotePlan.ProductBrief!==null) {
                                $scope.exclusionObject = CommonService.getExclusionObj(quote.QuotePlan.ProductBrief);
                            }

                            $scope.features = CommonService.getExclusionObj(quote.QuotePlan.KeyFeatures);
                            NewInvestmentService.getChartData().success(function(data){
                                if($scope.filterObject.toolId*1===2) {
                                    $scope.mapObject = data[0].debt;
                                    $scope.heading = "Government Bond vs Fixed Deposit performance";
                                }else{
                                    $scope.mapObject = data[1].equity;
                                    $scope.heading = "Market Performance";
                                }
                                $scope.filterObject.isChartLoaded = true;
                                $('#viewModalId').modal('show');



                                $timeout(function(){
                                    calcNewWeightage(CommonService.calculateTheWeightage(displayValueQuotes,$scope.filterObject.toolId,$scope.viewPlanQuote), $scope.viewPlanQuote);
                                    $scope.disableObject.redirectDisable = false;
                                },100);
                            });
                            
                            if(configObj.planIds.indexOf(quote.PlanID)>-1) {
                                CommonService.populateBIObject(leadId, custId, quote, $localStorage.customerDetails, $localStorage.searchDetails, $scope.filterObject.toolId).then(function (data) {
                                    if($scope.planid ==quote.PlanID)
                                    {
                                    if(!data.d.HasErrror) {
                                        if ($scope.generatedBIUrl === null) {
                                            $scope.generatedBIUrl = data.d.RetrunValue;
                                        }
                                    }
                                }
                                });
                            }

                            triggerQuoteSelectionEvent(enqId,custId,leadId,OmnitureService.getSelectedProduct(quote));
                        });
                   // RedirectToPostQuote(quote,leadId);



            };

            $scope.checkForError = function(){
                var x = angular.copy($scope.details.investment);
                x = x!==undefined?parseInt(x.toString().replace(/,/g, '')):0;
                if($scope.details.investmentType===2){
                    x = x*12;
                }
                $scope.isError = Math.ceil(x)<10000 || Math.ceil(x)>200000;
            };

            $scope.ApplyFilters = function(isValid,hide){
                if(!hide) {
                    $scope.details.amountFocusLost = true;
                    $scope.checkErrorForPay();
                    $scope.checkErrorForPolicy();
                    if(!$scope.isError && !$scope.isPayTermError && !$scope.isPolicyTermError) {
                        $scope.hideObject.editButtonHide = false;
                        previousDetails = angular.copy($scope.details);
                        if($scope.details.investmentType!==null) {
                            $localStorage.monthlyStatus = $scope.details.investmentType;
                        }
                        getQuotes();
                    }else{
                        $scope.details.investment = angular.copy(previousDetails.investment);
                        $scope.details.payTerm = angular.copy(previousDetails.payTerm);
                        $scope.details.policyTerm = angular.copy(previousDetails.policyTerm);
                    }
                }
            };

            $scope.undoChanges = function(){
                $scope.details.amountFocusLost = true;
                $scope.hideObject.payFocus = false;
                $scope.hideObject.policyFocus = false;
                $scope.hideObject.editButtonHide = false;
                if($scope.details.investmentType===1) {
                    $scope.details.investment = angular.copy(previousDetails.investment);
                }else{
                    $scope.details.investment = angular.copy(Math.round(parseInt(previousDetails.investment.toString().replace(/,/g, ''))/12));
                }
                var invest = parseInt($scope.details.investment.toString().replace(/,/g, ''));
                $scope.details.spaninvestment = CommonService.AmountToString(invest);
                $scope.details.payTerm = angular.copy(previousDetails.payTerm);
                $scope.details.payOutStart = angular.copy(previousDetails.payOutStart);
                $scope.checkErrorForPay();
                $scope.checkErrorForPolicy();
            };
			$scope.addLeadingZero = function(value) {
				if(value < 10) {
					value = "0" + value;
				}
				return value;
			};        
        
			$scope.currentDate = function() {
				var currentTime = new Date(),
				month = currentTime.getMonth() + 1,
				day = currentTime.getDate(),
				year = currentTime.getFullYear();
					  
				return ($scope.addLeadingZero(year) + '' + $scope.addLeadingZero(month) + '' + $scope.addLeadingZero(day));
			};

			$scope.currentTime = function() {
					var currentTime = new Date(),
					hour = currentTime.getHours(),
					minute = currentTime.getMinutes();
						  
				return ($scope.addLeadingZero(hour) + '' + $scope.addLeadingZero(minute));
			 };

           
            NewInvestmentService.GetHttpValues(custCookieObj).success(function(response){
                if( typeof response.d !== 'undefined' && response.d!=="") {
                    var custProfileData = JSON.parse(response.d);
                    custId = custProfileData.CustID;
                }
                 
            (function updateEnquiry(){
                if(enqId>0) {
                    if($localStorage.searchDetails===undefined){
                        $localStorage.searchDetails = {};
                    }
                    details = $localStorage.searchDetails;
                    NewInvestmentService.getEnquiryDetails(enqId).success(function (data) {
                        leadId = data.MatrixLeadId;
                        details = CommonService.prefillValuesFromService(details, data);
                        $scope.planFilters = CommonService.populateFilterList(data.InvestmentTypeId);
                        $scope.typeName = "Change To Monthly";
                        $scope.monthlyName = 'Yearly';
                        NewInvestmentService.getAllCombinations().success(function (data) {
                            if (parseInt(details.investmentFor) === 3) {
                                allComboList = data[1].child;
                            }else {
                                allComboList = data[0].notChild;
                            }
                            $scope.payTermList = CommonService.GetPayTermList(details,allComboList);
                            $scope.policyTermList = CommonService.getPolicyTermList(details,details.payTerm,allComboList);
                            $scope.details = CommonService.initializeValues(details);
                            previousPayTerm = $scope.details.payTerm;
                            previousPolicyTerm = $scope.details.payOutStart;
                            previousToolId = $scope.filterObject.toolId;
                            if(parseInt($scope.filterObject.toolId)===0){
                                $scope.details.payTerm = 10;
                                /*$scope.payTermList = CommonService.GetPayTermList(details,allComboList);*/
                                $scope.payTermList = [10];
                                $scope.policyTermList = [{'payOutStart':20}];
                                $scope.details.payOutStart = 20;
                                /*if($scope.payTermList.indexOf(20)==-1){
                                    $scope.details.payTerm = 10;
                                    $scope.details.payOutStart = 10;
                                }*/
                            }
                            previousDetails = angular.copy($scope.details);
                            getQuotes();
                        });
                        if(custId===null ||custId===undefined || custId===0){
                            custId = data.CustId;
                           // $cookies.put('CustId',custId);
                        }

                        CommonService.populateCustDetails(custId,$scope.planFilters,leadId,data.LeadSource).then(function(data){
                            mobileNo = data.MobileNo;
                        });
                        triggerPageParamEvents('quotes');
                    });
                }else{
                    $location.path('/');
                }
              
            })();
            });

//            function hdfcPlans() {
//                var hdfcPlans = "142,153,161,166,167,180,183,184,188,192,194,202,203,216,227,228";
//                return hdfcPlans.split(',');
//            }

            function HdfcImageTagging(quote, eventType) {
                var hdfcPlans = "142,153,161,166,167,180,183,184,188,192,194,202,203,216,227,228";
                var hdfcPlanIDs = hdfcPlans.split(',');
                var currentPlanID = quote.PlanID;
                var isHDFCPlan = false;
                var data = $scope.list;
                var hdfcQuotes = $.grep(hdfcPlanIDs, function (element, index) {
                    return element == currentPlanID;
                });
                isHDFCPlan = hdfcQuotes.length > 0;

                if (isHDFCPlan === false) { return; }
                var product = quote.QuotePlan.PlanName;
                var todaysDate = $scope.currentDate();
                var currentTime = $scope.currentTime();
                var imgID = eventType == 'click' ? 'imgHdfcClick_' : 'imgHdfcImp_';
                var divTagg = eventType == 'click' ? $('#divDropHdfcookieClick') : $('#divDropHdfcookieImp');
                imgID = imgID + product;
                var hdfcImageSrc = 'http://hdfclife.demdex.net/event?c_partner=policybazaar&c_event=' + eventType + '&c_product=' + product + '&c_category=' + 'investment' + '&c_device=' + 'desktop' + '&c_date=' + todaysDate + '&c_time=' + currentTime + '&c_content-banner&customerID=' + leadId;
                var imgImp = '<img id="' + imgID + '" src="' + hdfcImageSrc + '" />';
                divTagg.append(imgImp);
            }

            function HDFCImpressionTagging(eventType) {
                var data = $scope.list;
                // var hdfcQuotes = $.grep(data, function (element, index) {
                //     ids = hdfcPlans();
                //     qry = '';
                //     for (xi = 0; xi < ids.length; xi++) { qry = qry ? qry + ' || element.PlanID == ' + ids[xi] : 'element.PlanID == ' + ids[xi]; }
                //     return eval(qry);
                // });
                _.each(data, function (value, key) {
                    HdfcImageTagging(value, 'imp');
                });
            }

            function getQuotes() {
                var postObj = CommonService.populateObjForQuotes(previousDetails, details, $scope.filterObject.toolId);
                NewInvestmentService.GetQuotes(postObj).success(function (data) {
                    $scope.disableObject.quotesShown = true;
                    quotes = data.Data.InvestmentQuoteList;
                    getQuoteList();
                   /* HDFCImpressionTagging();*/
                });
                $localStorage.searchDetails.investment = previousDetails.investment;
                $localStorage.searchDetails.payTerm  = previousDetails.payTerm;
                $localStorage.searchDetails.payOutStart = previousDetails.payOutStart;
                if(window.$zopim && $zopim.livechat!==undefined) {
                    CommonService.setChatTags($scope.planFilters, leadId, custId);
                }
            }

            function calcWeightage(x, value) {
                    var duration = 0;
                    if (x > 0) {
                        duration = Math.round(100 * 8 / x);
                    } else {
                        duration = 1;
                        x = 1;
                    }
                    value.weightage = 0;
                    $interval(function () {
                        value.weightage++;
                    }, duration, x);
            }

            function calcNewWeightage(x, value) {
                var duration = 0;
                if (x > 0) {
                    duration = Math.round(100 * 8 / x);
                } else {
                    duration = 1;
                    x = 1;
                }
                value.newWeightage = 0;
                $interval(function () {
                    value.newWeightage++;

                }, duration, x);
            }

            $scope.applyAllFilters = function(){
                dummyQuotes = CommonService.addIRRValues(dummyQuotes);

                _.each(dummyQuotes,function(value,key){
                    value.isDisplay = false;
                    var isDisplay = true;
                    isDisplay = isDisplay && CommonService.applyFilters(value,$scope.filterObject);
                    value.isDisplay = isDisplay;
                });

                $scope.list = angular.copy(dummyQuotes);
                var dummyDisplayedValues = $scope.list.filter(function(ele){
                   return ele.isDisplay;
                });

                displayValueQuotes = angular.copy(dummyDisplayedValues);

                if(parseInt($scope.filterObject.prediction)===2) {
                    if (parseInt($scope.filterObject.toolId) === 4) {

                        dummyDisplayedValues = dummyDisplayedValues.sortByProp('TotalReturnEquity').reverse();
                    } else {
                        dummyDisplayedValues = dummyDisplayedValues.sortByProp('TotalReturn').reverse();
                    }
                    if(dummyDisplayedValues.length>0) {
                        greatest = parseInt($scope.filterObject.toolId) == 4 ? dummyDisplayedValues[0].TotalReturnEquity : dummyDisplayedValues[0].TotalReturn;
                        lowest = (parseInt($scope.filterObject.toolId) == 4 ? CommonService.getMinLastNonZeroValue(dummyDisplayedValues, 'TotalReturnEquity') : CommonService.getMinLastNonZeroValue(dummyDisplayedValues, 'TotalReturn')) || 0;
                    }
                }else{
                    if(parseInt($scope.filterObject.toolId)===4) {
                        dummyDisplayedValues = dummyDisplayedValues.sortByProp('historicalReturnsEquity').reverse();

                    }else{
                        dummyDisplayedValues = dummyDisplayedValues.sortByProp('historicalReturns').reverse();

                    }
                    greatest = parseInt($scope.filterObject.toolId)==4?dummyDisplayedValues[0].historicalReturnsEquity:dummyDisplayedValues[0].historicalReturns;
                    if(dummyDisplayedValues.length===1){
                        lowest = 0;
                    }else {
                        lowest = (parseInt($scope.filterObject.toolId)==4?CommonService.getMinLastNonZeroValue(dummyDisplayedValues,'historicalReturnsEquity'):CommonService.getMinLastNonZeroValue(dummyDisplayedValues,'historicalReturns'))||0;
                    }
                }
                _.each($scope.list, function (value, key) {
                    var x = 0;
                    if(parseInt($scope.filterObject.prediction)===2) {
                        if (parseInt($scope.filterObject.toolId) === 4) {
                            x = CommonService.calculatePerc(greatest,lowest,value.TotalReturnEquity);/*parseInt(((value.TotalReturnEquity-lowest)/(greatest-lowest) * 100), 10);*/
                        } else {
                            x = CommonService.calculatePerc(greatest,lowest,value.TotalReturn);/*parseInt(((value.TotalReturn-lowest)/(greatest-lowest) * 100), 10);*/
                        }
                    }else{
                        if(greatest>0 && lowest>=0) {
                            if (parseInt($scope.filterObject.toolId) === 4) {
                                x = CommonService.calculatePerc(greatest,lowest,value.historicalReturnsEquity);/*parseInt(((value.historicalReturnsEquity-lowest) / (greatest-lowest) * 100), 10);*/
                            } else {
                                x = CommonService.calculatePerc(greatest,lowest,value.historicalReturns);/*parseInt(((value.historicalReturns-lowest) / (greatest-lowest) * 100), 10);*/
                            }
                        }else{
                            x=0;
                        }
                    }

                    calcWeightage(x, value);
                });
                var filterArrayObject = populateFilterObject();
                var omnitureObject = CommonService.getOmnitureObject($scope.filterArray,$scope.list,$scope.filterObject.toolId);
                triggerPageFilterEvents(OmnitureService.getProduct(omnitureObject),custId,leadId,filterArrayObject);
            };

            $scope.applyFeatures = function(){
                $scope.isAnyFeatureSelected = CommonService.getFiltersSelectedArray($scope.planFilters).length>0;
                CommonService.applyFeatureWeightage($scope.planFilters,$scope.list);
                if($scope.list!==undefined && $scope.list!==null) {
                    CommonService.updateProductDetails($scope.list, leadId, $scope.filterObject, $scope.details.investmentFor);
                }
            };

            $scope.changeInToolId = function(){
                $scope.hideQuotes = false;
                if(parseInt($scope.filterObject.toolId)===0){
                    $scope.details.payTerm = 10;
                    //$scope.payTermList = CommonService.GetPayTermList(details,allComboList);
                    $scope.payTermList = [10];
                    $scope.policyTermList = [{'payOutStart':20}];
                                $scope.details.payOutStart = 20;
                    /*$scope.changePolicyTermList();*/
                    previousDetails = angular.copy($scope.details);
                    getQuotes();

                }else{
                    if(parseInt(previousToolId)===0){
                        $scope.payTermList = CommonService.GetPayTermList(details,allComboList);
                        $scope.details.payTerm = previousPayTerm;

                        $scope.changePolicyTermList();
                        /*$scope.ApplyFilters();*/
                        previousDetails = angular.copy($scope.details);
                        getQuotes();
                    }
                }
                previousToolId = $scope.filterObject.toolId;
                $localStorage.toolId = $scope.filterObject.toolId;
                getQuoteList();
                $timeout(function(){
                    $scope.hideQuotes = true;
                },100);
            };

            $scope.changeInPayoutType = function(){
                if(parseInt($scope.filterObject.toolId)===0){
                    $localStorage.payouttype = $scope.filterObject.payoutType;
                    $scope.applyAllFilters();
                    $scope.applyFeatures();
                }

            };

            $scope.checkErrorForPay = function(){

                $scope.isPayTermError = parseInt($scope.details.payTerm)<5 || parseInt($scope.details.payTerm)>30 || parseInt($scope.details.payTerm)>parseInt($scope.details.payOutStart);
                $scope.checkErrorForPolicy();
            };

            $scope.checkErrorForPolicy = function(){
                $scope.isPolicyTermError = parseInt($scope.details.payOutStart)<5 || parseInt($scope.details.payOutStart)>30|| parseInt($scope.details.payTerm)>parseInt($scope.details.payOutStart);

            };

            $scope.emailPlans = function(){
                var emailObj = {};
                var emailQuoteList = [];
                _.each($scope.list,function(value){
                    if(emailQuoteList.length<3) {
                        var obj = {};
                        obj.PlanID = value.PlanID;
                        obj.PlanName = value.QuotePlan.PlanName;
                        obj.AnnualPremium = value.AnnualPremium;
                        obj.InsurerID = value.QuotePlan.InsurerID;
                        obj.InsurerFullName = value.QuotePlan.PlanInsurer.InsurerName;
                        obj.InsurerLogoImage = value.QuotePlan.PlanInsurer.InsurerLogoImage;
                        emailQuoteList.push(obj);
                    }
                });
                emailObj.QuoteList = emailQuoteList;
                emailObj.EnquiryID = enqId;

                emailObj.RedirectURL = window.location.href+'&filt='+$localStorage.filt+'&tool='+$localStorage.toolId+'&pred='+$localStorage.pred+'&payouttype='+$localStorage.payouttype;

                showToast('Email has been sent successfully!');

                NewInvestmentService.emailQuotes(emailObj).success(function(data){

                });
            };

            $scope.clickToCall = function(){
                var obj = {};
                obj.Number = mobileNo;
                obj.Group1 = "NTM_IB";
                obj.Group2 = "";
                obj.LeadSource = "PB";
                obj.LeadID = leadId;
                $scope.hideAfterCall = true;
                showToast('Thank You! We will call you back shortly');
                NewInvestmentService.clickToCall(obj).success(function(){

                });
            };

            var getToastPosition = function() {
                return Object.keys(toastPosition)
                    .filter(function(pos) { return toastPosition[pos]; })
                    .join(' ');
            };

            $scope.directToPostQuotes = function (quote) {
                triggerBuyEvent(enqId, custId, leadId, OmnitureService.getSelectedProduct(quote));
                RedirectToPostQuote(quote, leadId);
                product = quote.QuotePlan.PlanName;
                /*HdfcImageTagging(quote,'click');*/
            };

            function RedirectToPostQuote(quote, userInfoId) {
                var obj = {};
                obj.planId = quote.PlanID;

                obj.premium = quote.AnnualPremium;
                obj.leadId = leadId;
                NewInvestmentService.saveSelection(obj).success(function(res) {
                    var redirectionUrl = '/life-insurance/life-investment-detail.aspx';
                    var url = "";
                    if (quote.QuotePlan.RedirectURL !== null && quote.QuotePlan.RedirectURL !== undefined && typeof(quote.QuotePlan.RedirectURL) !== "undefined" && quote.QuotePlan.RedirectURL !== '') {
                        if (quote.QuotePlan.RedirectURL.indexOf('http') !== -1) {
                            url = quote.QuotePlan.RedirectURL;
                        } else {
                            if (quote.QuotePlan.RedirectURL == "xyz") {
                                url = configObj.getredirectionUrl(redirectionUrl);
                            }
                            else {
                                url = configObj.getredirectionUrl(quote.QuotePlan.RedirectURL);
                            }
                        }
                    } else {
                        url = configObj.getredirectionUrl(redirectionUrl);
                    }
                    
                    if($scope.list!==undefined && $scope.list!==null) {
                        CommonService.updateProductDetails($scope.list, leadId, $scope.filterObject, $scope.details.investmentFor,quote);
                    }
                    $timeout(function(){
                    var form = $('<form action="' + url + '" method="post">' +
                        '<input type="hidden" id="enquiryID" name="enquiryID" value=' + CommonService.encode(enqId,true) + ' />' +
                        '<input type="hidden" name="VisitorProductID" value=' + userInfoId + ' />' +
                        '<input type="hidden" name="PlanID" value=' + quote.PlanID + ' />' +
                        '<input type="hidden" name="Premium" value=' + quote.AnnualPremium + ' />' +
                        '<input type="hidden" name="WebsitePremium" value=' + quote.AnnualPremium + ' />' +
                        '<input type="hidden" name="SumAssured" value=' + quote.SumInsured + ' />' +
                        '<input type="hidden" name="Payterm" value=' + quote.Payterm + ' />' +
                        '<input type="hidden" name="MoreOptionDone" value="false" />' +
                        '<input type="hidden" name="PlanPolicyTerm" value=' + quote.PolicyTerm + ' />' +
                        '<input type="hidden" id="quoteID" name="quoteID" value=' + quote.QuoteID + ' />' +
                        '<input type="hidden" id="lifeCover" name="lifeCover" value=' + quote.LifeCover + ' />' +
                        '</form>');
                    $('body').append(form);
                    $(form).submit();
                    },200);
                });
            }

            function triggerOmnitureAndPostData(){
                var deferred = $q.defer();

            }

            function showToast(message){
                $mdToast.show(
                    $mdToast.simple()
                        .content(message)
                        .position(getToastPosition())
                        .hideDelay(3000)
                );
            }

            $scope.viewMoreFeatures = function(){

                $scope.hideObject.viewMore = !$scope.hideObject.viewMore;
                if($scope.hideObject.viewMore){
                    $scope.viewMore = "View More";
                }else{
                    $scope.viewMore = "View Less";
                }
            };

            $scope.changeFeatureList = function(value){
                $scope.hideObject.showFeatureDetails = !value;
            };

            $scope.changePolicyTermList = function(){
                $scope.policyTermList = CommonService.getPolicyTermList(details,$scope.details.payTerm,allComboList);
                $scope.details.payOutStart = angular.copy($scope.details.payTerm);
            };

            $scope.showModalPanel = function(){
                showTheModal().then(function(){
                    $timeout(function(){
                    showModal();
                    },300);
                });
            };

            function showModal(){
                $('#toolsModalId').modal('show');
            }

            function showTheModal(){
                var deferred = $q.defer();
                $scope.hideObject.loadcomparetools = true;
                deferred.resolve(true);

                return deferred.promise;

            }

            function mapNames(obj){
                var allData = obj;
                var callback = function (allData) {
                    return allData.QuotePlan.PlanName;
                };
                return allData.map(callback);
            }

       


        }

    ]);

angular.element(document).ready(function () {
    if(screen.width >= 768){
        var elem = angular.element('.filter-head'),
            leftFilterTop = 340;

        angular.element(window).scroll(function(){
            if(angular.element(window).scrollTop() >= leftFilterTop){
                angular.element('.filter-head').addClass('fixed-quote-head').removeClass('fadeOut').addClass('fadeIn');
            } else {
                angular.element('.filter-head').removeClass('fixed-quote-head').removeClass('fadeIn');
            }
        });
    }
});
