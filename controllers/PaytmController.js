
/**
* Created by Prashants on 12/3/2015.
*/
app.controller('PaytmController',
    [
        '$route', '$scope', '$rootScope', 'CommonService', 'NewInvestmentService', '$location', '$cookies', '$timeout', '$localStorage', '$routeParams',
        function ($route, $scope, $rootScope, CommonService, NewInvestmentService, $location, $cookies, $timeout, $localStorage, $routeParams) {
            'use strict';
            var index;
            $scope.citywaterMark = 'Enter the City that you reside in';
            var defaultIncome = 100000;
            $scope.showStep = 'enquiry';
            $scope.stepNo = 1;
            $scope.Mobile_New_Disable = true;
            var newObj = {};
            newObj.key = "CustProfileCookie";

            NewInvestmentService.GetHttpValues(newObj).success(function (data) {
                CommonService.createVisit(data);
            });

            CommonService.GetCustIDFromCustProfileCookie().then(function (data) {
                var custIDJson = {};
                if (data && data !== "") {
                    custIDJson = JSON.parse(data);
                }
                var custIDd = custIDJson.CustID || 0;
                var planType;
                if ($location.$$search.planType) {
                    planType = $localStorage.searchDetails ? parseInt($localStorage.searchDetails.investmentFor) : parseInt($location.$$search.planType);
                }
                triggerEnquiryFormStartEvent(custIDd, planType);
            });

            triggerPageParamEvents('pre-quotes');

            $scope.showCustSection = false;
            $scope.discountsAndPremium = [];
            $rootScope.slideFlag = angular.isDefined($rootScope.slideFlag) ? $rootScope.slideFlag : 'right';
            $scope.focusObj = {};
            $scope.hideObject = {};
            $scope.hideObject.dropDownNotSeen = true;
            $scope.hideObject.investmentErrorMessageHide = true;
            $scope.hideObject.investmentErrorMessageHideForStep = true;
            $scope.hideObject.annualErrorMessageHide = true;
            $scope.hideObject.annualErrorMessageHideForStep = true;
            $scope.dobObj = {};
            $scope.dob = {
                date: CommonService.getDateList(),
                month: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                year: CommonService.getYearList()
            };


            $scope.annualMin = 100000;
            $scope.annualMax = 50000000;

            $scope.checkIfAnnualIncomeIsCorrect = function (objError) {                
                if (!CommonService.isEmptyObject(objError)) {                    
                    return;
                }
                $scope.hideObject.annualErrorMessageHide = $scope.details.annual >= $scope.annualMin && $scope.details.annual <= $scope.annualMax;
                if(!$scope.hideObject.annualErrorMessageHide){return;}
                $scope.hideObject.annualErrorMessageHideForStep = $scope.details.annual !== undefined && $scope.details.annual % 100000 === 0;
            };
            $scope.checkIfInvestmentAmountIsCorrect =  function (objError) {                
                if (!CommonService.isEmptyObject(objError)) {
                    return;
                }
                $scope.hideObject.investmentErrorMessageHide = $scope.details.investment >= $scope.investMin && $scope.details.investment <= $scope.investMax;
                if(!$scope.hideObject.investmentErrorMessageHide){return;}
                $scope.hideObject.investmentErrorMessageHideForStep = $scope.details.investment !== undefined && $scope.details.investment % 1000 === 0;
            };

            $scope.focusObj.termFocus = false;
            $rootScope.loaderProceed = false;
            $scope.buttonName = "Continue";
            $scope.showDropDown = false;
            var chosenRetirementValues = { yourAge: 0, retirementAge: 0, amountNeeded: 0 };
            var chosenGrowthValues = { yourAge: 0, payTerm: 0, amountToInvest: 0 };
            var chosenChildValues = { yourAge: 0, childAge: '', amountNeeded: 0, childPayTerm: 20 };
            var previousValue = 0;
            $scope.monthlyInvestment = 0;
            $scope.sliderObj = { min: 0, max: 100, value: 50, list: [] };
            $rootScope.pageClass = "choose";
            var plans = [{ 'name': 'retirement', 'selected': false, id: 2 }, { 'name': 'growth', 'selected': false, id: 1 }, { 'name': 'child', 'selected': false, id: 3}];

            $scope.plans = angular.copy(plans);

            if ($rootScope.isLanding) {
                $scope.discountsAndPremium = [];
                NewInvestmentService.getBanner().success(function (data) {
                    _.each(data, function (value) {
                        if (value.offerId === $location.$$search.offerid) {
                            $scope.imageUrl = value.imageUrl;
                            $scope.discountsAndPremium = value.discountAndPremium;
                        }
                    });

                });
            }


            $scope.details = {};
            function countUpValues(value) {
                if (value > 0) {
                    $scope.monthlyInvestment = value;
                    var options = {
                        useEasing: true,
                        useGrouping: true,
                        separator: ',',
                        decimal: '.',
                        prefix: 'Rs.',
                        suffix: ''
                    };
                    $timeout(function () {
                        var demo = new CountUp("myTargetElement", previousValue, value, 0, 1.5, options);
                        demo.start(function () {
                            /*$scope.$apply(function () {

                            });*/
                        });
                        previousValue = value;
                    }, 10);

                }
            }

            $scope.monthlyPayoutChange = function () {
                if (!($scope.isError || $scope.isTermError)) {
                    $scope.dontShowMessage = true;
                    $timeout(function () {
                        var postObj = {};
                        if (index === 1) {
                            chosenGrowthValues.amountToInvest = $scope.details.investment;
                            postObj.payterm = $scope.details.age;
                            postObj.premium = $scope.details.investment;
                        } else if (index === 2) {
                            var lifeExpectancy = 80;
                            postObj.payterm = $scope.details.age - $scope.details.yourAge <= 30 ? $scope.details.age - $scope.details.yourAge : 30;
                            postObj.premium = $scope.details.investment * ($scope.details.age < 80 ? (lifeExpectancy - $scope.details.age) : 1);

                            chosenRetirementValues.amountNeeded = $scope.details.investment;
                        } else {
                            chosenChildValues.amountNeeded = $scope.details.investment;
                            postObj.payterm = $scope.details.childTerm;
                            postObj.premium = $scope.details.investment;
                        }
                        var monthlyInvestment = 0;
                        if (index === 1) {
                            CommonService.getPremiumForPreQuote(postObj, index).then(function (value) {
                                monthlyInvestment = value.maturityBenefit;
                                countUpValues(monthlyInvestment);
                            });
                        } else if (index === 2) {
                            monthlyInvestment = CommonService.GetRetirementCalc($scope.details.investment, $scope.details.age, postObj.payterm);
                            countUpValues(monthlyInvestment);
                        } else if (index === 3) {
                            monthlyInvestment = CommonService.compoundInterest(postObj.premium, postObj.payterm);
                            countUpValues(monthlyInvestment);
                        }

                    }, 300);
                }
            };

            $scope.triggerSelectBox = function () {
                angular.element('.trigger-class-select').trigger('click');
            };

            function fillValues() {
                if (index === 1) {
                    $scope.index = 1;
                    $scope.investMin = 10000;
                    $scope.investMax = 200000;
                    $scope.investmentStep=1000;
                    $scope.firstTextHolder ="Please enter the amount that you wish to invest per year";
                    $scope.nameOfAgeHolder ="Select the number of years that you wish to pay for";
                    $scope.sliderObj.list = CommonService.getAmountList(10000, 100000, 125000, 500000, 1000, 25000);
                    $scope.sliderObj.max = $scope.sliderObj.list.length - 1;
                    $scope.firstText = "Invest per year";
                    $scope.secondText = "You will invest";
                    $scope.thirdText = "You will get";
                    $scope.finalText = "per year";
                    $scope.perText = "per month";
                    $scope.inYearsText = " in";
                    if (chosenGrowthValues.amountToInvest > 0) {
                        $scope.sliderObj.value = chosenGrowthValues.amountToInvest;
                        $scope.details.investment = $scope.sliderObj.list[chosenGrowthValues.amountToInvest];
                        $scope.details.investment = chosenGrowthValues.amountToInvest;
                        $scope.details.monthlyAmount = Math.round($scope.details.investment / 12);

                    } 
                    /**********************
                    else if (parseInt($scope.details.investment) <= 0 || $scope.details.investment === undefined) {
                        $scope.details.investment = 50000;
                    }
                    if ($scope.sliderObj.list.indexOf($scope.details.investment) < 0) {
                        $scope.details.investment = 50000;
                    }
                    **********************/
                    $scope.details.monthlyAmount = Math.round($scope.details.investment / 12);
                    $scope.sliderObj.value = $scope.sliderObj.list.indexOf($scope.details.investment);
                    chosenGrowthValues.amountToInvest = $scope.sliderObj.value;
                    if (chosenGrowthValues.payTerm > 0) {
                        $scope.details.age = chosenGrowthValues.payTerm;
                    }

                } else if (index === 2) {                    
                    $scope.index = 2;
                    $scope.investMin = 10000;
                    $scope.investMax = 200000;
                    $scope.investmentStep=1000;
                    $scope.firstTextHolder ="Please enter the monthly pension that you wish to receive post retirement";
                    $scope.nameOfAgeHolder ="Please enter the age at which you wish to retire";
                    $scope.sliderObj.list = CommonService.getAmountList(10000, 100000, 125000, 200000, 5000, 25000);
                    $scope.sliderObj.max = $scope.sliderObj.list.length - 1;
                    $scope.firstText = "Monthly pension";
                    $scope.secondText = "You will get";
                    $scope.thirdText = "You need to invest ";
                    $scope.finalText = "per month";
                    /*$scope.perText = "per month";*/
                    $scope.inYearsText = "per month";
                    if (chosenRetirementValues.amountNeeded > 0) {
                        $scope.sliderObj.value = chosenRetirementValues.amountNeeded;
                        $scope.details.investment = chosenRetirementValues.amountNeeded;
                    }                    
                    /************************
                    else if ((parseInt($scope.details.investment) <= 0 || $scope.details.investment === undefined) || parseInt($scope.details.investment) > 500000) {
                        $scope.details.investment = 50000;
                    }
                    if ($scope.sliderObj.list.indexOf($scope.details.investment) < 0) {
                        $scope.details.investment = 50000;
                    }
                    **********************/
                    $scope.details.monthlyAmount = Math.round($scope.details.investment / 12);
                    $scope.sliderObj.value = $scope.sliderObj.list.indexOf($scope.details.investment);
                    chosenRetirementValues.amountNeeded = $scope.sliderObj.value;
                    if (chosenRetirementValues.retirementAge > 0) {
                        $scope.details.age = chosenRetirementValues.retirementAge;
                    }
                } else {                    
                    $scope.investMin = 500000;
                    $scope.investMax = 50000000;
                    $scope.investmentStep=100000;
                    $scope.index = 3;                 
                    $scope.firstTextHolder ="Please enter the amount that you need for your Child’s future";
                    $scope.nameOfAgeHolder ="Please enter your child’s current age";
                    $scope.sliderObj.list = CommonService.getChildAmountList(500000, 5000000, 10000000, 50000000, 100000, 500000, 2500000);
                    $scope.sliderObj.max = $scope.sliderObj.list.length - 1;
                    $scope.firstText = "Amount you need";
                    $scope.secondText = "Your child gets";
                    $scope.thirdText = "You need to invest";
                    $scope.inYearsText = "per month";
                    if (chosenChildValues.amountNeeded > 0) {
                        $scope.sliderObj.value = chosenChildValues.amountNeeded;
                        $scope.details.investment = chosenChildValues.amountNeeded;
                    } 
                    /**********************
                    else if (parseInt($scope.details.investment) <= 0 || $scope.details.investment === undefined) {
                        $scope.details.investment = 2000000;
                    }
                    if ($scope.sliderObj.list.indexOf($scope.details.investment) < 0) {
                        $scope.details.investment = 2000000;
                    }
                    **********************/
                    $scope.details.monthlyAmount = Math.round($scope.details.investment / 12);
                    $scope.sliderObj.value = $scope.sliderObj.list.indexOf($scope.details.investment);
                    chosenChildValues.amountNeeded = $scope.sliderObj.value;
                    if (parseInt(chosenChildValues.childAge) >= 0) {
                        $scope.details.age = chosenChildValues.childAge;
                    } else {
                        $scope.details.age = '';
                    }
                    $scope.details.childTerm = chosenChildValues.childPayTerm;
                }
                $scope.details.index = index;
            }



            $scope.checkForError = function (value) {
                if (value !== undefined && value.toString().length == 2) {
                    $scope.focus = true;
                }
                $scope.isError = (value === undefined || value.length === 0) || (value > 60 || (index !== 2 && value < 18) || (index === 2 && value < 20) || (index === 2 && (value > $scope.details.age || $scope.details.age - value < 10)) || (index === 3 && (value - $scope.details.age < 18)));
                if (value !== undefined && value.toString().length == 2) {
                    var currentyear = new Date().getFullYear();
                    $scope.data.dobYear = currentyear - value;
                    $scope.data.dobDate = 1;
                    $scope.data.dobMonth = 1;
                    $scope.checkForTermError($scope.details.age);
                    $scope.monthlyPayoutChange();
                }
            };

            $scope.changeInYourAge = function () {
                if (index === 3) {
                    if ($scope.details.yourAge - $scope.details.age >= 18) {
                        chosenChildValues.yourAge = $scope.details.yourAge;
                    }
                } else {
                    if ($scope.details.yourAge >= 18) {
                        if (index === 1) {
                            chosenGrowthValues.yourAge = $scope.details.yourAge;
                        } else {
                            chosenRetirementValues.yourAge = $scope.details.yourAge;
                        }
                    }
                }
            };

            $scope.checkForTermError = function (value) {
                childTermChange();
                if (index === 1) {
                    $scope.isTermError = false;
                } else {
                    if (value !== undefined && value.toString().length > 0) {
                        $scope.focusObj.termFocus = true;
                    } else if (value !== undefined && value.toString().length === 0) {
                        $scope.focusObj.termFocus = false;
                    }
                    $scope.isTermError = (value === undefined || value.length === 0) || ((index === 3 && value > 17) || (index === 2 && (value < 50 || value > 80)) || (index === 2 && value < $scope.details.yourAge) || (index === 3 && $scope.details.yourAge - value < 18) || (index === 2 && value - $scope.details.yourAge < 10));
                    $timeout(function () {
                        if (!$scope.isError && !$scope.isTermError) {
                            $scope.monthlyPayoutChange();
                        }
                    }, 10);
                }
            };

            function childTermChange() {
                if (index === 3) {
                    $scope.details.age = parseInt($scope.details.age);
                    changeTermChild();
                }
            }

            $scope.changeInTerm = function (isValid) {
                $scope.checkForTermError($scope.details.age);
                $scope.checkForError($scope.details.yourAge);
                if (index === 2) {
                    chosenRetirementValues.retirementAge = $scope.details.age;
                } else if (index === 3) {
                    chosenChildValues.childAge = $scope.details.age;
                }
            };

            function changeTermChild() {

                $scope.details.childTerm = $scope.details.childTerm < 10 ? 10 : $scope.details.childTerm < 15 && $scope.details.childTerm > 10 ? 15 : $scope.details.childTerm > 15 ? 20 : 20;
                if ($scope.details.age >= 14) {
                    $scope.details.childTerm = 10;
                }
                if (parseInt($scope.details.childTerm) > 15 && (parseInt($scope.details.age) >= 9 && parseInt($scope.details.age) < 14)) {
                    $scope.details.childTerm = 15;
                }
                return parseInt($scope.details.childTerm);
            }

            $scope.changeInGrowthTerm = function (isValid) {
                $scope.checkForError($scope.details.yourAge, isValid);
                if (index === 1) {
                    chosenGrowthValues.payTerm = $scope.details.age;
                }
                if (isValid === undefined && !$scope.isError) {
                    $scope.monthlyPayoutChange();
                }
            };

            $scope.changeInChildTerm = function () {
                $scope.checkForTermError($scope.details.age);
                if (index === 3) {
                    chosenChildValues.childPayTerm = $scope.details.childTerm;
                }
                if (!$scope.isError && !$scope.isTermError) {
                    $scope.monthlyPayoutChange();
                }
            };

            $scope.changeInvestmentAmount = function () {
                if (!$scope.isError && !$scope.isTermError) {
                    $scope.monthlyPayoutChange();
                }
            };

            function showCustomerSection(form) {
                $scope.showStep = 'customer';
                $scope.stepNo = 2;
            }

            function sendMobileOTP(mobileNo) {
                var objLeadData = {};
                objLeadData.MobileNo = mobileNo;
                registerUpdateCustomer().success(function (data) {
                    NewInvestmentService.SendMobileOTP(objLeadData).success(function (data) {
                        var resp = data;
                    }).error(function (data) { });
                }).error(function (data) { });
            }

            function registerUpdateCustomer(enquiryId, SocialProfileID) {
                var obj = {};
                obj.IsAssistanceReq = true;
                obj.CountryID = $scope.details.CountryID;
                obj.CountryCode = $scope.details.CountryID;
                obj.CustomerName = $scope.details.CustomerName;
                obj.Email = $scope.details.Email;
                obj.MobileNo = $scope.details.MobileNo;
                obj.GenderID = $scope.details.gender;
                obj.IsNRI = $scope.details.CountryID != 392;
                obj.Title = $scope.details.gender === 1 ? 'Mr' : 'Ms/Mrs';
                obj.CityId = $scope.selectedCityItem ? $scope.selectedCityItem.cityId : 9999;
                obj.StateId = $scope.selectedCityItem ? $scope.selectedCityItem.stateId : 99;
                if (enquiryId !== undefined && enquiryId === 0)
                { obj.EnquiryId = enquiryId; }
                if (SocialProfileID !== undefined && SocialProfileID === 0)
                { obj.CustId = SocialProfileID; }
                return NewInvestmentService.RegisterCustomer(obj).success(function (data) { }).error(function (data) { });
            }

            $scope.ShowMobileVerification = function (isValid, form) {
                form.$submitted = true;
                if (!isValid) { return false; }
                ///isMobileNumberChanged();
                if(!$scope.IsOTPRequired){
                    $scope.createEnquiry(isValid, form);
                    $scope.isSMSVerified = true;
                    return false;
                }
                $scope.details.MobileNo_New = $scope.details.MobileNo;
                sendMobileOTP($scope.details.MobileNo_New);
                $scope.stepNo = 3;
                $scope.showStep = 'smsOTP';
                return false;
            };
            $scope.IsResendOTPValid = true;
            $scope.ResendOTP = function (src) {
                var resend = src === "resend";
                $scope.IsResendOTPValid = $scope.details.MobileNo_New !== '';
                if (!$scope.IsResendOTPValid) { $scope.ResendOTPError = "Enter mobile number"; return false; }
                $scope.IsResendOTPValid = $scope.details.MobileNo_New !== undefined && $scope.IsResendOTPValid && ($scope.details.MobileNo_New <= 9999999999 && $scope.details.MobileNo_New >= 6666666666) && ($scope.details.MobileNo_New.length === 10);
                if (!$scope.IsResendOTPValid) {
                    $scope.ResendOTPError = "Enter valid mobile number";
                    if (!resend) { $scope.Mobile_New_Disable = true; }
                    return false;
                }
                $scope.details.MobileNo = $scope.details.MobileNo_New;
                sendMobileOTP($scope.details.MobileNo_New);
                $scope.Mobile_New_Disable = true;
                return true;
            };
            $scope.isSMSVerified = true;
            $scope.VerifyOtpAndGo = function (form) {
                $scope.details.MobileNo_New = $scope.details.MobileNo;
                //var form = mobileForm;
                form.$submitted = true;
                var isValid = form.$valid;
                if (!isValid) { return; }
                var objLeadData = {};
                objLeadData.MobileNo = $scope.details.MobileNo_New;
                objLeadData.OTPsms = $scope.details.OTP;
                NewInvestmentService.VerifyMobileOTP(objLeadData).success(function (data) {
                    var resp = data;
                    var smsRight = resp.IsVerified;
                    if (resp.OKText === "1" && smsRight) {
                        $scope.createEnquiry(isValid, form);
                        $scope.isSMSVerified = true;
                    }
                    else {
                        $scope.isSMSVerified = false;
                    }
                }).error(function (data) {
                    $scope.isSMSVerified = false;
                });
            };
            $scope.createEnquiry = function (isValid, form) {
                $rootScope.isHeadIFrame = true;
                isValid = isValid && $scope.hideObject.investmentErrorMessageHide && $scope.hideObject.investmentErrorMessageHideForStep;
                isValid = (($scope.showStep !== 'enquiry') || checkForCity(isValid, form)) && isValid;
                if ($scope.details.yourAge < 18) { isValid = false; }
                if (isValid) {
                    $rootScope.slideFlag = 'right';                    
                    isValid = isValid && !$scope.isError && !$scope.isTermError;
                    if (form.$name === 'searchForm' && $scope.IsOTPRequired) {
                        showCustomerSection(form);
                        return;
                    }
                    if ($scope.details.CountryID === 392) {
                        isValid = $scope.details.MobileNo <= 9999999999 && $scope.details.MobileNo >= 6666666666;
                        $scope.errorMessageHide = $scope.details.MobileNo.length != 10 || !isValid;
                    }
                    $rootScope.slideFlag = 'right';
                    var cityObj = {};
                    cityObj.CityID = $scope.selectedCityItem.cityId;
                    cityObj.City = $scope.selectedCityItem.value;
                    cityObj.stateId = $scope.selectedCityItem.stateId;
                    if (isValid) {
                        $rootScope.loaderProceed = true;
                        $scope.buttonName = "Please Wait";
                        CommonService.saveEnquiryAndGenerateLead(index, $scope.details, $scope.monthlyInvestment, $scope.data, cityObj).then(function (data) {
                            $rootScope.loaderProceed = false;
                            if (data) {
                                /// $scope.buttonName = "Continue";
                            } else {
                                $scope.buttonName = "Retry";
                            }
                        });
                    }
                }
            };

            $scope.showDropDownfunc = function () {
                $scope.showDropDown = !$scope.showDropDown;
            };
            $scope.isAnyPlanSelected = function (planId) {
                previousValue = 0;
                angular.element(function () {
                    if ($rootScope.isLanding) {
                        $('html,body').animate({ scrollTop: 0 }, 600);
                    } else {
                        $('html,body').animate({ scrollTop: 255 }, 600);
                    }
                });
                if (planId !== index) {


                    $scope.plans = angular.copy(plans);
                    if (planId === 1) {
                        $scope.plans[1].selected = true;
                    } else if (planId === 2) {
                        $scope.plans[0].selected = true;
                    } else {
                        $scope.plans[2].selected = true;

                    }
                    index = CommonService.whichPlanSelected($scope.plans);
                    $scope.ages = CommonService.getAges(index);
                    /*****************************
                     $scope.details.age = CommonService.getAge(index);
                    *****************************/
                    $scope.nameOfAge = CommonService.getTypeName(index);
                    fillValues();
                    $scope.checkForError($scope.details.yourAge);
                    $scope.checkForTermError($scope.details.age);
                    if ($scope.details.yourAge !== undefined && $scope.details.yourAge.toString().length == 2) {
                        $timeout(function () {
                            $scope.monthlyPayoutChange();
                        }, 200);
                    }

                    if ($scope.planSelected) {
                        $scope.planSelected = false;
                        $timeout(function () {
                            $scope.planSelected = true;
                        }, 500);
                    } else {
                        $scope.planSelected = true;
                    }
                }
            };

            $scope.data = {
                dobDate: null,
                dobMonth: null,
                dobYear: null
            };

            $scope.changeDate = function (model) {
                $scope.data.dobDate = model;
                $scope.changeDateOfBirth();
            };

            $scope.changeMonth = function (model) {
                $scope.data.dobMonth = model;
                $scope.changeDateOfBirth();
            };

            $scope.changeYear = function (model) {
                $scope.data.dobYear = model;
                $scope.changeDateOfBirth();
            };

            $scope.changeDateOfBirth = function () {
                $scope.dobObj.dob = ($scope.data.dobDate !== undefined && $scope.data.dobDate !== null && $scope.data.dobDate.length === 1 ? '0' : '') + $scope.data.dobDate + "-" + ($scope.data.dobMonth !== undefined && $scope.data.dobMonth !== null && $scope.data.dobMonth.length === 1 ? '0' : '') + $scope.data.dobMonth + "-" + $scope.data.dobYear;
                if ($scope.isValidDateOfBirth()) {
                    $scope.details.yourAge = CommonService.getYourAge($scope.dobObj.dob);
                    $scope.checkForError($scope.details.yourAge);
                }
            };

            $scope.isAgeWithinRange = function () {
                return !$scope.isDOBMissing() && $scope.isValidDateOfBirth();
            };

            $scope.isAgeBelowMinimum = function () {
                return !$scope.isDOBMissing() && $scope.isValidDateOfBirth() && CommonService.getYourAge($scope.dobObj.dob) < 18;
            };

            $scope.isAgeAboveMaximum = function () {
                return !$scope.isDOBMissing() && $scope.isValidDateOfBirth() && CommonService.getYourAge($scope.dobObj.dob) > 65;
            };

            $scope.isValidAge = function () {
                return !$scope.isDOBMissing() && $scope.isValidDateOfBirth() && CommonService.getAge($scope.dobObj.dob) >= 18 && CommonService.getAge($scope.dobObj.dob) <= 65;
            };

            $scope.isYearSelected = function (index, dobYear) {
                return (index * 1 === $scope.dob.year.indexOf(parseInt(dobYear)));
            };

            $scope.isDOBMissing = function () {
                return (!$scope.data.dobDate || !$scope.data.dobMonth || !$scope.data.dobYear);
            };

            $scope.isValidDateOfBirth = function () {
                return $scope.dobObj.dob && CommonService.isValidDateOfBirth($scope.dobObj.dob);
            };

            function FillObjectFromLead() {
                var leadID = $location.$$search.leadid !== undefined ? $location.$$search.leadid : 0;
                if (leadID === 0) { return; }
                var objLeadData = {};
                objLeadData.MatrixLeadID = leadID;
                NewInvestmentService.getCity().success(function (data) {
                    $scope.city = CommonService.getCity(data.Data.CityList);
                    NewInvestmentService.GetLeadInfoByLeadId(objLeadData).success(function (data) {
                        $scope.selectedCityItem = CommonService.findCityFromId($scope.city, parseInt(data.CityID)) || {};
                        ///$scope.isAnyPlanSelected(parseInt(data.InvestmentTypeID));
                        index = parseInt(data.InvestmentTypeID);
                        var uGender = data.Gender;
                        if (isNaN(uGender)) { $scope.details.gender = uGender === 'Male' ? 1 : 2; }
                        else {
                            $scope.details.gender = data.Gender;
                        }
                        $scope.details.CustomerName = data.Name;
                        $scope.details.Email = data.EmailID;
                        $scope.details.MobileNo = data.MobileNo;
                        var dobDt = new Date(data.DOB);
                        $scope.data.dobDate = dobDt.getDate();
                        $scope.data.dobMonth = dobDt.getMonth() + 1;
                        $scope.data.dobYear = dobDt.getFullYear();
                        if (index === 1) {
                            $scope.details.age = parseInt(data.PayTerm);
                        }
                        $scope.index = index;
                        $scope.plans = angular.copy(plans);
                        if (index === 1) {
                            $scope.details.investment = obj.investment;
                        } else {
                            $scope.details.investment = obj.requiredAmt;
                        }
                        $scope.ages = CommonService.getAges(index);
                        $scope.details.age = CommonService.getAge(index);
                        $scope.nameOfAge = CommonService.getTypeName(index);

                        if ($scope.data !== null) {
                            $scope.dobObj.dob = (($scope.data.dobDate !== null && $scope.data.dobDate !== undefined) && $scope.data.dobDate.length === 1 ? '0' : '') + $scope.data.dobDate + "-" + (($scope.data.dobMonth !== null && $scope.data.dobMonth !== undefined) && $scope.data.dobMonth.length === 1 ? '0' : '') + $scope.data.dobMonth + "-" + $scope.data.dobYear;
                        }
                        $scope.details.yourAge = CommonService.getYourAge($scope.dobObj.dob);
                        $scope.planSelected = true;

                        $scope.plans = angular.copy(plans);
                        if (index === 1) {
                            $scope.plans[1].selected = true;
                        } else if (index === 2) {
                            $scope.plans[0].selected = true;
                        } else if (index === 3) {
                            $scope.plans[2].selected = true;

                        }
                        if ($scope.details.yourAge !== undefined && $scope.details.yourAge.toString().length == 2) {
                            $scope.checkForError($scope.details.yourAge);
                        }
                        fillValues();

                    }).error(function (data) { });

                }).error(function (data) { });

            }
            $scope.details.CountryID = 392;
            $scope.IsOTPRequired = true;
            function fillValuesByOthers() {
                $scope.details.CustomerName = $location.$$search.name || '';
                $scope.details.Email = $location.$$search.email || '';
                $scope.details.MobileNo = $location.$$search.mobileNo || '';
                $scope.IsOTPRequired = $location.$$search.mobileNo ?  false:true ;
            }
            function isMobileNumberChanged(){
            var obj = {};
                if ($localStorage.customerDetails === undefined) {
                    $scope.IsOTPRequired = true;
                    return;
                }
                obj = $localStorage.customerDetails;
                $scope.IsOTPRequired = $scope.details.MobileNo !== obj.MobileNo ;
                return;
            }
            
            function loadCustomerDetails(){         
                var obj = {};
                if ($localStorage.customerDetails !== undefined) {
                    obj = $localStorage.customerDetails;
                }               
                NewInvestmentService.getCity().success(function (data) {
                    $scope.city = CommonService.getCity(data.Data.CityList);
                    $scope.selectedCityItem = CommonService.findCityFromId($scope.city, obj.CityID) || {};
                    $scope.details.CustomerName = obj.CustomerName;
                    $scope.details.Email = obj.Email;
                    $scope.details.MobileNo = obj.MobileNo || '';
                    $scope.details.gender = obj.GenderID;   
                    $scope.details.CountryID = obj.CountryID || 392;
                }).error(function (data) {
                });
            }
            
            
            
            function onLoad() {
                ///$scope.details.annual = defaultIncome;
                $("div.zopim").hide();
                fillValuesByOthers();
                var obj = $localStorage.searchDetails;
                if (!CommonService.isEmptyObject(obj)) {
                    index = obj.investmentFor > 0 ? obj.investmentFor : 1;                    
                    if ($location.$$search.planType) {
                     index = parseInt($location.$$search.planType);
                    }
                    $scope.index = index;
                    $scope.details.gender = obj.gender;
                    $scope.details.yourAge = obj.yourAge;
                    $scope.details.age = index === 1 ? obj.payTerm : index === 2 ? obj.retirementAge : obj.childAge;
                    chosenChildValues.childAge = index === 3 ? obj.childAge : '';
                    chosenChildValues.childPayTerm = index === 3 ? obj.payTerm : 20;
                    $scope.plans = angular.copy(plans);
                    if (index === 1) {
                        $scope.plans[1].selected = true;
                    } else if (index === 2) {
                        $scope.plans[0].selected = true;
                    } else {
                        $scope.plans[2].selected = true;
                    }
                    $scope.data = {};
                    $scope.data.dobDate = obj.dobDate;
                    $scope.data.dobMonth = obj.dobMonth;
                    $scope.data.dobYear = obj.dobYear;
                    if ($scope.data !== null) {
                        $scope.dobObj.dob = (($scope.data.dobDate !== null && $scope.data.dobDate !== undefined) && $scope.data.dobDate.length === 1 ? '0' : '') + $scope.data.dobDate + "-" + (($scope.data.dobMonth !== null && $scope.data.dobMonth !== undefined) && $scope.data.dobMonth.length === 1 ? '0' : '') + $scope.data.dobMonth + "-" + $scope.data.dobYear;
                    }
                    $scope.details.yourAge = CommonService.getYourAge($scope.dobObj.dob);
                    $scope.planSelected = true;
                    $scope.ages = CommonService.getAges(index);
                    $scope.nameOfAge = CommonService.getTypeName(index);
                    if (index === 1) {   
                        var x =parseInt(obj.investment.toString().replace(/,/g,''));
                        $scope.details.investment = Math.round(x * 12 / 1000) * 1000; 
                    } else {
                        $scope.details.investment = obj.requiredAmt;
                    }

                    $scope.details.annual = obj.annualIncome;
                    fillValues();
                    if ($scope.details.yourAge !== undefined && $scope.details.yourAge.toString().length == 2) {
                        $scope.checkForError($scope.details.yourAge);
                        /*$scope.monthlyPayoutChange();*/
                    }
                }
                loadCustomerDetails();
                if ($rootScope.isLanding) {
                    //$scope.isAnyPlanSelected(parseInt($location.$$search.offerid));
                    $scope.isAnyPlanSelected(parseInt($location.$$search.planType));
                }
                else if ($location.$$search.planType) {
                    $scope.isAnyPlanSelected(parseInt($location.$$search.planType));
                    /*$location.search("planType",null);*/
                }
                else if (index === undefined) {
                    $scope.isAnyPlanSelected(1);
                }
            }

            $scope.selectedCityItem = {};

            $scope.blurObject = { customerNameBlurOut: false, emailBlurOut: false, mobileBlurOut: false, countryBlurOut: false, cityBlurOut: false, investmentBlurOut: false, annualBlurOut: false };

            $scope.trueThat = function (value) {
                if (value === 'customer') {
                    $scope.blurObject.customerNameBlurOut = true;
                } else if (value === 'email') {
                    $scope.blurObject.emailBlurOut = true;
                } else if (value === 'mobile') {
                    $scope.blurObject.mobileBlurOut = true;
                } else if (value === 'country') {
                    $scope.blurObject.countryBlurOut = true;
                } else if (value === 'city') {
                    $scope.blurObject.cityBlurOut = true;
                } else if (value === 'investment') {
                    $scope.blurObject.investmentBlurOut = true;
                } else if (value === 'annual') {
                    $scope.blurObject.annualBlurOut = true;
                }
            };

            $rootScope.leave = false;
            $scope.citySearchText = "";
            $scope.errorPresent = false;
            $scope.details = {};
            
            $scope.countryCodes = {};
            $scope.countryCodes.output = [];
            $scope.details.IsAssistanceReq = false;
            $scope.customerProceed = false;
            $scope.custFormSubmitName = "View Quotes";
            $scope.maxNum = 10;
            $scope.minNum = 10;
            $scope.buttonName = "View Quotes";
            var enquiryId = CommonService.decode($location.$$search.enquiryId) || $localStorage.equiryid;
            NewInvestmentService.getCountryCodes().success(function (data) {
                $scope.countryCodes = data;
            }).error(function (data) {

            });
            $scope.changeMaxLength = function (countryId) {
                if (countryId == 392) {
                    $scope.maxNum = 10;
                    $scope.minNum = 10;
                } else {
                    $scope.errorMessageHide = false;
                    $scope.maxNum = 18;
                    $scope.minNum = 0;
                }
            };

            // validation for city // PayTm Controller
            var checkForCity = function (isvalid, searchForm) {
                if ($scope.selectedCityItem === undefined || $scope.selectedCityItem === null || Object.keys($scope.selectedCityItem).length < 1) {
                    $scope.selectedCityItem = CommonService.findCityFromText($scope.city, $scope.citySearchText);
                    if ($scope.selectedCityItem === null || Object.keys($scope.selectedCityItem).length < 1) {
                        searchForm.cityDetails.$invalid = true;
                        $scope.errorPresent = true;
                        $scope.cityErrorMessage = "Enter city name";
                    }
                }
                return !searchForm.cityDetails.$invalid;
            };

            $scope.selectedCityItemChange = function (cityItem) {
               if(!CommonService.isEmptyObject(cityItem)){
                    $scope.errorPresent = false;
                }
                $scope.selectedCityItem = cityItem;
            };

            $scope.searchCityTextChange = function (citySearchText) {
                if (citySearchText != $scope.citySearchText) {
                    $scope.selectedCityItem = {};
                    $scope.citySearchText = citySearchText;
                }
                $scope.trueThat('city');
            };
            onLoad();
            $timeout(function () {
                $("input[name='cityDetails']").bind("keypress", function (e) { if (this.value === $scope.citywaterMark) { this.value = ''; } });
                $("input[name='cityDetails']").bind("blur", function (e) { if (this.value === "") { this.value = $scope.citywaterMark; } });
            });
        }
    ]);

