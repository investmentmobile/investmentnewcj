
/**
 * Created by Prashants on 8/12/2015.
 */
app.controller('CustomerController',
    [
        '$scope','$rootScope', 'CommonService','NewInvestmentService','$location','$cookies','$localStorage',
        function ($scope, $rootScope,CommonService,NewInvestmentService,$location,$cookies,$localStorage) {
            'use strict';
            $rootScope.pageClass = 'customer';
            $scope.changeSlideFlag = function(flag) {
                $rootScope.slideFlag = flag;
            };

            $scope.privacy = true;
            $rootScope.slideFlag = angular.isDefined($rootScope.slideFlag) ? $rootScope.slideFlag : 'right';

            $scope.blurObject = {customerNameBlurOut:false,emailBlurOut:false,mobileBlurOut:false,countryBlurOut:false,cityBlurOut:false};

            $scope.cityChangesObj = {};
            $scope.cityChangesObj = CommonService.prefillCityChangeObject($scope.cityChangesObj);

            $scope.trueThat = function(value){
                $scope.blurObject = CommonService.getBlurObjectChange(value,$scope.blurObject);
            };

            $rootScope.leave = false;

            $scope.errorPresent = false;
            var obj = {};
            if($localStorage.customerDetails!==undefined) {
                obj = $localStorage.customerDetails;
            }


            NewInvestmentService.getCity().success(function(data){
                $scope.selectedCityItem = {};
                var newData = _.uniq(data.Data.CityList,function(a){
                    return a.CityId;
                });

                var newDelhiObject = {"CityId":"551","CityName":"New Delhi","StateId":"35","state":"Delhi"};
                newData.unshift(newDelhiObject);
                $scope.city = CommonService.getCity(newData);
                if(obj.CountryID*1===392 && obj.CityID!==9999) {
                    $scope.selectedCityItem = CommonService.findCityFromId($scope.city, obj.CityID) || {};
                }
                if(obj.CountryID*1!==392){
                    $scope.cityChangesObj.citySearchText = "Others";
                }else if(CommonService.isEmptyObject($scope.selectedCityItem)){
                    $scope.cityChangesObj.citySearchText = "";
                }
                $scope.changeMaxLength($scope.customerEntity.CountryID);
            }).error(function(data){

            });

            $scope.customerEntity = {};
            $scope.customerEntity.CountryID = obj.CountryID!==undefined?obj.CountryID:"392";
            CommonService.prefillCustomerEntityObject($scope.customerEntity,obj);
            $scope.countryCodes = {};

            $scope.customerProceed = false;
            $scope.custFormSubmitName = "View Quotes";
            $scope.maxNum = 10;
            $scope.minNum = 10;
            $scope.buttonName = "View Quotes";
            $scope.cityChangesObj.countryCode = obj.countryCode*1;
            var enquiryId =  CommonService.decode($location.$$search.enquiryId)||$localStorage.equiryid;

            $scope.searchForCountryCode = function(countryId){

                $scope.cityChangesObj = CommonService.changeCityChangeObj($scope.countryCodes,$scope.cityChangesObj, countryId, obj);
                $scope.cityChangesObj.countryCode = CommonService.CheckForExistingCountryCodes($scope.cityChangesObj,$scope.countryCodes)?"":$scope.cityChangesObj.countryCode;
            };

            $scope.checkIfAvailableList = function(countryId){
                _.each($scope.countryCodes,function(val){
                    if(val.CountryCode===(countryId)*1){
                        $scope.customerEntity.CountryID = val.CountryCodeId;
                        $scope.cityChangesObj.isEditable = false;
                        $scope.changeMaxLength($scope.customerEntity.CountryID);
                        $scope.searchForCountryCode($scope.customerEntity.CountryID);
                    }
                });
            };

            NewInvestmentService.getCountryCodes().success(function(data){
                $scope.countryCodes = data;
                $scope.customerEntity.CountryID = obj.CountryID!==undefined?obj.CountryID.toString():"392";
                $scope.searchForCountryCode($scope.customerEntity.CountryID);

            }).error(function(data){

            });
            $scope.changeMaxLength = function(countryId){
                $scope.cityChangesObj.isNRI = false;
                $scope.cityChangesObj.citySearchText = 'Others';
                if(countryId*1 === 392){
                    $scope.cityChangesObj.isNRI = false;
                    $scope.cityChangesObj.minValue = 2;
                    if(CommonService.isEmptyObject($scope.selectedCityItem)){
                        $scope.cityChangesObj.citySearchText = '';
                    }

                }else{
                    $scope.cityChangesObj.minValue = 7;

                    $scope.cityChangesObj.citySearchText = 'Others';
                    $scope.cityChangesObj.isNRI = true;
                    $scope.errorMessageHide=false;
                    $scope.maxNum = 11;
                    $scope.minNum = 8;
                }
                _.each($scope.countryCodes,function(val){
                    if(countryId===val.CountryCodeId){
                        $scope.maxNum = val.MAX;
                        $scope.minNum = val.MIN;
                    }
                });
            };

            // validation for city
            var checkForCity = function(isvalid,searchForm){
                if($scope.selectedCityItem===undefined || $scope.selectedCityItem === null || Object.keys($scope.selectedCityItem).length<1){
                    $scope.selectedCityItem = CommonService.findCityFromText($scope.city,$scope.cityChangesObj.citySearchText);
                    if($scope.selectedCityItem === null || Object.keys($scope.selectedCityItem).length<1) {
                        searchForm.cityDetails.$invalid = true;
                        $scope.errorPresent = true;
                        $scope.cityErrorMessage = "Enter city name";
                    }
                }
                return !searchForm.cityDetails.$invalid;
            };

            $scope.searchCityTextChange = function(citySearchText){
                if(citySearchText!==$scope.cityChangesObj.citySearchText) {
                    if($scope.selectedCityItem===undefined) {
                        $scope.selectedCityItem = {};
                    }
                    $scope.citySearchText = citySearchText;
                }
            };

            /*$scope.$watch('cityChangesObj.citySearchText',function(val){
                if(CommonService.isEmptyObject($scope.selectedCityItem) && $scope.customerEntity.CountryID===999){
                    //$scope.cityChangesObj.citySearchText = $scope.cityChangesObj.citySearchText===undefined?"Others":$scope.cityChangesObj.citySearchText;
                    $scope.selectedCityItem = CommonService.findCityFromId($scope.city, obj.CityID) || {};
                }
            });*/

            $scope.selectedCityItemChange = function(cityItem){
                $scope.errorPresent = false;
                if(cityItem!==undefined &&cityItem!==null && $scope.selectedCityItem!==undefined && $scope.selectedCityItem!==null) {
                    if ($scope.selectedCityItem.cityId !== cityItem.cityId) {
                        if (cityItem !== undefined && cityItem !== null) {
                            $scope.selectedCityItem = cityItem;
                        }
                    }
                }
            };

            $scope.populateCustomerDetails = function(isValid,form){
                    if ($scope.customerEntity.CountryID*1 === 392) {
                        isValid = isValid && ($scope.customerEntity.MobileNo <= 9999999999 && $scope.customerEntity.MobileNo >= 6666666666);
                        $scope.errorMessageHide = $scope.customerEntity.MobileNo.length != 10 || !isValid;
                    }
                        $rootScope.slideFlag = 'right';
                    if ($scope.customerEntity.CountryID*1 === 392) {
                        isValid = checkForCity(isValid, form) && isValid;
                    }
    				if (!$scope.privacy) {
                        isValid = false;
                    }
                    if (isValid) {
                        $scope.customerProceed = true;
                        $scope.buttonName = "Please Wait";

                       obj = CommonService.setCustomerValuesToStorage($scope.customerEntity,$scope.selectedCityItem,$scope.cityChangesObj.countryCode);


                        $localStorage.customerDetails = obj;
                        CommonService.saveCustomerDetails(obj);

                        var searchObj = $localStorage.searchDetails||{};
                        searchObj.gender = $scope.customerEntity.gender;
                        CommonService.setSearchDetails(searchObj);
                        $localStorage.searchDetails = searchObj;
                        
                        CommonService.GetCustIDFromCustProfileCookie().then(function(data){
                        var SocialProfileID =0;
                            var custIDJson = {};
                            if(data!=="" && data) {
                                custIDJson = JSON.parse(data);
                            }
                        SocialProfileID = custIDJson.SocialProfileID || 0;
                        NewInvestmentService.RegisterCustomer(populateCustomerData(SocialProfileID)).success(function (data) {

                            if (!data.ResponseDetails.HasError) {
                               // CommonService.setCookies('CustId',data.CustId);
                                var custObj  ={};
                                var objCust ={};
                                custObj.key ="CustProfileCookie";
                                NewInvestmentService.GetHttpValues(custObj).success(function(response){
                                    var customerProfileData = {};
                                    if(response.d && response.d!=="") {
                                        customerProfileData = JSON.parse(response.d);
                                    }
                                objCust.CustID=data.CustId;
                                objCust.VisitorToken=customerProfileData.VisitorToken;
                                objCust.SocialProfileID=customerProfileData.SocialProfileID || "";           
                                var value ="'"+ JSON.stringify(objCust)+"'";
                                var dataToSend = '{"key":"CustProfileCookie","value":'+value+',"expires":-1}';
                                NewInvestmentService.SetHttpValues(dataToSend);
                                    CommonService.setShouldPageSkipCookie();
                                    CommonService.goToQuotesPage(enquiryId);
                                    $scope.buttonName = "View Quotes";
                                    /*$scope.customerProceed = false;*/
                                });

                            } else {
                                $scope.customerProceed = false;
                                CommonService.showQuoteToast();
                                $scope.buttonName = "Retry";
                            }


                        }).error(function () {
                            CommonService.showQuoteToast();
                        });
                    });
                    }
            };

            function populateCustomerData(SocialProfileID){
                var obj = {};
                obj.IsAssistanceReq = !$scope.customerEntity.IsAssistanceReq;
                obj.CountryID = $scope.customerEntity.CountryID;
                obj.CountryCode = $scope.cityChangesObj.countryCode;
                obj.CustomerName = $scope.customerEntity.CustomerName;
                obj.Email = $scope.customerEntity.Email;
                obj.MobileNo = $scope.customerEntity.MobileNo;
                obj.GenderID = $scope.customerEntity.gender;
                obj.IsNRI = $scope.customerEntity.CountryID!="392";
                obj.Title = $scope.customerEntity.gender===1?'Mr':'Ms/Mrs';
                obj.CityId = $scope.selectedCityItem?$scope.selectedCityItem.cityId:9999;
                obj.StateId = $scope.selectedCityItem?$scope.selectedCityItem.stateId:99;
                obj.EnquiryId = enquiryId;
                obj.CustId = SocialProfileID;
                return obj;
            }

            $scope.goBackToHome = function(){
                $rootScope.slideFlag = 'left';
                $location.path('/choose');
            };
        }
    ]);
