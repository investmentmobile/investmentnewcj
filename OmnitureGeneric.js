

    var userType = "";

    var callOnPageBottom = function () {
        this.pageName = "";
        this.channel = "";
        this.subSection1 = "";
        this.subSection2 = "";
        this.subSection3 = "";
        this.userType = "";

        this.pageBottomEvent = function () {
            var digitData = {
                page: {
                    'pageName': this.pageName,
                    'channel': this.channel,
                    'subSection1': this.subSection1,
                    'subSection2': this.subSection2,
                    'userType': this.userType
                }
            };
            return digitData;
        };
    };

    var callOnPageParam = function () {
        this.pageName = "";
        this.channel = "";
        this.subSection1 = "";
        this.subSection2 = "";
        this.subSection3 = "";
        this.userType = "";

        this.pageParamEvent = function () {
            var digitData = {
                page: {
                    'pageName': this.pageName,
                    'channel': this.channel,
                    'subSection1': this.subSection1,
                    'subSection2': this.subSection2,
                    'subSection3': this.subSection3,
                    'userType': this.userType
                }
            };
            return digitData;
        };
    };

    function getuserType() {
        userType = "guest";
        return userType;
    }

    userType = getuserType();

    var callOnPageFilter = function () {

        this.pageName = '';
        this.channel = '';
        this.subSection1 = '';
        this.subSection2 = '';
        this.customerId = '';
        this.userType = '';
        this.filterApplied = '';
        this.product = '';
        this.leadid = '';

        this.pageFilterEvent = function () {
            var digitData = {
                page: {
                    'pageName': this.pageName,
                    'channel': this.channel,
                    'subSection1': this.subSection1,
                    'subSection2': this.subSection2,
                    'userType': this.userType,
                    'filterApplied': this.filterApplied,
                    'product': this.product,
                    'leadid': this.leadid,
                    'customerId': this.customerId

                }
            };
            return digitData;
        };
    };

    var callOnQuoteSelection = function () {

        this.pageName = '';
        this.channel = '';
        this.subSection1 = '';
        this.subSection2 = '';
        this.enquiryId = '';
        this.userType = '';
        this.customerId = '';
        this.leadid = '';
        this.planname = '';

        this.quoteSelectionEvent = function () {
            var digitData = {
                page: {
                    'pageName': this.pageName,
                    'channel': this.channel,
                    'subSection1': this.subSection1,
                    'subSection2': this.subSection2,
                    'userType': this.userType,
                    'enquiryId': this.enquiryId,
                    'product': this.product,
                    'leadid': this.leadid,
                    'customerId': this.customerId

                }
            };
            return digitData;
        };
    };	
   
	var callOnEnquiryFormStart = function () {
        this.pageName = '';
        this.channel = '';
        this.subSection1 = '';
        this.subSection2 = '';
        this.subSection3 = '';
        this.customerId = '';
        this.userType = '';
        this.formname = 'Investment-prequotes';   
        this.journeyType = '';   		
        this.enquiryFormStartEvent = function () {
            var digitData = {
                page: {
                    'pageName': this.pageName,
                    'channel': this.channel,
                    'subSection1': this.subSection1,
                    'subSection2': this.subSection2,
                    'subSection3': this.subSection3,
                    'userType': this.userType,
                    'customerId': this.customerId,
                    'formname': this.formname,
                    'journeyType':this.journeyType				
                }
            };
            return digitData;
        };
    };

    var callOnEnquiryGeneration = function () {
        this.pageName = '';
        this.channel = '';
        this.subSection1 = '';
        this.subSection2 = '';
        this.subSection3 = '';
        this.customerId = '';
        this.enquiryId = '';
        this.userType = '';
        this.formname = 'Investment-prequotes';
        this.journeyType ='';
    this.enquiryGenerationEvent = function () {
        var digitData = {
            page: {
                'pageName': this.pageName,
                'channel': this.channel,
                'subSection1': this.subSection1,
                'subSection2': this.subSection2,
                'subSection3': this.subSection3,
                'userType': this.userType,
                'customerId': this.customerId,
                'enquiryId': this.enquiryId,
                'formname': this.formname,
                'journeyType': this.journeyType
            }
        };
        return digitData;
    };
};

var callOnLeadGeneration = function () {
    this.pageName = '';
    this.channel = '';
    this.subSection1 = '';
    this.subSection2 = '';
    this.leadid = '';
    this.customerId = '';
    this.enquiryId = '';
    this.userType = '';
    this.age = '';
    this.gender = '';
    this.city = '';
    this.formname = 'Investment-prequotes';
    this.journeyType = '';

    this.leadGenerationEvent = function () {
        var digitData = {
            page: {
                'pageName': this.pageName,
                'channel': this.channel,
                'subSection1': this.subSection1,
                'subSection2': this.subSection2,
                'leadid': this.leadid,
                'userType': this.userType,
                'customerId': this.customerId,
                'enquiryId': this.enquiryId,
                'formname': this.formname,
                'journeyType': this.journeyType,
                'age': this.age,
                'gender': this.gender,
                'city': this.city
            }
        };
        return digitData;
    };
};

    var omnitureCallBackEvents = function () {
        this.filterUsed = function () {
            _satellite.track('filterUsedCLEID');
        };
        this.compared = function () {
            _satellite.track('compared');
        };
        this.leadGeneration = function () {
            _satellite.track('enqlead-generated');
        };

        this.formStart = function () {
            _satellite.track('formstart');
        };
        this.enquiryGeneration = function () {
            _satellite.track('FormStepOneEnq');
        };
        this.pageBottom = function () {
            _satellite.pageBottom();
        };
        this.pageParam = function () {
            _satellite.track('Pageparam');
        };
        this.productSelection = function () {
            _satellite.track('productSelection');
        };
        this.formstep1Complete = function () {
            _satellite.track('formstep-1-Complete');
        };
        this.formSubmitted = function () {
            _satellite.track('form-submitted');
        };
        this.buyPlanClick = function () {
            _satellite.track('continue-btn');
        };
    };

    triggerPageBottomEvents = function(pageName) {
        try {
            var ominiObj = new callOnPageBottom();
            ominiObj.pageName = 'ac:investment-insurance-' + pageName;
            ominiObj.channel = 'Investment Insurance';
            ominiObj.subSection1 = 'Home';
            ominiObj.subSection2 = 'Investment Insurance';
            ominiObj.userType = getuserType();
            digitalData = ominiObj.pageBottomEvent();
            var OminiEvent = new omnitureCallBackEvents();
            OminiEvent.pageBottom();
        }
        catch (e) {
        }
    };

    triggerPageParamEvents = function(pageName) {
        try {
            var ominiObj = new callOnPageParam();
            ominiObj.pageName = 'pb:investment-insurance-' + pageName;
            ominiObj.channel = 'Investment Insurance';
            ominiObj.subSection1 = 'Home';
            ominiObj.subSection2 = 'Investment Insurance';
            ominiObj.subSection3 = 'Investment Insurance';
            ominiObj.userType = getuserType();
            digitalData = ominiObj.pageParamEvent();
            var OminiEvent = new omnitureCallBackEvents();
            OminiEvent.pageParam();
        }
        catch (e) {
        }
    };

    triggerPageFilterEvents = function(OmniData, customerid, leadid, filters) {
        try {
            var ominiObj = new callOnPageFilter();
            ominiObj.pageName = 'pb:investment-insurance-quotes';
            ominiObj.channel = 'Investment Insurance';
            ominiObj.subSection1 = 'Home';
            ominiObj.subSection2 = 'Investment Insurance';
            ominiObj.userType = getuserType();
            if (filters !== undefined) {
                ominiObj.filterApplied = filters.join('|');
            } else {
                ominiObj.filterApplied = '';
            }
            ominiObj.product = ';' + convertSpecialCharacterToHTML(OmniData);
            ominiObj.customerId = customerid;
            ominiObj.leadid = leadid;
            digitalData = ominiObj.pageFilterEvent();
            var OminiEvent = new omnitureCallBackEvents();
            OminiEvent.filterUsed();
        }
        catch (e) {
        }
    };

triggerLeadGenerationEvent = function (leadId, enquiryId, customerId, investmentTypeID,leadObj) {
    try {
        var ominiObj = new callOnLeadGeneration();
        investmentTypeID = investmentTypeID * 1;
        var investmentType = "Growth";
        if (investmentTypeID == 2) { investmentType = "Retirement"; }
        else if (investmentTypeID == 3) { investmentType = "Child"; }
        ominiObj.pageName = 'pb:investment-insurance-quotes';
        ominiObj.channel = 'Investment Insurance';
        ominiObj.subSection1 = 'Home';
        ominiObj.subSection2 = 'Investment Insurance';
        ominiObj.leadid = leadId;
        ominiObj.userType = 'guest';
        ominiObj.customerId = customerId;
        ominiObj.enquiryId = enquiryId;
        ominiObj.formname = 'Investment-preQuotes';
        ominiObj.journeyType = investmentType;
        ominiObj.age = leadObj.age;
        ominiObj.gender = leadObj.gender;
        ominiObj.city = leadObj.city;
        digitalData = ominiObj.leadGenerationEvent();
        var OminiEvent = new omnitureCallBackEvents();
        OminiEvent.leadGeneration();
       // OminiEvent.formSubmitted();
        return digitalData;
    }
    catch (e) {
    }
};

triggerEnquiryGenerationEvent = function (enquiryId, customerId, investmentTypeID) {
    try {
        var ominiObj = new callOnEnquiryGeneration();
        investmentTypeID = investmentTypeID * 1;
        var investmentType = "Growth";
        if (investmentTypeID == 2) { investmentType = "Retirement"; }
        else if (investmentTypeID == 3) { investmentType = "Child"; }
        ominiObj.pageName = 'pb:investment-insurance-quotes';
        ominiObj.channel = 'Investment Insurance';
        ominiObj.subSection1 = 'Home';
        ominiObj.subSection2 = 'Investment Insurance';
        ominiObj.userType = 'guest';
        ominiObj.customerId = customerId;
        ominiObj.enquiryId = enquiryId;
        ominiObj.formname = 'Investment-preQuotes';
        ominiObj.journeyType = investmentType;
        digitalData = ominiObj.enquiryGenerationEvent();
        var OminiEvent = new omnitureCallBackEvents();
        //OminiEvent.formStart();
        OminiEvent.enquiryGeneration();
        return digitalData;
    } catch (e) {
    }
};

triggerEnquiryFormStartEvent = function (customerId,investmentTypeID) {
    try {
        var ominiObj = new callOnEnquiryFormStart();
		if(investmentTypeID !== undefined){
		investmentTypeID = investmentTypeID * 1;
		}
        var investmentType = "Growth";
		if(investmentTypeID == 1) { investmentType = "Growth"; }
        else if (investmentTypeID == 2) { investmentType = "Retirement"; }
        else if (investmentTypeID == 3) { investmentType = "Child"; }
        ominiObj.pageName = 'pb:investment-insurance-prequotes';
        ominiObj.channel = 'Investment Insurance';
        ominiObj.subSection1 = 'Home';
        ominiObj.subSection2 = 'Investment Insurance';
        ominiObj.userType = 'guest';
        ominiObj.customerId = customerId;
        ominiObj.formname = 'Investment-preQuotes';
		ominiObj.journeyType = investmentType;
        digitalData = ominiObj.enquiryFormStartEvent();
        var OminiEvent = new omnitureCallBackEvents();
        OminiEvent.formStart();
        return digitalData;
    } catch (e) {
    }
};

triggerQuoteSelectionEvent = function (enquiryId, customerId, leadId, omniData) {
    try {
        var ominiObj = new callOnQuoteSelection();
        ominiObj.pageName = 'pb:investment-insurance-quotes';
        ominiObj.channel = 'Investment Insurance';
        ominiObj.subSection1 = 'Home';
        ominiObj.subSection2 = 'Investment Insurance';
        ominiObj.userType = getuserType();
        ominiObj.product = ';' + convertSpecialCharacterToHTML(omniData);
        ominiObj.customerId = customerId;
        ominiObj.leadid = leadId;
        ominiObj.enquiryId = enquiryId;
        digitalData = ominiObj.quoteSelectionEvent();
        var OminiEvent = new omnitureCallBackEvents();
        OminiEvent.productSelection();
    }
    catch (e) {
    }
};

    triggerBuyEvent = function(enquiryId, customerId, leadId,omniData) {
        try {
            var ominiObj = new callOnQuoteSelection();
            ominiObj.pageName = 'pb:investment-insurance-quotes';
            ominiObj.channel = 'Investment Insurance';
            ominiObj.subSection1 = 'Home';
            ominiObj.subSection2 = 'Investment Insurance';
            ominiObj.userType = getuserType();
            ominiObj.product = ';' + convertSpecialCharacterToHTML(omniData);
            ominiObj.customerId = customerId;
            ominiObj.leadid = leadId;
            ominiObj.enquiryId = enquiryId;
            digitalData = ominiObj.quoteSelectionEvent();
            var OminiEvent = new omnitureCallBackEvents();
            OminiEvent.buyPlanClick();
        }
        catch (e) {
        }
    };


    function convertSpecialCharacterToHTML(str) {
        return str.replace(/&/g, " and ").replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;");
    }
